# BRILL 325 JAAR UITGEVEN VOOR DE WETENSCHAP
# MDCLXXXIII
# 2008
<=.pb 1 =>
# Brill 325 jaar uitgeven voor de wetenschap
<=.pb 2 =>
<=.pb 3 =>
# Brill 325 jaar uitgeven voor de wetenschap
Sytze van der Veen
_Met bijdragen van_
Paul Dijstelberge
Mirte D. Groskamp
Kasper van Ommen
Leiden • Boston
2008
<=.pb 4 =>
<=.pb 5 =>
# Inhoudsopgave
7 Ten geleide
9 Van oude tijden. Het huis Luchtmans en het huis Brill: 1683-1848
45 De firma E.J. Brill: 1848-1896
77 De N.V. Boekhandel en Drukkerij voorheen E.J. Brill: 1896-1945
111 Het uitdijende universum van Brill: 1945-2008
153 Brill: heden en toekomst
171 Bijlage: directeuren van Luchtmans en Brill, 1683-2008
171 Bijlage: commissarissen van Brill sinds de oprichting van de N.V. in 1896
173 Noten
176 Literatuurlijst
178 Gebruikte afkortingen in onderschriften van illustraties
179 Personalia
180 Colofon
<=.pb 6 =>
<=.pb 7 =>
# Ten geleide
Het bereiken van de leeftijd van 325 jaar is een verjaardag die gevierd dient te worden. Koninklijke Brill is de oudste uitgeverij van Nederland en laat graag zien dat zij nog steeds springlevend is.

Vijfentwintig soms roerige, maar uiteindelijk zeer succesvolle jaren volgden op het derde eeuwfeest in 1983. De herdenking is ook deze keer bedoeld om Brill in zijn historische context te plaatsen. De uitgeverij treedt op als hoofdsponsor van de prachtige tentoonstelling ‘Leiden, Stad van Boeken’, die dit jaar in het Stedelijk Museum De Lakenhal wordt gehouden. Daarnaast werd in het kader van Brill 325 het archief van Brill over de jaren 1848-1991 toegevoegd aan het archief van Luchtmans, dat al eerder aan de Koninklijke Vereniging voor het Boekenvak werd geschonken. Het volledige archief wordt nu zorgvuldig beheerd door de afdeling Bijzondere Collecties van de Bibliotheek van de Universiteit van Amsterdam. Daardoor bestaat nu de gelegenheid de geschiedenis van onze uitgeverij, maar ook die van het boekenvak in ons land beter te bestuderen en wij verwachten dat de komende jaren interessante deelstudies zullen verschijnen.

Voor U ligt een informele bedrijfsgeschiedenis die grotendeels is samengesteld op basis van dit archief. Wij zijn Sytze van der Veen zeer erkentelijk voor de enthousiaste en kundige wijze waarop hij ons verhaal heeft verteld. Wij zijn met hem benieuwd naar het vervolg!

Aan de ordening van het archief, de samenstelling van de fondslijst en de bestudering van de bronnen is door velen gewerkt gedurende de laatste jaren. Onze bijzondere dank gaat uit naar Laurens van Krevelen, die als voorzitter van de Stichting Bibliotheek van het Boekenvak ons zeer heeft gesteund dit initiatief door te zetten. De staf van de afdeling Bijzondere Collecties, in het bijzonder Garrelt Verhoeven en Nico Kool, was altijd bereid mee te denken en te helpen. Mirte Groskamp heeft veel materiaal dat voor deze uitgave is gebruikt voorbereid; haar doorzettingsvermogen bij het temmen van het veelkoppige monster van ons archief moet hier vermeld worden. Aan de kant van Brill willen wij graag Gerrie de Vries met name noemen. Berry Dongelmans van de
Vakgroep Boekwetenschap van de Leidse Universiteit ontpopte zich als een gewaardeerd en kritisch meelezer van de tekst.

Terugkijken is vooral nuttig wanneer het inspiratie voor de toekomst biedt. Wij hopen dat wij een groeiende rol als uitgeverij voor de wetenschap zullen spelen en dat wij ons waardige opvolgers betonen van allen die ons bij Brill zijn voorgegaan.

Herman Pabbruwe,
directeur

<=.pb 8 =>

![](01-00.jpg)

<=.ill_01-00.jpg Rond 1750 liet Samuel Luchtmans dit allegorische schoorsteenstuk vervaardigen door de schilder Nicolaas Reyers. Het hing vanouds in het Luchtmanshuis aan het Rapenburg en tegenwoordig in het bedrijfsgebouw van Brill. Men ziet Pallas met haar schild en lans, in gezelschap van Hermes met zijn gevleugelde helm en staf; op de voorgrond spelen drie blote jongetjes met prenten van bepruikte heren, vermoedelijk Leidse hooggeleerden. De uitleg ligt voor de hand: wetenschap en handel waren in huize Luchtmans een vruchtbare vereniging aangegaan. Volgens de overlevering zouden de mythologische figuren vermommingen zijn van Samuel I Luchtmans, diens echtgenote Cornelia en hun zoontjes. Het vergt enige verbeeldingskracht om in de jeugdige Griekse god de vijfenzestigjarige Samuel te ontwaren, en in de bevallige Pallas de matrone Cornelia. De blote jongetjes liepen in werkelijkheid ook al tegen de dertig. Het schilderij heeft jarenlang gehangen in de zetterij van Brill aan de Oude Rijn, waar de zetters zich plachten te vermaken door met loden letters te schieten op de blote billen van de jongetjes. Een grondige restauratie was nodig voordat alle gaten waren gedicht. De gestileerde uitvoering van Pallas Athene en Hermes op het moderne logo van Brill is afgeleid van dit schilderij. Coll. Brill =>

<=.pb 9 =>
# Van oude tijden. het huis Luchtmans en het huis Brill: 1683-1848

## Continuïteit en verandering

Anno 2008 is Brill een uitgeverij van deze tijd, maar tevens de uitkomst van een ontwikkeling die 325 jaar geleden in gang is gezet. Weinig bedrijven in Nederland of elders kunnen bogen op zo’n lange geschiedenis. Tuta sub aegide Pallas - het schild van de godin der wijsheid heeft de eeuwen getrotseerd en is tot op de dag van vandaag de schutse van Brill. Niet alleen de bestaansduur, ook de bestendigheid van bepaalde uitgaven is opmerkelijk: sommige hedendaagse publicaties hebben hun voorlopers in de late zeventiende eeuw. Springt de continuïteit in het oog, op een termijn van meer dan drie eeuwen is deze allerminst vanzelfsprekend. Het tegendeel ligt op die lange duur veel meer voor de hand, gezien het feit dat de tand des tijds alle gelegenheid kreeg voor zijn destructieve arbeid.

Kon de oprichter in 1683 geen rekening houden met een toekomst in 2008, zo kan
Brill zijn huidige bestaansrecht niet ontlenen aan het verleden. Wél houdt het bedrijf zijn verleden in ere, maar continuïteit moet steeds worden bevochten op het wisselvallige heden. In historisch perspectief blijkt duurzaamheid veeleer een doorgaand proces van verandering dan een eeuwige terugkeer van hetzelfde. Continuïteit wordt slechts bereikt door heden en verleden op elkaar af te stemmen, met voorbijgaan van de waan van de dag. In dat licht is verandering geen klakkeloze aanpassing aan het heden, maar een zorgvuldige versmelting van verleden en heden. In die eigenaardige alchemie ligt de essentie van de geschiedenis van Brill.

Zoals gezegd gaat die geschiedenis terug tot het jaar 1683, maar voor een goed begrip is het nodig de reis door de tijd te onderbreken in 1848. Het spook van de revolutie waarde destijds door Europa, al viel daarvan in de goede stad Leiden weinig te merken. Wel verscheen daar op de eerste mei van dat jaar een gedrukte circulaire die enigszins spookachtig aandoet. ‘Ueds. dienstvaardige dienaren’ S. en J. Luchtmans geven in dat rondschrijven te kennen dat de naar hen genoemde firma wordt opgeheven. Clientèle en relaties krijgen het verzoek zich in de toekomst te wenden tot E.J. Brill, die sedert achttien jaar werkzaam is in hun bedrijf en ‘ons vervangen zal’.[^1] Hier lijken stemmen van gene zijde aan het woord: Samuel II Luchtmans was overleden in 1780, zijn broer Johannes had in 1809 het tijdelijke met het eeuwige verwisseld. Na zijn
dood werd het familiebedrijf drie jaar lang geleid door zijn neef Samuel III, zoon van Samuel II, maar na diens overlijden in 1812 was er geen Luchtmans die het roer kon overnemen.

Vanaf dat tijdstip lag de feitelijke leiding in handen van Johannes Brill, die tien jaar eerder bij de firma Luchtmans in dienst was getreden en daarnaast een eigen drukkerij

<=.pb 10 =>
exploiteerde in Leiden. In 1821 leverde de familie alsnog een nieuwe directeur in de persoon van Johannes Tiberius Bodel Nijenhuis, kleinzoon van Johannes Luchtmans. De erudiete Bodel Nijenhuis was meer een geleerde en verzamelaar dan een zakenman - de schitterende collectie kaarten en prenten die hij tijdens zijn leven bijeenbracht bevindt zich thans in de Universiteitsbibliotheek van Leiden. Hij was de auctor intellectualis van de uitgeverij, terwijl Brill zich belastte met de dagelijkse gang van zaken. Op basis van deze vruchtbare werkverdeling gaven zij gezamenlijk meer dan vijfentwintig jaar leiding aan de onderneming.

Johannes Brill had in 1848 de gezegende leeftijd van tachtig jaar bereikt en vond het langzamerhand tijd om terug te treden. Zijn zoon Evert Jan, sinds jaar en dag in dienst bij de firma, diende zich aan als opvolger. De vijftigjarige Bodel Nijenhuis had de samenwerking met Brill senior kunnen voortzetten met junior, maar hij wilde zich voortaan zonder zakelijke beslommeringen wijden aan zijn intellectuele bezigheden. In overleg met de andere vennoten - neven en nichten die een deelbelang hadden - werd besloten het familiebedrijf op te heffen en over te dragen aan E.J. Brill.

S. en J. Luchtmans konden in 1848 worden opgevoerd als naamgevers van de firma, maar de circulaire werd uiteraard opgesteld door hun ghostwriter Bodel Nijenhuis.

![](01-01.jpg)

<=.ill_01-01.jpg Circulaire waarin S. & J. Luchtmans te kennen geven dat hun bedrijf wordt voortgezet door E.J. Brill. Drukproef met correcties. AL/UBA =>

<=.pb 11 =>
Getuige de neerslag in het bedrijfsarchief gingen verscheidene ontwerpversies over en weer, voordat Bodel en Brill jr. de definitieve tekst vaststelden: ‘Vindt gij het zoo ook niet beter?’ Bodel had het volste recht om te ondertekenen met de firmanaam, maar het ontbreken van zijn eigen naam wekt de indruk van een mystificatie: het lijkt alsof de geesten van het voorgeslacht worden opgeroepen om hun zegen te geven aan de gedaanteverandering. Vanuit de nevelen van het verleden garandeerden de gebroeders Luchtmans, respectievelijk geboren in 1725 en 1726, de continuïteit tussen het oude en het nieuwe bedrijf. Op de keerzijde van het rondschrijven liet E.J. Brill de geachte relaties weten dat de firma Luchtmans onder zijn naam verder ging, op de oude voet en het oude adres. Hij beloofde ‘uwe bestellingen binnen den kortst mogelijken tijd en op dezelfde voorwaarden te blijven uitvoeren’. Aan die belofte van continuïteit heeft hij zich gehouden, wat hem overigens niet belette om nieuwe wegen in te slaan.

## Jordaan Luchtmans: de beginjaren, 1683-1708
Sinds deze wisseling van de wacht neemt Brill 160 jaar van de bedrijfsgeschiedenis voor zijn rekening, terwijl de 165 jaar daaraan voorafgaande in het teken staan van Luchtmans. Het eigenlijke begin van het bedrijf valt te bepalen op 17 mei 1683. Op die datum werd Jordaan Luchtmans (1652-1708), afkomstig uit het dorp Woudrichem, ingeschreven als boekverkoper bij het Leidse boekengilde. De jongeman was eerst in de leer geweest bij een Haagse boekhandelaar en had zijn leertijd voltooid bij de Leidse gebroeders Gaesbeek.

![](01-02.jpg)

<=.ill_01-02.jpg De buitenlandse relaties werden op de hoogte gesteld in een Franstalige versie van de circulaire. Drukproef met correcties. AL/UBA =>

<=.pb 12 =>
Een kleine week later deed hij een tweede gewichtige stap: op 23 mei trad hij in het huwelijk met Sara van Musschenbroek, stammend uit een Leidse familie met academische banden. Haar vader Samuel († 1681) was in zijn tijd de gerespecteerde instrumentmaker van de universiteit geweest, haar ongeboren neef Petrus (1692-1761) zou in zijn tijd de coryfee van de nieuwe en newtoniaanse natuurkunde worden.[^2] Als echtgenote van een boekhandelaar beschikte Sara bovendien over imponerende papieren in het boekenvak: zij was een achterkleindochter van Christoffel Plantijn, de befaamde zestiende-eeuwse drukker uit Antwerpen die enige tijd in Leiden had gewerkt. De schoonfamilie verschafte Jordaan Luchtmans een goede entree in universitaire kringen. Gezien de grote hoeveelheid concurrenten in de boekenstad Leiden kon een beginnend boekhandelaar zijn voordeel doen met zo’n connectie.

### Bibliopolis
In 1697 betrok het echtpaar Luchtmans het pand Rapenburg 69B, waar ook de boekhandel werd gevestigd. Het herenhuis werd aangekocht voor de niet geringe som van f 6500 en moest bovendien voor een aanzienlijk bedrag worden vertimmerd.[^3] Jordaan had sinds 1683 een zekere naam opgebouwd met zijn uitgaven, maar deze kostbare verhuizing vergde een steuntje in de rug van zijn schoonfamilie. Het nieuwe onderkomen was niet alleen op stand, het bevond zich aan het deel van de gracht met de meest gerenommeerde boekhandels van Leiden. Luchtmans gaf door zijn nieuwe vestiging te kennen dat hij zich wilde meten met de besten van het vak.

Het Rapenburg bij het Academiegebouw was de boekengracht bij uitstek: gewijde aarde voor bibliofielen, kopers dan wel verkopers van boeken. In deze grachtenpanden was de wetenschappelijke boekhandel gevestigd, die dankzij de bloei van de universiteit

![](01-03.jpg)

<=.ill_01-03.jpg Het Rapenburg met het Academiegebouw. Gravure van A. Delfos naar tekening van J. J. Bylaerdt. RAL =>

<=.pb 13 =>
een hoge vlucht had genomen. De Academie met de aanpalende boekwinkels vormde het wetenschappelijke hart van de stad. In ruimere zin vormde deze buurt zelfs het hart van de geleerde wereld in Europa, want Leiden was rond 1700 nog steeds toonaangevend in de wetenschap en in wetenschappelijke boeken. Dit deel van het Rapenburg werd wel aangeduid als ‘het Rijk van Pallas’, om aan te geven dat de godin van de wetenschap hier verblijf hield. Zelfbewust deelde Luchtmans sinds 1697 op het impressum van zijn Nederlandse boeken mee dat hij was gevestigd ‘nevens d’Academie’. Hij was niet de enige. Vijftien van de veertig boekhandels die Leiden omstreeks 1700 telde bevonden zich in de onmiddellijke nabijheid van het Academiegebouw.

Sommigen van zijn buren op het Rapenburg hadden klinkende namen. Op de nummers 71-73, vlak naast het Academiegebouw, was vanouds het bedrijf van de Elzeviers gehuisvest. De dynastie der stads- en academiedrukkers was met Abraham, achterkleinzoon van aartsvader Lodewijk, gevorderd tot het vierde geslacht. Abraham Elzevier verkeerde destijds in zijn nadagen, evenals zijn bedrijf. Het werk van de academiedrukker was rond 1700 zozeer beneden de maat geworden, dat Leidse promovendi voor hun proefschriften geregeld uitweken naar Utrecht. Later, na het overlijden van de laatste Elzevier, zou op hetzelfde adres de bekende boekhandel van Pieter van der Aa worden gevestigd. Luchtmans’ directe buurman op nr. 69A was de Gazette de Leyde, een krant die in de achttiende eeuw een internationale verspreiding had. Op nr. 54 bevond zich de boekhandel van Johannes Verbessel (later van Johannes van der Linden jr.), op nr. 64 die van Cornelis Boutesteyn. Zo zou men nog enige tijd kunnen doorgaan met het citeren van het adresboek van de Leidse boekhandel.[^4] Het viel rond 1700 niet te voorzien dat van al deze namen op den duur alleen die van Luchtmans zou overblijven.

![](01-04.jpg)
![](01-05.jpg)

<=.ill_01-04.jpg In 1685 verscheen bij Jordaan Luchtmans de _Historia Generalis Insectorum_ van de vijf jaar eerder overleden Jan Swammerdam. Het insectenboek was de eerste geïllustreerde uitgave van Luchtmans. Zoals gebruikelijk in die tijd werden de drukvellen en prenten in ongebonden staat verkocht. Op de laatste pagina staat een aanwijzing voor de binder op welke wijze de prenten ingebonden moeten worden. UBA =>

![](01-05.jpg)

<=.pb 14 =>

![](01-06.jpg)

<=.ill_01-06.jpg Binnenwerk van het Opus Aramaeum van Carolus Schaaf, uitgegeven door Jordaan Luchtmans in 1686. Omdat het Aramees van rechts naar links wordt gelezen, loopt de paginanummering van achteren naar voren. UBA =>

<=.pb 15 =>
<!-- ![](01-06.jpg) -->

<=.pb 16 =>
Het boekenvak was in die tijd minder gespecialiseerd dan tegenwoordig. Luchtmans was niet alleen actief als uitgever, hij was ook de uitbater van een winkel waarin hij zijn eigen boeken en die van andere uitgevers verkocht. Meestal was de koopwaar geen boek in eigenlijke zin, maar een stapel gedrukte vellen. De afwerking - het vouwen van de katernen en het inbinden - kwam voor rekening van de klant. Om die reden was aan de boekhandel een binderij verbonden, waar de Leidenaren ook uiteenvallende oude boeken opnieuw konden laten binden.[^5] Sommige uitgevers beschikten naast hun boekhandel over een eigen drukkerij, maar Luchtmans placht zijn drukwerk uit te besteden; pas veel later, in de tijd van Johannes Brill, werd een drukkerij aan het bedrijf verbonden.

Het aanbod in zijn winkel bestond slechts ten dele uit recente publicaties, daarnaast waren de schappen gevuld met oudere en antiquarische werken. Ook het veilen van particuliere bibliotheken was een belangrijke nevenactiviteit. Dankzij de concentratie van geleerden en boeken binnen de muren van Leiden kwamen zulke veilingen regelmatig voor. Soms veilde een boekhandelaar de nalatenschap van een auteur die tijdens zijn leven bij hem had gepubliceerd. Zo kon het gebeuren dat de theoloog Jacob Trigland in drie verschillende gedaanten opdook in het fonds van Jordaan Luchtmans: eerst als schrijver van een theologisch werk (1701), dan als onderwerp van een lijkrede (1705) en tenslotte als wijlen de eigenaar van een te veilen bibliotheek (1706).

### Experiment met lood
Luchtmans had weinig tijd en aandacht overgehouden voor de uitgeverij, wanneer hij naast zijn andere besognes ook een drukkerij moest exploiteren. Hij had in dat geval de persen draaiende moeten houden met drukwerk van derden en was onder dwang der omstandigheden meer drukker dan uitgever geworden. Was de uitbesteding van drukwerk een begrijpelijke keuze, niettemin raakte hij betrokken bij een opmerkelijk typografisch experiment. De uit Duitsland afkomstige Johann Müller, luthers predikant in Leiden, had rond 1690 een druktechniek uitgevonden die later bekend zou staan als de ‘stereotypie’.[^6] Het procédé kwam erop neer dat van het zetwerk een gipsen mal werd gemaakt en daarvan weer een loden afgietsel: het zetwerk werd in deze bewerking dus gekopieerd. Met behulp van zulke platen kon het boek bij een volgende editie meteen worden gedrukt, zonder dat het opnieuw hoefde te worden gezet. De innovatie was bewerkelijk en leende zich alleen voor boeken met grote oplagen en regelmatige herdrukken - in de praktijk bleek de uitvinding van de dominee met name bruikbaar voor het goedkoop vermenigvuldigen van bijbels.

Met dat heilzame doel stichtte Müller een vennootschap met twee boekhandelaren, te weten Jordaan Luchtmans en Cornelis Boutesteyn. De compagnons zetten een stereotypische drukkerij op, al is het begrip ‘drukkerij’ in dit verband enigszins misleidend: in plaats van boeken werden hier drukplaten voor boeken gedrukt. Na het opdoeken van de drukkerij in 1716 bleef de vennootschap bestaan, zij het met gewijzigde bezetting door de dood van de oorspronkelijke compagnons; Müller en Boutesteyn werden vervangen door hun weduwen, Luchtmans door zijn zoon Samuel. In de loop van de achttiende eeuw werden met behulp van de drukplaten verscheidene folio- en quarto-edities van de bijbel uitgebracht, waarbij het zetwerk beperkt kon blijven tot het aanpassen van het jaartal op het titelblad.

![](01-07.jpg)

<=.ill_01-07.jpg Stereotypie: drukplaat voor het Nieuwe Testament op quarto-formaat, sinds 1711 gebruikt voor verschillende edities. SML =>

<=.pb 17 =>
De derde generatie Luchtmans bracht nog steeds zulke bijbels uit, nu in samenwerking met de firma Enschedé uit Haarlem.[^7] Deze gemechaniseerde reproductie van het Woord moet voor de belanghebbenden een aardige bron van inkomsten zijn geweest, maar op den duur raakten de drukplaten versleten. De folio-editie van 1791 bleek dermate onleesbaar dat de ontevreden kopers hun bijbels terugstuurden naar de uitgever. Na dat débacle kwam een roemloos einde aan de stereotypie, die na haar ondergang in Nederland opnieuw zou worden uitgevonden in Frankrijk. De oude drukplaten werden omgesmolten en slechts enkele exemplaren bleven als curiosa bewaard.

### Mededinging en samenwerking
De geleidelijke teloorgang van Abraham Elzevier speelde Jordaan Luchtmans in
de kaart, maar hij was niet de enige startende ondernemer in Leiden. De zeven jaar jongere Pieter van de Aa was reeds in 1677 tot het gilde toegelaten en iets eerder dan Luchtmans met een eigen boekhandel begonnen. Zij waren beiden in de leer geweest bij de gebroeders Gaesbeek, al kan hun leertijd slechts ten dele zijn samengevallen. Van der Aa kocht in 1708 het pand Rapenburg 32, voorheen de vestiging van zijn leermeester Gaesbeek. Pas vijf jaar later zou hij doordringen tot de eigenlijke gouden bocht naast de Academie, waar Luchtmans al sinds 1697 woonde.[^8] Beide boekhandelaren richtten zich op het uitgeven van wetenschappelijk werk en reden elkaar dus in de wielen bij het verwerven van kopij. Naast hun wetenschappelijke fonds brachten ze allebei ook uitgaven voor een breder publiek op de markt. Van der Aa dong naar de gunst van klanten met reisbeschrijvingen, atlassen en schitterende prentwerken, Luchtmans zocht het meer in bijbels met grote oplagen, stichtelijke werken en preken. Ook in het degelijke segment van de markt viel een goede boterham te verdienen. Van der Aa had een meer gewaagde stijl van ondernemen dan Luchtmans en schroomde niet bij tijd en wijle een roofdruk uit te brengen.

Ook concurrerende boekverkopers werkten bij gelegenheid samen. In 1690 veilden Luchtmans en Van der Aa gezamenlijk de bibliotheek van de classicus Theodorus Rijcke, en in datzelfde jaar brachten ze gezamenlijk een boek uit (_Strategematum libri octo_, een Latijnse vertaling van het ‘Boek der krijgslisten’ van de Griekse auteur Polyainos). Kennelijk was de samenwerking geen succes, want van dit boek verschenen in 1690-91 nog twee edities, beide als uitgaven van Luchtmans en Du Vivié. Sindsdien werkte Luchtmans nog tweemaal samen met Van der Aa, maar bij die projecten waren steeds andere boekhandelaren betrokken. De gedachte dringt zich op dat hij de risico’s van een compagnon als Van der Aa liever spreidde over meerdere partijen. Gewoonlijk ging Jordaan Luchtmans in zee met Cornelis Boutesteyn, zijn partner in de stereotypie, of met Johannes du Vivié.

Bij het verwerven van wetenschappelijke kopij waren Luchtmans en Van der Aa aan elkaar gewaagd. Op dit front waren persoonlijke contacten binnen de academische wereld van groot belang, zodat beide boekhandelaren moeite deden om hooggeleerde auteurs aan zich te binden. Van der Aa had er slag van om professoren en regenten voor zich in te nemen, maar Luchtmans had het voordeel van zijn Musschenbroekconnectie, vermoedelijk ook van zijn meer gedegen reputatie. Mede dankzij de tekortkomingen van Abraham Elzevier was de wetenschappelijke spoeling dik genoeg voor twee nieuwkomers: de namen van de toenmalige universitaire kopstukken duiken op

![](01-08.jpg)

<=.ill_01-08.jpg In 1863 maakte J.T. Bodel Nijenhuis een afdruk van een stereotypie-plaat die bewaard was gebleven. Het was een van de platen die bij Luchtmans in de achttiende eeuw waren gebruikt voor het drukken van de foliobijbel. Geen wonder dat de laatste editie van 1791 onverkoopbaar bleek: door slijtage van de plaat is het drukwerk van inferieure kwaliteit. AL/UBA =>

<=.pb 18 =>
in de fondslijsten van beide uitgevers, zij het dat Luchtmans op het gebied van de theologie beter scoorde dan Van der Aa.

### Heidelbergse catechismus en Syrische grammatica
In de vijfentwintig jaar tussen 1683 en 1708 gaf Jordaan Luchtmans in totaal 170 werken uit, dat wil zeggen zes à zeven per jaar. Twintig titels hebben betrekking op door hem georganiseerde boekveilingen en tien andere vallen in de categorie van het gelegenheidswerk - lijkredes, oraties en wat dies meer zij. De Nederlandse afdeling van zijn fonds bestond uit ruim twintig titels, voornamelijk van godsdienstige aard. De populariteit van zulke stichtelijke werkjes moet niet worden onderschat: de _Oefeningen over den Heidelbergschen catechismus_ van Hendrik Groenewegen haalde niet minder dan zeven drukken, terwijl David Knibbe’s _Leere der gereformeerde kerk_ goed was voor drie edities. Luchtmans smeerde niet alleen balsem op de ziel van zijn klanten, hij had ook oog voor hun lichamelijke noden: het _Nieuw ligt der vroet-vrouwen_ van Hendrik van Deventer en de _Redenering en aanmerkingen omtrent de ziektens ter zee voorvallende_ van William Cockburn voorzagen in een behoefte aan medische voorlichting. Het gros van Luchtmans’ fonds werd gevormd door 120 wetenschappelijke werken in het Latijn, met als uitzondering op die regel één medisch handboek in het Frans. De medische wetenschap was met 39 titels koploper in de fondslijst, een afspiegeling van de productiviteit van de medische faculteit in deze jaren. De traditionele humanistische geleerdheid was vrijwel even nadrukkelijk aanwezig met 35 titels van filologische en klassiek-historische aard. Soms liep de filologie over in de theologie, zoals bij Johannes Leusdens _Philologus Hebraeo-Graecus_ van 1685 - het beginpunt van een traditie in judaïca die door Brill nog steeds in ere wordt gehouden. Ook Brills huidige edities op het gebied van het oud-Syrisch hebben hun wortels bij Luchtmans in de late zeventiende eeuw, getuige de _Schola Syriaca_ (1685) van Johannes Leusden en het _Opus Aramaeum complectens grammaticam Chaldaico-Syriacam_ van Carolus Schaaf (1686). De theologie in eigenlijke zin stond op derde plaats met ongeveer twintig titels. De andere faculteiten waren slechts spaarzaam vertegenwoordigd in de fondslijst: de natuurwetenschappen en de rechtsgeleerdheid kwamen niet verder dan enige titels.

![](01-09.jpg)

<=.ill_01-09.jpg Getuige het _Opus Aramaeum_ van Carolus Schaaf (1686) publiceerde Jordaan Luchtmans al in een vroeg stadium in en over het oud-Syrisch. In 1717 bracht zijn zoon Samuel een _Novum Testamentum Syriacum_ uit, vervaardigd met het procédé van de stereotypie. De Syrische traditie van Luchtmans zet zich tot op heden voort bij Brill: in 1993 verscheen een kloeke zesdelige _Concordance to the Syriac New Testament_. De meest recente publicatie op dit gebied is B. ter Haar Romeny, _The Peshitta: Its Use in Literature and Liturgy_ (Leiden, 2007). Daarnaast is Brill uitgever van het tijdschrift _Aramaic Studies_, gewijd aan de Syrische taal en letteren. UBA =>

<=.pb 19 =>

![](01-10.jpg)

<=.ill_01-10.jpg Aanschouwelijk medisch onderwijs. Prent uit J.B. van Lamsweerde, _Joannis Sculteti Armamentarium Chirurgicum_, uitgegeven door Jordaan Luchtmans in 1693. UBA

<=.pb 20 =>

![](01-11.jpg)

<=.ill_01-11.jpg Titelprent van de _Geographia Sacra_ (1692) van S. Bochart, een gezamenlijk project van J. Luchtmans, C. Boutesteyn en de Utrechtse boekhandelaar W. van de Water. Een kostbare uitgave, maar de aardrijkskunde van het Heilige Land was een godsdienstig nevenproduct dat goed in de markt lag. UBA =>

<=.pb 21 =>
## Samuel I Luchtmans: consolidatie en uitbouw, 1708-1755
Jordaan Luchtmans overleed op 18 juni 1708, kort na een dubbel jubileum: een maand voor zijn sterfdag had zijn bedrijf vijfentwintig jaar bestaan en amper drie weken tevoren had hij zijn zilveren huwelijksfeest gevierd. Hij werd begraven in het familiegraf van de Musschenbroeks in de Hooglandse Kerk in Leiden. Uit zijn huwelijk met Sara waren vier zonen geboren, van wie slechts één in leven bleef: Samuel (1685-1757), genoemd naar zijn grootvader Musschenbroek. Na de dood van zijn vader nam hij de leiding van het bedrijf in handen, al bleef zijn moeder tot haar overlijden in 1710 formeel de eigenaresse. Samuel had de praktijk van het boekenvak van zijn vader geleerd, maar was dankzij een degelijke opleiding ook in intellectueel opzicht vertrouwd met boeken: hij doorliep de Latijnse school en studeerde enige jaren rechten aan de Leidse universiteit. Tot 1755 zou hij het bedrijf leiden, voortbouwend op de fundamenten die door zijn vader waren gelegd. Ook in zijn huwelijkskeuze trad hij in de voetsporen van zijn vader: hij trouwde in 1721 met zijn veertien jaar jongere nichtje Cornelia van Musschenbroek (1699-1784), met wie hij in totaal negen kinderen zou krijgen.

Toen Samuel de zaak overnam, was Luchtmans reeds een gevestigde naam in het Leidse boekenbedrijf. Voor het zogenaamde familiegeld van 1715 werden in de stad in totaal 35 boekverkopers aangeslagen, waarvan slechts vier voor het hoogste tarief van twintig gulden: Samuel Luchtmans, Pieter van der Aa, Daniël van den Dalen en Anthony de la Font, uitgever van de Gazette de Leyde.[^9] In 1714 maakte Samuel de balans op van zijn bedrijf, de waarde van alle boeken, kopijrechten en andere bezittingen bij elkaar optellend. Zijn berekening leidde tot de indrukwekkende somma van f 64.360 en 9 stuivers. Dat bedrag kwam overeen met de door hemzelf getaxeerde verkoopwaarde van zijn aardse goederen op dat moment (‘als ’t nu bij mijn afsterven moeste verkogt werden’). De feitelijke waarde op langere termijn (‘als de Almooghende God mij nog eenige Jaaren in dit leeven liet’) lag in zijn ogen zelfs een kleine dertigduizend
gulden hoger.

![](01-12.jpg)

<=.ill_01-12.jpg Jordaan Luchtmans overleed op de leeftijd van 56 jaar, een paar weken na zijn zilveren bruiloft. Zijn huurder Hendrik Snakenburg schreef voor die gelegenheid het nevenstaande gedicht. Snakenburg (1674-1750), conrector en later rector van de Latijnse School, greep elke gelegenheid aan om een gelegenheidsgedicht te maken. Hij woonde meer dan een halve eeuw bij de Luchtmansen, eerst bij Jordaan en later bij Samuel. Het erfstuk van de familie deelde lief en leed met drie opeenvolgende generaties. Na Snakenburgs dood in 1750 bracht Samuel een omvangrijke verzamelbundel uit van diens gedichten (_Poezy_, 1753). Uit zijn voorwoord blijkt hoezeer hij op deze huisvriend was gesteld. UBA =>

![](01-13.jpg)

<=.ill_01-13.jpg Hendrik Snakenburg. SML =>

<=.pb 22 =>
Samuel Luchtmans was ten tijde van deze balans nog geen dertig jaar oud en zou met Gods hulp de gezegende leeftijd van 72 jaar bereiken. In 1747 maakte hij nogmaals de balans op en thans berekende hij de verkoopwaarde van zijn bezittingen op ruim honderdtwintigduizend gulden, bijna twee keer zo veel als in 1714. In dat eindbedrag waren niet eens zijn onroerende goederen meegerekend, zodat men mag concluderen dat hij in de tussenliggende jaren goed had geboerd.10 Die indruk wordt bevestigd door het Leidse Verpondingsregister van 1742, waaruit blijkt dat slechts twee van de 32 aangeslagen boekverkopers een jaarinkomen hadden van meer dan tweeduizend gulden. Het inkomen van Hendrik van Damme werd geschat tussen de f 2500 en f 3000, maar diens aanslag berustte op fiscale overschatting: bij nader inzien werd hij ingedeeld in de meer bescheiden inkomensklasse van f 1200 tot f 1500. Samuel Luchtmans prijkte als enige Leidse boekverkoper in klasse 12, wat wil zeggen dat zijn geschatte inkomen lag tussen de f 4500 en f 5000 per jaar.[^11] Volgens die maatstaf behoorde hij royaal tot de gezeten burgerij, zij het niet tot de categorie der zeer rijken met een inkomen boven f 12.000 per jaar. Samuel kon zich met enig recht de uitstraling van degelijke zelfvoldaanheid aanmeten, waarmee hij in 1748 werd geportretteerd door de schilder Hiëronymus van der Mij.

![](01-14.jpg)

<=.ill_01-14.jpg Samuel Luchtmans in 1748 als gezeten burger. Portret door Hiëronymus van der Mij. SML =>

![](01-15.jpg)

<=.ill_01-15.jpg Cornelia van Musschenbroek, echtgenote van Samuel Luchtmans. Portret door Hiëronymus van der Mij. SML =>

<=.pb 23 =>
### Geleerdheid en een koekoeksei
De tweede Luchtmans liet in 1714 een catalogus drukken met de 331 boektitels die hij op dat moment in voorraad had.[^12] Het merendeel (261 titels) bestond uit wetenschappelijke werken in het Latijn, maar daarnaast bleek Samuel zeventig Nederlandse boeken in de aanbieding te hebben. Het aanbod aan Franse boeken bedroeg slechts vier titels - later in de eeuw zou het Frans meer plaats opeisen in de winkel, overeenkomstig de ruimte die het in beslag nam in de publieke opinie. Ongeveer zestig procent van de Latijnse werken was uitgegeven door Jordaan of Samuel Luchtmans, al dan niet in combinatie met andere boekverkopers. Bij de Nederlandse sectie, voornamelijk preken en stichtelijke werken voor een lekenpubliek, lag het percentage eigen uitgaven eveneens rond de zestig.

Het zwaartepunt van de Latijnse afdeling lag in de traditionele humanistische geleerdheid: ruim dertig procent van de opgevoerde titels was filologisch of klassiekhistorisch van aard, met inbegrip van enige hebraïca en syriaca. Theologische werken namen ruim een kwart van het wetenschappelijke aanbod voor hun rekening, medische werken een kleine twintig procent en juridische een kleine tien procent. De resterende vijftien procent in de catalogus bestond uit filosofische en natuurwetenschappelijke titels, voornamelijk zeventiende-eeuwse werken van Descartes, Gassendi, Geulinx, Hobbes, Swammerdam, Leeuwenhoek en Huygens. De fysica van Newton en het paradigma van de ‘proefonderlijkvindelijke wijsbegeerte’ zouden pas in de jaren twintig opgang maken in Leiden.

Luchtmans was een boekhandelaar van strikte zeden, maar temidden van zijn stichtelijke ‘Nederduytsche Boeken’ legde hij een minder stichtelijk koekoeksei. Tussen Soermans’ _Kerkelijk Register der Predikanten van Zuydholland_ en Steengragts _Binnenste Heyligdom geopend om te sien de gantsche Godgeleertheyd_ stuit men op een boek dat kortweg wordt aangeduid als _Spinoza regtsinnige Theologant_. De titel is des te intrigerender omdat de wijsgeer Spinoza in de ogen van de tijdgenoot allerminst rechtzinnig was. De volledige titel luidt _De rechtsinnige theologant, of Godgeleerde Staatkunde_ en bij nadere bestudering blijkt het boek een vertaling van Spinoza’s _Tractatus Theologicopoliticus_. De _Tractatus_ verscheen in 1670 in het Latijn en werd als enige van Spinoza’s werken tijdens diens leven gepubliceerd. Enige jaren later werd het boek door het Hof van Holland verboden wegens het ‘godslasterlijcke ende zielverdervende’ karakter.

Rond die tijd bestond al een Nederlandse vertaling, maar met het oog op zijn zielerust en veiligheid hield Spinoza († 1677) de publicatie daarvan tegen. In 1693-94 werd deze vertaling alsnog uitgebracht in twee clandestiene edities met de titel _Rechtsinnige Theologant_. Het boek deed de ronde in het ondergrondse circuit en moest onder de toonbank worden verkocht. Het is bij uitstek een exponent van de intellectuele stroming die tegenwoordig wel wordt omschreven als de ‘Radicale Verlichting’. De _Rechtsinnige Theologant_ is geen boek dat men zou verwachten bij een rechtzinnige boekhandelaar als Samuel Luchtmans. Het wekt enige verbazing dat hij de titel opvoerde in zijn catalogus. Hij liep het risico op een forse boete, zoals zijn Amsterdamse collega Gerrit Bom zelfs in 1761 nog overkwam wegens het verkopen van een boek van Spinoza. Getuige Samuels aantekening in de catalogus had hij elf exemplaren op voorraad, mogelijk een onderdeel van de erfenis van zijn vader.[^13]

![](01-16.jpg)

<=.ill_01-16.jpg Onder de toonbank bij Luchtmans: _Een rechtsinnige theologant, of godgeleerde staatkunde_ (1694), met het fictieve impressum ‘Bremen, by Hans Jurgen von der Weyl’. De eerste druk (1693) was zogenaamd verschenen ‘Te Hamburg, bij Henricus Koenraad’. Dat impressum was ook gebruikt voor de verboden _Tractatus theologicopoliticus_ van Spinoza, waarvan de _Rechtsinnige theologant_ de Nederlandse vertaling was. De man achter de clandestiene vertaling was de Amsterdamse boekhandelaar Aart Wolsgryn. UBA =>

<=.pb 24 =>
### De ridder van San Marco
Tengevolge van het overlijden van Abraham Elzevier was in augustus 1712 het ambt van stads- en academiedrukker vrijgekomen. De dood van de een en het brood van de ander - naar alle waarschijnlijkheid deed Samuel Luchtmans een poging om voor de vacature in aanmerking te komen, maar hij slaagde daar niet in. Werd hij te jong bevonden of was het bezit van een eigen drukkerij een harde eis van de curatoren van de universiteit? Hij was niet de enige gegadigde, want ook Pieter van der Aa was van plan het lucratieve en eervolle ambt naar zich toe te trekken. Vooruitlopend op de toekomst was de ambitieuze uitgever al enige jaren bezig om zijn kandidatuur veilig te stellen, onder meer door aan te bieden op eigen kosten een nieuwe catalogus van de universiteitsbibliotheek uit te brengen. Dat genereuze aanbod werd door de curatoren gaarne aanvaard, maar bleek niet afdoende voor zijn benoeming tot academiedrukker. De boekverkoper en drukker Jacob Poereep, tevens pedel van de universiteit, werd aangesteld als opvolger van Elzevier.

Van der Aa liet het er niet bij zitten en deed alle moeite om het drukkerschap van de academie alsnog naar zich toe te trekken. In de eerste plaats verzekerde hij zich van de steun van de curator Jacob van Wassenaer Obdam, aan wie hij een van zijn uitgaven opdroeg. Verder kocht hij het grootste deel van de boedel van Abraham Elzevier, zowel de inventaris van diens drukkerij als diens panden aan het Rapenburg.

![](01-17.jpg)
![](01-18.jpg)
![](01-19.jpg)
![](01-20.jpg)
![](01-21.jpg)

<=.ill_01-17.jpg Een drukkersmerk was het uithangbord van een uitgever, maar ook een stempel van echtheid en een (verhoopte) bescherming tegen roofdrukken. Jordaan Luchtmans gebruikte vanaf 1683 het rustieke merk van de ploegende boer met het motto ‘Spes alit agricolas’ (hoop geeft de boeren kracht). Pallas Athene, de godin van wetenschap en wijsheid, werd in 1714 door Samuel I in de firma geïntroduceerd. Optimistisch geeft zij te kennen dat zij veilig en wel achter haar schild verkeert - ‘Tuta sub aegide Pallas’. Tussen de ploeterende boer en de hoopvolle godin komt het verschil tussen de eerste en de tweede generatie tot uitdrukking: Samuel hoefde het bedrijf niet meer vanaf de grond op te bouwen. Als symbool van de wetenschap verwees Pallas tevens naar de belangrijkste pijler van Luchtmans’ uitgeverij. Wellicht had haar veilige zelfverzekerdheid ook te maken met de Vrede van Utrecht (1713-14), die zojuist een einde had gemaakt aan meer dan tien jaar oorlog.

Pallas was een blijvertje, al werden rond het midden van de eeuw andere merken gebruikt. Waarschijnlijk waren dat experimenten van de derde generatie, want zij vonden plaats in de tijd waarin de firma zich presenteerde als ‘Samuel Luchtmans en zoonen’. Een dame met een geestrijke vlam op haar hoofd en met talrijke allegorische attributen geeft te kennen dat zij zich laat leiden door God, vindingrijkheid, kunst, waakzaamheid en inspanning (ze had een hele mondvol Latijn nodig: ‘Deo duce ingenio arte vigilantia labore’). Een andere mevrouw in een idyllische setting laat weten het nuttige met het aangename te combineren (‘Utile dulci’). Zinnebeeldige jeugdzonden van Samuel II en Johannes, die na 1755 terugkeerden naar een gestileerde Pallas. In de negentiende eeuw raakten de drukkersmerken uit de mode, maar in het begin van de twintigste herstelde Brill het oude motto ‘Tuta sub aegide Pallas’ in ere. =>

<=.pb 25 =>
De oude huizen werden gesloopt en op hun plaats liet Van der Aa een indrukwekkend nieuw complex oprichten, bestaande uit een woonhuis annex winkel en drukkerij. Tot dusverre stond hij alleen als boekverkoper bij het gilde ingeschreven, maar thans liet hij zich ook als drukker inschrijven. Zijn omtrekkende beweging had het beoogde effect, temeer daar Poereep weinig van zijn ambt terecht bracht: in mei 1715 werd Van der Aa aangesteld als academiedrukker. Een paar weken eerder was hij al benoemd tot stadsdrukker.

Van 1715 tot 1730 was Pieter van der Aa het stralende middelpunt van het Leidse
boekenbedrijf. Samuel Luchtmans bracht in die jaren eveneens zijn bedrijf tot bloei, maar hij kon slechts gedijen in de schaduw van zijn buurman. De uitgaven van Van der Aa in zijn gloriejaren waren niet te overtreffen - de Doge van Venetië was zozeer van een van zijn publicaties onder de indruk dat hij de boekhandelaar benoemde tot ridder in de Orde van San Marco. Met zo’n klinkende titel kon Pieter zich vertonen op het Rapenburg. Het hoogtepunt van zijn productie was de magnifieke _Galérie Agréable du Monde_ (1729), het mooiste prentwerk dat ooit in Nederland is verschenen. Het was tevens de typografische zwanenzang van Van der Aa, die bij het toenemen der jaren steeds meer met zijn gezondheid tobde.

In augustus 1730 nam hij noodgedwongen ontslag als stads- en academiedrukker en in het voorjaar van 1731 deed hij zijn drukkerij van de hand. Samuel Luchtmans organiseerde de veiling waarop de inventaris van de drukkerij werd verkocht. Pieter van der Aa overleed in 1733, zonder mannelijke nakomeling; zijn weduwe zette de boekhandel aan het Rapenburg nog enige tijd voort, maar in 1735 werd ook de winkel opgedoekt.[^14]

![](01-22.jpg)

<=.ill_01-22.jpg Hoofdbrekens van een uitgever. Kladbriefje uit 1717 waarop Samuel Luchtmans de hoeveelheid letters op een regel en de hoeveelheid regels op een octavo-pagina berekende. In het verlengde maakte hij een schatting van de hoeveelheid benodigde drukvellen van 16 pagina’s. Met een ander lettertype zou hij minder papier nodig hebben, maar in dat geval moest hij nieuwe letters laten maken. De berekening heeft betrekking op de Hongaarse bijbel die Luchtmans in 1718 uitbracht (_Magyar Biblia Avagy az O és Uj Testamentom_). De aanleiding tot deze Hongaarse buitenissigheid is niet duidelijk. Samuel correspondeerde over het boek met een zekere Desobrie, mogelijk een vertegenwoordiger van de protestantse gemeenschap in Hongarije. AL/UBA =>

<=.pb 26 =>

![](01-23.jpg)

<=.ill_01-23.jpg De ontwikkeling van het Hebreeuwse alfabet via Abraham, Enoch en Ezdras. Prent uit Van der Aa’s _Galérie Agréable du Monde_ (1729), een hoogtepunt in de Nederlandse prentkunst. =>

<=.pb 27 =>
### Hoogtij van Samuel
Wederom stond de dood van de een niet los van het brood van de ander - het verdwijnen van Pieter van der Aa kwam Samuel Luchtmans in hoge mate ten goede. Hij werd op 8 augustus 1730 aangesteld als academiedrukker, toen zijn buurman dat ambt om gezondheidsredenen neerlegde. Deze keer was hij degene die weloverwogen op de toekomst was vooruitgelopen: reeds in 1728 waren bij hem de _Wetten ende Statuten van de Universiteyt tot Leyden_ verschenen. Op 7 februari 1730, acht maanden voor zijn benoeming tot academiedrukker, had hij zich tegenover de curatoren contractueel verplicht ‘ten sijnen kosten... [te] laeten drukken seeker Arabies manuscript in folio’.[^15] Het was dezelfde tactiek die Van der Aa indertijd had toegepast om bij de universitaire bestuurderen een wit voetje te halen.

Deze uitgave voor rekening van de uitgever verscheen in 1732 onder de titel _Vita et res gestae Sultani Almalichi Alnasiri Saladini_ \[Leven en daden van Sultan Saladin\], met de Arabische tekst en de Latijnse vertaling naast elkaar in twee kolommen. Het was de eerste Arabische uitgave van het huis Luchtmans en als zodanig de opmaat van Brills traditie van oriëntalia. De Latijnse vertaling was van de hand van de theoloog Albert Schultens, stichter van een dynastie van Leidse arabisten die over drie generaties zou lopen. Tussen de familie Schultens en de familie Luchtmans ontstond een hechte samenwerking.

![](01-24.jpg)

<=.ill_01-24.jpg Binnenwerk van de _Grammatica Arabica_ , in 1747 uitgegeven door Samuel Luchtmans. De arabist Albert Schultens bewerkte de oudere grammatica van Thomas Erpenius. Coll. Brill =>

<=.pb 28 =>
De nieuwe academiedrukker werd in 1730 tevens aangesteld als ‘Ordinaris Stads Drukker’, wat niet betekende dat hij daadwerkelijk een drukkerij exploiteerde; stad en universiteit gingen ermee akkoord dat hun drukwerk werd uitbesteed. Het wegvallen van Van der Aa schiep de ruimte waarin Luchtmans zijn bedrijf tot grote bloei kon brengen, een ruimte die hij ten volle benutte. De jaren tussen 1730 en 1755 vormden het hoogtij van Samuel I, vooral wat betreft de uitgeverij. De dissertaties, disputaties en oraties die voortvloeiden uit het academiedrukkerschap kwamen de omzet ten goede, zij het dat ze in mindere mate bijdroegen tot de winst. In zijn wetenschappelijke uitgaven hield Samuel vast aan het vertrouwde stramien van theologie en filologie, maar hij begaf zich ook op nieuwe paden. De geest van Newton was vaardig geworden over de Leidse academie en vertoonde zich tevens in de uitgaven van haar _typographus academiae_.

![](01-25.jpg)

<=.ill_01-25.jpg De monumentale vierde editie van _Bayles Dictionaire Historique et Critique_ (1730) was een monsterproductie van Samuel Luchtmans en zeven Amsterdamse boekhandelaren. Het boek raakte na verloop van tijd uitverkocht en in 1740 werd een vijfde editie uitgebracht. Deze keer namen maar liefst vijftien boekhandelaren deel aan de onderneming. Luchtmans participeerde voor een twaalfde deel in de kosten, in ruil voor een twaalfde deel van de oplage. De totale kosten voor 3288 exemplaren bedroegen f 46.000, een onvoorstelbaar bedrag in die tijd. Samuel moest ruim f 3800 investeren en kon aanspraak maken op 275 exemplaren met een kostprijs van f 14. UBA =>

![](01-26.jpg)
![](01-27.jpg)

<=.ill_01-26.jpg Petrus van Musschenbroek was een zwager en een volle neef van Samuel Luchtmans. De natuurkundige was bovendien een van zijn best verkopende auteurs. AL/UBA =>

<=.pb 29 =>
Ontbraken natuurwetenschappelijke boeken vrijwel geheel in het fonds van Jordaan Luchtmans, in dat van zijn zoon namen zij een belangrijke plaats in. Reeds in 1727 publiceerde hij de _Matheseos universalis elementa_, het boek waarmee W.J. ’s Gravesande de ‘universele wiskunde’ van Newton in Nederland introduceerde. Bij het ontwikkelen van dit nieuwe domein had Samuel opnieuw baat bij de Musschenbroek-connectie: zijn neef en zwager Petrus (1692-1761) was een van de sterren van de nieuwe natuurkunde. Na zijn studie in Leiden was Musschenbroek achtereenvolgens hoogleraar in Duisburg en Utrecht, om in 1740 als opvolger van ’s Gravesande terug te keren naar Leiden. Hij publiceerde de meeste van zijn werken bij zijn neef Luchtmans. Zijn _Elementa Physicae_ (1734) was tot in de jaren zestig een bestseller die met grote regelmaat werd herdrukt en ook in Franse en Nederlandse vertalingen werd uitgebracht.

Het publiceren van meertalige edities om een breder publiek te bereiken paste Luchtmans in deze jaren vaker toe. De Verlichting speelde zich niet alleen af op het verheven niveau van grote geesten en wetenschappelijke ontdekkingen, zij manifesteerde zich evenzeer in koffiehuizen, salons, leesgezelschappen en geleerde genootschappen. In dergelijke kringen ontwikkelde zich een weetgierige klandizie die de omzet van de uitgeverij stimuleerde. De boekhandel en de tijdgeest vonden elkaar in een vruchtbare wisselwerking.

### Van vader op zoons
Aldus groeide Samuel uit tot een man van aanzien en zijn boekhandel tot een zaak van gewicht, terwijl hij en Cornelia tussen de bedrijven door negen kinderen kregen. Het waren in alle opzichten voorspoedige tijden. De twee oudste zonen, Samuel II (1725-1780) en Johannes (1726-1809), werden na verloop van tijd in de zaak opgenomen. Zij doorliepen beiden de Latijnse school en studeerden daarna enige tijd aan de universiteit van hun vaderstad. Zoals alle hoger opgeleiden van hun tijd beheersten zij het Latijn en het Frans, terwijl ze daarnaast een degelijke scholing kregen in het Duits, Engels en Italiaans. Die talen vielen buiten het gangbare curriculum, maar in de boekenbranche kwamen ze uiteraard goed van pas.

Samuel jr. werd in 1741 bij het gilde ingeschreven als boekverkoper - rijkelijk jong, gezien zijn leeftijd van zestien jaar. De wat ongebruikelijke procedure werd vergemakkelijkt door de leidende positie van Samuel sr. binnen het boekengilde. Kennelijk wilde hij de continuïteit van zijn bedrijf veilig stellen, want in datzelfde jaar werd Samuel jr. tevens benoemd tot zijn opvolger als stads- en academiedrukker. De jongen was nog student en trad pas in de loop van de jaren veertig daadwerkelijk in dienst bij zijn vader. Johannes maakte vanaf 1749 deel uit van de firma - hij zou er zestig jaar aan verbonden blijven en was daarmee de Luchtmans met de langste staat van dienst.

Het familiebedrijf voer sindsdien onder de vlag van ‘Samuel Luchtmans en zoonen’, totdat de vader in 1755 terugtrad. Hij had de leeftijd van zeventig jaar bereikt en begon de last van de ouderdom te voelen. Twee jaar later, in de winter van 1757, overleed Samuel Luchtmans. Hij werd begraven in het familiegraf dat hij in de Pieterskerk had aangekocht, een ligging die paste bij zijn stand. Cornelia overleefde hem vele jaren en zou zich pas in 1784 bij hem voegen.

![](01-28.jpg)

<=.ill_01-28.jpg Ook in het geval van zijn neef Petrus van Musschenbroek berekende de uitgever nauwkeurig de kosten. AL/UBA =>

<=.pb 30 =>
## De firma S. & J. Luchtmans, 1755-1848
Nadien zou de firma bijna een eeuw lang door het leven gaan als ‘S. & J. Luchtmans’, zij het dat de naamgevende gebroeders Samuel II en Johannes slechts tot 1780 gezamenlijk leiding gaven aan het familiebedrijf. Zij scheelden weinig in leeftijd en maken door hun onafscheidelijkheid de indruk van een tweeling. Hun broederlijke eensgezindheid strekte zich uit tot andere bereiken des levens, getuige het feit dat beiden trouwden met een meisje Reytsma - Johannes in 1763 met Maria Johanna, Samuel anderhalf jaar later met haar zusje Constantia Elisabeth.

Als stads- en academiedrukkers traden ze in de voetsporen van hun vader en bekleedden evenals hij leidende ambten in het gilde. In tegenstelling tot hun vader hoefden ze geen gezeten burgers te worden, aangezien ze die status van huis uit hadden meegekregen. Ze werden niet gerekend tot de eigenlijke regenten van Leiden, maar kwamen als kapitaalkrachtige middenstanders aardig in de buurt van de stedelijke toplaag. Waren de Luchtmansen tot dusverre lidmaten geweest van de Gereformeerde Kerk, de broers en hun echtgenotes gingen over tot de Waalse Kerk. De reden van hun overstap is niet duidelijk, kennelijk voelden ze zich beter thuis in het milieu van de Église Wallonne of vonden ze die gezindte meer in overeenstemming met hun stand.[^16]

### Over de grenzen
Niet alleen op het Rapenburg deden de gebroeders hun zaken, tussendoor maakten ze verschillende reizen naar het buitenland. Hun reislustigheid hadden ze niet geërfd van hun vader, naar de geest een kosmopoliet maar naar het lichaam eerder het tegendeel. Men krijgt de indruk dat Samuel I een tamelijk honkvast bestaan heeft geleid, van zijn aanwezigheid op de Buchmessen van Frankfurt en Leipzig valt althans geen spoor te vinden. Zakelijk gezien was zo’n acte de présence ook geen noodzaak, want evenals de meeste Nederlandse boekverkopers kon hij zich ter plaatse laten vertegenwoordigen door een commissionair. De reislust van de derde generatie doet eerder denken aan hun grootvader Jordaan, die tegen het einde van de zeventiende eeuw verschillende keren de Duitse ‘boekenmissen’ bezocht.[^17] Getuige Samuels balans van 1714 had Jordaan zelfs een tijdlang filialen in Duisburg en Lingen, die door de oorlogsomstandigheden in het eerste decennium van de achttiende eeuw in onbruik raakten.

In het archief-Luchtmans is een vijftal reisjournalen bewaard gebleven, waarin Samuel en Johannes aantekenden wat zij in den vreemde zoal meemaakten.[^18] De Buchmesse van Leipzig, die in de loop van de eeuw die van Frankfurt overvleugelde, was hun favoriete bestemming buiten de grenzen; verder maakte Samuel in 1768 een reis naar Frankrijk, terwijl Johannes in 1772 enige tijd in Engeland verbleef.[^19] De gezamenlijkheid van de broers werd alleen doorbroken tijdens zulke reizen, steeds bleef een van hen in Leiden om de boekhandel te bemannen.

Een reis van Leiden naar Leipzig duurde al gauw een dag of tien en was vanwege het gehobbel met postkoetsen over slechte wegen geen onverdeeld genoegen. De boeken die ter Messe gingen werden afzonderlijk vervoerd, gewoonlijk over het water: van Amsterdam naar Hamburg en vandaar stroomopwaarts over de Elbe naar Leipzig. De boeken, meestal in ongebonden staat, werden verpakt in waterdichte vaten om ze tegen waterschade te beschermen. Om alle kosten voor reis, verblijf en transport eruit te

![](01-29.jpg)

<=.ill_01-29.jpg Samuel II Luchtmans op de leeftijd van dertig jaar. Portret (1755) door Nicolaas Reyers. SML =>

![](01-30.jpg)

<=.ill_01-30.jpg Johannes Luchtmans in 1755. Portret door Nicolaas Reyers. SML =>

<=.pb 31 =>
halen moesten in Leipzig goede zaken worden gedaan. Gezien de rompslomp wekt het nauwelijks verwondering dat gemiddeld slechts twee à drie Nederlandse boekhandelaren de Messe bezochten.[^20]

Het voordeel van zo’n internationale stapelmarkt was gelegen in het directe contact met vakgenoten: men verkocht de eigen boeken en kocht buitenlandse die interessant leken voor de thuismarkt. Bovendien ging men niet alleen naar Leipzig vanwege de handel, maar ook vanwege het vertier - de Messe was een uitbundige kermis waarop van alles viel te doen en te bekijken. Dat laatste valt overigens niet zozeer op te maken uit het verslag dat Samuel maakte van zijn verblijf in mei 1764. Zo te zien was hij druk aan het netwerken en zocht hij zijn vertier vooral in ontmoetingen met andere boekhandelaren:

>‘26 Mei. ’s Middags ten eeten by Hr. Reich, alwaar ik vond Hr. Kraus met zyn
Vrouw, van Weenen, Hr. Walther met zyn Vrouw en Zoon, van Dresden, en
Hr. Nicolai met zyn Vrouw, van Berlyn, ook de Heeren Bohn en Brand van
Hamburg, allen boekhandelaren, en ons wel diverteerden. Ik ben... geweest by
de Hr. Burgemeester Bom op die neue Markt; hier hadden ook haar nederlagen
\[onderkomen\] Hr. Meisnar van Wolfenbuttel en Hr. Mumine van Copenhage…’[^21]

![](01-31.jpg)

<=.ill_01-31.jpg De deelnemers aan de Buchmesse in Leipzig troffen elkaar in koffiehuis Richter. St. Mus. Leipzig =>

![](01-32.jpg)

<=.ill_01-32.jpg Verslag van Samuel II Luchtmans over zijn reis in 1764 naar Leipzig, Dresden en Berlijn. AL/UBA =>

<=.pb 32 =>
### Handelaars met allure
Was hun vader meer uitgever dan boekhandelaar, bij de zoons lag die verhouding omgekeerd. De uitgeverij zette zich tijdens het bewind van de gebroeders voort in een gestage stroom van weinig opzienbarende werken, voornamelijk op het gebied van klassieke talen. Naast geannoteerde tekstedities door de toenmalige classici van de universiteit werden veel oudere werken uit het fonds van de firma opnieuw uitgegeven. De uitgeverij had haar élan verloren, wat niet losstond van het feit dat de Leidse universiteit en ook de stad Leiden in de tweede helft van de achttiende eeuw in verval raakten. Daarentegen hadden de gebroeders een assortiment boeken in huis dat in vergelijking met hun vader ronduit verbijsterend was.

Duizenden en duizenden titels werden opgesomd in de lijvige catalogi die S. & J. Luchtmans regelmatig lieten verschijnen.[^22] Daaronder bevonden zich veel antiquarische werken, zelfs wiegedrukken uit het laatste kwart van de vijftiende eeuw. Samuel en Johannes pasten op de winkel en deden dat op onovertroffen wijze. Om die enorme voorraad te verwerven onderhielden ze schriftelijke contacten met talloze boekhandelaren in binnen- en buitenland. Hun kosmopolitische correspondentie bestreek Duitsland, Frankrijk, België, Engeland, Zwitserland, Italië, Spanje, Portugal, Denemarken, Zweden, Rusland en Hongarije; zelfs een boekhandelaar in het verre Bombay kreeg bij tijd en wijle post uit Leiden. Deels waren zulke contacten te danken aan het nijvere netwerken van de gebroeders op de Messe. Eigenlijk deden ze vanachter hun schrijftafel op het Rapenburg hetzelfde wat Samuel lijfelijk deed in Leipzig: zij knoopten vruchtbare betrekkingen aan met vakgenoten.

### Familiekroniek
In 1780 werd de twee-eenheid van de broers verbroken door de dood van Samuel, waarop Johannes het bedrijf alleen voortzette. De erfopvolging was gegarandeerd in de persoon van Samuel Samuelszn., geboren in 1766; Johannes had twee dochters maar diens enige zoon was kort na de geboorte gestorven. Ten tijde van het overlijden van zijn vader was Samuel III een jongen van veertien jaar, zodat hij eerst zijn school en zijn studie moest voltooien. Na het behalen van zijn meestertitel in de rechten trad hij toe tot de firma, maar het boekenbedrijf had niet zijn hart. Hij liet het bestuur van het familiebedrijf over aan zijn oom en wijdde zich liever aan het bestuur van de stad: tijdens de Bataafse en Franse jaren bekleedde hij diverse functies in Leiden. Johannes Luchtmans leidde de boekhandel tot 1809, toen hij op de leeftijd van 83 jaar in het harnas stierf. Samuel III nam daarop de leiding in handen, maar zijn bewind zou slechts drie jaar duren. De laatste telg van de Luchtmans-dynastie overleed in 1812 zonder mannelijke nazaat.

De firma bleef echter bestaan, als vanouds onder de naam van S. & J. Luchtmans maar niet langer op het aloude adres Rapenburg 69B: rond deze tijd verhuisde de boekhandel naar de nummers 78-80 aan de overkant van de gracht. Het bedrijf vererfde in de vrouwelijke lijn via Johannes’ dochter Magdalena Henriëtta, getrouwd met de Amsterdamse arts Evert Bodel Nijenhuis. Dit echtpaar had twee kinderen, Johannes Tiberius en Catharina. Toen de moeder in 1799 overleed was de vader niet in staat de opvoeding van de kleine kinderen voor zijn rekening te nemen. Johannes Tiberius en Catharina groeiden op in het huis van hun grootvader Johannes in Leiden en werden

<=.pb 33 =>

![](01-33.jpg)

<=.ill_01-33.jpg Prent uit Y. van Hamelsveld, _De gewigstigste geschiedenissen des Bybels, afgebeeld in twee honderd en vijftig printverbeeldingen_, in 1791 uitgegeven door Johannes Luchtmans. UBA =>

<=.pb 34 =>
voornamelijk opgevoed door hun ongetrouwde tante Cornelia. De kleinzoon was de voorbestemde opvolger van de grootvader, maar was ten tijde van diens overlijden veel te jong om de zaak over te nemen.


In deze fase van de familiekroniek raakt de naam Luchtmans verweven met die van Brill. Omdat Samuel III andere besognes had en Johannes op leeftijd kwam, hadden ze in 1802 de Leidse drukker Brill aangetrokken als bedrijfsleider. Johannes Brill (1767-1859) stamde uit een Hollands predikantengeslacht, al hield hij die traditie niet in ere en was hij evenmin voorbestemd voor het drukkersvak. Hij was sinds 1786 werkzaam op de secretarie van stadhouder Willem V, maar bleef werkeloos achter toen de prins in 1795 de wijk nam naar Engeland. Brill stond uiteraard te boek als orangist, wat in Bataafse tijden bepaald geen aanbeveling was. Om in zijn levensonderhoud te voorzien zette hij een drukkerij op in Leiden en verzorgde in die hoedanigheid opdrachten voor de firma Luchtmans. Vanaf 1802 leidde hij een eigenaardig dubbelbestaan als zelfstandig drukker en bedrijfsleider van Luchtmans, een dualiteit die hij
tot 1848 zou handhaven. Hij werd de huisdrukker van Luchtmans, zij het dat hij daarnaast drukwerk voor anderen verrichtte.

Toen Samuel III in 1812 overleed, was Johannes Brill geheel ingewijd in de geheimen van de boekhandel. De bedrijfsleider trad nu op als bewindvoerder van S. & J. Luchtmans, in afwachting van het tijdstip waarop de jonge Bodel Nijenhuis de volwassen leeftijd bereikte. Bodel kreeg een soortgelijke opleiding als zijn voorvaderen in de achttiende eeuw: hij doorliep de Latijnse school en studeerde vervolgens rechten aan de universiteit van Leiden. Hij verknoopte in zijn studie de geschiedenis van het familiebedrijf met zijn eigen toekomst: in 1819 promoveerde hij op een historisch-juridisch proefschrift over de rechten van drukkers en boekverkopers, dat uiteraard werd gepubliceerd door S. & J. Luchtmans.[^23]

### Tweemanschap
In theoretisch opzicht kwam Bodel beslagen ten ijs, toen hij in 1821 zijn intrede deed in het bedrijf. Voor de praktijk van het boekenvak was hij minder goed voorbereid, maar tot zijn geluk vond hij de ervaren Brill naast zich. Tot 1848 bestierden zij gezamenlijk de firma, Brill als zakelijk leider en Bodel als intellectueel leidsman. Bodel, meer boekenmens dan boekhandelaar, was degene die de relaties met de wetenschappelijke wereld onderhield. De firma Luchtmans was nog steeds academiedrukker, wat een zekere omzet aan dissertaties garandeerde. De tekstedities van klassieken vormden als vanouds een constante in het uitgavenpatroon.

Belangrijk in deze jaren was de systematische publicatie van Arabische manuscripten uit het befaamde Legatum Warnerianum van de Leidse universiteit. Opeenvolgende arabisten als Hamaker en Dozy publiceerden bij Luchtmans hun edities van deze manuscripten. Deze oriëntaalse traditie bouwde voort op de vroegere uitgaven van Luchtmans en anticipeerde op het latere fonds van Brill. Met de komst van hoogleraren als Reuvens en Janssens verwierf de archeologie een institutionele status binnen de universiteit, wat zich vertaalde in diverse uitgaven op dit terrein. De egyptologie vond vanaf 1839 haar neerslag in de reeks _Aegyptische Monumenten_, naderhand door Brill voortgezet tot 1904.

![](01-34.jpg)

<=.ill_01-34.jpg Johannes Luchtmans op de leeftijd van 65 jaar. Pastel door Johann Anspach rond 1791. SML =>

<=.pb 35 =>
Politieke ontwikkelingen hadden repercussies op de publicaties van S. & J. Luchtmans. Zo werd de deelname van Leidse studenten aan de Tiendaagse Veldtocht tegen België bejubeld in het gelegenheidswerkje _De terugkomst der vrijwillige jagers_ (1831) van de theoloog N.C. Kist. Een ander gevolg van de Belgische opstand was de ‘volhardingspolitiek’ van koning Willem I, die tot 1839 tevergeefs probeerde zijn zuidelijk rijksdeel terug te winnen. Op hun beurt leidden de sterk gestegen militaire uitgaven van het moederland tot een meer intensieve exploitatie en exploratie van de Oost-Indische koloniën. Van de weeromstuit groeide in Leiden de wetenschappelijke belangstelling voor de Oost, evenals het aantal publicaties op dat gebied bij Luchtmans. De effecten
van het Cultuurstelsel deden zich gevoelen op het Rapenburg: ‘Op last van den Koning’ (en dus met steun van de overheid) verscheen bij Luchtmans een schitterende driedelige _Natuurlijke Geschiedenis der Nederlandsche Overzeesche bezittingen_ (1839-44) van C.J. Temminck e.a. Het typografische hoogstandje vormde de opmaat van de Indische traditie die later in de eeuw door Brill werd gecultiveerd.

Andere uitgaven hadden te maken met de historische belangstelling van Bodel, zoals de monumentale bronnenpublicatie Archives de la Maison de Orange-Nassau van Groen van Prinsterer. Bodel was bevriend met Groen en vervaardigde eigenhandig de index op de ettelijke delen. Vermoedelijk waren Bodels godsdienstige gevoelens verwant aan die van zijn vriend en was hij eveneens een aanhanger van het Réveil. Groens _Ongeloof en Revolutie_, grosso modo een lofzang op God, Nederland en Oranje, verscheen in 1847 bij Luchtmans. Ook Isaäc da Costa, bij uitstek de belichaming van het Réveil, publiceerde enige boeken bij Luchtmans.

Zo blijkt een uitgeverij op allerlei manieren verweven met de wereld om haar heen. Meer dan dat, zij is een medium waarin de stromingen van een tijd samenkomen. Bodel was een man van zijn tijd, ook al koesterde hij evenals Da Costa bezwaren tegen ‘de geest der eeuw’ en was hij evenals Groen van Prinsterer een ‘anti-revolutionair’. Grootvader Johannes, geboren in 1726, zou weinig hebben begrepen van de negentiendeeeuwse habitat van zijn kleinzoon. De wereld was veranderd en met haar de firma Luchtmans. En in al die veranderingen was de firma Luchtmans zichzelf gebleven, want zij was steeds de spiegel van haar tijd.

![](01-35.jpg)

<=.ill_01-35.jpg Een circulaire uit 1821 deelde mee dat J.T. Bodel Nijenhuis in dienst trad bij de firma Luchtmans. AL/UBA =>

![](01-36.jpg)

<=.ill_01-36.jpg C.J. Temminck e.a., _De Natuurlijke Geschiedenis der Nederlandsche Overzeesche bezittingen_ (3 dln., 1839-1844). UBA =>

<=.pb 36 =>

![](01-37.jpg)

<=.ill_01-37.jpg De prenten in het koloniale prachtwerk van C.J. Temminck waren handgekleurd. UBA =>

<=.pb 37 =>
### De nieuwe man
Er hing meer verandering in de lucht. Sinds 1 juli 1829 werkte de zeventienjarige Evert Jan Brill bij de firma, zo valt op te maken uit een document van 1831.[^24] Kennelijk was de jongeman in de smaak gevallen, want S. & J. Luchtmans wilden hem aan het bedrijf binden. Zij besloten ‘de gemelde zoon van den heer J. Brill te zijner aanmoediging voor ieder den twee jaren, welke hij alzoo is werkzaam geweest, ingegaan met den 1e july 1829, toe te leggen de som van honderd vijftig gulden en daarmede als nog twee jaren te continueren in het vast vertrouwen dat gemelde zoon van J. Brill onder de opleiding van zijne vader aan het doel in dezen manifesteert, alleszinds zal blijven beantwoorden’.

Een hele mondvol van Bodel, maar Brill jr. bleek het vertrouwen en de tweejaarlijkse bonus van honderdvijftig gulden meer dan waard. Onder leiding van zijn vader ontwikkelde hij zich tot een allround vakman - drukker, uitgever en boekhandelaar. In de loop van de jaren veertig liet hij zelfs op naam van ‘E.J. Brill’ een aantal Arabische uitgaven verschijnen. Hij verzorgde in 1846 ook de uitgave van een Franstalig boek over Oost-Indië van C.J. Temminck, directeur van ’s Rijks Museum voor Natuurlijke Historie en bezorger van het koloniale pronkstuk dat kort tevoren bij Luchtmans was uitgebracht. Evert Jan had kennelijk de ambitie om zelf uitgever te worden, maar het lag in de lijn der verwachtingen dat hij zijn vader zou opvolgen als bedrijfsleider van Luchtmans.

De zaak werd op de spits gedreven in het revolutiejaar 1848, zoals in het begin van dit hoofdstuk werd beschreven. Mogelijk droegen de tijdsomstandigheden bij tot het besluit van Bodel om zich uit het bedrijf terug te trekken. Hij wilde zich voortaan uitsluitend wijden aan zijn intellectuele bezigheden, terwijl uit zijn uitlatingen bovendien valt op te maken dat de winstgevendheid van de firma was verminderd. Brill sr. had al eerder te kennen gegeven dat hij in Scherpenzeel wilde gaan genieten van een rustige oude dag. Het lag niet op voorhand vast dat Evert Jan, beoogd opvolger van zijn vader als bedrijfsleider, op eigen kracht de firma Luchtmans zou overnemen. Bodel was aanvankelijk van plan het bedrijf te verkopen aan de boekhandelaar Frederik Muller in Amsterdam.

Deze wees het aanbod echter van de hand, zich vijfentwintig jaar later afvragend of hij zulks ‘te goeder of te kwader ure’ had gedaan.[^25] Toen zich geen andere gegadigden aandienden, werd besloten de zaak over te dragen aan Brill jr. De financiële bijzonderheden van de overdracht zijn niet bekend, al kan men gezien de staat van dienst van beide Brills enige coulantie verwachten van de zijde van S. & J. Luchtmans. Niettemin moet Evert Jan Brill behoorlijk zware financiële verplichtingen zijn aangegaan. Zijn zelfvertrouwen bleek niet alleen uit het besluit de firma Luchtmans over te nemen, maar evenzeer uit het besluit haar onder zijn eigen naam voort te zetten. Brill was van plan zichzelf te bewijzen, zonder te teren op de oude luister van Luchtmans.

![](01-38.jpg)

<=.ill_01-38.jpg Nogmaals Temminck, in 1846 uitgegeven op naam van E.J. Brill. UBA =>

![](01-39.jpg)

<=.ill_01-39.jpg J.T. Bodel Nijenhuis rond 1840. Portret door J.L. Cornet. Coll. UBL =>

<=.pb 38 =>

![](01-40.jpg)

<=.ill_01-40.jpg Plaat uit G. Sandifort, _Museum Anatomicum Academiae Lugduno-Bataviae_, T. IV. De indrukwekkende anatomische atlas van vader en zoon Sandifort was een langlopend project van Luchtmans. Het eerste deel verscheen in 1793, het vierde in 1835. UBA =>

<=.pb 39 =>
## Het Luchtmans-archief
Het archief van de firma Luchtmans beslaat vrijwel de gehele periode 1683-1848 en vormt een unieke bron voor de geschiedenis van de Nederlandse boekhandel. Het is hier te lande het enige archief dat zo’n lange periode omvat en zo’n breed spectrum van het boekbedrijf in beeld brengt. Ook het achttiende-eeuwse archief van de firma Enschedé uit Haarlem is ten dele bewaard gebleven, maar dat bestrijkt minder tijd en is minder volledig. Bovendien was Enschedé geen boekhandelshuis met internationale allure, maar in de eerste plaats drukker en uitgever van de _Oprechte Haarlemsche Courant_. Het Luchtmans-archief is ondergebracht bij de Bijzondere Collecties van de Universiteitsbibliotheek van Amsterdam en heeft een omvang van circa elf strekkende meter. Onder verwijzing naar de inventaris wordt hier volstaan met het noemen van de belangrijkste bestanddelen.[^26]

Om te beginnen is een indrukwekkende serie van 31 folianten bewaard gebleven, de zogenaamde ‘Boekverkopers Grootboeken’ dan wel ‘Boekverkopers Schuldboeken’. Met uitzondering van het eerste deel over de jaren 1683-1697 bestrijken zij een aaneengesloten periode van 1697 tot 1845. In deze grootboeken werden de transacties van Luchtmans met boekhandelaren in binnen- en buitenland bijgehouden. Men kocht boeken van elkaar en bedreef daarnaast commissiehandel voor elkaar: de Leidse firma verkocht in commissie boeken van andere uitgevers, die op hun beurt Luchtmans’ uitgaven in commissie hadden.

Vrijwel alle bekende namen uit de achttiende-eeuwse boekhandel worden in deze registers aangetroffen. In het buitenland onderhield Luchtmans relaties met boekverkopers in Duitsland, Engeland, Frankrijk, Italië, de Zuidelijke Nederlanden en Zwitserland. Elk van deze zakenpartners had in de boekverkopersboeken een lopende rekening waarin debet en credit werden bijgehouden. Dergelijke relaties tussen boekhandelaren hadden veelal een langdurig karakter en waren gebaseerd op wederzijds vertrouwen. Verrekening vond soms plaats op een termijn van enige jaren en gebeurde niet noodzakelijkerwijze in de vorm van geld: ruilhandel van boeken was een gebruikelijke manier van vereffening (over de jaren 1741-1788 werden zulke transacties in natura ook aangetekend in een serie ‘Changeboeken’).


Luchtmans’ netwerk van zakenrelaties komt ook naar voren in vier ‘Auctieboeken’ over de jaren 1706-1806. Inkoop van boeken gebeurde veelal op veilingen van boekverkopers, voornamelijk binnen de provincie Holland. Bij dergelijke gelegenheden werd niet alleen in boeken gehandeld, maar ook in rechten van boeken. Het kopijrecht hield niet uitsluitend het recht in om een bepaald boek te drukken, in de hoop dat anderen daardoor werden weerhouden van illegaal nadrukken; het recht was verhandelbaar en kon tevens legitimeren dat een bepaalde hoeveelheid exemplaren van een boek werd nagedrukt. In de Auctieboeken werden niet alleen de aankopen op veilingen bijgehouden, maar ook Luchtmans’ onkosten bij het uitbrengen van nieuwe boeken, al dan niet in samenwerking met anderen. Inkoop van papier, porti, betalingen aan drukkers, vertalers, correctoren, ‘plaatsnijders’ oftewel gravuremakers - alle kosten die de uitgever maakte werden nauwkeurig bijgehouden.

![](01-41.jpg)

<=.ill_01-41.jpg Etruskische inscripties uit L.J.F. Janssens, _Musei Lugduno-Batavi Inscriptiones Etruscae_. In 1840 uitgegeven door S. & J. Luchtmans, terwijl E.J. Brill in 1854 een Nederlandse vertaling uitgaf met de titel _De Etrurische Grafreliëfs_. UBA =>

<=.pb 40 =>

![](01-42.jpg)

<=.ill_01-42.jpg Boekverkopersboek 1773: het debet van de Amsterdamse boekhandelaar Marc Michel Rey. AL/UBA =>

<=.pb 41 =>
De relaties met individuele klanten zijn terug te vinden in de zogenaamde ‘Particulieren Grootboeken’, ook wel ‘Burgerschuldboeken’ genoemd (negen delen over de periode 1702-1842). In deze boeken werden de aankopen van de klanten geregistreerd, gewoonlijk in de vorm van een uitstaande rekening met een looptijd van een jaar. Met behulp van deze registers valt na te gaan wie de regelmatige bezoekers waren van Luchtmans’ winkel en hoe het klantenbestand was opgebouwd.[^27] Helaas waren niet alle klanten solvabel, zeker niet in een studentenstad als Leiden. Zo constateerde Samuel I in 1714 met pijn in het hart dat hij voor meer dan f 2200 aan onbetaalde rekeningen had uitstaan, maar dat ruim een derde afgeschreven moest worden als ‘kwaade schulden’. De bibliofiele wanbetaler is van alle tijden.[^28]

In kasboeken werden de dagelijkse uitgaven genoteerd en andere notities gemaakt, terwijl een drietal balansen (1714, 1747 en 1810) een doorsnede geeft van vaste en vlottende activa van het bedrijf. In pakhuisboeken werden de opgeslagen voorraden boeken aangetekend. De uitgaande correspondentie werd bijgehouden in zogenaamde kopieboeken. Verder valt in het archief een grote hoeveelheid losse documenten te vinden - contracten, interne correspondentie en bewaard gebleven notities. Zoals te verwachten bij een boekhandelaar treft men een grote hoeveelheid catalogi aan, gedrukt dan wel handgeschreven. En uiteraard zijn er curiosa, zoals de reeds genoemde reisdagboekjes waarin Samuel II en Johannes hun belevenissen vastlegden tijdens hun buitenlandse reizen.

### Van Leiden naar Amsterdam
Het jaartal 1848 vormde geen strikte waterscheiding tussen het oude en het nieuwe bewind: het laatste boekverkopersgrootboek van Luchtmans bevat aantekeningen van Evert Jan Brill, die zijn gemaakt na de opheffing van de oude firma. Het meest recente deel van het archief-Luchtmans, opgebouwd sinds de verhuizing in het begin van de negentiende eeuw, bevond zich in de bedrijfspanden Rapenburg 78-80. Bodel Nijenhuis bleef wonen in het oude familiehuis Rapenburg 69B en annexeerde in 1852 het buurpand 69A ten behoeve van zijn uitdijende collectie prenten, kaarten en boeken.[^29] Het archief over de periode vóór 1800 moet in zijn woning zijn achtergebleven; hij liefhebberde in geschiedenis en snuffelde geregeld in de oude papieren, zoals uit verschillende uitlatingen blijkt.

Bodel Nijenhuis overleed op 8 januari 1872. Hij was twee keer getrouwd geweest, maar beide huwelijken waren kinderloos gebleven. Na de dood van zijn tweede echtgenote liet hij op 22 juni 1866 zijn testament opmaken, onder meer met de bepaling dat hij zijn unieke collectie prenten en kaarten naliet aan de Universiteit van Leiden.[^30] Erfgenamen van zijn overige eigendommen waren de kinderen van wijlen zijn zuster Catherina Johanna, weduwe van Louis Pierre Bienfait. Zijn neef en executeur-testamentair Henri Bienfait erfde naast een gouden horloge en diamanten dasspeld een verzameling familiepapieren, waarmee vermoedelijk het oudste deel van het Luchtmans-archief werd bedoeld. Frederik Muller vermeldde in zijn necrologie van Bodel dat deze de oude papieren van de familie had nagelaten aan zijn verwanten.[^31]

Het archief over de periode 1800-1848 belandde enige tijd later eveneens in Bienfaits huis aan de Sarphatistraat in Amsterdam. De verplaatsing van dit deel kan worden gedateerd op 1883, in samenhang met een verhuizing van de firma Brill binnen Leiden.

![](01-43.jpg)

<=.ill_01-43.jpg Bodel Nijenhuis rond 1870. Coll. Brill =>

<=.pb 42 =>
Evert Jan Brill was eind 1871 gestorven, zes weken vóór Bodel Nijenhuis, en na zijn dood werd het bedrijf overgenomen door het nader te noemen duo Adriaan van Oordt en Frans de Stoppelaar. Toen de onderneming in 1883 verhuisde van het Rapenburg naar de Oude Rijn, was dat vermoedelijk aanleiding om het niet meer gebruikte archief van Luchtmans over te dragen aan Henri Bienfait. Na het overlijden van Bienfait in 1885 stelde diens zoon Jean Louis zich in verbinding met de Vereeniging ter Bevordering van de Belangen des Boekhandels. Het was Bienfait jr. ter ore gekomen dat de Vereeniging ‘bouwstoffen’ verzamelde voor de geschiedenis van de Nederlandse boekhandel en hij wilde het archief van ‘de beroemde firma Luchtmans’ aan haar overdragen.[^32] Het aanbod werd in dank aanvaard, zij het dat de Vereeniging voorstelde om vier onderdelen ‘te laten vernietigen als waardeloos’: een serie kladboeken over de jaren 1782-1839 met concepten van uitgaande brieven; 48 pakken ingekomen brieven van boekverkopers over de jaren 1801-1848; en tenslotte een grote hoeveelheid kwitanties en reçu’s uit de eerste helft van de negentiende eeuw. Gelukkig maakte Bienfait bezwaar tegen de vernietiging van de kladboeken en de brieven, zodat alleen een kist vol kwitanties en reçu’s bij het oud papier belandde.[^33]

In 1958 gaf de Vereeniging, omgedoopt tot de Koninklijke Vereniging van het Boekenvak, haar gehele collectie in bruikleen aan de bibliotheek van de Universiteit van Amsterdam. Dit fonds, met inbegrip van het archief-Luchtmans, vormt sinds 2005 de Bibliotheek van het Boekenvak. Zij is ondergebracht in het gebouw van de Bijzondere Collecties van de Universiteitsbibliotheek aan de Oude Turfmarkt in Amsterdam.

<=.pb 43 =>

![](01-44.jpg)

<=.ill_01-44.jpg Catalogus van 1714 met aantekeningen van Samuel I Luchtmans. Al/UBA =>

<=.pb 44 =>

![](02-01.jpg)

<=.ill_02-01.jpg R.P.A. Dozy, _Notices sur quelques manuscrits Arabes_ (1847-1851, met het impressum ‘E.J. Brill’). Reinhart Pieter Anne Dozy (1820-1883) was een van de meest vooraanstaande arabisten van zijn tijd. Sommige van zijn werken zijn honderdvijftig jaar na verschijning nog steeds toonaangevend. Het boek bevat een facsimile van de handschriften van al-Makrizi en diens copiïst. De facsimile is een meerkleurige lithografie, gemaakt door de Leidse lithogaaf T. Hooiberg. Dozy sprak de hoop uit dat zulke voorbeelden van autografen vaker werden gepubliceerd, aangezien ze ‘onderzoekingen van deze aard zeer zouden vereenvoudigen’. UBA =>

<=.pb 45 =>
# De firma E.J. Brill: 1848-1896

## Tussen de oude en de nieuwe tijd: Evert Jan Brill, 1848-1871

Het huis S. & J. Luchtmans nam afscheid van de wereld met een grootse verkoop van boeken.[^1] De voormalige firma ontdeed zich van alle voorraden die zich in de loop van anderhalve eeuw hadden opgehoopt in de winkel en de pakhuizen - een ontlading van boeken zonder weerga. De veilingen van Luchtmans waren in 1848-49 hét evenement van de Nederlandse boekenwereld. Evert Jan Brill belastte zich met de organisatie en had als beginnend ondernemer de handen vol aan de nalatenschap van zijn voorgangers. Ongetwijfeld zette hij bij de inventarisatie de boeken opzij die hijzelf wilde houden, hij handelde bij wijze van spreken met voorkennis. Overname van het volledige boekenbestand van Luchtmans kan voor Brill nauwelijks een optie zijn geweest. Het zou hem hebben opgezadeld met enorme financiële lasten én met een enorme voorraad onverkoopbare boeken.

Tussen oktober 1848 en april 1850 vonden vier veilingen plaats waarop de gebonden boeken van Luchtmans werden verkocht. De opbrengsten van de eerste veilingen werden voornamelijk gebruikt voor het betalen van schulden en voor de lopende uitgaven van de firma in haar nadagen. De voorraad ongebonden boeken werd op 20 augustus 1849 en volgende dagen geveild in gebouw Odeon aan het Singel in Amsterdam. Dit grootscheepse boekenbal werd georganiseerd door de Amsterdamse handelaren Radink en Van Kesteren, in samenwerking met Brill. De opkomst was groot en onder de belangstellenden bevonden zich bekende boekhandelaren als Frederik Muller, Martinus Nijhoff en de Gebroeders van Cleef. Zelfs Duitse en Franse handelaren maakten vanwege de veiling een speciale reis naar Amsterdam.

Naast meer recente uitgaven van Luchtmans vermeldde de catalogus vele achttiendeeeuwse werken die men sinds lang uitverkocht waande. Zowaar doken enige exemplaren op van de vierde editie van Bayle’s _Dictionnaire_ van 1730, het eerder genoemde mammoetproject van Samuel I en een stel Amsterdamse boekverkopers. De catalogus bevatte meer dan drieduizend nummers: 540 titels met kopijrecht van Luchtmans en een assortiment van 2634 titels zonder rechten. In gebonden staat zou deze massieve hoeveelheid bedrukt papier bij benadering overeenkomen met 50.000 boeken.

### Tegenvallende verkoop
Het aanbod op de veiling van ongebonden boeken was overweldigend, maar de verkoop viel tegen. Vele titels brachten minder op dan was verwacht en wegens gebrek aan belangstelling moesten maar liefst zevenhonderdvijftig nummers worden teruggetrokken. Luchtmans beschikte over een schare winkeldochters die men zelfs aan de straatstenen niet meer kon slijten. Monumenten van achttiende-eeuwse geleerdheid hadden

<=.pb 46 =>
in 1849 nog niet de status van kostbare antiquiteiten en wisselden voor een paar dubbeltjes of kwartjes van eigenaar. Na aftrek van de commissie voor de veilinghouders bedroeg de netto opbrengst van de verkoop f 25.464,44 - veel geld voor die tijd, maar minder dan de erven Luchtmans hadden gehoopt. De helft van dat bedrag was voor Bodel Nijenhuis, de andere helft voor zijn neven en nichten. Pas in augustus 1850 kon Brill aan Bodel berichten dat alle zaken waren afgewikkeld en alle schulden waren betaald. Helaas bleek ook sprake te zijn van ‘eene massa kwade schulden’, aangezien vele vorderingen oninbaar waren en afgeschreven moesten worden.

Brill zelf was een van de kopers op de Amsterdamse veiling. Hij deed voor het eerst mee met de groten van het vak, zij het als een kleine speler: hij kocht in totaal 97 titels. Meer kon hij zich vermoedelijk niet veroorloven, al is het ook de vraag of hij meer had willen aanschaffen. Hij was van plan een eigen fonds op te bouwen en had geen belang bij onnodige ballast van Luchtmans. Hij verwierf 48 titels met kopijrecht, maar een derde daarvan bestond uit achttiende-eeuwse geleerdheid met een geringe handelswaarde. Het medische werk van Albinus, in 1747 uitgegeven door Samuel I, was hoogstens een curiosum (de rechten waren nog ouder en stamden uit de dagen van Jordaan Luchtmans). Ruim dertig aankopen van kopij hadden betrekking op uitgaven uit het meer recente verleden van Luchtmans, voornamelijk op het gebied van klassieke filologie en theologie. Uitgaven in het Arabisch zouden een pijler worden van zijn eigen fonds, maar dergelijke boeken bevonden zich niet in de nalatenschap van Luchtmans: de afgelopen jaren plachten de arabica al te verschijnen onder Brills eigen naam. Wel kocht hij een werk van R.P.A. Dozy, een auteur die in de komende jaren veel tot zijn fonds zou bijdragen. Andere aankopen van recent werk lijken eveneens ingegeven door de wens de auteurs aan zich te binden.[^2]

![](02-02.jpg)

<=.ill_02-02.jpg Tot 1883 was de firma E.J. Brill gevestigd op het Rapenburg nrs.78-80. Coll. Brill =>

![](02-03.jpg)

<=.ill_02-03.jpg In zijn exemplaar van de catalogus hield E.J. Brill bij welke boeken hij kocht op de veiling van Luchtmans in 1849. AB/UBA =>

<=.pb 47 =>
### Hart voor de zaak
Brill was drukker, uitgever en beheerde daarnaast zijn boekhandel annex antiquariaat. De drukkerij was sinds de overname in het bedrijf geïntegreerd, maar de bedrijfsvoering van Brill verschilde niet wezenlijk van die van S. & J. Luchtmans. Dat viel ook nauwelijks te verwachten, aangezien hij het vak daar had geleerd en de firma in haar nadagen had geleid. Toch ging hij niet klakkeloos verder op de oude voet, hij stond om zo te zeggen met het ene been in het verleden en met het andere in de toekomst. In vergelijking met Bodel Nijenhuis was hij een toonbeeld van nieuwe zakelijkheid, al scheelde hij in leeftijd maar vijftien jaar met zijn voormalige patroon. Brill zou de invoering van de stoommachine in het drukkerswezen niet meer meemaken, maar veel meer dan zijn voorganger was hij een negentiende-eeuwse ondernemer.

![](02-04.jpg)

<=.ill_02-04.jpg Jacob Brill, met bril. Coll. Brill

Vermoedelijk was de amateur-theoloog Jacob Brill (1639-1700) een voorvader van Evert Jan Brill. De Leidse lakenwever en catechiseermeester was een volgeling van de Zeeuwse predikant Pontiaan van Hattem (±1641-1706). Deze stond in orthodoxe kringen slecht aangeschreven vanwege zijn mystieke piëtisme dat verwant was met de ideeën van Spinoza. Van Hattem en zijn navolgers legden de nadruk op persoonlijke eenwording met het goddelijke en wezen de erfzonde af. Zulks was vloeken in de Gereformeerde Kerk van die dagen.

Ook Jacob Brill bewoog zich in deze mystieke en pantheïstische denkwereld. Hij ontvouwde zijn religieuze gedachten in een boek dat verscheen in 1705, vijf jaar na zijn dood: _De werken, klaar en grondig aanwijsende het pit en merg van de ware, wesentlijke en dadelijke God-geleerdheid_. Een van de felste bestrijders van de Hattemisten was de eveneens Zeeuwse dominee-dichter Carolus
Tuinman (1659-1728), die voor Brill geen goed woord over had:

>... Daar steekt een Vrygeest in zyn vel,
>Die zich bedrieglyk wil door brabbeltaal bedekken.
>Zyn boek hoe zeer verwart, kan tot een bril verstrekken,
>Om klaar te zien in die geheimen van de hel.

Van het boek van Jacob Brill verscheen een Duitse vertaling, getuige deze titelprent met gedicht. De schrijver wordt door een sympathisant geprezen als de godsman die een uitweg biedt uit de Babylonische spraakverwarring van tegenstrijdige geloven:

>Was Brill seij für ein Gottes Mann,
>Kan seine Schrifft dir Leser zeigen an
>Er läst hierin die Secten alle stehen,
>Und zeigt den Weg aus Babel ausszugehen.
=>

<=.pb 48 =>
De mens Evert Jan Brill lijkt grotendeels samen te vallen met de firma E.J. Brill. Hij was vrijgezel en leefde meer dan twintig jaar lang uitsluitend voor zijn bedrijf. Zijn ziel, zaligheid en al zijn tijd stak hij in zijn werk, wat op den duur ten koste ging van zijn gezondheid. Hij was gewend alles zelf te doen en was vertrouwd met alle drie bedrijfsonderdelen. Gezien de kleinschaligheid van de onderneming was het fysiek ook mogelijk de verschillende activiteiten in één hand bijeen te houden. Bij de drukkerij werkten bij benadering een man of tien: een meesterknecht, een stuk of drie zetters, evenveel drukkers en een paar leerjongens. Op grond van een inventaris van het kantoormeubilair uit 1871 krijgt men de indruk dat in de winkel en de uitgeverij ongeveer vijf mensen werkzaam waren. Het totale personeelsbestand van de firma E.J. Brill zal in de jaren vijftig en zestig van de negentiende eeuw rond de vijftien personen hebben gelegen.

### Andere tijden
Gestaag werkte Brill aan de opbouw van zijn bedrijf en de tijd werkte mee. Nederland ontwaakte uit een langdurige sluimer en begon aarzelend aan een periode van opbloei. Het zwaartepunt van die ontwikkeling viel na 1870, maar de aanzet was te merken in 

![](02-05.jpg)

<=.ill_02-05.jpg Evert Jan Brill in 1866, op de leeftijd van 55 jaar. Coll. Brill =>

![](02-06.jpg)

<=.ill_02-06.jpg Veranderende tijden in Leiden: het station van de Hollandsche IJzeren Spoorwegmaatschappij rond 1850. De oudste Nederlandse spoorlijn van Amsterdam naar Haarlem, aangelegd in 1839, werd in 1842 doorgetrokken naar Leiden. A. Montagne, _De Stad Leiden_, 1850-51. UBA =>

<=.pb 49 =>

![](02-07.jpg)

<=.ill_02-07.jpg Afbeelding uit C.G.C. Reinwardt, _Plantae Indiae Batavae Orientalis_; ed. W.H. de Vriese (1856). Caspar Georg Carl Reinwardt (1773-1854) was een Nederlandse botanicus van Duitse afkomst. Hij was de stichter en eerste directeur van de botanische tuin te Bogor en later (1823-1845) hoogleraar te Leiden. Dit botanische reisverslag is kenmerkend voor de vroege negentiende eeuw, toen wetenschappelijk onderzoek en economisch nut voor het eerst hand in hand gingen. De beschrijvingen van geneeskrachtige planten en hardhout zijn aangevuld met gegevens over het voorkomen ervan. De in twee kleuren uitgevoerde litho’s zijn gedrukt in Gent. UBA =>

<=.pb 50 =>
de voorafgaande decennia. De geest van de nieuwe tijd deed zich ook gevoelen in het gezapige Leiden, waar postkoets en trekschuit moesten wijken voor de stoomtrein. De talloze herdrukken van de _Camera Obscura_ (1839) van Nicolaas Beets weerspiegelen de ingrijpende verandering van de maatschappij: de genoeglijke kleine wereld die Beets beschreef behoorde korte tijd later reeds tot een verleden dat met nostalgie werd gekoesterd. De modernisering van de samenleving ging ook anderszins gepaard met een sterke toename van de vraag naar boeken, met als gevolg dat het aantal drukkerijen in Nederland tussen 1850 en 1880 meer dan verdubbelde. Ook de wetenschap werd gegrepen door de tijdgeest en in het zicht van haar derde eeuwfeest ontwikkelde de Leidse universiteit een nieuw elan. Ontdekkingen volgden elkaar op, nieuwe disciplines werden in het leven geroepen en de internationale uitwisseling tussen geleerden nam toe.

In dat scharnierende tijdsgewricht tekent Brill zich af als een overgangsfiguur tussen vroeger en later - de hekkensluiter van Luchtmans en de wegbereider van een nieuwe tijd. Sinds 1853 was hij stads- en academiedrukker, zoals de Luchtmansen dat sinds 1730 waren geweest. Dat ambt gaf nog steeds een zekere status en garandeerde een zekere omzet van dissertaties, oraties en levensberichten. Het veilen van particuliere bibliotheken bleef een belangrijke nevenactiviteit, met een gemiddelde van twee veilingen per jaar. Brill stuurde geregeld zendingen boeken naar de Buchmesse in Leipzig, al zijn er geen aanwijzingen dat hijzelf daar ooit is geweest. De trein maakte de reis overigens een stuk comfortabeler dan de hobbelende postkoets waarop Samuel Luchtmans in zijn tijd was aangewezen.

### Het fonds van Brill
Brill zette de klassiek-filologische traditie van Luchtmans voort met uitgaven van Leidse classici als C.G. Cobet en J. Bake. Niettemin waren de klassieke talen in zijn fonds minder nadrukkelijk aanwezig dan in dat van zijn voorgangers, ook omdat het Latijn na 1850 als wetenschappelijke taal steeds meer in onbruik raakte. Ten dele werd die ontwikkeling gecompenseerd door een serie edities van klassieke teksten voor de gymnasia, als uitvloeisel van de Wet op het Middelbaar Onderwijs. Een nieuwe verschijning op het gebied van de klassieke letteren was het tijdschrift _Mnemosyne_, in 1852 opgericht door Cobet en uitgegeven door Brill. Het bestaat tot op de dag van vandaag en wordt nog steeds uitgegeven door Brill.

Tijdschriften waren destijds sterk in opkomst en vormden een aantrekkelijke groeimarkt voor de uitgeverij. Brill begon meer tijdschriften uit te geven, zoals de _Annales Academici_ van de universiteit, het tijdschrift van de Maatschappij voor Letterkunde en dat van het Museum van Natuurlijke Historie. Uit zijn relatie met dat museum vloeiden ook belangrijke uitgaven voort, zoals _Bouwstoffen voor een fauna van Nederland_ (3 dln., 1853-66) van J.A. Herklots en _Plantae Indiae Batavae Orientalis_ (1856-57) van C.G.C. Reinwardt. Een soortgelijke band met het Rijksherbarium leidde tot het geïllustreerde _Museum Botanicum Lugduno-Batavum_ van E.L. Blume (2 dln., 1849-56).

Dankzij de historicus R.J. Fruin verwierf de geschiedenis een nieuwe wetenschappelijke status, wat zich weerspiegelde in een sterke historiografische component in het fonds van Brill. Hetzelfde kan worden gezegd van de Nederlandse taal- en letterkunde. Willem Gerard Brill, een broer van Evert Jan, was in Utrecht hoogleraar in dat

![](02-08.jpg)

<=.ill_02-08.jpg Willem Gerard Brill (1811-1896), hoogleraar in de Nederlandse taal en letterkunde in Utrecht. Hij was een tweelingbroer van Evert Jan, maar niet diens evenbeeld. Coll. Brill =>

<=.pb 51 =>
vakgebied en ontwikkelde zich tot een uiterst productief geleerde. Vele van zijn werken werden door zijn broer uitgegeven. De Hollandsche Spraakleer van W.G. Brill was tot het aanbreken van de twintigste eeuw de bijbel van studenten neerlandistiek. De eerste uitgave was in 1847 bij Luchtmans verschenen, wat Brill achteraf niet naar de zin was: de grammatica van zijn broer was de duurste aanschaf die hij zich op de veiling van 1849 veroorloofde.

### Vreemde letters
Brill bestreek met zijn uitgaven een breed spectrum van letteren en wetenschappen, maar hijzelf had een speciale oriëntatie in gedachten. Wat hem voor ogen stond kwam tot uitdrukking in Het Gebed des Heeren in veertien talen dat hij in 1855 uitbracht. Brill drukte het Onze Vader in alle exotische fonts waarover hij de beschikking had - Hebreeuws, Chaldeeuws, Samaritaans, Sanskriet, Koptisch, Syrisch, Arabisch, Perzisch, Tartaars, Turks, Javaans, Maleis en Grieks, vaak in verschillende varianten. Meer dan een proef van de vreemde letters die de drukker in huis had was het boekje een beginselverklaring, misschien zelfs een voorbede van de uitgever. Brill wilde zich specialiseren in talen die buiten het bereik van andere uitgevers vielen. Hij initieerde daarmee een ontwikkeling waarop zijn opvolgers tot op de dag van vandaag voortbouwen.

![](02-09.jpg)

<=.ill_02-09.jpg De beroemde reeks _Aegyptische Monumenten van het Nederlandsch Museum van Oudheden te Leiden_ verscheen tussen 1839 en 1904 in twaalf banden, zowel in een Nederlandse als een Franse editie. Het was het levenswerk van Conrad Leemans, directeur van het Museum van Oudheden, en werd na zijn dood voortgezet door zijn opvolger Willem Pleyte. De fraaie
platen waren van de hand van de Leidse lithograaf T. Hooiberg. De eerste delen verschenen bij Luchtmans en de voortzetting bestreek het gehele werkzame leven van E.J. Brill. De afgebeelde plaat is afkomstig uit het derde deel van 1846. UBA =>

<=.pb 52 =>
Boeken in of over het Arabisch werden ook door de Luchtmansen uitgebracht, maar in hun tijd waren dat incidentele uitgaven. Zoals gezegd was in het tweede kwart van de negentiende eeuw een aanvang gemaakt met de systematische publicatie van Arabische manuscripten uit het Legatum Warnerianum. De boeken werden gedrukt door Johannes Brill en uitgegeven door Luchtmans, zodat Evert Jan van twee kanten het Arabisch met de paplepel kreeg toegediend. Het was zijn jeugdliefde en hij hield haar op latere leeftijd in ere. Zijn bevlogenheid verleidde hem in 1854 tot een opmerkelijk pedagogisch experiment, namelijk het uitgeven van een Arabische grammatica voor de gymnasia. De auteur J.J. de Gelder hoopte het Arabisch op te waarderen tot een vast onderdeel van het schoolprogramma, maar slaagde niet in die opzet - helaas, is men geneigd achteraf te denken.

Brills liefde voor het Arabisch werd in de hand gewerkt door de uitstekende arabisten die destijds werkzaam waren aan de Leidse universiteit. De samenwerking met geleerden als R.P.A. Dozy en M.J. de Goeje stelde hem in staat een naam te verwerven voor zijn ‘Oostersche’ uitgaven. Vooral Dozy, gespecialiseerd in de Arabische geschiedenis van Spanje, was een uitermate vruchtbare auteur. Sommige edities van Arabische teksten werden ondernomen door samenwerkende arabisten uit verschillende landen, wat Brills buitenlandse reputatie als uitgever van arabica ten goede kwam. Die internationale samenwerking in langlopende projecten zou ook naderhand de arabistiek kenmerken.

Waren Arabische uitgaven al tijdens het leven van Brill een constante in het fonds, in andere talen van het Onze Vader was hij een voorloper. Zo werd de indologie in Leiden pas in de jaren zeventig een gevestigde discipline, terwijl Brill reeds in 1851 een boek over het Sanskriet publiceerde. In 1856 gaf hij een Maleis heldendicht uit, maar de studie van de Indonesische talen zou pas na zijn dood tot bloei komen. Hij zou het Onze Vader ook in het Chinees of Japans hebben gedrukt, ware het niet dat deze fonts hem destijds ontbraken. In 1858 verwierf de universiteit echter een verzameling druktypen van deze beide talen en in 1864 publiceerde Brill de _Ta Hio_ of _Dai Gaku_ in het Chinees en het Japans. De bewerking van de teksten was van de hand van J. J. Hoffmann, die in 1855 was aangesteld als eerste hoogleraar in de talen van het Verre Oosten. Drie jaar later verscheen in een coproductie met A.W. Sijthoff de _Japansche Spraakleer_, gedrukt met ‘s Rijks Chineesche en Japansche drukletters van dezelfde auteur, waarvan in 

![](02-10.jpg)

<=.ill_02-10.jpg Het Onze Vader in het Koptisch. Pagina uit _Het gebed des Heeren in veertien talen_, uitgegeven door E.J. Brill in 1855. UBA =>

<=.pb 53 =>
![](02-11.jpg)

<=.ill_02-11.jpg Een van de weinige kinderboeken die door E.J. Brill werden uitgegeven: H. Tollens, _Die Holländer auf Nova Zembla_ (1850). Willem Barentz. en Jacob van Heemskerck probeerden in 1596 ‘om de noord’ naar Indië te varen, maar hun schepen bleven steken in het ijs bij het Russische eiland Nova Zembla. De zeelieden bouwden een huis van wrakhout en hielden zich in leven door poolvossen en ijsberen te schieten. Tollens wijdde een vaderlandslievend gedicht aan de ijselijke overwintering, dat in het Duits werd vertaald ‘tot lering van de jeugd’. Eerzame helden probeerden de lange poolnacht door te komen met het zingen van het Wilhelmus en andere liederen over de strijd tegen de Spanjaarden. In het voorjaar van 1597 bouwden de overlevenden sloepen om terug te keren naar de bewoonde wereld. Helaas overleed de dappere Barentsz. een week na het vertrek van Nova Zembla. UBA =>

![](02-12.jpg)

<=.ill_02-12.jpg A. Rutgers, _De Sanskrit-drukletters typografisch gerangschikt en met eene proeve van Sanskrit-tekst voorzien_ (1851). Antonie Rutgers (1805-1884) was de leermeester van de latere hoogleraar indologie Hendrik Kern. Het boek was bedoeld als correctie van een vergelijkbare uitgave van P.P. Roorda van Eysinga, aan wie volgens Rutgers ‘de vereischte kennis der Sanskrit-letters ontbrak’. De drukletters waren indertijd op verzoek van de hoogleraar H.A. Hamakers aangekocht door de Leidse universiteit en werden na diens overlijden ‘onder eenige bepalingen in het belang der Leidsche Akademie’ afgestaan aan Brill. De ordening in de letterkasten was verkeerd, maar met deze handleiding konden de letters toch worden gebruikt om het ingewikkelde Sanskriet te zetten. Het boek is de eerste uitgave van Brill in het Sanskriet. UBA =>

<=.pb 54 =>

![](02-13.jpg)

<=.ill_02-13.jpg Illustratie uit J.J. Hoffmann, _Japanese Grammar_ (1868). Op aandringen van Hoffmann, de eerste Leidse hoogleraar in de talen van het Verre Oosten, kocht het Ministerie van Koloniën in 1858 een verzameling Chinese en Japanse druktypen. De _Japanese Grammar_ (1868) van Hoffmann is een van de eerste Japanse boeken die door E.J. Brill werden gedrukt. De afgebeelde tijdcirkel maakt duidelijk hoe de Japanse klok werkt. Coll. Brill =>

<=.pb 55 =>
1868 door Brill een Engelse editie werd uitgebracht. Ook deze uitgaven waren eenlingen, anticiperend op de latere ontwikkeling van de sinologie en de japanistiek. De egyptologie kende in Leiden een langere traditie, getuige de reeks _Aegyptische Monumenten_ van C. Leemans, die in 1839 begonnen was bij Luchtmans en tot het begin van de twintigste eeuw verder liep bij Brill. Nieuw was echter het drukken van het hiëratisch en hiëroglyfisch schrift, door Brill voor het eerst toegepast in de driedelige _Études égyptologiques_ (1866-69) van W. Pleyte. De auteur had zelf zijn oud-Egyptische druktypen ontworpen en ze vervolgens laten snijden en gieten bij lettergieterij Tetterode in Amsterdam.[^3]

### ‘Nog bij mijn leven’
Na meer dan twintig jaar van noeste arbeid besloot Brill het wat rustiger aan te doen. Hij verkeerde in omstandigheden waarin hij dat kon doen, aangezien zijn bedrijf goed liep en minder bemoeienis vergde. Wellicht was hij ook gedwongen een stap terug te doen vanwege zijn verslechterende gezondheid. Hij organiseerde begin 1871 een verkoping van ongebonden boeken, door hem gekocht op veilingen dan wel door hemzelf uitgegeven. Zoals gebruikelijk kregen de vakbroeders de catalogus toegestuurd en in het voorwoord lichtte Brill zijn motief voor de verkoping toe: bij gebrek aan een opvolger wilde hij ‘nog bij mijn leven’ orde op zaken stellen. De zorgelijke ondertoon van die woorden was onmiskenbaar. Verder deelde hij mee dat ‘mijne uitgaven in Oostersche talen’ buiten de veiling waren gehouden. Brill hield zijn kroonjuwelen voor zichzelf.

De veiling werd op 14 maart 1871 gehouden in gebouw Eensgezindheid op het Spui in Amsterdam. Brill bood 157 titels aan, waarvan ongeveer een derde door hemzelf was gekocht op de Luchtmans-veiling van 1849. Achttien exemplaren van F. Pomey’s _Pantheum Mysticum_ (Amsterdam, 1777), indertijd aangeschaft voor drie gulden per stuk, mochten nu weg voor f 1,25. Het valt te vrezen dat sommige van Luchtmans’ winkeldochters hun bestaan hadden geprolongeerd bij Brill. De koopprijs van J. Bake’s _Over de vertegenwoordiging der wetenschap_ (1846) was in 1849 f 0,60, de richtprijs in 1871 f 0,55; van de 280 exemplaren die Brill had aangeschaft waren nog 96 over. De _Oratio de re herbaria_ van de plantkundige W. H. de Vriese (1845) liep minder goed: Brill had in 1849 achttien stuks gekocht à f 0,30 en wilde nu de resterende vijftien verkopen voor dezelfde prijs.[^4]


De opbrengst van de veiling is niet bekend, maar de opzet is duidelijk: Brill hield opruiming en wilde zijn rompslomp verminderen. Kort nadat hij orde op zaken had gesteld in zijn bedrijf deed hij dat ook in zijn privé-leven: op 4 mei 1871 trad hij in het huwelijk met Cornelia Hermina Dibbets. Het huwelijk zou slechts korte tijd duren. Op 29 november van dat jaar overleed Evert Jan Brill plotseling, zestig jaar oud.

![](02-14.jpg)

<=.ill_02-14.jpg Catalogus van de veiling van 1871. Brill tekende in dit exemplaar zijn instructies aan voor de veilingmeester. Bij sommige titels is de minimumprijs aangegeven, bij de meeste staat ‘verkoopen’, dat wil zeggen verkopen tegen iedere prijs. AB/UBA =>

<=.pb 56 =>
## Mannen van de nieuwe tijd: Adriaan van Oordt en Frans de Stoppelaar, 1872-1896
De boekhandel en boekdrukkerij van wijlen E.J. Brill werden voorlopig op dezelfde voet voortgezet, aldus de executeurs-testamentair Mart. Nijhoff en N.H. de Graaf in het Nieuwsblad voor den Boekhandel van 5 december 1871. Maar hoe moest Brill verder zonder Brill? De overledene had zijn beide broers tot erfgenamen van het bedrijf benoemd: Willem Gerard, de eerder genoemde hoogleraar Nederlandse taal- en letterkunde in Utrecht, en de in Woudrichem woonachtige Hendrik Johannes, van beroep tekenaar en kinderboekenschrijver. De executeurs en erfgenamen kwamen in overleg met de weduwe tot de conclusie dat verkoop de beste oplossing was. Vrijwel gelijktijdig diende zich een koper aan in de persoon van Adriaan Pieter Marie van Oordt (1840-1903), van huis uit theoloog. Na het afronden van zijn studie had hij een reis gemaakt door Amerika, wat in de jaren zestig van de negentiende eeuw nog een avontuurlijke onderneming was. Na zijn terugkeer in het vaderland werkte hij aan een dissertatie, maar helaas bleek een Duitse theoloog kort tevoren op hetzelfde onderwerp te zijn gepromoveerd. Van Oordt zag af van zijn proefschrift en werd evenmin dominee; zijn zwakke gezondheid en mogelijk een zwakke roeping deden hem besluiten een andere weg in te slaan. Na zijn huwelijk ging hij in 1871 in Leiden wonen, met de bedoeling daar een studie rechten te volgen. Aan het einde van zijn eerste semester als rechtenstudent werd het bedrijf van Brill te koop aangeboden. Van Oordt besloot daarop zijn leven over een andere boeg te gooien en zijn kans te wagen in het boekenvak.[^5]

### De boedel van Brill
Met ingang van 15 februari 1872 mocht van Van Oordt zich eigenaar noemen van het bedrijf. Getuige de verkoopakte bezat Evert Jan Brill de panden Rapenburg 68-70 (boekwinkel, woonhuis en uitgeverij) en het daarachter gelegen nummer 74 (de drukkerij met eigen uitgang naar het voormalige Begijnhof ). De drie panden moesten gezamenlijk f 8000 opbrengen. Alle lettersoorten, letterkasten, persen, papiervoorraden en ander toebehoren van de drukkerij waren in de koop inbegrepen, evenals de titels van het uitgeversfonds met bijbehorende rechten. Uitstaande schulden en vorderingen, gerekend naar de stand van zaken op 29 november 1871 - de sterfdag van Brill - , gingen

![](02-15.jpg)

<=.ill_02-15.jpg Akte van de verkoop van het bedrijf aan A.P.M. van Oordt, 21 januari 1872. AB/UBA =>

![](02-16.jpg)

<=.ill_02-16.jpg Adriaan Pieter Marie Van Oordt (1840-1903). KVB/UBA =>

<=.pb 57 =>

![](02-17.jpg)

<=.ill_02-17.jpg Pagina uit W. Pleyte, _Catalogue raisonné de types Égyptiens hiératiques de la fonderie de N. Tetterode_ (1865). De egyptoloog Willem Pleyte (1836-1903) was conservator en later directeur van het Museum van Oudheden in Leiden. Hij ontwierp eigenhandig een hiëratisch schrift, dat in het oude Egypte naast de meer bekende hiërogliefen werd gebruikt voor het beschrijven van papyrus. De druktypen zijn genummerd, wat het mogelijk maakte om ze te zetten zonder kennis van het schrift. De zetter kreeg een reeks nummers en nam aan de hand daarvan tekens uit de letterbak. De druktypen zijn voor het eerst gebruikt voor Pleyte’s _Études égyptologiques_ (1866-69), gedrukt en uitgegeven door E.J. Brill. Naderhand, in 1883, verkocht Pleyte zijn Egyptische typen aan de firma Brill. UBA =>

<=.pb 58 =>
over op de nieuwe eigenaar. Ook het kassaldo van de boekhandel werd aan Van Oordt overgedragen, zijnde welgeteld zevenentwintig gulden en vijfenzeventig cent.

De prijs van alle roerende zaken werd vastgesteld op f 32.000, wat vermeerderd met f 8000 voor het onroerend goed een totale koopsom van f 40.000 opleverde. Het kantoormeubilair - boekenkasten, een bureau, drie tafels, twee lessenaars, enige stoelen, twee kachels met toebehoren en een paar ladders - werd getaxeerd op f 150 en moest om niet te achterhalen redenen afzonderlijk worden betaald. Van Oordt verplichtte zich op 15 februari 1872 een eerste betaling te doen van f 25.000 plus f 150 voor het kantoormeubilair. De resterende f 15.000 kon hij voldoen in vijf jaarlijkse termijnen van f 3000, de eerste te betalen op 15 februari 1873.[^6]

Volgens artikel 7 van het koopcontract stond het Van Oordt vrij het bedrijf voort te zetten onder de naam van Brill, al dan niet in combinatie met zijn eigen naam. De nieuwe eigenaar vond het niet nodig zichzelf te vernoemen en gaf de voorkeur aan de gevestigde naam ‘E. J. Brill’. Overigens stipuleerde hetzelfde artikel dat Van Oordt het recht op de naam van Brill niet mocht afstaan aan anderen, noch het kon nalaten aan zijn erfgenamen. Kennelijk waren latere generaties zich niet langer bewust van deze clausule, die het strikt genomen onmogelijk maakte dat de naam ‘Brill’ kon worden overgedragen op het huidige bedrijf.

### De geest van Brill
Omdat Van Oordt in het boekenvak onbeslagen ten ijs kwam, vroeg hij zijn vriend
Frans de Stoppelaar (1841-1906) hem bij te staan als mededirecteur. Overigens kon De Stoppelaar evenmin bogen op veel ervaring: hij was leraar Nederlands aan de onlangs in het leven geroepen Rijks Hoogere Burgerschool in Deventer en auteur van een veel gebruikt schoolboek.[^7] Hij stortte zich niettemin in het Leidse avontuur van zijn vriend en verbrandde zijn Overijsselse schepen achter zich. Volgens een vennootschapsakte van 17 oktober 1872 was de samenwerking aanvankelijk op proef en zou deze eindigen op 31 december 1873. Wanneer een van beide heren de vennootschap wilde verbreken, moest hij dat zes maanden van tevoren schriftelijk aan zijn compagnon meedelen. Zonder een dergelijke interventie werd de overeenkomst stilzwijgend verlengd, telkens voor de duur van een jaar. Het zou een pact voor het leven blijken.[^8]

Frederik Muller, sinds jaar en dag met de firma verbonden door zijn vriendschap met Bodel Nijenhuis en Brill, maakte zich aanvankelijk zorgen over het gebrek aan ervaring van de nieuwe eigenaren. Hij vreesde dat ze tekort zouden schieten in de

![](02-18.jpg)

<=.ill_02-18.jpg Frans de Stoppelaar (1841-1906). AB/UBA =>

![](02-19.jpg)

<=.ill_02-19.jpg H. Kern, _The Ãryabhat˜ı ya. A manual of astronomy with the commentary Bhatad˜ı pikã of Paramãdı˜ cvara_ (1874). De Indiër Ãryabhata, geboren in 476, schreef in rijm een verhandeling over de wiskunde en de sterrenkunde van zijn tijd. De wiskundige en astronomische kennis stond in India op een verrassend hoog peil: Ãryabhata kwam tot een juiste berekening van het getal π en was zich bewust dat de schijnbare omwenteling van de sterrenhemel werd veroorzaakt door de omwenteling van de aarde om haar as. De editie werd verzorgd door Hendrik Kern, sinds 1865 hoogleraar in het Sanskrit in Leiden. Het boek van Kern (1833-1917) geniet een klassieke status in de indologie en werd in 1906 vertaald in het Hindi. UBA =>

<=.pb 59 =>
hoogstaande kwaliteit die men vanouds was gewend van Brill en van Luchtmans. Hij voelde zich zelfs verplicht beide heren in het _Adresboek van den Nederlandschen Boekhandel_ (1875) te vermanen dat de overname van een gerenommeerd bedrijf verplichtingen met zich meebracht: ‘Noblesse Oblige’.

De zorgen van de oude heer Muller waren misplaatst. Het is opmerkelijk hoezeer Van Oordt en De Stoppelaar het bedrijf voortzetten in de geest van Brill, ondanks het feit dat ze hun voorganger nooit hadden gekend. Wat ze gedurende de eerste jaren tekort kwamen aan ervaring compenseerden ze door hun inzet en intelligentie. Bovendien namen ze een goedlopend bedrijf over, dat door Brill naar zijn hand was gezet en van zijn geest was doortrokken. Zij oogstten wat hij had gezaaid en gingen voort op de weg die hij had ingeslagen. De overname door Van Oordt en De Stoppelaar stond in het teken van de continuïteit, zoals dat ook in 1849 het geval was geweest bij het aantreden van Brill.

De nieuwe eigenaren zetten ook de veilingactiviteiten van Brill voort. Bodel Nijenhuis overleed op 8 januari 1872, nog geen zes weken na Evert Jan Brill. In 1873-74 organiseerden Van Oordt en De Stoppelaar in samenwerking met Fred. Muller een reeks van zeven veilingen waarop Bodels omvangrijke nalatenschap te koop werd aangeboden. Niet alleen zijn bibliotheek, ook de prenten die geen deel uitmaakten van zijn legaat aan de Leidse universiteit werden geveild. De veilingen van de boeken vonden plaats in Bodels woonhuis op het Rapenburg nr.69, sinds 1697 in bezit van de familie Luchtmans. Zelf kochten de veilinghouders bij deze gelegenheid onder meer een statenbijbel die had toebehoord aan de familie Luchtmans. Naar oud-vaderlands

![](02-20.jpg)

<=.ill_02-20.jpg Arabeske versiering op het titelloze omslag van Beoefenaren der Oostersche talen in Nederland en zijne Overzeesche Bezittingen. Het boek was een luxueuze ‘feestgave’ van de firma Brill ter gelegenheid van het derde eeuwfeest van de Leidse Universiteit in 1875. UBA =>

<=.pb 60 =>

![](02-21.jpg)

<=.ill_02-21.jpg Afbeelding uit L. Serrurier, _Encyclopédie Japonaise. Le chapitre des quadrupèdes_ (1875). Serrurier droeg dit werk op aan zijn leermeester J.J. Hoffmann. De kleine encyclopedie was in Japan een populair leesboek voor kinderen en in Leiden een leerboek voor studenten. Het bevatte een facsimile van de Japanse editie, fonetische transcripties van het Japans, een letterlijke woord-voor-woord vertaling en een lopende vertaling. Naast echte dieren worden in het boek fabeldieren opgevoerd zoals de eenhoorn. Is deze in de Europese mythologie een zachtaardig en schrikachtig wezen dat zich alleen door maagden laat aanhalen, in zijn Japanse gedaante is hij een verscheurend monster dat slechteriken aanvalt en rechtvaardigen ontziet. UBA =>

<=.pb 61 =>
gebruik hadden de opeenvolgende generaties daarin op het schutblad het geslachtsregister van de familie bijgehouden. De firma Brill in haar nieuwe gedaante had gevoel voor traditie en koesterde ook de geest van Luchtmans.[^9]

### Arabistiek
De continuïteit van Brill zonder Brill werd in de hand gewerkt doordat verschillende auteurs trouw bleven aan de uitgeverij. Dat gold in de eerste plaats voor de arabisten, die de expertise van de zetters en drukkers van Brill wisten te waarderen. R.P.A. Dozy, inmiddels in zijn nadagen, publiceerde in 1877 een indrukwekkend tweedelig woordenboek (_Supplément aux dictionnaires arabes_). Zijn leerling M.J. de Goeje en diens leerling C. Snouck Hurgronje hielden de traditie van de arabistiek in ere, evenals de traditie
om hun werk te laten verschijnen bij Brill. De relaties tussen de Leidse arabisten en de uitgeverij waren zeer hecht in deze jaren; De Goeje was persoonlijk bevriend met De Stoppelaar en was in een later stadium werkzaam voor het bedrijf. Het eerste deel van zijn reeks edities van Arabische geografische teksten (_Bibliotheca Geographorum Arabicorum_) verscheen in 1870, nog bij het leven van E.J. Brill, het achtste en laatste deel werd uitgebracht in 1894.

Een soortgelijk langlopend project was de door De Goeje geïnitieerde editie van de _Annalen van al-Tabarî_, verschenen in zestien delen tussen 1879 en 1901.[^10] Deze complexe uitgave vereiste de inzet van veertien arabisten uit zes landen, maar evenzeer die van de firma Brill. Zulke monumentale reeksen getuigden ook van de moed van de uitgevers, die een aanzienlijk risico op zich namen. Het stramien van internationale samenwerking in een langlopend, door Brill gecoördineerd project zou rond de eeuwwisseling eveneens ten grondslag liggen aan de befaamde _Encyclopedie van de Islam_.

## Indologie en Oost-Indië
In 1865 was H. Kern aan de Leidse universiteit aangesteld als eerste hoogleraar in het Sanskriet. E.J. Brill zelf beschikte reeds over druktypen van het Sanskriet en had die in 1851 gebruikt voor een uitgave, maar de indologie zou pas na zijn dood tot bloei komen. De publicaties van Kern en zijn Utrechtse collega W. Caland hadden tot gevolg dat ook buitenlandse auteurs hun indologische werken onderbrachten bij de uitgeverij. Van Oordt en De Stoppelaar zagen kans ook dit vakgebied te ontwikkelen tot een specialisme
van Brill.

![](02-22.jpg)

<=.ill_02-22.jpg Deze geschiedenis van de Arabieren in Spanje is het belangrijkste werk van de historicus en arabist Reinhart Pieter Anne Dozy (1820-1883). Dozy promoveerde op zijn vierentwintigste tot doctor in de letteren, werd in 1850 buitengewoon en in 1857 gewoon hoogleraar te Leiden. Dozy was een van die universele geleerden die zich met geschiedenis én taal- en letterkunde bezighielden. Hij publiceerde talloze wetenschappelijke verhandelingen en gaf ook tekstedities uit van Arabische geschiedschrijvers. De vierdelige _Histoire des musulmans_ leest als een roman en geldt nog steeds als een standaardwerk. UBA =>

<=.pb 62 =>

![](02-23.jpg)

<=.ill_02-23.jpg Illustratie uit F.P.L. Pollen en D.C. van Dam, _Faune de Madagascar et de ses dépendances_ (1867-1877). Dit vijfdelige werk (waarvan maar vier delen verschenen omdat het derde nooit werd voltooid) is een typisch negentiende-eeuwse uitgave. Verre streken werden grondig onderzocht, waarbij wetenschappelijke belangstelling hand in hand ging met onderzoek van de economische mogelijkheden. Deze uitgave verscheen losbladig, zodat de kopers zelf het prachtwerk moesten laten inbinden. Het bestaat uit reisverslagen, wetenschappelijke beschrijvingen en een verzameling litho’s. Die zijn opmerkelijk: sommige zijn overduidelijk gemaakt naar foto’s en missen het onbewuste pathos van een kunstenaar. Een deel van de afbeeldingen is handgekleurd, een ander deel is in kleur gedrukt en met de hand bijgewerkt. UBA =>

<=.pb 63 =>
In zijn tijd had E.J. Brill zelf een paar boeken in het Maleis uitgegeven, maar zijn opvolgers bestreken een breed spectrum aan uitgaven over Nederlands Indië. Wederom bevruchtte de tijdgeest de uitgeverij: de openstelling van de Nederlandse koloniën voor het particulier kapitaal ging gepaard met groeiende wetenschappelijke aandacht voor de Gordel van Smaragd. Een nieuwe filologische traditie in Leiden richtte zich op het publiceren van Indonesische manuscripten, literaire teksten en mondelinge overleveringen.

Ook de vraag naar gebruiksboeken in alle mogelijke Indonesische talen nam in het laatste kwart van de negentiende eeuw sterk toe, in samenhang met de toenemende aanwezigheid van Nederlanders in de koloniën. Brill gaf niet alleen woordenboeken uit van het Maleis en het Javaans, maar ook van de minder bekende talen van de archipel - het Madoerees, Rotinees, Tontemboan, Bare’e, Toba-Batak en wat dies meer zij. De land- en volkenkunde van het Nederlandse imperium in de Oost waren rijkelijk vertegenwoordigd in het fonds. De wetenschappelijke belangstelling werd gestimuleerd door het in 1851 opgerichte Koninklijk Instituut voor Taal-, Land- en Volkenkunde, eerst in Delft, naderhand in Den Haag en tenslotte, sinds 1966, in Leiden. Van overheidswege werden verschillende expedities georganiseerd naar de buitengebieden en binnenlanden van Indonesië, waarvan de resultaten in lange reeksen werden gepubliceerd bij Brill. De arabist Snouck Hurgronje verbleef lange tijd in de Oost en leverde met zijn _Atjehers_ (2 dln., 1893-94) een belangrijke bijdrage aan de etnografie.

![](02-24.jpg)

<=.ill_02-24.jpg Een sterauteur van Brill in deze jaren was de arabist en islamkenner Christiaan Snouck Hurgronje (1857-1936). Zijn proefschrift _Het Mekkaanse feest_ werd in 1880 door Brill uitgegeven. In 1884 reisde Snouck naar Saoedi-Arabië en wist als eerste westerling door te dringen in de heilige stad Mekka, waar hij heiligdommen, inwoners en gebruiksvoorwerpen fotografeerde. Ter wille van zijn participerend veldonderzoek noemde hij zich Abd al-Ghaffar en kleedde zich in de lokale dracht, zoals op deze foto te zien is. Het gerucht wilde dat hij nog verder was gegaan in zijn assimilatie en moslim was geworden. De foto’s werden gebruikt voor zijn boek _Bilder aus Mekka_ dat in 1889 verscheen bij Brill. Snouck verbleef naderhand in Nederlands Indië en werd in 1908 benoemd tot hoogleraar Arabisch in Leiden. Snoucks _Mekka in de tweede helft van de negentiende eeuw - Schetsen uit het dagelijks leven_ werd in 2007 opnieuw uitgegeven. UBL =>

<=.pb 64 =>
### Verre Oosten
Ook in de sinologie en japanistiek traden Van Oordt en De Stoppelaar in de voetsporen van hun voorganger. In 1877 werd G. Schlegel aangesteld als hoogleraar in de talen van het Verre Oosten, als opvolger van J.J. Hoffmann. Twee jaar eerder hadden Van Oordt en De Stoppelaar de Chinese en Japanse druktypen gekocht die Brill in de jaren zestig had gebruikt voor zijn eerste uitgaven in deze talen. Zij namen in 1875 deze verzameling over van het Ministerie van Koloniën voor het respectabele bedrag van f 3514,45.[^11] Het was een forse investering voor de beginnende ondernemers, maar het bezit van deze druktypen verschafte de uitgeverij een unieke positie. Schlegel publiceerde vanaf het midden van de jaren zeventig een groot aantal boeken over het Verre Oosten bij Brill, culminerend in een vierdelig Nederlands-Chinees woordenboek (1886-90). Ook steeds meer buitenlandse auteurs wendden zich met hun werk tot Brill, een van de weinige uitgeverijen in Europa met deze expertise. Vanaf 1890 gaf Brill het tijdschrift _T’oung Pao_ uit, dat zich richtte op de studie van de talen en culturen van het Verre Oosten. Het blad met de ondertitel _Revue internationale de sinologie_ is nog steeds het toonaangevende medium van het vakgebied en wordt nog steeds uitgegeven door Brill.

Voor het zetten van Chinees en Japans was een minimale bekendheid met het schrift een vereiste. De eerste zetters in deze talen werden in een opleiding van zes weken door Schlegel vertrouwd gemaakt met de basiskenmerken van de karakters. De meesterzetter J.P. van Duuren, vanwege zijn oosterse specialisatie binnen het bedrijf aangeduid als ‘de mikado’, vertelde in 1927 aan de Rotterdamse journalist M.J. Brusse hoe hij vijftig jaar eerder door de professor was ingewijd in de geheimen van het Chinees. Op zijn beurt had hij weer enige generaties zetters in het Chinees opgeleid. De sinoloog E. Zürcher memoreerde in 1983 hoe hij kort na de Tweede Wereldoorlog bij Brill de meesterzetter P.W. Martijn had leren kennen. Martijn was rond 1912 bij Brill gaan werken als leerjongen en had het Chinese zetwerk in de loop der jaren geleerd van Van Duuren. Zürcher herinnerde zich levendig hoe de zetter aan het werk was: ‘een taoïstische priester die een dansritueel uitvoerde en met ongelooflijke snelheid en accuratesse het juiste karakter uit een verzameling van achtduizend tekens wist te pakken’.[^12]

### De geest van de nieuwe tijd
De firma E.J. Brill maakte in het laatste kwart van de negentiende eeuw een stormachtige groei door. Onder het bewind van Van Oordt en De Stoppelaar verkreeg het bedrijf een aanzien waarin men zonder moeite de grondtrekken van de huidige uitgeverij Brill kan herkennen. De toon was in de jaren vijftig en zestig gezet door Evert Jan Brill, met zijn voorliefde voor vreemde letters; zijn opvolgers waren zo verstandig daarop voort te bouwen en het bedrijf steeds nadrukkelijker te oriënteren op de oriëntalia. Brill profileerde zich rond 1900 als een internationale wetenschappelijke uitgeverij met een specialisatie in exotische talen. Die ‘core-business’ doet soms bijna vergeten dat Brill daarnaast een breed fonds uitgaf voor de binnenlandse markt, met als belangrijkste componenten Nederlandse taal- en letterkunde, geschiedenis en theologie. Ook het aantal tijdschriften dat bij Brill verscheen nam zienderogen toe.

![](02-25.jpg)

<=.ill_02-25.jpg Sinologisch monument: een pagina uit het _Nederlandsch-Chineesch Woordenboek_ van de hoogleraar Gustav Schlegel, dat tussen 1886 en 1891 in vier kloeke delen werd uitgegeven door Brill. UBA

<=.pb 65 =>
![](02-26.jpg)
![](02-27.jpg)

<=.ill_02-26.jpg Van 1883 tot 1985 was Brill gevestigd in het pand Oude Rijn 33a, zoals duidelijk blijkt uit de gevelstenen. Het kolossale gebouw met monumentale ingangspartij was oorspronkelijk onderdeel van het Weeshuis, dat doorloopt naar de Hooglandsekerkgracht. In de jaren zestig van de vorige eeuw was het pand aan de Oude Rijn te klein geworden voor de drukkerij. Deze verhuisde in 1961 naar een nieuw gebouw aan de Plantijnstraat, aan de rand van Leiden. De uitgeverij bleef nog meer dan twintig jaar in het oude bedrijfspand gevestigd, totdat zij in 1985 naar dezelfde locatie vertrok.

De foto met de trekschuit moet zijn gemaakt in 1883, toen Brill verhuisde naar de Oude Rijn. Ruim vijfentwintig jaar later vervaardigde de kunstenaar L.W.R. Wenckebach (1860-1937) naar die foto een tekening voor het Het Nieuws van den Dag. De trekschuit was tegen die tijd al een pittoresk curiosum uit het verleden geworden. Wenckebach, ook bekend als illustrator van de Verkade-albums, tekende een ophaalbruggetje dat op de foto niet is te zien. Het staat in werkelijkheid iets verderop, maar ter wille van de compositie haalde hij het naar voren. Coll. Brill =>

<=.pb 66 =>
De internationale schaal waarop Van Oordt en De Stoppelaar opereerden was voor Evert Jan Brill ondenkbaar geweest. De wereld onderging na 1870 een proces van globalisering dat zijn weerga niet kende in de geschiedenis. Het imperialisme van de Europese mogendheden drong door tot de verste uithoeken der aarde, het kapitalisme beleefde zijn liberale hoogtij en de industriële revolutie voltrok zich in alle hevigheid. Reizen naar het buitenland werd steeds gemakkelijker en ook de internationale wetenschappelijke contacten namen toe. Zo werd om de drie jaar in een Europese stad een Internationaal Congres van Oriëntalisten gehouden, waarop arabisten, sinologen, sanskritisten en andere beoefenaren van occulte wetenschappen elkaar troffen. Frans de Stoppelaar was een vaste bezoeker van deze bijeenkomsten en legde daar vele vruchtbare contacten.

De geest van de nieuwe tijd bood mogelijkheden die Van Oordt en De Stoppelaar
volop wisten te benutten. De internationale reputatie die zij opbouwden kwam tot uitdrukking in de buitenlandse eerbewijzen die hun werden verleend. In 1873 verwierf de firma E.J. Brill op de Wereldtentoonstelling in Wenen een gouden medaille voor haar ‘Oostersche drukken’, die met enig recht kan worden opgevat als een postume bekroning van haar naamgever. Op de Parijse Wereldtentoonstelling van 1878 won het bedrijf nogmaals een gouden medaille. Van Oordt werd in 1883 door de koning van Italië benoemd tot ridder in de Kroonorde vanwege zijn verdiensten als uitgever, hij ontving in 1889 van de sultan van het Ottomaanse rijk het commandeurskruis in de Orde van Medjidié en werd in datzelfde jaar door de Zweedse koning geridderd in de Orde van Wasa. De Stoppelaar liet de buitenlandse eerbewijzen aan zijn vriend, maar werd door koning Willem III benoemd tot Ridder in de Orde van de Nederlandsche Leeuw. Aldus staken de opvolgers van Luchtmans met hun eretitels alsnog Pieter van
der Aa de loef af, die het in het begin van de achttiende eeuw niet verder had gebracht dan ‘Ridder van San Marco’.

### Nieuwe huisvesting
Het bedrijf groeide rond 1880 uit zijn voegen en de panden aan het Rapenburg werden te klein. Een nieuwe behuizing werd gevonden in het voormalige Heilige Geest of Arme Wees- en Kinderhuis aan de Oude Rijn 33a, dat na een ingrijpende verbouwing werd betrokken in het voorjaar van 1883. Alle bedrijfsonderdelen - zetterij, drukkerij, binderij, boekhandel, antiquariaat en uitgeverij - werden in het ruime gebouw gehuisvest. Op de begane grond aan de straatkant bevond zich de expeditie waar materialen en andere inkopen binnenkwamen en van waaruit de eigen boeken werden verzonden naar klanten in binnen- en buitenland. De achterzijde van de benedenverdieping deed dienst als opslagplaats en herbergde in een afgeschermd ketelhuis de stoommachine die de persen in beweging bracht. De verhuizing werd aangegrepen om de drukkerij aan te passen aan de eisen van de tijd.

Uiterst modern voor toenmalige begrippen waren de twee liften die door de stoommachine werden aangedreven en die zware goederen als rollen papier naar de bovenliggende verdiepingen brachten. Op de eerste verdieping was het kantoor van de directeuren te vinden en tevens de zetterij en drukkerij. Het zetten en drukken van Arabische, Chinese, Japanse en andere exotische schriften gebeurde in een afzonderlijke ruimte op de tweede verdieping; het drukken van ‘oostersche’ werken werd tot ver in de twintigste eeuw met de hand gedaan.

![](02-28.jpg)

<=.ill_02-28.jpg Van Oordt en De Stoppelaar huurden het pand aan de Oude Rijn van de regenten van het Heilige Geest of Arme Wees- en Kinderhuis. AB/UBA =>

<=.pb 67 =>
![](02-29.jpg)

<=.ill_02-29.jpg Plattegrond van het weeshuiscomplex aan de Oude Rijn, waarop de plaats van de stoommachine is aangegeven. De gemeente Leiden verleende toestemming voor het plaatsen van het ‘gaskrachtwerktuig’. AB/UBA =>

![](02-29bis.jpg)
![](02-29ter.jpg)

<=.ill_02-29bis.jpg Technische uitgaven als dit _Leerboek der Stoomwerktuigkunde_ (2 dln., 1882) van L.A. Dittlof Tjassens waren bij Brill een uitzondering. De typografische techniek die werd aangewend voor de stoomwerktuigkunde laat niets te wensen over. Coll. Brill =>

<=.pb 68 =>
Op de tweede verdieping bevond zich ook de boekwinkel met een expositieruimte voor nieuwe uitgaven, terwijl daar tevens de boeken opgeslagen waren die met het oog op de verkoop onder handbereik moesten liggen. Voor het antiquariaat moest men naar de derde verdieping, waar ook de minder courante boeken waren opgeslagen.[^13] In het gebouw werd gasverlichting aangelegd, maar de zetters en drukkers waren slecht te spreken over die nieuwerwetsigheid. De gaslampen gaven een onaangenaam licht en produceerden te veel hitte. Op aandrang van het personeel schakelde men weer over op petroleumlampen.[^14]

### Nieuwe statuten
De verhuizing vergde aanzienlijke investeringen en in samenhang daarmee werden de statuten van de onderneming gewijzigd. Tot dusverre was De Stoppelaar mededirecteur en was Van Oordt volledig eigenaar van het bedrijf. Volgens een akte van 23 januari 1881 gaf Van Oordt zijn exclusieve eigendom op en werd De Stoppelaar mede-eigenaar van de firma.[^15] Alle uitgaven en handelingen kwamen voortaan voor gemeenschappelijke rekening en alle activa van het bedrijf werden beschouwd als gemeenschappelijk eigendom. De nieuwe status van De Stoppelaar hing vermoedelijk samen met het feit dat de kapitaalsinjectie voor de verhuizing afkomstig was van zijn schoonfamilie. Hij was getrouwd met Clara Lulofs en zijn zwager Jan Doekes Lulofs, bankier en effectenhandelaar in Amsterdam, trad op als geldschieter.

![](02-30.jpg)

<=.ill_02-30.jpg Met dit kunstzinnige visitekaartje presenteerde de firma Brill zich aan het einde van de negentiende eeuw. Het donkere leesgezelschap op de achtergrond doet rembrandtesk aan, terwijl in het middendeel arabeske motieven zijn aangebracht. De buste van de dame op de voorgrond lijkt meer op de Franse Marianne dan op de Griekse Pallas Athene. Het ontwerp is gemaakt door een zekere W.S. en stamt uit 1885. AB/UBA =>

![](02-31.jpg)

<=.pb 69 =>

![](02-32.jpg)

<=.ill_02-32.jpg _Menu du diner offert au VIIe Congrès International des Orientalistes. Stockholm, le 7 sept. 1889_. Een typografisch hoogstandje van Brill: het menu van het diner dat in 1889 werd aangeboden aan de deelnemers aan het Zevende Internationale Congres van Oriëntalisten in Stockholm. Alle gangen waren voorzien van een commentaar door een geleerde, in de vorm van een pastiche op de geschriften waarin hij specialist was: Arabische geschiedschrijving, Chinese poëzie, Javaanse volkskunst, Babylonische rechtsteksten enzovoort. De bijdragen werden afgedrukt in het bijbehorende schrift - vulgair Egyptisch Arabisch, Chinees, Ethiopisch, Sanskriet, Maleis, Hebreeuws, Manchurees, Javaans, Akkadisch, Turks, Koptisch, Hiërogliefen, Boecharees, Japans, Djagatai en klassiek Arabisch. De laatste taal werd door een abstinerend theoloog gebruikt voor een geleerd commentaar op de champagne. UBA =>

<=.pb 70 =>
In de nieuwe statuten werd ook de taakverdeling tussen beide directeuren vastgelegd. Vanwege zijn zwakke gezondheid zou Van Oordt zich beperken tot de administratie, waaronder het toezicht op de druk- en papierrekening. Wel werd uitdrukkelijk gesteld dat hij het recht behield zich met elk aspect van het bedrijf te bemoeien. De Stoppelaar was verantwoordelijk voor de dagelijkse gang van zaken, met uitzondering van de administratie. De jaarlijkse balans moest door beide firmanten worden goedgekeurd. De Stoppelaar kon aanspraak maken op een preferent winstaandeel van tweeduizend gulden, de rest van de winst werd gelijkelijk verdeeld onder beiden.

### De geleerde van de Gereinigde Lusthof
Waren boeken over de Arabische wereld prominent aanwezig in het fonds van Brill, mensen uit de Arabische wereld zag men in die dagen weinig in Leiden. Een Syrische zetter werkte rond 1885 een jaar lang bij het bedrijf aan een specialistische Arabische uitgave, maar hij was een witte raaf aan de Oude Rijn. Twee jaar eerder had een zekere Amien ibn Hassan Holwani al-Madani zijn opwachting gemaakt bij Brill. Hij was afkomstig uit de heilige stad Medina en noemde zich ‘de geleerde van de Gereinigde Lusthof’, een titel die was ontleend aan de plek waar hij godsdienstig onderwijs placht te geven. Naderhand maakte hij verre reizen door de islamitische wereld met het doel manuscripten te verzamelen. Hij woonde sedert enige tijd in Caïro, toen hem het bericht bereikte dat in 1883 in Amsterdam een Wereldtentoonstelling werd gehouden.

Zo’n mondiale bazar leek Amien ibn Hassan een uitstekende gelegenheid om zijn manuscripten aan de man te brengen, in de verwachting dat deze in Europa meer waard zouden zijn dan in Egypte. Welgemoed begaf hij zich naar Amsterdam en zette zich neer achter een tafel op de Wereldtentoonstelling, maar helaas had geen enkele bezoeker belangstelling voor zijn exotische koopwaar. Een voorbijganger attendeerde hem op de firma Brill in Leiden, die mogelijk wel interesse had voor de geschriften. Aldus vervoegde de geleerde handelaar zich aan de Oude Rijn, waar hij met alle égards werd ontvangen door Van Oordt en De Stoppelaar. Ook in zakelijk opzicht was zijn bezoek een succes, want de uitgevers kochten zijn verzameling van 665 manuscripten. Op hun beurt verkochten zij deze aan de Leidse universiteit, waar ze werden toegevoegd aan het Legatum Warnerianum.[^16]


De Arabische geleerde viel in Leiden met zijn neus in de boter, aangezien het Zesde Internationale Congres van Oriëntalisten in 1883 in de sleutelstad werd gehouden. Amien ibn Hassan werd als onverwachte vertegenwoordiger van de Arabische wereld uitgenodigd het evenement mee te maken. Hij beschreef zijn indrukken van het congres in de Borhân, een krant die verscheen in Caïro. Zijn artikelen werden in het Nederlands vertaald door Snouck Hurgronje en gepubliceerd in een boekje dat door Brill werd uitgegeven.[^17]

De geleerde van de Gereinigde Lusthof werd in de watten gelegd en genoot zichtbaar van de attenties die hem ten deel vielen. Hij verbaasde zich hogelijk over de wetenschappelijke belangstelling die men in het Westen aan de dag legde voor het Oosten; zoveel intellectuele bevlogenheid had hij niet verwacht in het door materialisme gecorrumpeerde Europa. Niet minder dan driehonderd geleerden van allerlei snit - indologen,

![](02-33.jpg)

<=.ill_02-33.jpg Het gebouw van de Wereldtentoonstelling in 1883 op het IJsbaanterrein - het latere Museumplein - in Amsterdam. UBA =>

<=.pb 71 =>
sinologen, assyriologen, egyptologen, arabisten - hadden zich in Leiden verzameld. Amien woonde voornamelijk de bijeenkomsten van de arabisten bij, met wie hij gemakkelijk van gedachten kon wisselen. Hij was de enige Arabier in een gezelschap van zestig Europese arabisten.

Hun kennis van de Arabische letteren maakte grote indruk op hem, zelfs in die mate dat hij het enigszins zorgwekkend vond. Gezien hun wetenschappelijke ijver vreesde hij dat de geleerden van het Westen zich meester zouden maken van de geleerdheid van het Oosten: ‘want als de Franken \[Europeanen\] zich eenmaal op eene wetenschap toeleggen, geven zij die niet weer op, maar duiken in hare zeeën en visschen de parels uit haren bodem’.[^18] De opvatting van de arabistiek als een vorm van intellectueel imperialisme werd een kleine eeuw later naar voren gebracht door Edward W. Saïd in diens bekende boek _Orientalism_ (1978). De meeste arabisten voelen zich door dat verwijt niet aangesproken, des te minder omdat de beroepsgroep tegenwoordig een gevarieerder samenstelling heeft dan in de dagen van Amien ibn Hassan.

### Loodkoliek en kinderarbeid
In 1890 werd een landelijke enquête gehouden naar de arbeidsomstandigheden in fabrieken en werkplaatsen. De ‘Sociale Quaestie’ stond destijds hoog op de agenda, mede dankzij het feit dat de arbeiders zich begonnen te organiseren in vakbonden en het socialisme als politieke beweging in opkomst was. Op 5 augustus 1890 deed de enquêtecommissie onderzoek naar de stand van zaken bij de firma E.J. Brill.

![](02-34.jpg)

<=.ill_02-34.jpg Amien ibn Hassan Holwani al-Madani, de geleerde van de Gereinigde Lusthof. Litho uit C. Snouck Hurgronje, _Het Leidsche Oriëntalistencongres_ (Leiden, 1883). UBA =>

<=.pb 72 =>
Het verslag biedt een inkijk in het dagelijkse reilen en zeilen van het bedrijf.[^19] Vanuit een hedendaags standpunt valt op de omstandigheden bij Brill het nodige aan te merken, maar in vergelijking met andere drukkerijen waren ze zeer behoorlijk.

Pieter de Groot de Bruin, 36 jaar oud en zetter in het Arabisch werd door de commissie ondervraagd over zijn werktijden. In de zomer bleek hij 10,5 uur per dag te werken: van zes uur ‘s ochtends tot zeven uur ’s avonds, met een rusttijd van tweeënhalf uur; in de winter 10 uur per dag van acht uur ‘s ochtends tot acht uur ’s avonds, met een rusttijd van twee uur. Bij een werkweek van zes dagen was dat in de zomer 65 uur per week en in de winter 60 uur. Dat stak gunstig af bij de drukkerij van P.W.M. Trap in Leiden, waar de werkweek in de zomer 70,5 uur bedroeg en in de winter 63,5 uur.[^20] De lonen bij Brill lagen ongeveer even hoog als bij andere grafische bedrijven in Leiden, misschien een fractie hoger. Gespecialiseerde zetters in de ‘Oostersche talen’ als De Groot verdienden f 11 per week, terwijl gewone zetters f 8 tot f 9 kregen, afhankelijk van hun bekwaamheid. De drukkers verdienden evenveel en ook bij hen werden de specialisten in vreemde talen beter betaald. Leerjongens kregen f 1,50 tot f 2 per week.


De commissie ondervroeg De Groot over de hygiënische omstandigheden in het bedrijf. Hij deelde mee dat de zetterij regelmatig werd geveegd en geschrobd, maar dat de letterbakken niet werden schoongehouden. Gezien de ‘ontzaglijke groote lettervoorraad’ bij Brill was het volgens hem ondoenlijk om de letterbakken uit te blazen. Zoals in alle zetterijen hing er dus veel loodstof in de lucht en loodvergiftiging, de gevreesde typografische beroepsziekte, kwam ook bij Brill voor:

> ‘Ik had last van voortdurende maagpijn, waarvoor ik eindelijk geneeskundige
hulp inriep. Tot dusverre had ik mij met huismiddeltjes geholpen, daar ik
de oorzaak der kwaal begreep. Het was loodkoliek, waardoor men traagheid
gevoelt en de maag zekere walgelijke aandoening, bij het ontvangen van spijs,
doet gevoelen’.

De Groot meldde een ander geval van loodvergiftiging en ook tering (tuberculose) kwam volgens hem geregeld voor, al wist hij niet of die ziekte verband hield met het werk. Het ziekteverzuim was hoog: ‘Het is voorgekomen dat wij op de veertig mensen \[in de drukkerij en zetterij\] een negental zieken hadden’. De firma Brill betaalde een ziekengeld van zestig procent van het weekloon, wat in die tijd geen wettelijke verplichting was. Oude werknemers werden tot in lengte van jaren aan het werk gehouden met behoud van hun vroegere loon. Bij gebrek aan een pensioenvoorziening was ook dat een humane praktijk van de werkgevers. De Groot noemde het voorbeeld van een drukker die reeds 67 jaar werkzaam was in het bedrijf - hij was dus in 1823 als leerjongen aangenomen door Johannes Brill. De bejaarde werknemer kon gaan en komen wanneer hij wilde en verrichtte uitsluitend lichte werkzaamheden.

De commissie was met name geïnteresseerd in kinderarbeid. Het kinderwetje van Van Houten, dat arbeid van kinderen beneden de twaalf jaar verbood, dateerde al van 1874.

<=.pb 73 =>

![](02-35.jpg)

<=.ill_02-35.jpg Een hardhandige ingreep van Van Oordt en De Stoppelaar in oktober 1894. Een ‘oproerig bulletin’ van de Algemene Nederlandse Typografenbond was hun in het verkeerde keelgat geschoten. Weliswaar erkenden zij het recht van vereniging van hun werknemers, maar als werkgevers wensten zij zich niet de kaas van het brood te laten eten. De directeuren eisten van hun drukkers en zetters een verklaring dat ze geen lid waren van deze vakbond. Wanneer de arbeiders die verklaring niet gaven, werden ze ontslagen. Allen tekenden. AB/UBA =>

<=.pb 74 =>
Recentelijk gingen stemmen op om de minimumleeftijd te verhogen, in samenhang met invoering van de leerplicht. In december 1889 had de Tweede Kamer een wet aangenomen die verbood om jongens onder de zestien jaar langer te laten werken dan zeven uur ‘s avonds.

Desgevraagd betoonde De Groot zich geen voorstander van zulke maatregelen. Het loon dat de jongens inbrachten was een welkome aanvulling op het gezinsinkomen en het zetten was volgens hem voor een jongen van twaalf of dertien niet een te zware belasting. Ook directeur Van Oordt werd door de commissie ondervraagd over de leerjongens en bleek het geheel eens met zijn werknemer:

> ‘Vraag: Zou verhooging van den minimum-leeftijd door u worden betreurd?
> Antwoord: Voor ons vak, ja; een jongen van twaalf jaar heeft voldoend onderwijs genoten; in ons vak wordt hij voortdurend ontwikkeld. De handigheid van den zetter wordt het best verkregen, als de jongen vroeg begint bij het vak’.

Van Oordt mocht dan weinig op hebben met maatregelen tegen de kinderarbeid, in de context van zijn tijd tekende hij zich af als een werkgever met hart voor zijn mensen:

> ‘Vraag: Zoudt gij wettelijke verplichting tot verzekering van den werkman tegen ongelukken en den ouden dag, grootendeels ten laste van den werkgever, toejuichen of betreuren?
> Antwoord: Het eerste’.

### In het zicht van de twintigste eeuw
In eendrachtige samenwerking hadden de ex-theoloog en de voormalige leraar in vijfentwintig jaar een bloeiend bedrijf opgebouwd. Ze hadden de mogelijkheden aangegrepen die zich aandienden en waren door hun onbevangenheid daartoe des te beter in staat. Ze waren in 1872 begonnen met vijftien werknemers en hadden nu ongeveer zestig mensen in dienst.[^21] In de jaren negentig werden ze echter geconfronteerd met hetzelfde probleem waarvoor Evert Jan Brill zich indertijd zag geplaatst: ze hadden geen opvolger. De zoon van Van Oordt zou aanvankelijk de positie van zijn vader overnemen, maar overleed op jeugdige leeftijd aan tuberculose. De twee zoons van De Stoppelaar volgden een eigen loopbaan en wilden geen van beiden in het bedrijf stappen. De vraag naar de toekomst van de onderneming werd des te dringender doordat Van Oordt zich vanwege zijn slechte gezondheid minder met de dagelijkse gang van zaken wilde bemoeien. De twee directeuren besloten daarop de continuïteit veilig te stellen door de firma E.J. Brill om te zetten in de ‘N.V. Boekhandel en Drukkerij voorheen E.J. Brill’. Onder die noemer begon het bedrijf aan een nieuwe fase in zijn bestaan.

<=.pb 75 =>

![](02-36.jpg)

<=.ill_02-36.jpg In 1878 verwierf de firma E.J. Brill op de Wereldtentoonstelling in Parijs een gouden medaille voor haar ‘Oostersche drukken’. Coll. Brill =>

<=.pb 76 =>

![](03-01.jpg)

<=.ill_03-01.jpg L. Serrurier, _De wajang poerwa: eene ethnologische studie_ (1896). Dit werk van Lindor Serrurier (1846-1901), directeur van het Museum voor Volkenkunde, is even monumentaal als zeldzaam. De prachtige litho’s werden door Brill uitbesteed bij de Leidse drukker P.J. Mulder. De Nederlandse overheid liet tweehonderd exemplaren drukken, die werden geschonken aan buitenlandse musea en aan hoogwaardigheidsbekleders op staatsbezoek. UBA =>

<=.pb 77 =>
# De N.V. Boekhand el en Drukkerij voorheen E.J. Brill: 1896-1945

## De eeuwwisseling, 1896-1906
Bij nader inzien hadden de naamloze vennoten bekende namen. Van Oordt en De
Stoppelaar bleven in de nieuwe bedrijfsvorm aan als directeuren en kregen versterking van een derde man: Cornelis Marinus Pleyte, aangetrokken met de bedoeling hen op termijn op te volgen. De Raad van Commissarissen bestond uit vijf leden, oude bekenden van het bedrijf dan wel familieleden van de directeuren: de hoogleraar Michaël Jan de Goeje, arabist, vooraanstaand auteur van het huis Brill en sinds jaar en dag huisvriend; Albert Cornelis Vreede, hoogleraar in de Indonesische talen en eveneens auteur bij Brill; de egyptoloog Willem Pleyte, inmiddels directeur van het Rijksmuseum van Oudheden, sinds dertig jaar auteur bij Brill en vader van de zojuist aangestelde derde directeur; en tenslotte twee broers van de oude directeuren, te weten Willem Hendrik van Oordt, burgemeester van de gemeente Valkenburg bij Leiden en Jan de Stoppelaar, rentenier in Den Haag.

Het doel van de vennootschap werd omschreven als ‘het voortzetten, en verder exploiteeren der in volle werking zijnde boekdrukkerij, binderij en uitgeverszaak, benevens van den handel in Oostersche en oude boeken en Ethnographica, zooals een en ander tot dusver werd gedreven onder de firma E.J. Brill’. Het aandelenkapitaal bedroeg f 100.000, verdeeld in twintig aandelen op naam. Alle aandelen waren in handen van de directeuren en commissarissen: de twee oude directeuren hadden ieder zes aandelen, de nieuwe directeur had er drie en de vijf commissarissen ieder één. Van Oordt en De Stoppelaar brachten in eigenlijke zin geen kapitaal in, maar ‘de in volle werking zijnde boekdrukkerij en binderij met alle daartoe behorende utensiliën,
de inrichting der zaak met het daartoe behoorend meubilair’.[^1]

De inboedel van het bedrijf werd dus stilzwijgend getaxeerd op f 60.000 en de feitelijke hoeveelheid nieuw kapitaal in de vorm van aandelen bedroeg f 40.000. De N.V. Brill werd genoteerd aan de Amsterdamse Beurs, niet onder de gewone fondsen maar op de incourante markt: de aandelen stonden op naam en waren niet vrij verhandelbaar. De beursnotering was vooral van belang omdat de oprichters van plan waren via de beurs een obligatielening uit te schrijven. Ook de snelle groei van het bedrijf was reden voor de omzetting in een naamloze vennootschap, omdat in deze vorm gemakkelijker kapitaal kon worden aangetrokken voor investeringen. Het beperkte aantal aandelen op naam garandeerde een ‘soevereiniteit in eigen kring’, aangezien houders van obligaties geen bestuurlijke invloed hadden binnen het bedrijf.

<=.pb 78 =>
### Obligaties
Het uitgeversfonds met bijbehorende rechten, de voorraad boeken en de papiervoorraad hadden Van Oordt en De Stoppelaar niet ingebracht in de nieuwe bedrijfsvorm. De naamloze vennootschap was derhalve verplicht deze activa over te nemen van de voormalige firma. Het fonds werd getaxeerd op f 108.239,035, de voorraad boeken op f 12.738,60 en de papiervoorraad op f 4126,95. Op de eerste Algemene Vergadering van 28 maart 1896 werd besloten dat de N.V. ten behoeve van deze drieledige aanschaf een obligatielening van f 150.000 zou uitschrijven. De honderdvijftig obligaties van duizend gulden à 4,5% hadden een looptijd van twintig jaar, ingaande 1 mei 1896, en zouden op basis van uitloting worden afgelost.

De naam van E.J. Brill wekte vertrouwen bij de investeerders en de lening werd moeiteloos volgetekend. Meer dan viervijfde van de opbrengst werd gebruikt om de eigenaren van de voormalige firma uit te kopen en het restant - welgeteld f 24.895,415 - diende als versterking van het bedrijfskapitaal. Samen met de financiële inbreng van de aandeelhouders - acht aandelen met een gezamenlijke waarde van f 40.000 - beschikte het bedrijf over een riante kapitaalsreserve van f 65.000. Een deel daarvan werd gebruikt voor de aanschaf van een nieuwe pers en nieuwe druktypen.

Van Oordt had in 1872 de gehele firma E.J. Brill overgenomen voor f 40.150, terwijl in 1896 alleen het fonds al werd getaxeerd op f 108.239,03. Vermeerderd met de inventaris en de voorraden bedroeg de totale waarde van het bedrijf ten tijde van de omzetting in de N.V. ruim f 185.000. In nog geen vijfentwintig jaar was de waarde van het bedrijf meer dan vierenhalf keer zo groot geworden, terwijl het personeelsbestand in dezelfde periode een vergelijkbare groei had doorgemaakt. Van Oordt en De Stoppelaar hadden goed geboerd.

### Presentiegeld
De directeuren waren belast met de dagelijkse leiding en regelden onderling hun werkzaamheden. De commissarissen, drie tot vijf in getal, moesten tevens aandeelhouder zijn. Zij zagen toe op naleving van de statuten, hielden toezicht op de directeuren en hadden in voorkomende gevallen de bevoegdheid een directeur te schorsen. Benoeming en ontslag van de directeuren berustte echter bij de Algemene Vergadering van Aandeelhouders - aangezien Van Oordt en De Stoppelaar samen drievijfde van de aandelen in handen hadden, hoefden ze zich over dat aspect weinig zorgen te maken. De commissarissen kregen geen salaris, maar uitsluitend een presentiegeld van tien gulden per bijgewoonde vergadering.

Voor deze vergoedingen werd een budget van f 250 per jaar uitgetrokken, zodat de vijf commissarissen maximaal vijf betaalde vergaderingen per jaar konden houden. De statuten verplichtten hen om ten minste twee keer per jaar bijeen te komen. Elk jaar moest een van de commissarissen aftreden, zij het dat hij meteen herkiesbaar was. In de vergadering van de commissarissen hadden de directeuren alleen een adviserende stem, maar in die van de aandeelhouders hadden zij de zwaarste stem in het kapittel. Volgens de statuten moest eenmaal per jaar een Algemene Vergadering van Aandeelhouders worden gehouden, en wel in Leiden op de eerste, tweede of derde donderdag van juni. (Die bepaling zou eind jaren tachtig van de vorige eeuw tijdens een bestuurscrisis enig ongemak veroorzaken, omdat de geldigheid van zo’n

![](03-02.jpg)

<=.ill_03-02.jpg Notulen van de eerste Algemene Vergadering van Aandeelhouders, gehouden op 28 maart 1896 ten huize van De Stoppelaar. AB/UBA =>

<=.pb 79 =>
vergadering werd aangevochten op grond van het feit dat deze had plaatsgevonden in Leiderdorp).

### De spiegel van Brill
In de vergaderingen van commissarissen en aandeelhouders kwamen vrijwel alle aspecten van het bedrijf aan de orde. De notulen van die vergaderingen weerspiegelen de gang van zaken in de verschillende bedrijfsonderdelen en vormen een rijke bron voor de geschiedenis van de N.V. Brill.[^2] Ook over foutieve inschattingen en mislukte projecten valt uit deze bronnen het een en ander te leren. Zo bracht De Stoppelaar in 1896 op de eerste vergadering van commissarissen een ambitieus project voor een geïllustreerde bijbel ter sprake, opgezet door Brill in samenwerking met andere uitgevers. Een keur van binnen- en buitenlandse schilders was aangetrokken voor het vervaardigen van het honderdtal platen waarmee de bijbel zou worden verlucht. De uitgave moest een ongeëvenaard kunstwerk worden waarvan ook Engelse, Franse en Duitse edities zouden verschijnen.

De kosten waren begroot op f 135.000 en Brill nam voor f 10.000 deel in de vennootschap die met het oog op deze publicatie was opgericht. De uitgave lag vooral De Stoppelaar na aan het hart en hij prees de zaak dan ook van harte aan: ‘De goede uitslag is niet twijfelachtig en de voordelen beloven zelfs bij een matig succes aanzienlijk te zijn’. Waarop de commissarissen besloten dat de N.V. het belang van de firma in deze onderneming zou overnemen. Was in 1897 nog sprake van gunstige vooruitzichten, in de jaren daarna werd de toon geleidelijk pessimistischer. In 1902 zag de N.V. zich gedwongen f 1000 af te schrijven op haar investering en een jaar later nog eens f 1500, ‘zonder nog geheel te wanhopen aan het welslagen’. Vanaf 1904 werd jaarlijks f 1000 afgeboekt, ‘in de hoop dat eenmaal de dag zal aanbreken die eene 

![](03-03.jpg)

<=.ill_03-03.jpg Aandelenboek van de N.V. Boekhandel en Drukkerij voorheen E.J. Brill. AB/UBA =>

<=.pb 80 =>

![](03-04.jpg)

<=.ill_03-04.jpg Nogmaals een fraaie afbeelding van wajangpoppen uit L. Serrurier, _De wajang poerwa: eene ethnologische studie_ (1896). UBA =>

<=.pb 81 =>
<!-- ![](03-04.jpg) -->

<=.pb 82 =>
gunstige wending in de zaken der ‘Vennootschap de Geïllustreerde Bijbel’ zal brengen’. Die blijde dag brak nimmer aan en na lang sukkelen ging de prentenbijbel in 1911 definitief ter ziele. Het totale verlies van Brill in de mislukte onderneming bedroeg ruim f 16.000. Als pleister op de wonde ontving men uit het faillissement het schilderij ‘David speelt voor Saul op den harp’, door Jozef Israëls vervaardigd voor de geïllustreerde bijbel. Het werd verkocht voor f 2000 om het verlies te verkleinen.[^3]

Een ander bijbelproject had een gunstiger verloop. Al in de jaren tachtig hadden Van Oordt en De Stoppelaar het plan opgevat om een nieuwe vertaling van de bijbel uit te brengen. Vanaf het midden van de jaren negentig was een groep vertalers onder leiding van professor H. Oort daadwerkelijk bezig de gehele bijbel vanuit de grondtekst opnieuw te vertalen. Deze zogenaamde ‘Leidsche Vertaling’ was bedoeld als alternatief voor de Statenvertaling, in gebruik sinds 1637. Het Oude Testament werd door Brill uitgegeven in 1906 en het Nieuwe in 1913, beide voorzien van inleidingen en annotaties. Het was een langlopend project met een indrukwekkend resultaat, al slaagde de nieuwe
vertaling er niet in de oude te verdringen. In orthodoxe kringen werd het eigentijdse taalgebruik van de Leidsche Vertaling te vrijzinnig bevonden.[^4] Brill was zijn tijd ver vooruit: een nieuwe bijbelvertaling zou pas in de jaren vijftig van de vorige eeuw ingang vinden bij de kerken en werd op haar beurt in 2007 vervangen door een nog nieuwere vertaling.

![](03-05.jpg)
![](03-06.jpg)

<=.ill_03-05.jpg F.E. Blaauw, _A monograph of the cranes_ (1897). Een van de mooiste uitgaven van Brill is dit boek over kraanvogels, waarvan slechts 170 exemplaren werden gedrukt. De afbeeldingen zijn aquarellen die in de dierentuin Artis geschilderd werden naar levende voorbeelden. Artis-directeur Westerman had het boek zelf willen schrijven, maar besteedde het werk uit aan Frans Ernest Blaauw (1860-1935), een amateur-bioloog die op zijn landgoed Gooilust exotische, met uitsterven bedreigde dieren hield. De band is een prachtig voorbeeld van de Jugendstil die door Brill vaker werd gebruikt voor wetenschappelijke uitgaven. UBA =>

<=.pb 83 =>
### Stijfkopje
Dergelijke projecten weerspiegelden de belangstelling van Van Oordt, van huis uit theoloog, en van De Stoppelaar, actief lidmaat van de Doopsgezinde Gemeente. De kerkelijke gezindte van de laatste genereerde een golfje mennonitica in de uitgeverij, terwijl ook de Doopsgezinde Bijdragen jarenlang door Brill werden uitgegeven. Overigens waren boeken op het gebied van theologie en kerkgeschiedenis destijds eerder een incidenteel verschijnsel dan een structureel kenmerk van de uitgeverij. Pas na de Tweede Wereldoorlog ontstonden de reeksen monografieën op het gebied van oud- en nieuwtestamentische studies die doorlopen in het huidige fonds.

Placht de voormalige firma E.J. Brill zich nog academiedrukker te noemen, de naamloze vennootschap liet dat aloude etiket achterwege. De titel was in onbruik geraakt, nadat het theoretische monopolie op academisch drukwerk in de praktijk al eerder was afgeschaft. Dissertaties, oraties en publicaties van Leidse wetenschappers verschenen in deze jaren bij verschillende Leidse uitgevers, onder meer bij Brill. De opbloei van de natuurwetenschappen aan de Leidse universiteit weerspiegelde zich ook in het fonds van Brill, in de vorm van uitgaven van de Nobelprijswinnaars H.A. Lorentz en H. Kamerlingh Onnes.

De ‘Oostersche drukken’ vormden de werkelijke pijlers van het fonds, rustend op de fundamenten die in het laatste kwart van de negentiende eeuw waren gelegd. De zestiendelige reeks van De Goeje’s _Annalen van Al-Tabbari_, begonnen in 1879, werd afgerond in 1901. In de loop van de jaren negentig begon een nieuw internationaal project gestalte aan te nemen: de Encyclopedie van de Islam. De Goeje en De Stoppelaar hadden met een aantal buitenlandse arabisten een comité gevormd om de uitgave van de grond te krijgen, maar tussen droom en werkelijkheid stonden praktische bezwaren in de weg. Nadat in 1899 de Utrechtse hoogleraar M.Th. Houtsma de taak op zich had genomen om de internationale samenwerking te coördineren, kwam eindelijk enig schot in de zaak.[^5] Niettemin duurde het nog tot 1908 voordat de eerste aflevering met de letter ‘A’ werd uitgebracht. Het eerste gebonden deel van de encyclopedie verscheen in 1913 en het zou uiteindelijk duren tot 1936 voordat de vierdelige eerste editie werd voltooid.


Brill had rond 1900 een solide internationale reputatie opgebouwd in wetenschappelijke uitgaven op het gebied van arabistiek, assyriologie, indologie, sinologie en aanpalende vakgebieden. Steeds meer buitenlandse auteurs en instellingen lieten hun publicaties verzorgen door de uitgeverij. Etnografie en biologie waren vanouds belangrijke bestanddelen van het fonds en werden rond de eeuwwisseling steeds belangrijker. De resultaten van een serie wetenschappelijke expedities naar Nieuw-Guinea (1903-1920) werden in zestien delen door Brill gepubliceerd. De fameuze Siboga-expeditie onder leiding van Max Weber verkende in 1899-1900 de verste uithoeken van de Indonesische archipel, deed talloze vondsten en genereerde een reeks van meer dan honderdveertig afleveringen bij Brill. Ook buitenlandse expedities vonden hun wetenschappelijke neerslag bij Brill: zo werd in 1903 een contract gesloten met het Museum of Natural History in New York om de bevindingen van de Jessup-expeditie naar Alaska en het noordoosten van Siberië te publiceren.[^6]

![](03-07.jpg)

<=.ill_03-07.jpg Afbeelding uit W.F.R. Suringar, _Musée botanique de Leide_ (1897). Willem Frederik Reinier Suringar (1832-1898) was een belangrijk botanicus, directeur van de Hortus en het Rijksherbarium in Leiden. In de jaren 1884-85 ondernam hij met zijn zoon Jan een reis naar West-Indië. Hij was vooral geïnteresseerd in planten van het geslacht melocactus, waaraan de _Musée Botanique_ een speciaal deel wijdde. Hij liet in de Hortus een kas bouwen voor deze cactussen, die hem regelmatig van overzee werden toegezonden. Suringar onderscheidde zijn cactussen in verschillende soorten, maar naderhand bleken ze toch tot één en dezelfde soort te behoren. Het losbladige werk bevat tekeningen en foto’s van cactussen. Foto’s gaven de beste weergave van de realiteit, maar sommige details kwamen beter tot hun recht in een tekening. UBA =>

<=.pb 84 =>

![](03-08.jpg)

<=.ill_03-08.jpg C.T.J. Louis Rieber, _Het Koninklijk Paleis te Amsterdam_ (1900, herdruk 1940). Prachtwerk op groot-folio formaat (46 bij 66 cm.) over het voormalige stadhuis van Amsterdam. Om de prospectus van Brill te citeren: ‘Het groote plaatwerk van Rieber geeft op voortreffelijke, het gebouw alleszins waardige wijze, een indrukwekkend beeld van al de pracht welke men hier aantreft’. Coll. Brill =>

<=.pb 85 =>
Werden in dit uitgavenpatroon de lijnen vanuit het verleden doorgetrokken, rond de eeuwwisseling experimenteerde Brill ook op onverwachte terreinen. Zo verscheen in de loop van de jaren negentig bij de gerenommeerde wetenschappelijke uitgeverij de Stijfkopje-trilogie van de Duitse schrijfster Emmy von Rhoden - een mijlpaal in de ontwikkeling van het verantwoorde meisjesboek, destijds aangeduid als ‘het bakvischboek’. Een andere klassieker was de eerste Nederlandse uitgave van Lewis Carolls _Alice in Wonderland_ (1901), met illustraties uit het Engelse origineel. Kinderboeken zouden zich binnen het fonds niet ontwikkelen tot een volwassen corpus van uitgaven, ze bleven zogezegd steken in de kinderschoenen.

Hetzelfde gold voor de romans waaraan Brill zich bij tijd en wijle waagde, zonder de uitgesproken ambitie zich te profileren als literair uitgever. Rond de eeuwwisseling experimenteerde de uitgeverij ook met een reeks publicaties op technisch gebied, met verrassende uitgaven als J.A. Jongmans, _Bouw en inrichting van ijzeren en stalen schepen, bewerkt voor aspirant-stuurlieden_ (1905; tweede druk 1919) en J.A. van der Kloes, _Handleiding voor den metselaar, tevens bevattende eenige aanwijzingen voor den stukadoor_ (1908).

![](03-09.jpg)

<=.ill_03-09.jpg De leden van de Siboga-expeditie op het dek van de gelijknamige kanonneerboot. Zittend in het midden de bioloog Max Wilhelm Carl Weber (1852-1937), de leider van de expeditie, en zijn echtgenote Annie van den Bossche. De expeditie van 1899-1900 verkende de uithoeken van de Indische archipel en deed een schat aan kennis op: zo werden maar liefst 131 nieuwe vissoorten ontdekt. Pas in de jaren zeventig van de vorige eeuw voltooide Brill de reeks met onderzoeksresultaten, bestaande uit 142 delen. UBA/Artis =>

<=.pb 86 =>
Ongewild onderstreepten zulke zijsprongen naar andere terreinen het eigenlijke karakter van Brill: een wetenschappelijke en internationaal opererende uitgeverij met een specialisatie in de buitengebieden van de humaniora.

### Goede tijden
De naamloze vennootschap floreerde gedurende de eerste tien jaar van haar bestaan en trad welgemoed de twintigste eeuw binnen. De omzet nam gestaag toe en het bedrijf maakte jaar op jaar winst. Een strop van f 16.000 als van de Geïllustreerde Bijbel kon zonder moeite worden opgevangen. De stoommachine van 1883 was na twintig jaar aan vervanging toe; de aanschaf van een nieuwe ‘motor’ van welgeteld zes paardenkrachten leverde geen enkel probleem op, evenmin als een nieuwe drukpers van f 6000. Met het oog op toekomstige investeringen en de aanstaande aflossing van de obligatielening werd een behoorlijke kapitaalsreserve opgebouwd, die zo verstandig mogelijk werd belegd. Na aftrek van reserveringen en afschrijvingen werd de nettowinst gedurende de eerste tien jaren steeds bepaald op een bedrag van f 8000 à f 9000, overeenkomend met een bescheiden dividend van 6-6,5% per aandeel. De commissarissen konden keer op keer met een gerust hart de balans goedkeuren, want de zaak liep op rolletjes. In overeenstemming met de statuten fiatteerden ze de cijfers zelfs twee keer, eerst als commissarissen en vervolgens, op de Algemene Vergadering, nog eens als aandeelhouders. Ter wille van het gemak werd de vergadering van de aandeelhouders vaak gecombineerd met een vergadering van de commissarissen, die eerder op dezelfde dag werd gehouden.

Drukkerij en zetterij draaiden geheel op het werk dat werd aangeleverd door de uitgeverij - een optimale combinatie. Deze drie-eenheid van synergetische onderdelen vormde het hart van het bedrijf. De veilingactiviteiten en het grootste deel van het

![](03-10.jpg)

<=.ill_03-10.jpg G.A.F. Molengraaff, _Geologische verkenningstochten in Centraal-Borneo_ (1900). De Borneo-expeditie van de geoloog Molengraaf (1860-1942) naar de onbekende binnenlanden van dat eiland duurde bijna een jaar. Het omvangrijke reisverslag ging niet alleen over gesteenten, maar ook over zeden en gewoonten van de bevolking. Observaties over verkiezeld hout en basalt worden afgewisseld met opmerkingen over de bouwwijze van de Dajaks. De beschrijvingen van het kamperen in hevige slagregens zijn prachtig. De vechtende honden van de Dajaks verstoorden de nachtrust van de ontdekkingsreiziger: hij werd uit zijn slaap gehaald door een ‘infernaal gegil en gehuil, dat ik niet met iets kon vergelijken, dat ik ooit had gehoord’. De band is een fraai Jugendstil-ontwerp van J.G. Veldheer. UBA =>

<=.pb 87 =>
![](03-11.jpg)

<=.ill_03-11.jpg Soms ging de degelijke uitgeverij Brill zich te buiten aan frivole boekomslagen. Rond 1900 werkten veel Nederlandse sierkunstenaars in de stijl van de ‘Nieuwe Kunst’, de Nederlandse variant van de Art Nouveau of Jugendstil. Bekende kunstenaars als R.W.P. de Vries (1874-1952) en L.W.R. Wenckebach (1860-1937) ontwierpen omslagen voor Brill. De afgebeelde band is gemaakt door de onbekende H.H., die ‘Brill’ spelde als ‘Bril’. De vormentaal is meer geënt op de Franse Art Nouveau dan op de Nieuwe Kunst. De reeks _Romans in proza_, waarvan het eerste deel verscheen in 1897, werd verzorgd door de Leidse hoogleraar Jan ten Brink. Coll. Brill =>

![](03-12.jpg)

<=.ill_03-12.jpg Natalie Bruck-Auffenberg, _De vrouw ‘comme il faut’_ (1897). Ook populair-wetenschappelijke werken werden soms door Brill uitgegeven, zoals deze bewerking van een Oostenrijks boek. De feministische schrijfster houdt een pleidooi voor de emancipatie van de vrouw, die vooral moet gaan studeren. Vrouwen zijn daarvoor volgens haar geschikter dan mannen - een profetische uitspraak, gezien de huidige studentenpopulatie. Een breed scala aan vrouwelijke activiteiten komt aan de orde, van het controleren van het libido tot het correcte presenteren van thee ‘waarbij men tenminste de rechterhandschoen uittrekt’. UBA =>

<=.pb 88 =>
antiquariaat werden ten tijde van de omzetting in een naamloze vennootschap afgestoten. Twee voormalige medewerkers van Brill, Burgersdijk en Niermans, zetten de veilingactiviteiten voort onder hun eigen naam en richtten een antiquariaat op met de voorraad van 250.000 boeken die ze van Brill overnamen. Op den duur werd de firma Burgersdijk en Niermans in Leiden het antiquariaat en veilinghuis bij uitstek.[^7] De oriëntalia van Brill maakten geen deel uit van deze transactie, maar werden verkocht via het eigen ‘Oostersche’ antiquariaat. Dit minst succesvolle bedrijfsonderdeel wekt de indruk van een permanent zorgenkindje. Opmerkingen dienaangaande in de vergadering van commissarissen kwamen gewoonlijk neer op verzuchtingen over de kwakkelende toestand dan wel aansporingen om de zaak nieuw leven in te blazen. In 1910 was zelfs sprake van opheffing van het antiquariaat, maar bij nader inzien vond men dat te ver gaan.

### De zoon van de egyptoloog
De nieuwe directeur Cornelis Marinus Pleyte (1863-1917) werd geacht zich in te werken in alle aspecten van het bedrijf, terwijl hij daarnaast de etnografische component van het fonds moest versterken. Pleyte had enige tijd als volontair gewerkt bij het in 1883 geopende Ethnographische Museum in Leiden en kreeg in 1887 een baan in Amsterdam als conservator van de volkenkundige verzameling van Artis. Hij had zijn positie bij Brill te danken aan de invloed en financiële inbreng van zijn vader Willem Pleyte, egyptoloog, directeur van het Museum van Oudheden en commissaris van de vennootschap.

![](03-12bis.jpg)

<=.ill_03-12bis.jpg Het fraai vormgegeven Koningin Wilhelmina-album (1898) was geredigeerd door de Leidse dichter Fiore della Neve (pseudoniem van mr. M.G.L. van Lochem). Het geschenk aan de jonge koningin bevatte gedichten en korte verhalen van beroemde schrijvers uit die tijd, nu alleen nog bekend onder vakspecialisten: Helène Swarth, Melati van Java en J. Reddingius. Cyriel Buysse schreef een mooi verhaal over de hopeloze liefde van een scheepsarts voor een oppervlakkige Amerikaanse schoonheid. Een prachtuitgave met een sierlijke, door Wenckebach ontworpen band. De jonge vorstin werd door Van Lochem vergeleken met de nieuwe lente, maar verder viel de aanbidding van Wilhelmina wel mee. UBA =>

<=.pb 89 =>
Het valt te vrezen dat Pleyte jr. wat te ongedurig was voor de bedrijfscultuur van Brill. Tot ergernis van Van Oordt en De Stoppelaar bleek hun gedoodverfde opvolger niet in het minst geïnteresseerd in de drukkerij en uitgeverij. Alleen de volkenkunde had zijn hart, hoewel hij ook op dat gebied niet het hoofd aan de dag legde dat men van een uitgever mocht verwachten. Pleyte greep elke gelegenheid aan voor een reisje naar het buitenland en verkeerde geregeld in België, Duitsland of Engeland. In 1898 werd hij door het Ministerie van Koloniën uitgenodigd om in Indonesië een verzameling volkenkundige objecten bijeen te brengen, bedoeld voor het Nederlandse paviljoen op de Parijse Wereldtentoonstelling van 1900. Van Oordt en De Stoppelaar legden zich tegensputterend neer bij zijn vertrek naar de Oost, waar hij volgens de hoopvolle inschatting van de commissarissen vruchtbare relaties voor het bedrijf kon aanknopen. De expeditie zou een half jaar duren, maar Pleyte had het naar zijn zin in de koloniën en bleef veel langer weg. In de woorden van de schrijver van zijn ‘Levensbericht’: ‘Die reis heeft hem veel doen genieten en veel geleerd. Zulk een zwervend leven met het voorttrekken van de eene plaats naar de andere trok hem bijzonder aan’.[^8]

Toen hij eind 1899 eindelijk weer in Leiden opdook, was Brill hem vreemd geworden. Dat gevoelen was wederzijds, al verliep het verbreken van de banden tamelijk onverkwikkelijk: nadat Pleyte ontslag had gevraagd om redenen van gezondheid, probeerde hij met voorbijgaan van de vennoten zijn aandelen te verkopen aan derden. Daarop vielen boze woorden in de vergadering van commissarissen, waar de oude heer Pleyte deze keer om tactische redenen verstek liet gaan. Van Oordt en De Stoppelaar waren geschokt door de trouweloosheid van Pleyte jr. Ze namen de gelegenheid te baat om zijn tekortkomingen op te sommen en voor te rekenen hoeveel schade hij het bedrijf had berokkend. Pleyte zag af van een loopbaan als uitgever - hij keerde terug naar de Oost, met achterlating van vrouw en kinderen, en wijdde zich de rest van zijn levensdagen aan zijn volkenkundige liefhebberijen.[^9]

![](03-13.jpg)
![](03-13bis.jpg)

<=.ill_03-13.jpg Afschriften van de uitgaande post werden bijgehouden in zogenaamde kopieboeken. De afbeelding laat een brief zien van onderdirecteur Peltenburg aan de arabist Snouck Hurgronje, die op dat moment in Batavia verbleef. Coll. Brill =>

<=.pb 90 =>
Kennelijk hadden Van Oordt en De Stoppelaar al eerder twijfels over de geschiktheid van hun beoogde opvolger, want in het voorjaar van 1898 droegen zij Cornelis Peltenburg voor als onderdirecteur. Peltenburg, sinds 1880 werkzaam bij Brill, had onlangs een interessant aanbod gekregen voor een baan elders. Van Oordt en De Stoppelaar wilden hem aan het bedrijf binden en stelden de commissarissen cq. aandeelhouders voor hem te benoemen tot onderdirecteur. Peltenburg zou op termijn worden aangesteld als volwaardig directeur, ‘zoo de omstandigheden ertoe leiden, hetgeen volgens den loop der dingen vroeger of later gebeuren moet, dat een der tegenwoordige directeuren moet worden vervangen’.[^10] Van zijn kant moest Peltenburg deze wissel op de toekomst bekrachtigen met een gelofte van trouw aan Brill. Eerder dan verwacht deed zich de gelegenheid voor dat een der directeuren moest worden vervangen: na Pleyte’s vertrek in 1900 werd Peltenburg benoemd tot directeur.[^11]

Van Oordt en De Stoppelaar hadden er goed aan gedaan hun opvolging te regelen, want andere omstandigheden leidden ertoe dat ook zij moesten worden vervangen. Adriaan van Oordt overleed op de laatste dag van het jaar 1903, Frans de Stoppelaar op 8 juni 1906. Beiden werden begraven op het kerkhof aan de Groenesteeg in Leiden. Zij hadden hun gehele leven gezamenlijk opgetrokken en zetten hun samenzijn na hun dood voort.

![](03-14.jpg)

<=.ill_03-14.jpg M.F. Onnen, _De draadloze telegrafie en hare toepassing_ (1906). In deze door Brill uitgegeven brochure wordt de regering opgeroepen in Indië een draadloos telegrafiesysteem aan te leggen. In de uitgestrekte Indonesische archipel was zo’n voorziening natuurlijk van groot belang. Onnes legt in kort bestek uit hoe draadloze telegrafie werkt, waarom een dergelijk systeem in handen moet zijn van de overheid en geeft tenslotte een gedetailleerd overzicht van de kosten. In 1911 begon de Indische PTT met de aanleg, een proces dat door het uitbreken van de Eerste Wereldoorlog werd versneld. Ook werd een draadloze verbinding met Nederland tot stand gebracht. UBA =>

<=.pb 91 =>
## De vaste hand van Cornelis Peltenburg, 1906-1934

### De generaal van Brill
Cornelis of Corneille Peltenburg werd in 1853 geboren in Oegstgeest (als francofiel en lidmaat van de Waalse kerk had hij een voorkeur voor de Franse variant van zijn naam). In zijn jeugd koesterde hij de droom om beroepsmilitair te worden, maar zijn vader verzette zich tegen een loopbaan in het leger.[^12] In plaats daarvan kreeg Cornelis een grondige praktijkopleiding in het boekenvak: op jonge leeftijd trad hij in dienst bij boekhandel Hazenberg in Leiden, werkte vervolgens bij boekhandels in Den Haag en Amsterdam, en keerde toen terug naar Hazenberg. In 1880 haalde De Stoppelaar hem naar Brill, waar hij zich van vertegenwoordiger opwerkte tot chef de bureau en mededirecteur. Na de dood van De Stoppelaar was hij de enige directeur en tot zijn eigen dood in 1934 zou hij dat blijven.

Aanvankelijk was zijn alleenheerschappij geenszins vanzelfsprekend. De commissarissen waren van oordeel dat twee directeuren een betere garantie vormden voor de continuïteit en droegen hem daarom op zorg te dragen voor een plaatsvervanger. Ze gebruikten zelfs het argument dat een tweede directeur een statutaire verplichting was. In de loop der jaren trok Peltenburg verschillende jongelieden aan als ‘directeur in opleiding’, maar op een of andere wijze verdwenen zulke aspiranten na korte tijd weer in het niets. Peltenburg kon geen tweede man naast zich verdragen en vatte het aandringen van de commissarissen op als een motie van wantrouwen jegens hem. Waarop dezen zich verplicht voelden de gepikeerde directeur te verzekeren dat ze het volste vertrouwen in hem hadden, en de plaatsvervanger geleidelijk van de agenda werd afgevoerd.[^13]

Peltenburg was inderdaad zeer wel in staat de onderneming op eigen kracht te leiden. Omzet, winst, kapitaalsreserve en dividend stegen in de periode voorafgaande aan de Eerste Wereldoorlog jaar op jaar. In de drukkerij en zetterij werkten inmiddels meer dan zestig mensen en het totale aantal werknemers lag rond de tachtig. Het jaar 1911 was voor de drukkerij en uitgeverij ‘zeer gunstig’ geweest, meldde de directeur desgevraagd aan de Leidse Kamer van Koophandel: ‘De drukkerij had voortdurend overvloed van werk, voornamelijk door het zich meer en meer uitbreiden der buitenlandsche relatiën, waartoe de uitgebreidheid van den voorraad van verschillende oostersche lettertypen aanleiding geeft’.[^14]

Samen met twee andere Leidse bedrijven, A.W. Sijthoff en de Nederlandsche Rotographure Maatschappij, presenteerde de N.V. Brill zich in 1913 op de Internationale Graphische Tentoonstelling in het Amsterdamse Paleis voor Volksvlijt. Peltenburg had zelfs zitting in de ‘Algemeene Commissie’ van de tentoonstelling. Vol trots kon hij na afloop aan de commissarisssen meedelen dat Brills inzending van ‘oostersch drukwerk’ de hoogste onderscheiding had behaald, te weten het erediploma.[^15]

De zaak marcheerde, om een uitdrukking te gebruiken die in dit verband min of meer letterlijk moet worden genomen. Peltenburg had aan zijn gefnuikte jongensdroom een militaire manier van doen overgehouden en cultiveerde een bijpassend uiterlijk: kaarsrechte houding, opgestreken knevel, doordringende blik en een stemgeluid met rollende r’s. Hij had de allure van een officier in de civiele hoedanigheid van een uitgever, terwijl de uitgever in zijn vrije tijd optrad als commandant van de

![](03-15.jpg)

<=.ill_03-15.jpg Peltenburg liet rond 1909 dit sierlijke ‘druppelvignet’ ontwerpen door de kunstenaar R.W.P. de Vries (1874-1952). Het oude Luchtmans-motto ‘Tuta sub aegide Pallas’ werd in ere hersteld in het nieuwe drukkersmerk van Brill. De compositie van Pallas Athene en Hermes is ontleend aan het allegorische schilderij van de familie Luchtmans, dat zich bevond in het bedrijfspand van Brill (zie afbeelding bij hoofdstuk I). Het godenpaar in het huidige logo van Brill heeft dezelfde origine. Het druppelvignet in de stijl van de Art Nouveau was in gebruik tot het midden van de jaren dertig. =>

<=.pb 92 =>
Burgerwacht van Oegstgeest. Ook zijn strikte erecode was van militaire snit, hij zette zich volledig in voor het bedrijf en eiste van zijn ondergeschikten dezelfde toewijding. Peltenburg drilde Brill.

### Dreigende staking
Op zijn patriarchale wijze had Peltenburg het beste voor met zijn ‘werklieden’, maar het paste niet in zijn autoritaire visie dat ze zich organiseerden in vakbonden en eisen stelden. In 1913 was hij gedwongen de eerste collectieve arbeidsovereenkomst tussen werkgevers en werknemers in de grafische sector te aanvaarden, wat voor hem een traumatische ervaring was. Op grond van dat akkoord werd de werkweek vastgesteld op 57 uur, terwijl bij Brill destijds zestig uur in de week werd gewerkt. De arbeidsovereenkomst bracht daarnaast een loonsverhoging met zich mee: het overeengekomen weekloon voor een volwassen gezel zou voortaan f 13,68 bedragen, terwijl de lonen bij Brill nog op hetzelfde peil lagen als in 1890: f 9,50 voor ‘gewone’ drukkers en zetters en f 12 voor mensen met een kwalificatie in het Chinees of Arabisch.

De Wet op de Arbeidsovereenkomst (1907) had de mogelijkheid voor zulke contracten geschapen, hoewel ze niet dwingend konden worden opgelegd aan werkgevers. De meeste grafische bedrijven in Leiden hadden in de loop van 1913 de cao aanvaard, maar A.W. Sijthoff en E.J. Brill lagen dwars en weigerden haar in te voeren. Toen de arbeiders van Sijthoff in staking gingen en die van Brill dreigden mee te gaan, ging Peltenburg ten langen leste overstag. Zijn grootste grief was niet de stijging van de loonkosten, maar het feit dat hij als directeur niet langer zelfstandig kon bepalen wat zijn personeel verdiende.

![](03-16.jpg)

<=.ill_03-16.jpg J.O. van der Kellen Dzn., _Nederlandsche zeeschepen van ongeveer 1470 tot 1830_ (1913). Het doel van deze losbladige platenatlas wordt in de inleiding duidelijk: ‘de firma Brill verdient lof deze onderneming... waardoor zij medewerkt aan het hooghouden van onze nationale eer, aangedurfd te hebben’. De reproducties, lichtdrukken van de Haarlemse firma Emrik en Binger, zijn van zeer goede kwaliteit. De geplande Duitse en Engelse edities, die de nationale eer buiten de grenzen hoog moesten houden, zijn nooit verschenen. UBA =>

<=.pb 93 =>
Hij vatte de cao op als een gebrek aan loyaliteit van de werknemers, die kennelijk meer vertrouwen stelden in hun vakbonden dan in hem. Hij ervoer de gang van zaken als een persoonlijke nederlaag en wenste zijn ondergeschikten duidelijk te maken dat hij niet met zich liet sollen. De cao trad in werking per 1 januari 1914 en zeven weken later deelde Peltenburg de werknemers mee dat vijf van hen ontslagen werden. Het ontslag werd in verband gebracht met de stijging van de loonkosten en de slapte in het bedrijf. Dat laatste was niet waar: het jaar 1914 vormde de bekroning van de gestage groei die het bedrijf sinds 1896 doormaakte. De achterliggende reden van het ontslag was veeleer dat de directeur zijn gezag wilde herstellen.[^16]

### De Eerste Wereldoorlog
Weliswaar ging de oorlog aan Nederland voorbij, maar bij Brill waren de gevolgen duidelijk te merken. Doordat vier jaar lang de verbindingen met het buitenland waren verbroken, slonk de orderportefeuille en nam de omzet sterk af. Reeds op 10 augustus 1914, vlak na het uitbreken van de oorlog, werd de arbeidstijd op de drukkerij en zetterij verlaagd tot 45 en later zelfs tot 42 uur per week. Met ingang van september 1915 werd de normale werkweek van 57 uur weer ingevoerd, maar tot 1918 bleef geregeld sprake van arbeidstijdverkorting. Deels werd de vermindering van werk gecompenseerd door de mobilisatie, die een aantal werknemers onder de wapenen riep ter bewaking van de grenzen. Bij Brill werkten echter relatief veel oudere werknemers: eind 1915 hadden 25 personeelsleden een staat van dienst van 25 jaar of langer.[^17]

Dankzij de financiële reserves die in goede jaren waren opgebouwd had de vennootschap een stevige buffer, maar in de bedrijfshuishouding ging nu veel minder geld om. De balans van 1913 vermeldde liquide middelen ten bedrage van f 115.000 en die van 1914, net voor het begin van de oorlog, zelfs van f 122.500. Ongeveer de helft van dat kastegoed was belegd, terwijl daarnaast in de loop der jaren een aanzienlijke hoeveelheid kapitaal via het zogenaamde ‘Reservefonds’ was ondergebracht in diverse langlopende beleggingen. De balans van 1917 kwam niet verder dan een bedrag van ruim f 60.000: ongeveer de helft van het geld dat vóór de oorlog in het bedrijf omging.

De vennootschap zette in de magere jaren de tering naar de nering, in de hoop de kapitaalsreserves te kunnen ontzien. Dat streven slaagde wonderwel - men zag in de oorlogsjaren zelfs kans f 23.000 af te lossen op de obligatielening van 1896, zonder uit het Reservefonds te putten. Het bedrijf maakte zowaar nog winst, zij het beduidend minder dan in de vooroorlogse jaren. In 1917 schafte Brill als een van de laatste Leidse drukkerijen een elektromotor aan, ter vervanging van de stoommachine. Voor de aanschafkosten van f 3000 moesten een paar obligaties te gelde worden gemaakt, maar deze waren afkomstig uit de belegde liquide middelen, niet uit het Reservefonds. Dezelfde financiering werd toegepast voor het pand Rapenburg 22, dat in 1917 werd aangekocht voor f 17.500. Het was de bedoeling het oosterse antiquariaat daar onder te brengen, in de hoop dat het op die locatie meer rendabel zou zijn. De verhuizing van het antiquariaat kwam nooit verder dan de bedoeling, maar als geldbelegging behield het pand zijn waarde.

<=.pb 94 =>
### De eerste vrouw
Het bedrijf was in wezen gezond, maar had tijdens de oorlogsjaren te kampen met
sterk gedaalde inkomsten en sterk gestegen kosten. Tussen 1900 en 1914 bracht Brill 450 titels uit, terwijl tussen 1914 en 1918 niet meer dan zeventig verschenen. Een groot deel van de oorlogsproductie was het uitvloeisel van vroeger werk, zoals de doorgaande publicaties over de Siboga-expeditie en nieuwe delen in enige langlopende reeksen over Indonesië. Een opsteker was de _Encyclopedie van Nederlandsch Indië_, die tijdens de oorlogsjaren met overheidssubsidie tot stand kwam. Daarentegen kwam de _Encyclopedie van de Islam_ geheel stil te liggen: het eerste deel was verschenen in 1913 en het tweede zou pas in 1927 het licht zien. Gezien de schaarste aan werk hoefde Peltenburg niet lang te aarzelen, toen hem in 1916 de uitgave van het Leidsch Predikbeurtenblad werd aangeboden.

Terwijl de inkomsten daalden, stegen de kosten. De papierprijzen rezen tot schrikbarende hoogten en de loonkosten maakten in de oorlogsjaren een sterke stijging door. Bij de invoering van de nieuwe driejarige cao per 1 januari 1917 durfde Peltenburg niet opnieuw dwars te gaan liggen, bevreesd voor een staking. Het was het jaar van de Russische Revolutie en van de mislukte Nederlandse revolutie van Pieter Jelles Troelstra - de arbeidersvuist was dezer dagen gebald en Peltenburg vond het geraden wat minder militant op te treden. De loonsverhoging op grond van de cao was echter onvoldoende om de stijging van de kosten van levensonderhoud op te vangen. De prijzen van primaire levensbehoeften stegen sterk in de laatste oorlogsjaren en de lonen moesten herhaaldelijk worden opgetrokken om arbeidsonrust te voorkomen. Zowel in de gezinnen als in het bedrijf werd het steeds lastiger de eindjes aan elkaar te knopen.[^18] Temidden van deze oorlogsperikelen valt een verheugend nieuwtje te melden. Begin 1918 deed de eerste vrouw haar intrede op het kantoor van Brill, in de persoon van mejuffrouw Catharina Sytsma. De ouderwetse Peltenburg had aanvankelijk zijn twijfels over de vrouwelijke employé, maar het viel hem alles mee: naar hij de commissarissen meedeelde had zij ‘zijne verwachtingen, zoowel wat hare geschiktheid, als wat hare
werkkracht betreft, verre overtroffen’.[^19]

![](03-17.jpg)

<=.ill_03-17.jpg Enige lemmata van de letter ‘B’ uit het eerste deel van de _Enzyklopädie des Islam_, verschenen in 1913. UBA =>

<=.pb 95 =>
### ‘Grootheid en verval’
Het aanbreken van de vrede betekende niet het herstel van de vooroorlogse toestand. Integendeel, de moeizame omstandigheden van de laatste oorlogsjaren duurden voort tot in de jaren twintig. De kosten van levensonderhoud en ook de lonen bleven stijgen, terwijl de vakbonden tegelijkertijd actie voerden voor vermindering van de werkweek. De inzet was de werkdag van acht uur, overeenkomend met een werkweek van 48 uur. Na massale demonstraties van de arbeidersbeweging werd deze maatschappelijke verworvenheid ingevoerd per 1 januari 1920.

Peltenburg beklaagde zich bij de commissarissen over de desastreuze gevolgen van deze ontwikkelingen op de bedrijfsvoering. Zijn berekeningen en plannen werden voortdurend op losse schroeven gesteld door de ‘steeds stijgende eischen der arbeiders’. Het werd hem in deze onzekere tijden onmogelijk gemaakt ‘met uitzicht op winst te kunnen arbeiden’. De hoge lonen dreven de prijzen voor drukwerk omhoog en hadden een nadelig effect op de vraag. Bij gebrek aan orders zag hij zelfs na de verkorting van de werkweek geen kans alle personeel aan het werk te houden. Verscheidene werknemers, onder wie ervaren drukkers en zetters, waren gedwongen hun heil elders te zoeken. De eigen binderij werd in 1920 opgedoekt omdat ze niet langer rendabel was. Buitenlandse orders bleven uit omdat de gulden te duur was ten opzichte van de gedevalueerde buitenlandse valuta. Met name van Duitsland hoefde men voorlopig niets te verwachten, gezien de hollende na-oorlogse inflatie.[^20]


Het waren weinig florissante tijden, maar ook in deze jaren wist het bedrijf zich staande te houden zonder de kapitaalsreserves aan te spreken. Volgens een artikel in een vakbondsblad was de toestand bij Brill echter veel slechter dan men voorgaf. De schrijver diende zich aan als ‘Een oud Leidsch typo’ en lichtte het bedrijf door onder de aansprekende titel ‘Grootheid en verval’.[^21] Zijn conclusie luidde dat van de voormalige grootheid van Brill weinig over was, en dat anno 1920 de N.V. zich op de rand van de afgrond bevond. Het verval was niet een uitvloeisel van de oorlogsomstandigheden,

![](03-18.jpg)

<=.ill_03-18.jpg Herman Boerhaave (1668-1738) was de beroemdste medicus van de achttiende eeuw. Volgens de overlevering werden brieven met de simpele adressering ‘Boerhaave, Holland’ zonder problemen afgeleverd bij zijn woonhuis aan het Rapenburg nr.31. Brill deed nauwelijks onder voor Boerhaave: op grond van deze tekening van een bril begreep de postbode dat hij het bijgaande pakketje moest bezorgen bij uitgeverij Brill aan de Oude Rijn. Een adressering met alleen een getekend brilletje en ‘Leiden, Holland’ wordt door hedendaagse postbodes niet meer begrepen, zo is onlangs proefondervindelijk vastgesteld. De desbetreffende brief werd geretourneerd naar de afzender. Coll. Brill =>

<=.pb 96 =>
maar was volgens de ‘typo’ te wijten aan het wanbeleid van de directeur C. Peltenburg. Voor de goede orde stuurde de anonieme schrijver een exemplaar van het blad naar Peltenburg, die in grote woede ontstak. Hij vatte het artikel op als een wraakactie van een ontslagen werknemer en liet in allerijl een speciale vergadering van Raad van Commissarissen beleggen. Dezen probeerden de opgewonden directeur te sussen, hij moest de zaak niet opvatten als een persoonlijke aanval. Veeleer moest hij het artikel zien als ‘eene uiting van ongerustheid der werknemers over hunne toekomst’. Peltenburg moest de arbeiders in een ‘gemoedelijk onderhoud’ duidelijk maken dat zij niet bang hoefden te zijn voor het verlies van hun werkgelegenheid en dat zij op Brill konden vertrouwen. De commissarissen waren inmiddels tot het inzicht gekomen dat stroop in de strijd tussen kapitaal en arbeid een beter wapen was dan azijn.[^22]

### Sigaren van de baas
Gemoedelijke omgang met het personeel was niet Peltenburgs sterkste punt. Hij liet zich amper zien op de werkvloer en regelde het werk via de meesterknechten, die dagelijks van hem hun instructies kregen. De directeur placht elk jaar een nieuwjaarstoespraak te houden en verschanste zich de rest van het jaar in zijn kantoor.

![](03-19.jpg)

<=.ill_03-19.jpg Portret van Peltenburg door Willem Hendrik van der Nat (1864-1929), een bekende Leidse schilder. Na een opleiding tot lithograaf bij de steendrukker C. Bos volgde hij een studie als kunstschilder aan de Haagse Academie. Als bijverdienste maakte hij boekillustraties voor de uitgeverij Bolle in Rotterdam. Nadat hij zich rond 1900 in Leiden had gevestigd, legde hij zich geheel toe op de schilderkunst. Naast portretten schilderde Van der Nat veel landschappen, die in Engeland en de Verenigde Staten zeer gewild waren. In 1923 vervaardigde hij dit portret van Peltenburg naar aanleiding van diens zeventigste verjaardag. Coll. Brill =>

<=.pb 97 =>
Zijn afstandelijkheid werd versterkt door het besef dat de arbeiders een vuist konden maken en zich niet langer schikten in de rol van ondergeschikten.

Eind 1923, ter gelegenheid van zijn zeventigste verjaardag, deed Peltenburg een
poging de muren van stand en klasse te slechten. Op zaterdagavond werd al het personeel uitgenodigd op het kantoor om zijn verjaardag te komen vieren. Er werd wijn geschonken en elke werknemer kreeg een doos met 25 sigaren van de directeur - hopelijk werd juffrouw Sytsma op andere wijze bedacht. Het was voor Brillse begrippen een hoogst ongebruikelijk evenement en jaren na dato werd nog over het feest gesproken.[^23] Hare Majesteit droeg bij tot de feestvreugde door de jarige directeur te benoemen tot Ridder in de Orde van Oranje-Nassau.

In datzelfde jaar 1923 kwam een einde aan de na-oorlogse malaise. De internationale handel herstelde zich en buitenlandse bibliotheken begonnen weer op grote schaal boeken aan te kopen. Het bedrijf maakte behoorlijke winsten, zodat in een paar jaar tijd het restant van de obligatielening van 1896 kon worden afgelost. Na 1925 brak een periode aan van zes ongekend vette jaren, waarin de aandeelhouders jaar op jaar werden onthaald op een riant dividend van 40%. Kennelijk vonden de directeur en de commissarissen het niet nodig te investeren in de modernisering van het bedrijf.[^24]

### De wondere wereld van Brill
De journalist M.J. Brusse, bekend als schrijver van het jongensboek _Boefje_ (1903; twintig drukken tot 1950), maakte in 1927 voor de _Nieuwe Rotterdamsche Courant_ een reeks reportages over uitgeverijen. Op zijn zwerftocht door uitgeversland belandde hij ook in het voormalige weeshuis aan de Oude Rijn in Leiden. Brusse had wel eens van Brill gehoord en dacht een stukje te gaan schrijven over de zoveelste Nederlandse uitgeverij. Het pakte anders uit, want aan dat ‘schilderachtig oud-Hollandsch grachtje’ trad hij nietsvermoedend binnen in de wondere wereld van Brill. De journalist had een feuilleton van drie artikelen nodig om van zijn verbazing te bekomen.[^25]

Brill bleek boeken uit te geven in talen waarvan Brusse nog nooit had gehoord. Samaritaans, oud-Syrisch, Manchoerees, Ethiopisch - je kon geen vreemde taal of eigenaardig schrift bedenken of Brill had daarin boeken gepubliceerd. Wanneer de uitgeverij een manuscript kreeg aangeboden in een exotisch schrift waarvan de drukkerij geen druktypen had, liet men die maken. Onlangs had Brill een handschrift in het Lydisch uitgegeven, waarvan nergens ter wereld druktypen bestonden. De inscripties die bij opgravingen waren gevonden werden zorgvuldig nagetekend en op grond daarvan werden matrijzen gemaakt voor het Lydisch. Achter de oud-Hollandse façade van Brill bleek tot Brusse’s verbazing een internationaal opererend bedrijf schuil te gaan. De journalist vroeg Peltenburg - ‘den bejaarden doch nog zóó jeugdigen ondernemenden leider dezer klassieke firma’ - naar het geheim van Brill:

> ‘Maar hoe ter wereld exploiteert u al deze omvangrijke en voor het gros van de menschheid onleesbare, meerendeels zoo kostbare folianten en stapels folianten van eindelooze vervolgwerken sedert jaren?’

Peltenburg legde hem uit hoe de firma in meer dan vijftig jaar een internationale reputatie had opgebouwd in oriëntalia. De huidige directeur plukte de vruchten van

![](03-20.jpg)

<=.ill_03-20.jpg De _Panorama_ wijdde op 11 juli 1923 een artikel aan de N.V. Boekhandel en Drukkerij v.h. E.J. Brill, in verband met het feit dat het bedrijf sinds 75 jaar deze naam voerde. Op de foto in koperdiepdruk oogt Peltenburg beduidend jonger dan op het portret van Van der Nat, dat in hetzelfde jaar werd vervaardigd. De Panorama bestond sinds 1913 en werd uitgegeven door de Nederlandsche Rotographure Maatschappij in Leiden. =>

<=.pb 98 =>
![](03-21.jpg)
![](03-22.jpg)

<=.ill_03-21.jpg In 1920 herdachten Amerikanen en Nederlanders het vierde eeuwfeest van de oversteek van de Pilgrim Fathers van Leiden (via Delfshaven) naar Amerika. Deze Engelse calvinisten hadden in het begin van de zeventiende eeuw hun toevlucht gezocht in Leiden, omdat de katholiek gezinde koning Jacobus I beperkingen oplegde aan de uitoefening van hun godsdienst. Vervolgens besloten zij in Amerika een kolonie te stichten waar zij naar eigen inzichten konden leven. Zij deden dat in een oord aan de oostkust van Amerika waaraan zij de naam ‘Boston’ gaven. Deze eerste kolonisten worden door Amerikanen beschouwd als de aartsvaders van de natie. Ter gelegenheid van de herdenking bracht Brill twee titels uit: een facsimile-uitgave met annotaties van de _Leyden documents relating to the Pilgrim Fathers_ en de  monografie _The Pilgrim Fathers in Holland (1608-1620)_ van J. Irwin Brown. Sinds 1998 heeft Brill in Boston zijn eigen ‘Pilgrim Fathers’, in de vorm van vijftien personen die het filiaal in die stad bemannen. UBA =>

<=.pb 99 =>
het werk van zijn voorgangers - Evert Jan Brill, Adriaan van Oordt en Frans de Stoppelaar. Hij koesterde de traditie van het bedrijf en hield die in ere. Brill was niet een doorsnee uitgeverij met een eigenaardige specialisatie, het bedrijf had zich in de loop van de tijd ontwikkeld tot het knooppunt van een internationaal netwerk in de wetenschappelijke wereld. Dat web van buitenlandse betrekkingen was van vitaal belang en werd met zorg onderhouden. Peltenburg correspondeerde met wetenschappers in allerlei landen en was evenals De Stoppelaar een vaste bezoeker van de Internationale Congressen van Oriëntalisten.

In een gestage ontwikkeling van meer dan een halve eeuw had Brill een unieke positie verworven als uitgever van de internationale oriëntalistiek. Geleerden uit alle windstreken van Europa, uit Azië en uit Amerika kenden de naam van Brill en wisten de weg naar de Oude Rijn te vinden. Op grond van haar reputatie kreeg de uitgeverij allerlei werken aangeboden, terwijl zij daarnaast als intermediair van de wetenschap ook eigenhandig uitgaven entameerde. Peltenburg haalde voor de journalist zijn paradepaardje van stal: de _Encyclopedie van de Islam_, waarvan het tweede deel dat jaar zou verschijnen in het Duits, Frans en Engels. ‘Voor Engeland heb ik de uitgave verkocht aan Luzac & Co te Londen, voor Duitschland aan Otto Harrassowitz te Leipzig, voor Frankrijk aan Aug. Picard te Parijs’. De encyclopedie was niet alleen een standaardwerk dat door alle specialisten werd gebruikt en door alle bibliotheken, waar ook ter wereld, werd aangeschaft. De uitgave bevestigde tevens de centrale rol van de uitgever, want in vele lemmata werd verwezen naar boeken die bij Brill waren verschenen dan wel stonden te verschijnen.

Peltenburg benadrukte dat de specialisatie van Brill een zaak was van lange adem. Een internationale uitgever voor de wetenschap moest verder durven kijken dan het gewin op korte termijn. De wetenschappelijke boeken die door Brill werden geproduceerd hadden een veel lagere omloopsnelheid dan populaire uitgaven voor de binnenlandse markt. De afzet verliep voornamelijk via bibliotheken en instellingen in het buitenland. Het duurde vaak jaren voordat een oplage was uitverkocht en alleen op de lange duur betaalden de investeringen zichzelf terug: ‘Want een goed boek op het gebied van ons fonds blijkt steeds weer in de geleerde wereld onmisbaar te zijn’.

### De nadagen van de patriarch
Brusse had Peltenburg meegemaakt in zijn gloriedagen, maar niet lang daarna pakten donkere wolken zich samen aan de horizon. De beurskrach van november 1929 vormde de opmaat van een wereldwijde economische crisis die de eerste helft van de jaren dertig zou aanhouden. Onvermijdelijk deed de ineenstorting van de economie zich na verloop van tijd ook bij Brill gevoelen. Kon over 1931 nog een mooie winst van f 61.000 worden geboekt, in 1933 was dat teruggelopen tot f 24.000. De omzet van alle bedrijfsonderdelen was in de tussenliggende jaren schrikbarend gedaald. De drukkerij lag in 1934 vaak stil bij gebrek aan werk.

Peltenburg was een verklaard tegenstander van machinaal zetten en had sinds jaar en dag de invoering van zetmachines tegengehouden. Sijthoff werkte voor de Eerste Wereldoorlog al met twaalf zetmachines, maar bij Brill kwamen ze de deur niet in: ‘zo lang ik leef geen zetmachine op mijn zaak’, placht de oude heer te zeggen.[^26] Om in het begin van de jaren dertig nog enigermate te kunnen concurreren was hij gedwongen

<=.pb 100 =>
het duurdere handzetten te berekenen tegen de prijs van machinaal zetten. Uiteindelijk moest hij zijn verzet opgeven en in weerwil van zijn lijfspreuk deed in het begin van 1934 de eerste zetmachine zijn intrede bij Brill. De late eenling kon het tij van de crisis niet keren.


Op 28 december 1933 vierde Peltenburg zijn tachtigste verjaardag met een druk bezochte receptie in Huize Bruyns in Leiden. De jarige werd bedolven onder de cadeaus en de toespraken. Vooraanstaande wetenschappers en uitgevers kwamen hem gelukwensen en ook de burgerwacht van Oegstgeest bracht een eresaluut aan haar commandant. Men prees hem gelukkig omdat hij op de leeftijd der zeer sterken nog steeds leiding gaf aan een groot bedrijf, men bracht hem hulde en roemde zijn verdiensten. F.C. Wieder, bibliothecaris van de Leidse universiteit en presidentcommissaris van Brill, bood hem namens de Raad van Commissarissen een divan aan - wellicht een voorzichtige hint om het in de toekomst wat rustiger aan te doen. Snouck Hurgronje verleende Peltenburg in zijn toespraak de eretitel van ‘oudste oriëntalist’ in Nederland, in welke categorie hijzelf op de tweede plaats stond.[^27]

Na al die eerbewijzen werd Peltenburg per 1 juli 1934 eervol ontslag verleend. Meer dan dertig jaar lang had hij Brill geleid in goede en slechte tijden, om in zijn nadagen te worden geconfronteerd met een achteruitgang die hem boven de macht ging. Het was de bedoeling dat de ex-directeur zou aanblijven als adviseur, maar zijn tijd was gekomen. Ruim drie maanden later, op 7 oktober 1934, overleed Cornelis Peltenburg na een ziekbed van anderhalve maand. Zijn naam leeft bij Brill voort in het Peltenburg Pensioenfonds, dat enige jaren na de dood van de naamgever werd opgericht met een aanzienlijk legaat uit zijn nalatenschap.[^28] Postuum bracht Peltenburg een blijvende toenadering tot stand tussen hemzelf en de werknemers van Brill: het Peltenburgfonds fungeert tot op de dag van vandaag als de pensioenvoorziening van de onderneming.

![](03-22.jpg)

<=.ill_03-22.jpg A. de Jong, _Gramat Volapuka_ (1931). In 1888 had de Duitse priester Johann Martin Schleyer een droom waarin hem door God werd bevolen een internationale kunsttaal te ontwerpen. De geestelijke besloot de Babylonische spraakverwarring ongedaan te maken en bedacht het Volapuk. Een succes was deze eerste kunstmatige taal niet, het aantal sprekers was gering en onder hen ontstonden de nodige ruzies en schismata. In 1931 schreef dr. Arie de Jong met instemming van leidende Volapukkers een vereenvoudigde grammatica, die de kunsttaal enige aanhang bezorgde in Nederland en Duitsland. De nazi’s beschouwden het Volapuk en het Esperanto echter als volksvijandige uitingen en verboden de kunsttalen. Tegenwoordig schijnt het nog door ongeveer twintig mensen te worden gesproken. UBA =>

<=.pb 101 =>
## Theunis Folkers: schipperen in zwaar weer, 1934-1945

### De plaatsvervanger
Pas in de laatste maanden van zijn bewind had Peltenburg zijn verzet tegen de zetmachine opgegeven, en ook de kwestie van zijn opvolging stelde hij uit tot het laatste moment. In al die jaren was hij er niet in geslaagd een plaatsvervanger te vinden, zodat men in mei 1934 buitenshuis op zoek ging naar een geschikte kandidaat. Deze werd gevonden in de persoon van Theunis Folkers, die per 1 juli van dat jaar Peltenburg opvolgde als directeur.[^29]

Folkers, geboren in Groningen in 1879, kwam goed beslagen ten ijs in de uitgeverij. Als jongen van zeventien jaar was hij in dienst getreden bij de firma Noordhoff in zijn vaderstad, waar hij bleef werken tot 1914. Vervolgens kreeg hij een baan bij Martinus Nijhoff in Den Haag, bij welke uitgeverij hij zich in de loop der jaren opwerkte tot chef. Folkers, een energieke doener met moderne inzichten, nam de leiding van Brill over op het dieptepunt van de crisis. Met zijn scherpe oog voor nieuwe mogelijkheden was hij de juiste man om het bedrijf weer op de been te helpen.

De nieuwe directeur was een goede manager, maar een zekere ijdelheid was hem niet vreemd. Hij kon bijvoorbeeld de verleiding niet weerstaan zijn eigen initialen te verwerken in het uitgeversvignet met de initialen ‘E.J.B’. Een dergelijke privatisering van de naam van Brill was in het geval van zijn voorganger ondenkbaar geweest, noch kwam die gedachte op bij zijn opvolgers.

### Machines en reclame
Bij zijn aantreden liet Folkers een frisse wind waaien aan de Oude Rijn. Hij stuitte bij Brill op veel ouderwetse stoffigheid, die hij slecht kon verdragen en die het bedrijf zich in deze moeizame tijd niet langer kon veroorloven. De nieuwe directeur liet eerst maar eens de vijf voorraadzolders opruimen, waar de hoeveelheid boeken tot gigantische proporties was uitgedijd. Hij gaf opdracht het oosterse antiquariaat te inventariseren, dat werken in maar liefst 87 verschillende talen bleek te bevatten. Bij voorkeur had

![](03-23.jpg)

<=.ill_03-23.jpg De papierboekhouding van 1934: een prozaïsch bedrijfsonderdeel met poëtische namen als Colombier, Imperiaal, Grootmediaan en Bijkorf. AB/UBA =>

<=.pb 102 =>
Folkers het noodlijdende antiquariaat afgestoten, maar de Raad van Commissarissen wees dat voorstel in 1935 van de hand. Dat was maar goed ook, want in 1939 rechtvaardigde het antiquariaat zijn bestaan door onverwachts een verzameling boeken ter waarde van f 30.000 te verkopen aan een Japanse instelling.

De drukkerij en zetterij baarden Folkers de meeste zorgen. Hij liet voor enige boeken die bij Brill werden gedrukt een prijsopgave maken bij andere drukkers en kwam tot de conclusie dat de eigen drukkosten ruim een derde hoger lagen dan elders - ‘voorwaar een ernstige toestand’. Men had de gangbare marktprijzen uit het oog verloren, omdat de drukkerij haar opdrachten uitsluitend ontving van de eigen uitgeverij. Het hoge prijspeil van de drukkerij was voornamelijk te wijten aan de hoge loonkosten van de zetterij.

Ten dele was dat onvermijdelijk, want boeken in buitenissige talen moesten nu eenmaal met de hand worden gezet. Het gewone zetwerk moest volgens Folkers echter zoveel mogelijk worden gemechaniseerd om de loonkosten terug te dringen. Hij schafte in 1935 twee nieuwe zetmachines en een jaar later een derde. De commissarissen verleenden hem een volmacht om voor f 60.000 nieuwe machines aan te kopen, wat hem in staat stelde ook het verouderde materieel van de drukkerij te vervangen. De investeringen waren mogelijk dankzij het feit dat het aandelenkapitaal van Brill in 1934 was verhoogd van f 100.000 tot f 150.000. Folkers had nog meer

![](03-24.jpg)
![](03-25.jpg)

<=.ill_03-24.jpg In het kader van de modernisering van het bedrijf schafte Folkers het ‘druppelvignet’ af dat sinds 1909 in gebruik was. In 1936 introduceerde hij het zakelijke ‘zegelringvignet’ met de initialen ‘E.J.B.’. De kunstenaar
Jan van Krimpen (1892–1958) ontwierp daarvoor vier versies. Het vignet met de krulletters rechtsboven werd gekozen, maar ook de variant linksonder werd regelmatig gebruikt. Folkers vulde die twee vignetten aan met zijn initialen ‘T.F’, welke na zijn aftreden in 1947 werden verwijderd. Coll. Brill =>

<=.pb 103 =>
machines willen aanschaffen, maar het gebouw aan de Oude Rijn leende zich niet voor verdere uitbreiding van het machinepark.


De ingrijpende modernisering zou Peltenburg niet naar de zin zijn geweest, maar gelukkig hoefde hij het niet meer mee te maken. Een andere belangrijke innovatie was de ‘exploitatie-afdeeling’ die door de nieuwe directeur in het leven werd geroepen: ‘Wij moeten niet alleen een boek kunnen maken, maar ook verkoopen’. Folkers was heilig overtuigd van de noodzaak van reclame, een aspect dat tot dusverre bij Brill was veronachtzaamd. Nieuwe en oudere uitgaven moesten voortdurend onder de aandacht van het publiek worden gebracht om de verkoop te stimuleren.[^30]

Folkers was ook de eerste die het jaartal 1683 - het jaar van oprichting van de firma Luchtmans - gebruikte als marketing-strategie om de lange historie van de uitgeverij te benadrukken. Zo bracht Brill in 1937 een nieuwe en rijk verzorgde catalogus uit van het oosterse fonds. De trotse titel luidde voluit _Catalogue de Fonds de la Librairie Orientale E. J. Brill. Maison fondée en 1683_. Het oprichtingsjaar werd voortaan ook vermeld in andere uitgaven.

### Uit het dal naar de Nijl
In 1935 was een kentering te bespeuren in de malaise. Het ergste leed van de crisis was geleden en de vernieuwingen van Folkers begonnen hun vruchten af te werpen. De uitgeverij herleefde, de drukkerij kreeg meer werk en ook de verkoop nam zienderogen toe. Het aarzelende herstel zette door in 1936 en sindsdien was de stijgende lijn onmiskenbaar. De hoeveelheid opdrachten was bij tijden zo groot dat men het werk amper aan kon. Bij de zetmachines werd zelfs een tweeploegendienst ingevoerd, zodat ze van zes uur ’s ochtends tot acht uur ’s avonds in bedrijf konden worden gehouden (het streven was, aldus de pragmatische Folkers, om het uiterste uit de machines te halen zonder in botsing te komen met de arbeidswet).

Bedroeg de omzet in 1934 nog f 132.000, in 1939 was deze opgelopen tot f 294.626 - meer dan een verdubbeling in zes jaar tijd. De toon van Folkers’ verslagen in de jaarlijkse vergadering van aandeelhouders werd steeds juichender. Jaar op jaar kon hij stijgende resultaten overleggen in het etablissement ‘In den Vergulden Turck’, waar de aandeelhouders traditiegetrouw op een donderdag in juni bijeenkwamen. Door het uitsterven van de eerste generatie aandeelhouders waren de aandelen van eigenaar gewisseld, terwijl in 1934 tien nieuwe aandelen waren uitgegeven ter waarde van f 50.000. Niettemin waren de commissarissen en aandeelhouders nog steeds oude bekenden van Brill dan wel nakomelingen of verwanten van de vroegere directeuren. Dertig jaar na het overlijden van Van Oordt en De Stoppelaar leefden hun namen voort in de kring van Brillse intimi.

Het moeten aangename bijeenkomsten zijn geweest in ‘De Vergulde Turck’, te meer
daar de dividenden hoog waren: van 1935 tot 1939 werd jaar op jaar 25% uitgekeerd. De directeur placht het percentage voor te stellen en de commissarissen, zelf per definitie aandeelhouders, moesten het goedkeuren. Folkers deed moeite om het zijn aandeelhouders naar de zin te maken en wilde kennelijk niet onderdoen voor zijn voorganger, die hen jarenlang had verwend met hoge dividenden. Over 1934, het eerste jaar

![](03-26.jpg)

<=.ill_03-26.jpg Theunis Folkers achter zijn bureau in de directiekamer van Brill (1939). =>

<=.pb 104 =>
van zijn bewind, lijken de cijfers enigszins geflatteerd: het bedrijf had nog steeds te lijden van de crisis en bij een lage omzet van f 132.000 werd een winst opgevoerd van f 54.000, waarover een dividend van 30% werd uitgekeerd.

De zaken van Brill floreerden tijdens de laatste jaren van het interbellum. In 1938 werd zowaar een automobiel aangeschaft op rekening van de zaak. De goede naam van het bedrijf was doorgedrongen in Egypte, getuige het feit dat men in datzelfde jaar opdracht kreeg de catalogus te verzorgen voor een belangrijke tentoonstelling in Caïro. Een groot deel van de opgevoerde titels was verschenen bij Brill en was op de tentoonstelling in boekvorm te bezichtigen. De presentatie aan de oevers van de Nijl leidde tot een vererende order, want koning Faroek I van Egypte besloot alle uitgaven van Brill aan te schaffen voor zijn privé-bibliotheek.[^31]

### De oogst van 1936
Tegenwoordig brengt Brill jaarlijks zeshonderd titels uit en worden meer dan honderd tijdschriften uitgegeven. Voor de oorlog was de productie een stuk overzichtelijker met een gemiddelde van ongeveer dertig titels per jaar. Een doorsnede van het jaar 1936 maakt het stramien van de toenmalige uitgeverij zichtbaar.

In dit jaar verscheen het vierde en laatste deel van de _Encyclopedie van de Islam_, met een kloeke omvang van 1200 pagina’s. Een aanvullend deel met register was in de maak en stond op de agenda voor 1938. Inmiddels had Folkers al een overeenkomst gesloten met uitgeverij Kegan Paul in Londen om een beknopte Engelse editie van de encyclopedie uit te brengen.[^32] Tevens werd in 1936 het eerste deel van de _Concordance et indices de la tradition musulmane_ gepubliceerd, begonnen door de arabist A.J. Wensinck
en voortgezet door anderen; het achtste en laatste deel van de reeks zou verschijnen in 1988. Ook T. Huitema leverde met _De voorspraak (Shafi’a) in den Islam_ dit jaar een bijdrage aan de arabistiek. Tegelijk was men bezig met de voorbereiding van A. Jeffery’s _Materials for the history of the text of the Qur’ãn_, waarvan het eerste deel in 1937 uitkwam.

Op het gebied van de egyptologie verscheen _Egyptische Oudheden_ van W.A. van Leer en de assyriologie was dit jaar vertegenwoordigd door J.G. Lautners _Altbabylonische Personenmiete und Erntearbeiterverträge_. Het Verre Oosten diende zich aan in de vorm van _Japanese Music_ van S. Katsumi, terwijl K.S.J.M. de Vreese de oude wijsheid van Kashmir onder de aandacht bracht in zijn _Nilamata or the teachings of Nila_.

De reeksen bevindingen van wetenschappelijke expedities uit het begin van de eeuw liepen in 1936 gestaag door. Dit jaar werden twee afleveringen van de Siboga-serie uitgebracht: G. Stiasny-Wijnhoff, _Die Polystilifera der Siboga Expedition_ (over zeewormen) en L. Döderein, _Die Asteriden der Siboga Expedition. Die Unterfamilie Oreasterinae_ (over zeesterren). Verder verzorgde L.F. de Beaufort het achttiende deel van _Nova Guinea: résultats de l’expédition scientifique neérlandaise_. A.A. Beekman bleef dichter bij huis met zijn _Lijst der aardrijkskundige namen van Nederland_, terwijl R. Hennig in zijn _Terrae Incognitae_ de ontdekkingsreizen beschreef uit het tijdvak voorafgaande aan Columbus. 

De oudheid leverde in 1936 twee titels op. S. Peppink wijdde een studie aan ‘Het geleerdenbanket’, een dialoog van de Hellenistische auteur Athenaeus uit de tweede eeuw (_Observationes in Athenaei Deipnosophistas_); daarnaast publiceerde R.J. Forbes een boek met de intrigerende titel _Bitumen and Petroleum in Antiquity_. Op historisch gebied verschenen twee boeken die naar tijd en materie enigszins verwant waren:

![](03-27.jpg)

<=.ill_03-27.jpg Pagina uit de _Catalogue de Fonds de la Librairie Orientale E. J. Brill. Maison fondée en 1683_. De fondscatalogus van oriëntalia (1937) had een omslag van blauw pseudo-velours en was geïllustreerd met foto’s van binnen en buitenlandse wetenschappers. AB/UBA =>

<=.pb 105 =>
J. Coert, _Spinoza en Grotius met betrekking tot het volkenrecht_ en A.M. Vaz Dias, _Uriël da Costa: nieuwe bijdrage tot diens levensgeschiedenis_.

De eigentijdse geschiedenis kwam tot haar recht met J. de Jongh, _Documentatie van de geschiedenis der vrouw en der vrouwenbeweging_ en P.L. Dubourcq, _Schijn en wezen rondom geld: een schets ter verheldering van het begrip der sociaal-economische verschijnselen_. V. van Wijk bewoog zich op het terrein van de godsdienstgeschiedenis met _De naam Maria: over zijn beteekenis en velerlei vormen, zijn verspreiding en vereering+. Verder verscheen dit jaar de negende druk van een Duitstalige editie van Goethe’s _Faust_. De opsomming over het jaar 1936 is niet compleet, maar biedt een representatieve doorsnede van Brills uitgaven in deze periode.

### De bezetting
Op 3 mei 1940 vergaderde de Raad van Commissarissen in tegenwoordigheid van
Folkers. De directeur presenteerde bij deze gelegenheid de cijfers over het boekjaar 1939, die wederom een verheugende stijging lieten zien. De nettowinst bedroeg f 76.981,415 - men rekende nog steeds in halve centen -, zodat Folkers voorstelde het dividend te bepalen op 26%. De vooruitzichten voor 1940 waren volgens hem gunstig. Natuurlijk, de internationale toestand was gespannen; Duitsland was in 1939 Polen binnengevallen en verkeerde thans op voet van oorlog met Engeland en Frankrijk. Dankzij de neutraliteit van Nederland kon het bedrijf echter zijn voordeel doen met deze situatie. Duitse uitgevers zagen zich afgesneden van hun afzetgebieden in Engeland en Frankrijk, en wilden dat opvangen door Brill in de arm nemen als intermediair. De Sovjet-Unie, op dat moment nog bondgenoot van Nazi-Duitsland, kampte met soortgelijke problemen en had Brill verzocht haar gehele import van Franse en
Engelse boeken te verzorgen. Met het oog op deze nieuwe activiteiten had de directeur een speciale exportafdeling in het leven geroepen.[^33]

Een week later vielen de Duitsers Nederland binnen en zag de wereld er opeens
heel anders uit. Dat was te merken op de aandeelhoudersvergadering van 6 juli 1940, waar Folkers van de weeromstuit pessimistische geluiden liet horen: ‘Helaas heeft de in Mei losgebroken oorlog ook ons bedrijf zwaar getroffen, daar de algehele uitvoer

![](03-28.jpg)

<=.ill_03-28.jpg R. Hennig, _Terrae incognitae. Eine Zusammenstellung und kritische Bewertung der vorkolumbischen Entdeckungsreizen_. De historicus Hennig beschreef aan de hand van allerlei bronnen vergeten ontdekkingsreizen in de Oudheid en de Middeleeuwen. Het werk verscheen in vier delen bij Brill tussen 1936 en 1939 en beleefde een herdruk tussen 1944 en 1956. UBA =>

<=.pb 106 =>

![](03-29.jpg)

<=.ill_03-29.jpg A.J. de Lorm en G.L. Tichelman, _Beeldende kunst der Bataks_ (1941). Soms klinkt de oorlog door in wetenschappelijke uitgaven. ‘Het feit toch, dat thans de behoefte aan een boek als het onderhavige wordt gevoeld, strekt tot verheuging’, schreef inleider Schrieke, directeur van de afdeling volkenkunde van het Koloniaal Museum - waarbij hij stilzwijgend refereerde aan het feit dat Nederlands Indië in 1941 nog werd bestuurd in naam van de koningin. De auteurs zochten in hun inleiding een verklaring voor de ondergang van Nederland: ‘te sterk toegespitst individualisme heeft te sterke differentiatie tot gevolg gehad, zoodat het gevoel voor de eenheid des levens verloren is gegaan’. Daar zal de bezetter niet van hebben opgekeken. In een volgende alinea spraken de auteurs echter over het ‘Fransche expressionisme der na-oorlogsche jaren, geïnspireerd op de door ons uit kunstzinnig oogpunt gewaardeerde plastische uitingen der negers …’ Onverwachte uitlatingen in een tijd dat expressionisme van hogerhand werd bestempeld als ‘ontaarde kunst’ en jazz werd verboden als decadente ‘joodschangelsaksische negermuziek’. UBA =>

<=.pb 107 =>
naar het buitenland totaal is lamgelegd’. De veelbelovende exportafdeling ging ter ziele nog voordat zij zich had kunnen bewijzen. In deze omstandigheden kon niet het gebruikelijke dividend worden uitgekeerd en Folkers verzocht de aandeelhouders genoegen te nemen met 5%, in plaats van de 26% die hij hun aanvankelijk had toebedacht. Buitenlandse debiteuren konden hun rekeningen niet betalen, er kwam geen geld binnen en het was van levensbelang voldoende contanten in het bedrijf te houden.[^34]


Van mei tot september 1940 verkeerde het bedrijf in een staat van acute verlamming, maar tijdens het laatste kwartaal van het jaar zette een aarzelend herstel in. Uiteindelijk bedroeg de omzet over 1940 toch nog f 220.000, ongeveer een kwart minder dan in 1939. In 1941 steeg de omzet ondanks de oorlogsomstandigheden tot f 270.000. Folkers zette alles op alles om ‘het bedrijf met zijn volledige bezetting... door dezen slechten tijd heen te helpen’.[^35] Hij begon met een reeks schoolboeken voor de binnenlandse
markt om het wegvallen van de buitenlandse afzet te compenseren. Dankzij jaarboeken en vervolgwerken was de uitgeverij toch nog in staat de gebruikelijke productie van dertig titels per jaar te halen. De drukkerij draaide op volle capaciteit, terwijl de meeste concurrenten gedwongen waren arbeidstijdverkorting in te voeren.

In 1941 rantsoeneerde de bezetter het papier, maar de directeur zag kans de toewijzing voor Brill te verruimen. Hij organiseerde in het bedrijf een kantine die dagelijks een warme maaltijd verstrekte aan de werknemers. De nieuwe machthebbers bemoeiden zich op allerlei manieren met de bedrijfsvoering - de inkoop van lood werd onderworpen aan beperkingen, machines werden gevorderd, elektriciteit was slechts mondjesmaat

![](03-30.jpg)

<=.ill_03-30.jpg Een aantal posten op de balans van 1940. AB/UBA =>

<=.pb 108 =>
beschikbaar, steenkolen voor verwarming waren bijna niet meer te krijgen en werknemers dreigden te worden opgeroepen voor de ‘Arbeitseinsatz’ in Duitsland. Tot overmaat van ramp voerde de bezetter een winstbelasting in van 50% en werd binnen het bedrijf een vertegenwoordiger van het Nationaal Arbeidsfront aangesteld als pottenkijker.

Folkers was voortdurend bezig zulke aanslagen te pareren: ‘Niets gaat meer zooals vroeger en alles moet met veel moeite aan den gang gehouden worden. De overheidsbemoeiïngen zijn van dien aard, dat zoowat alles aan banden is gelegd en men alleen nog iets kan bereiken, door om al deze belemmeringen heen te zeilen of vriendschapsrelaties in den arm te nemen’.[^36] Naar eigen zeggen was hij in die kunst aardig bedreven geraakt. Zo wist hij dankzij een bezoek aan het ‘Reichskommissariat’ in Den Haag zijn werknemers te vrijwaren van de arbeidsdienst.

### Stuurmanskunst
De positie van Folkers was vergelijkbaar met die van een burgemeester in bezettingstijd. Hij moest schipperen om het bedrijf door de tijd heen te helpen, maar men kan zich niet aan de indruk onttrekken dat hij bij tijden te scherp aan de wind zeilde. Vanouds deed Brill veel zaken met Duitsland en zaten in het fonds veel Duitse boeken. Door het wegvallen van de verbindingen met andere landen werd Duitsland vanaf 1941 als afzetgebied steeds belangrijker. Een bijkomend gevolg van de bezetting was dat geheel ‘Groot-Duitschland’ nu tot de thuismarkt van Brill behoorde.

Folkers maakte enige reizen naar Duitsland om de verkoop te bevorderen en opdrachten binnen te halen. Uit de contacten die hij legde vloeiden uitgaven voort zoals een indologisch werk in opdracht van de universiteit van Marburg en een uitermate succesvol _Handwörterbuch des Islam_. Dergelijke wetenschappelijke publicaties lagen in het verlengde van het vooroorlogse patroon van Brill en konden het daglicht verdragen. Zeer winstgevend bleek ook de levering van boeken aan Duitse instellingen, in sommige gevallen bibliotheken die tengevolge oorlogsschade opnieuw moesten worden ingericht. De platgebombardeerde Beierse Staatsbibliotheek in München bleek een uitstekende klant. Zulke leveranties konden met enige goede wil worden beschouwd als een pragmatisch inspelen op de omstandigheden. Het antiquariaat van Brill beleefde dankzij de sterk toegenomen vraag van Duitse bibliotheken gouden tijden.

In bepaalde gevallen neigde Folkers’ pragmatisme echter naar een cynisch dan wel naïef opportunisme. Zo aanvaardde hij van het ‘Dolmetscher Büro’ in Berlijn een order voor een Russisch leerboek met een oplage van 15.000 stuks. Voor het ‘Sprachmittelverlag’, eveneens in Berlijn, drukte hij 5000 exemplaren van het _Hilfsbuch für Wehrmachtssprachmittler_, dat Duitse officieren onder meer instrueerde in de distinctieven en wapens van het Russische leger. De opdrachtgevers waren aan de Duitse overheid gelieerde bedrijven, maar indirect waren dit opdrachten voor het Duitse leger. In opdracht van het Nederlandse Oostinstituut vervaardigde Folkers een _Zakwoordenboek in drie talen: Russisch-Duitsch-Nederlandsch_ met een oplage van 10.000 exemplaren, waarvan de uitgever zelf 3000 stuks mocht verspreiden. Voor dezelfde opdrachtgever was sprake van een order voor een meertalig woordenboek (Nederlands-Duits-Russisch-Oekraïens-Pools) in een oplage van 5500 exemplaren, waaraan ook de Arbeiderspers wilde deelnemen.[^37] Het Oostinstituut was de nationaal-socialistische instantie die in

![](03-31.jpg)

<=.ill_03-31.jpg Souvenir uit de oorlog: een kogel tussen de documenten uit de bezettingstijd in het Archief Brill. AB/UBA =>

<=.pb 109 =>
Nederland ‘Arische’ kolonisten probeerde te ronselen voor het ‘Slavische Oostland’. Folkers liet zulke opdrachtgevers in het vage en gewaagde van ‘drukwerk voor derden’. Hij bekommerde zich niet al te zeer om de morele aspecten: ‘Met het oog op de tijdsomstandigheden zullen dit ongetwijfeld een paar bestsellers blijken te zijn. Ik stel er mij veel van voor’.[^38]


Folkers’ inspanningen leidden ertoe dat de omzet van Brill in de oorlogsjaren ongekende hoogten bereikte. In 1942 steeg deze tot f 447.000 en in 1943 werd voor het eerst in de geschiedenis de magische grens van een half miljoen gepasseerd (f 579.000). De omzet van 1944 lag in dezelfde orde, wat een verdubbeling betekende ten opzichte van het vooroorlogse recordjaar 1939. De nettowinst bedroeg in 1942 f 94.956 en in 1943 f 140.000. De aandeelhouders brachten hulde aan de directeur, die ondanks de slechte tijden wegen had gevonden om zulke schitterende resultaten te behalen.[^39] Het gevolg van Folkers’ beleid was echter dat de liquide middelen binnen het bedrijf in hoge mate van Duitse origine waren. Na de bevrijding zou dat leiden tot problemen, omdat zulke gelden toen werden aangemerkt als ‘vijandelijk’.

Dat kon Folkers niet voorzien, al voorzag hij eind 1943 wel dat Duitsland de oorlog zou verliezen. Ten behoeve van de commissarissen schreef hij een notitie waarin hij de ontwikkeling van Brill na de oorlog schetste. Hij voorzag een toekomst waarin Brill zou optreden als bemiddelaar tussen ‘Germanen en Engelschen’. Zijn opsomming van mogelijke projecten gaf aanleiding tot de voorspelling dat het bedrijf na de oorlog minstens drie keer zo groot zou worden - een prognose die zou worden bewaarheid.[^40] Folkers was per 1 juli 1944 tien jaar in dienst en zijn aanstelling werd verlengd voor vijf jaar. Weliswaar zou hij binnenkort vijfenzestig worden, maar de commissarissen verzochten hem aan te blijven totdat hij een opvolger had gevonden. Het zou echter allemaal anders lopen.

![](03-32.jpg)

<=.ill_03-32.jpg Abu Muhammad ‘Ali Ibn Hazm, _Halsband der Taube_. Vertaald door M. Weisweiler (1944). Niet alle Duitse wetenschappelijke uitgaven werden onder het Derde Rijk ideologisch bijgekleurd. De oriëntalist Weisweiler is vooral bekend als kenner van de Arabische boekbanden, waarvoor hij een typologie ontwierp. Hij vertaalde naast sprookjes een van de beroemdste teksten uit de Arabische letterkunde: het boek over liefde en verliefden, de _Tawq al-hamâma_ oftewel de halsband van de duif. Niet alleen de inhoud, ook de oorsprong van dit boek is uniek: het is overgeleverd in een enkel handschrift dat in het midden van de zeventiende eeuw door de gezant Levinus Warner werd gekocht in Istanbul. Het manuscript bevindt zich sinds 1665 in het Legatum Warnerianum van de Leidse Universiteitsbibliotheek. Toen het boek in 1942 voor het eerst door Brill werd uitgegeven, verwachtte Folkers er weinig van. Tot zijn verbazing bleek het de bestseller van het jaar, waarvan binnen de kortste keren in Duitsland 17.000 exemplaren werden verkocht. Het afgebeelde exemplaar vermeldt ‘zweiundvierzigste unveränderte Auflage’, maar alleen de laatste drukken waren verschenen bij Brill. UBA =>

<=.pb 110 =>

![](04-01.jpg)
![](04-01bis.jpg)
![](04-01ter.jpg)

<=.ill_04-01.jpg Tijdsbeelden uit de jaren kort na de oorlog. Volgens de Amerikaanse auteur Fritz Sternberg (1948) was een verenigd Europa de beste manier om de Russen tegen te houden en een atoomoorlog te voorkomen. Die gedachte zou in het begin van de jaren vijftig ook ten grondslag liggen aan de Europese Economische Gemeenschap, de latere Europese Unie. Brill probeerde in 1947 een nieuwe marketing-strategie uit: geef de medemens voor de verandering eens geen roman ten geschenke, maar verblijd hem met een boek waar hij wat aan heeft. In 1949 verscheen bij Brill de Nederlandse vertaling van Trotsky’s biografie over Stalin. Een geruchtmakend boek, niet alleen vanwege de onthullingen over Stalin, maar ook omdat de in Mexico woonachtige Trotsky tijdens het schrijven werd vermoord door een handlanger van de Russische geheime dienst. AB/UBA =>

<=.pb 111 =>
# Het uitdijende universum van Brill: 1945-2008

## Nasleep van de oorlog en wederopbouw: Nicolaas Willem Posthumus, 1947-1958

### Exit Folkers
In het kader van de Bijzondere Rechtspleging werd kort na de bevrijding een landelijk onderzoek op touw gezet naar politieke en economische collaboratie. Op lokaal niveau werden daarvoor speciale politiediensten in het leven geroepen: de Politieke Recherche Afdelingen (PRA’s) voor de opsporing van politieke verdachten en de Politieke Recherche Afdelingen Collaboratie (PRAC’s) voor het onderzoek naar economische delicten.

De Leidse PRAC nam in april 1946 de boekhouding van Brill in beslag en constateerde de aanwezigheid van vijandelijke gelden in het bedrijf. Folkers’ beleid tijdens de oorlog werd door de PRAC opgevat als collaboratie met de bezetter. De zaak werd dermate ernstig bevonden dat in september de directeur in hechtenis werd genomen, een maatregel waartoe men in die dagen tamelijk gemakkelijk overging. Op 23 mei 1947 werd Folkers door de Zuiveringsraad voor de Uitgeverij voor een periode van twee jaar ontzet uit het recht een leidende functie uit te oefenen in de uitgeverij, de boekhandel, het antiquariaat of het drukkersbedrijf. Bovendien werd hem na een gerechtelijke uitspraak een boete opgelegd van f 10.000. Folkers was van mening dat de boete voor rekening kwam van de vennootschap, maar deze wees de verantwoordelijkheid van de hand: de directeur was op persoonlijke titel veroordeeld en de N.V. Brill stond ‘als zodanig geheel buiten het geding’.[^1]

De uitspraak van de Zuiveringsraad werd op 23 september 1947 bekrachtigd door de Centrale Zuiveringsraad voor het Bedrijfsleven, drie dagen voordat Folkers de leeftijd van 68 jaar bereikte. Op zijn eigen verzoek werd hem met ingang van 1 januari 1948 ontslag verleend - van het predikaat ‘eervol’ kon in de gegeven omstandigheden geen sprake meer zijn. Folkers was intussen ernstig ziek geworden en zou drie jaar later aan de gevolgen daarvan overlijden. Voor de man die zich met hart en ziel had ingezet voor Brill was het een hard gelag zijn loopbaan en zijn leven op deze wijze te moeten beëindigen. Ongetwijfeld had hij zich tijdens de oorlog bewogen in het schemergebied van samenwerking met de bezetter, maar hetzelfde kan worden gezegd van een groot deel van het Nederlandse bedrijfsleven.

<=.pb 112 =>
### Grijstinten
Hoe ‘fout’ was Folkers? Volgens de maatstaven van de Bijzondere Rechtspleging gold ‘het drukken en/of uitgeven van Duits propagandamateriaal en/of nationaal-socialistische geschriften’ als een strafbaar feit.[^2] Folkers had opdrachten uitgevoerd voor Duitse en Duitsgezinde instanties, maar woordenboeken en grammatica’s vielen niet in de gewraakte categorieën. Wél had hij ‘in een leidende positie in een onderneming den vijand hulp... verleend’, zoals het Besluit Zuivering Bedrijfsleven van 3 september 1945 het formuleerde. Degene die zich daaraan schuldig had gemaakt kon volgens datzelfde Besluit worden ontzet uit het recht een leidende functie uit te oefenen.[^3] Op grond van die algemene richtlijn moet men constateren dat Folkers een gerechte straf kreeg.

Zijn straf was in werkelijkheid zwaarder door het stigma van ‘foutheid’ dat uit zijn veroordeling door de Zuiveringsraad voortvloeide. De grijstinten in het schemergebied van economische collaboratie ontgingen aan de publieke opinie, die tot lang na de oorlog snelrecht pleegde in eenduidige termen van ‘goed’ of ‘fout’. Folkers had misstappen begaan tijdens de bezetting, maar was zeker geen oorlogsmisdadiger. Van lidmaatschap van de NSB of verwante organisaties was geen sprake. Zijn vergrijpen waren licht in vergelijking met andere voorbeelden van collaboratie. In zijn streven om de N.V. Brill groot te maken had hij wat te veel geschipperd.

Folkers’ houding tijdens de bezetting is niet boven kritiek verheven, maar de Bijzondere Rechtspleging is dat evenmin. Met name de ‘zuivering’ van het bedrijfsleven was een halfslachtige vertoning. Van meet af aan stond de noodzaak van wederopbouw op gespannen voet met de afrekening met het oorlogsverleden. Folkers was een van de velen die werden beschuldigd van economische collaboratie, getuige de 30.000 dossiers die de PRAC’s in korte tijd aanlegden. Hij was een van de weinigen die werden

![](04-02.jpg)

<=.ill_04-02.jpg Meesterzetter Arie Visser aan het werk. Foto rond 1950. AB/UBA =>

<=.pb 113 =>
veroordeeld, aangezien tweederde van de dossiers nimmer werd afgehandeld door de overbelaste Zuiveringsraden. De Bijzondere Rechtspleging ging gepaard met grote willekeur, terwijl de PRAC’s door hun amateuristisch optreden veel kritiek wekten. Na het topjaar 1947 liep de zuivering met een sisser af en reeds in de zomer van 1948 werden de PRAC’s opgeheven.[^4]

### Hooggeleerde uitgever
Ter vervanging van de gearresteerde Folkers werd op 16 september 1946 Nicolaas Willem Posthumus, lid van de Raad van Commissarissen, benoemd tot waarnemend directeur. Posthumus (1880-1960) was een markante figuur in het intellectuele leven van Nederland. Hij was de pionier van de economische en sociale geschiedenis, een nieuwe discipline binnen de historische wetenschap. In 1913 kreeg hij een benoeming als eerste hoogleraar Economische Geschiedenis aan de Nederlandsche Handelshogeschool in Rotterdam, de voorloper van de Erasmus Universiteit; in 1922 stapte hij over naar de Gemeentelijke Universiteit van Amsterdam, waar een soortgelijke leerstoel werd gecreëerd. Samen met kopstukken als J.M. Romein en W.A. Bonger behoorde hij tijdens het Interbellum tot de avant-garde van progressieve intellectuelen. Zijn voorliefde voor economische geschiedenis stond niet los van het historisch materialisme dat hij zich in zijn marxistische jonge jaren eigen had gemaakt. Zijn belangstelling richtte zich met name op Leiden, getuige zijn _Bronnen tot de geschiedenis van de Leidsche textielnijverheid_ (een bronnenpublicatie in zes delen, 1910-22) en zijn _Geschiedenis van de Leidsche lakenindustrie_ (dl. I, 1908; dl. II, 1939).

Naast een productief wetenschapper was Posthumus een onstuitbaar organisator, oprichter van verschillende instellingen die nog steeds bestaan. In 1914 stond hij aan de wieg van het Nederlandsch Economisch Historisch Archief in Den Haag, waarvan hij tot 1948 directeur en tot 1960 bestuursvoorzitter was. In 1935 was hij de man achter de oprichting van het Internationale Instituut voor Sociale Geschiedenis in Amsterdam, waarvoor hij onder meer het befaamde archief van Karl Marx en Friedrich

![](04-03.jpg)

<=.ill_04-03.jpg Rechts van het Leidse Gravensteen staat een mooi poortje, de voormalige toegang tot de rectorswoning van de Latijnse School aan de Pieterskerkgracht. De poort is in 1613 gemaakt door stadssteenhouwer Willem Claesz van (N)Es. In het timpaan staan de woorden ‘TUTA EST ÆGIDE PALLAS’ (Pallas, de godin van de wetenschap, is veilig achter haar schild). Mogelijk is dit de oorsprong van het achttiende-eeuwse motto van Luchtmans dat later werd overgenomen door Brill. =>

<=.pb 114 =>
Engels verwierf. Hij was in 1945 betrokken bij de oprichting van het Rijksinstituut voor Oorlogsdocumentatie (het tegenwoordige NIOD) en behoorde in 1947 met Jan Romein tot de geestelijke vaders van de nieuwe Faculteit der Politieke en Sociale Wetenschappen in Amsterdam. Door zijn toedoen werd in 1956 aan de Amsterdamse universiteit het Instituut voor het Midden en Nabije Oosten (IMNO) in het leven geroepen - een instelling die voortvloeide uit zijn werkzaamheid bij Brill, want in eerdere jaren viel de Arabische wereld buiten zijn gezichtsveld.


Posthumus was in het midden van de jaren dertig in aanraking gekomen met Brill. Hij publiceerde zijn werken gewoonlijk bij Nijhoff in Den Haag, waar Folkers tot 1934 werkzaam was als chef. Folkers haalde hem in 1936 over het _Economisch Historisch Jaarboek van het Nederlandsch Economisch Historisch Archief_ voortaan bij Brill te laten verschijnen. Ook de _International Review of Social History_, het tijdschrift van het nieuwe Internationale Instituut voor Sociale Geschiedenis, werd in 1936 bij Brill ondergebracht. Posthumus, die tijdens de bezetting ontslag nam als hoogleraar, publiceerde in 1943 zijn Nederlandsche Prijsgeschiedenis bij Brill. In datzelfde jaar nam hij zitting in de Raad van Commissarissen van de vennootschap, waar een vacature was ontstaan tengevolge van het overlijden van F.C. Wieder.

In 1949, kort voor zijn zeventigste verjaardag, ging Posthumus met emeritaat als hoogleraar. Vrijwel gelijktijdig, op 12 maart 1949, werd zijn waarnemende hoedanigheid bij Brill omgezet in een volwaardig directeurschap. De professor had de afgelopen jaren zijn hart verpand aan Brill en begon welgemoed aan een nieuwe loopbaan als uitgever. Posthumus was geen wereldvreemde geleerde - integendeel, hij had een scherp zakelijk inzicht en wist wat hij wilde. Met zijn prestige en zijn contacten in de wetenschappelijke wereld was hij de aangewezen figuur om gestalte te geven aan de wederopbouw van Brill. Frederik Casper Wieder jr., de zoon van de voormalige president-commissaris, werd aangesteld als adjunct-directeur en zou hem op termijn opvolgen.

Temidden van zijn zakelijke besognes koesterde Posthumus het voornemen om de geschiedenis van Brill op papier te zetten. Hij had de bronnen onder handbereik, aangezien het bedrijfsarchief destijds nog aanwezig was in het pand aan de Oude Rijn. Een betere geschiedschrijver had Brill zich niet kunnen wensen. Helaas moest de historicus wijken voor de directeur, die zijn handen meer dan vol had aan het leiden van het bedrijf.[^5]

### Nasleep
Ook Posthumus en Willem van Oordt, sinds 1943 voorzitter van de Raad van Commissarissen, moesten zich in het najaar van 1947 verantwoorden voor de Zuiveringsraad voor de Uitgeverij. Zij werden in hun hoedanigheid van commissarissen beticht van laakbare nalatigheid - met andere woorden, ze hadden Folkers op de vingers moeten tikken bij diens aanvaarding van penibele opdrachten. In geval van een veroordeling door de Zuiveringsraad hadden ook zij hun functies bij Brill moeten opgeven. Zij konden aannemelijk maken dat zij van niets hadden geweten en werden beiden vrijgesproken.

Posthumus deed op de aandeelhoudersvergadering van 1946 een poging om het bezoedelde blazoen van Brill wat op te poetsen. Hij vermeldde dat Folkers op listige wijze een poging van de Wehrmacht had verijdeld om de drukkerij te gebruiken voor

![](04-05.jpg)

<=.ill_04-05.jpg Letterproef van Brill uit de jaren vijftig. Coll. Brill =>

<=.pb 115 =>
haar publicaties. Hij verklaarde voorts dat ‘op vele andere wijzen, naar mij bekend is, de Duitsers werden verhinderd voor onze N.V. schadelijke plannen ten uitvoer te brengen’. Hij trad niet in bijzonderheden, maar noemde wel de levering van geld en papier aan het verzet.[^6]

De affaire-Folkers kreeg in 1949 een onaangenaam staartje. Bij controle van de in beslag genomen boekhouding constateerde de Vermogens Recherche Dienst dat de voormalige directeur een bedrag van f 48.818,48 niet had opgegeven als vijandelijk vermogen. In 1945 was een wet van kracht geworden die ondernemingen verplichtte gelden van Duitse origine te laten registreren. Folkers had met boekhoudkundige kunstgrepen geprobeerd dit geld te ‘witten’, teneinde het in het bedrijf te kunnen houden. Hij had eind 1944 een opdracht aanvaard van de ‘Reichsarbeitschaft Turkestan’ in Dresden - wederom een dubieuze instantie - voor het vervaardigen van een atlas. De opdrachtgever had een voorschot gegeven van bijna f 200.000, terwijl de rekening van Brill niet meer bedroeg dan f 150.000. Folkers had nagelaten het restant van het voorschot te restitueren, of had gemakshalve aangenomen dat zijn opdrachtgever in de ondergang van het Derde Rijk het loodje had gelegd.


Dit bedrag van bijna f 50.000 werd nu opgeëist door het Beheersinstituut, belast met de financiële afwikkeling van oorlogskwesties. De aanvankelijke vordering dreigde met andere posten en met een forse boete van 25% op te lopen tot f 78.000. Posthumus tekende protest aan tegen de boete, die was gebaseerd op het feit dat Brill nalatig was geweest in het aangeven van vijandelijke gelden. Zijn verweer was dat Folkers destijds op eigen houtje had gehandeld, met voorbijgaan van de commissarissen; nadien had men geen aangifte kunnen doen, aangezien de boekhouding door de PRAC in beslag was genomen. Verder voerde hij aan dat Brill ‘een wereldnaam als uitgeversfirma heeft,

![](04-06.jpg)

<=.ill_04-06.jpg ‘Erelijst’ uit 1953 met namen van werknemers die sinds 1940 vijftig, veertig, of vijfentwintig jaar in dienst waren bij Brill. AB/UBA =>

<=.pb 116 =>

![](04-07.jpg)

<=.ill_04-07.jpg Het oosters antiquariaat van Brill was sinds 1949 gevestigd op de Nieuwe Rijn nr.2. Het pand met de geschilderde visserman boven de deur stond in de volksmond bekend als de winkel van Brill. In de jaren vijftig werd het antiquariaat geleid door de heer Ritsema, in de jaren zestig door de formidabele juffrouw Gouda en vanaf 1970 door de legendarische Rijk Smitskamp. De laatste zette het na 1990 voor zijn eigen rekening voort onder de naam ‘Het Oosters Antiquarium’. Het oude pand met zijn nauwe trappen, gangetjes en boekenstellingen diende in 2001 als decor voor de film _De Ontdekking van de Hemel_ van Jeroen Krabbé, naar het boek van Harry Mulisch. De winkel werd in 2006 gesloten en het jaar daarop schonk Smitskamp het archief van Het Oosters Antiquarium aan de Leidse Universiteitsbibliotheek. Aquarel George Boellaard. UBL =>

<=.pb 117 =>
van hoge standing is en in de internationale geleerde wereld alom bekend... In geheel West-Europa is geen drukkerij en uitgeverij te vinden, die in drie en dertig talen drukken kan als zij dit doet. Het is daarom ook van cultureel belang voor ons land zelf, dat Brill niet door een hardhandige regeling te veel in haar levenskracht wordt aangetast’.

Het Beheersinstituut erkende de hoge standing van Brill en toonde zich bereid de levenskracht van het bedrijf te ontzien. De boete werd kwijtgescholden en de uiteindelijke vordering werd vastgesteld op f 57.000. Tweederde van dat bedrag kon worden verrekend met de vermogensaanwasbelasting, waarvoor Brill de afgelopen jaren te veel had afgedragen. Voor het restant werd een betalingsregeling getroffen met een looptijd van drie jaar.[^7]

### Magere jaren: 1945-1950
Na de bevrijding ging het beduidend slechter met het bedrijf dan tijdens de bezetting. De winsten liepen sterk terug in vergelijking met de oorlogsjaren. Werd in 1947 nog een winst van f 42.500 geboekt en in 1948 van f 52.451, in 1949 leed de N.V. Brill een fors verlies van bijna twee ton. De dividenden waren laag en werden in sommige jaren alleen maar uitgekeerd om bij de buitenwereld de schijn van welvarendheid hoog te houden. De uitgeverij slaagde erin de gebruikelijke dertig à veertig titels per jaar uit te brengen, maar de verkoop was gering. Vooral het wegvallen van de Duitse markt beperkte de afzet, terwijl ook de handel met andere landen in de eerste jaren na de oorlog moeizaam verliep. De strikte controle op deviezen vormde voor een internationaal opererend bedrijf als Brill een ernstige belemmering.

Terwijl de verkoop stagneerde, stegen de productiekosten van de uitgeverij en drukkerij. Posthumus schatte in 1948 dat deze kosten drie keer zo hoog waren als voor de oorlog. Papier was schaars en duur, nog afgezien van de moeilijkheid dat men voor deze grondstof afhankelijk was van de toewijzing door het Rijksbureau voor Papier. Het machinepark was na de oorlog dringend toe aan vervanging en uitbreiding, maar nieuwe machines waren amper te krijgen. In verband met de schaarste aan deviezen moest voor de aankoop van nieuw materieel bovendien een vergunning worden aangevraagd bij de overheid.

In 1947 vroeg Brill toestemming voor de aanschaf van een Monotype-installatie voor het zetten, bestaande uit een geheel van twee giet- en drie tikmachines. Het bedrijf kreeg slechts één giet- en twee tikmachines toegewezen, met als gevolg dat de nieuwe Monotype slechts op halve kracht kon werken. Brill diende in 1948 een aanvraag in voor een aanvullende tik- en gietmachine, die een jaar later werden geplaatst. De totale kosten van de nieuwe apparatuur bedroegen f 110.000, met inbegrip van een verbouwing. Balken werden versterkt en muren werden uitgebroken - men moest aan de Oude Rijn toen al woekeren met de beschikbare ruimte.


Gebrek aan ruimte was een reden om het pand Nieuwe Rijn nr.2 te huren, waar in september 1949 de boekhandel en het antiquariaat werden gehuisvest. Daarnaast koesterde men de hoop dat deze bedrijfsonderdelen door de verhuizing beter zouden gaan presteren, want beide waren sinds 1945 verliesgevend. Afgezien van dalende verkopen en stijgende kosten kreeg Brill in deze jaren te maken met nieuwe fiscale maatregelen van de overheid. De verhoging van de vennootschapsbelasting en

<=.pb 118 =>
de invoering van de vermogensaanwasbelasting verzwaarden de financiële druk op het bedrijf. De lage omzet en de hoge kosten droegen ertoe bij dat het bedrijf voortdurend kampte met liquiditeitsproblemen. Om meer financiële ruimte te scheppen werd in 1948 het aandelenkapitaal vergroot met f 100.000, maar die maatregel bleek niet afdoende tegen de geldschaarste. Bij tijden bedroeg het kastegoed niet meer dan f 20.000, veel te weinig voor de lopende uitgaven van het bedrijf. Posthumus zag zich gedwongen tot noodmaatregelen: hij maakte een groot deel van het Reservefonds te gelde en gaf daarnaast ‘investeringscertificaten’ uit op het Peltenburg Pensioenfonds. Het uitzetten van meer aandelen, onderhands dan wel via de Beurs, was in deze magere jaren geen haalbare kaart. In 1949 wendde Brill zich tot de Nationale Herstelbank met het verzoek voor een krediet van f 200.000. Gezien het belang van het bedrijf voor de Nederlandse export werd de lening goedgekeurd. De condities impliceerden wel dat de Herstelbank het financiële beleid van Brill in hoge mate controleerde.[^8]

### Duits-Amerikaans avontuur
Nadat in 1948 de Bondsrepubliek West-Duitsland tot stand was gekomen, werd de Duitse markt - althans het westelijk deel - weer min of meer toegankelijk. De Nederlandse overheid kreeg toestemming om in 1949 voor een bedrag van $100.000 boeken te exporteren naar de Bondsrepubliek, een quotum dat werd verdeeld over meerdere uitgeverijen. Brill kreeg $15.000 toebedeeld - weinig in vergelijking met de vroegere handel met Duitsland, maar voldoende om hoop te scheppen voor de toekomst. Het vooruitzicht op hernieuwde Duitse afzet verleidde het bedrijf tot een riskant avontuur.

![](04-08.jpg)

<=.ill_04-08.jpg De eerste vergadering van de redactiecommissie voor de tweede editie van de _Encyclopedie van de Islam_, gehouden in Leiden in april 1949. Geen van de heren zou de voltooiing van het werk meemaken. AB/UBA =>

<=.pb 119 =>
In het voorjaar van 1948 was Posthumus in contact gekomen met de Amerikaanse historicus James William Christopher, docent aan de universiteit van Oxford. Christopher bood Posthumus een manuscript aan over de Amerikaanse buitenlandse politiek in het Verre Oosten, dat in 1950 door Brill werd uitgegeven.[^9] Daarnaast bleek professor Christopher als overblijfsel uit een vorig leven te beschikken over een lege naamloze vennootschap, die geregistreerd stond in Wilmington, North Carolina. Posthumus was ten zeerste geïnteresseerd in de sluimerende firma van Christopher, die hij dacht te kunnen gebruiken als vehikel voor een vestiging van Brill in de Verenigde Staten. Overleg met Christopher leidde tot de verrassende gedachte dat men de lege Amerikaanse vennootschap ook kon ombouwen tot een instrument om in West-Duitsland een marktaandeel te veroveren.

Begin 1949 reisden Posthumus en Christopher een week lang door de nieuwe Bondsrepubliek om de markt te verkennen. Posthumus kwam terug met de stellige overtuiging dat de tijd rijp was voor een Duitse vestiging. Van zijn jongere reisgenoot - Christopher was destijds vijfendertig - had hij een gunstige indruk gekregen: ‘hij is zeker wat wild, maar vol plannen en ideeën die Brill wat goeds kunnen opleveren’.[^10] De Raad van Commissarissen stemde zonder morren in met Posthumus’ voorstel om een nevenbedrijf te vestigen in Heidelberg.

De organisatorische opzet was ingewikkeld: E.J. Brill Verlag Incorporated in Heidelberg viel niet onder de N.V. Brill in Leiden, maar onder de N.V. van Christopher in Wilmington, U.S.A. Brill participeerde voor $ 18.000 - destijds f 60.000 - in de onderneming, overeenkomend met drievijfde van het aandelenkapitaal. Christopher werd aangesteld als directeur van het bedrijf en zou naast een aardig salaris een winstaandeel krijgen. Posthumus en Van Oordt, president-commissaris van Brill, namen zitting in de Board of Directors van de Amerikaanse vennootschap - althans op papier, want tussen Leiden in Zuid-Holland en Wilmington in North Carolina lag de Atlantische Oceaan. Wellicht kwam de Amerikaanse dekmantel mede voort uit een schroom om vlak na de oorlog als Nederlands bedrijf in Duitsland te gaan opereren. Gezien de nadrukkelijke Amerikaanse aanwezigheid in de Bondsrepubliek bood de constructie ook strategische voordelen.

### Valse Brill
Vanaf juni 1949 was Christopher bezig het bedrijf op te zetten. Van Oordt reisde in 1950 een paar keer naar Heidelberg en was danig onder de indruk - de nieuwe vestiging had dertien personeelsleden en zag er piekfijn uit. Na verloop van tijd begon echter de twijfel toe te slaan. Christopher gaf geen inzicht in zijn boekhouding, ondanks herhaalde verzoeken van Posthumus. Brill-Leiden leverde op grote schaal boeken aan Brill-Heidelberg, maar de rekeningen werden niet betaald. Bovendien bleek Christopher heimelijk te onderhandelen met Elsevier om Brill-Heidelberg te laten optreden als Duitse vertegenwoordiger van die uitgeverij. Dat bericht schoot de heren in Leiden helemaal in het verkeerde keelgat, want Elsevier was in hun ogen ‘geen behoorlijke partner’ van Brill.[^11] \(Voor de goede orde: het ging niet om de Leidse firma Elzevier die in 1712 ter ziele was gegaan, maar om de uitgeverij die tegen het einde van de negentiende eeuw in Rotterdam was ontstaan en alleen de naam gemeen had met haar illustere voorganger\).

<=.pb 120 =>
Bij controle in Wilmington bleek Christopher zijn verplichtingen niet te zijn nagekomen, terwijl aan zijn naamloze vennootschap van alles mankeerde. Het vertrouwen in de Amerikaanse partner daalde in Leiden tot onder het nulpunt en in juli 1950 werd besloten de samenwerking met hem te verbreken. Posthumus en Wieder vertrokken begin augustus naar Heidelberg om orde op zaken te stellen. Tot hun schrik bleek Christopher intussen een nieuwe firma te hebben opgericht, die hij nota bene als ‘E.J. Brill Verlag G.m.b.H.’ wilde laten inschrijven in het plaatselijke handelsregister. Hij had namelijk in Heidelberg een zekere heer E.F.J. Brill leren kennen, die met weglating van zijn tweede naam ‘Franz’ bereid was op te treden als naamgever van het bedrijf. Door onmiddellijk een kort geding aan te spannen konden Posthumus en Wieder de schijnfirma Brill tijdig torpederen. Na het opnemen van de schade bleek het avontuur met professor Christopher een verlies van ruim dm. 100.000 te hebben opgeleverd.[^12] Brill-Heidelberg bleef bestaan, maar werd omgezet in een filiaal dat ondergeschikt was aan Brill-Leiden (een zogenaamde ‘Zweigstelle’). Met ingang van 1953 werd het filiaal van Heidelberg verplaatst naar Keulen, waar de dochteronderneming weer meer autonomie kreeg.

Christopher liet het er intussen niet bij zitten en spande een rechtzaak aan die zich tot in lengte van jaren voortsleepte. Hij eiste van Brill de betaling van achterstallig salaris en maakte bovendien aanspraak op een royale schadevergoeding. In 1953 kwam zijn advocaat met een voorstel tot schikking, maar Brill was absoluut niet genegen daarop in te gaan. Een jaar later was ‘in de stand van zaken t.o.v. de heer Christopher generlei verandering gekomen’. In februari 1955 werd Brill door de rechtbank volledig in het gelijk gesteld. Christopher gaf de moed niet op en ging in hoger beroep. Om van

![](04-09.jpg)

<=.ill_04-09.jpg Het filiaal van Brill aan de Kettengasse in Heidelberg. Mogelijk is een van de personen voor de deur de oplichter Christopher. Naast de politie-agent is vaag het bordje met ‘E.J. Brill’ te zien. Coll. Brill =>

<=.pb 121 =>
het gezeur af te zijn accepteerde Brill in 1960 het schikkingsvoorstel van $ 3000, dat hij door zijn advocaat liet overleggen. Waarop de heer Christopher voorgoed verdween uit de annalen van Brill.[^13]

### Wederopbouw: 1950-1958
‘Indië verloren, rampspoed geboren’. Aldus de gangbare opvatting, maar voor Brill bracht het verlies van Indië een onverwachte voorspoed. Tijdens de Ronde Tafelconferentie van 1949 over de soevereiniteitsoverdracht aan Indonesië waren ook afspraken gemaakt over de wetenschappelijke boedelscheiding. Als uitvloeisel daarvan verwierf Brill enige lucratieve contracten met de nieuwe Republiek voor de inrichting van een aantal bibliotheken. De voornaamste partner was de Hatta Stichting, genoemd naar vice-president Mohammed Hatta. De grootschalige levering van boeken aan deze instelling zorgde enige jaren voor een ongekend hoge omzet. Het antiquariaat, na de oorlog weggezakt in een vegetatief bestaan, beleefde opnieuw gouden tijden. De boekenverkoop
aan de Hatta Stichting genereerde in 1950 een omzet van f 700.000 en in
1951 van f 780.000.

Daarnaast kwam een overeenkomst tot stand met het Ministerie van Onderwijs, Opvoeding en Cultuur van de Republiek. De hieruit voortvloeiende leveranties voor drie bibliotheken hadden over een periode van vier jaar een waarde van ruim drie ton.

![](04-10.jpg)

<=.ill_04-10.jpg De zetterij van Brill in de jaren vijftig. Coll. Brill =>

<=.pb 122 =>
Een van deze bibliotheken werd ter plaatse ingericht door de anarchist en schrijver Arthur Lehning (1899-2000), een vriend van zowel Posthumus als Hatta.[^14] Op aandringen van Posthumus probeerde hij Hatta te overreden de boekenimport na 1954 te verlengen, maar helaas waren de fondsen uitgeput. In Leiden droomde men al van een Brill-Jakarta, maar de plannen voor een Indonesische vestiging werden na die teleurstelling op de lange baan geschoven. Niettemin kan zonder overdrijving worden gezegd dat de Republiek Indonesië een belangrijke bijdrage leverde aan de wederopbouw van Brill.


De Indische ‘boom’ deed de totale omzet van de vennootschap stijgen tot een recordhoogte van meer dan anderhalf miljoen gulden. Na 1952 werden de baten uit Indonesië minder, maar die teruggang werd gecompenseerd door het aantrekken van de Europese economie. In de loop van de jaren vijftig steeg de omzet geleidelijk naar twee miljoen gulden in 1958. Over dezelfde periode vertoonde de winst een overeenkomstig beeld van gestage groei: van anderhalve ton in 1951 naar twee ton in 1958. Gemiddeld bedroeg de winst 10% van de omzet.

Brill kreeg in de jaren vijftig weer toegang tot de wereld. De verkoop in de Verenigde Staten werd steeds belangrijker en ook het aantal Amerikaanse opdrachten voor de uitgeverij nam toe. Posthumus ontwikkelde een netwerk van vruchtbare relaties met

![](04-11.jpg)

<=.ill_04-11.jpg Het personeel van Brill in de jaren vijftig. De vrouwen zaten op de voorgrond en waren duidelijk in opkomst. Coll. Brill =>

<=.pb 123 =>
Amerikaanse universiteiten en uitgevers.[^15] Ook de betrekkingen met diverse Europese landen werden geïntensiveerd. Brill presenteerde zich op wetenschappelijke congressen en internationale bijeenkomsten in Frankrijk, Duitsland, Zwitserland en Engeland. Spanje kwam in beeld dankzij een grote boekenleverantie aan het Instituto Hispano-Árabe in Madrid, een connectie die Posthumus een tijdlang de gedachte deed koesteren aan een filiaal in de Spaanse hoofdstad. Duitsland, of althans het westelijk deel daarvan, was binnen Europa het belangrijkste afzetgebied: een derde van Brills boekenverkoop werd gerealiseerd in de Bondsrepubliek.[^16]

### ‘Uitzettingsdrang’
Jaar op jaar sprak Posthumus in zijn jaarverslag van een ‘bevredigende vooruitgang’, hoewel hij het niet nodig vond de aandeelhouders meer dan 10% dividend uit te keren. Bovendien was niet alles rozengeur en maneschijn, want Brill had nog steeds te kampen met liquiditeitsproblemen. Door de groei van het bedrijf werden deze eerder groter dan kleiner - niet alleen de winsten stegen, maar ook de lonen van het personeel en de prijzen van machines en grondstoffen.

In zijn jaarverslag van 1953 bracht Posthumus deze paradox onder woorden als de spanning tussen de ‘benauwde behuizing’ en de ‘uitzettingsdrang’ van de firma. In letterlijke zin refereerde hij aan het krappe bedrijfspand aan de Oude Rijn, in figuurlijke zin bedoelde hij dat Brill te weinig financiële armslag had om zich te kunnen ontwikkelen. Het eigen vermogen op basis van aandelen bedroeg ruim tweeënhalve ton, terwijl volgens de directeur een half miljoen meer nodig was om de groei van het bedrijf veilig te stellen. Zoveel kapitaal kon niet worden binnengehaald door het uitgeven van nieuwe aandelen. De vennootschap was gedwongen in de loop van de jaren vijftig opnieuw aan te kloppen bij de Herstelbank en bij andere banken.

Ook probeerde Posthumus de liquiditeit te verbeteren door de drukkerij te laten werken voor derden. De Bataafsche Petroleum Maatschappij, beter bekend als de Shell, werd in de jaren vijftig een goede klant van de drukkerij. Het mes sneed aan twee kanten, want ook de uitgeverij kreeg opdrachten van Shell. De oliemaatschappij sponsorde een Engelse editie van de _Geschiedenis van de Koninklijke_ van de Utrechtse hoogleraar C. Gerretson, met als pendant een technische bedrijfsgeschiedenis door de eerder genoemde oliehistoricus R.J. Forbes.[^17]

### Doorbuigende zolders
Neonverlichting en een adresseermachine waren in 1950 de voorboden van een nieuw industrieel tijdperk. Naast de eerdere werden nog twee Monotypes aangeschaft, terwijl men overwoog om ook voor het Arabische en Hebreeuwse zetwerk over te gaan op de machine. Oude drukpersen werden vervangen door nieuwe met een grotere capaciteit. Elke volgende machine moest met passen en meten in het oude gebouw op zijn plaats worden gezet. De zolders bogen door onder het gewicht van het staande zetsel voor herdrukken en de groeiende voorraad boeken. Aanvullende opslagruimte werd gehuurd in De Burght en met hetzelfde doel werd een pand gekocht in de Joost van Zonneveldpoort. Tezamen met de gebouwen aan de Oude en de Nieuwe Rijn beschikte Brill nu over vier locaties in Leiden. Het pand Rapenburg nr.22, dat Peltenburg in 1916 had gekocht voor het antiquariaat, was voor de oorlog al weer afgestoten.

<=.pb 124 =>
Nieuw was ook dat bij de uitgeverij enige wetenschappelijk geschoolde redacteuren werkzaam waren. De classicus en assyrioloog B.A. van Proosdij bleek bovendien te beschikken over een zakelijk instinct en zou in 1958 adjunct-directeur worden.[^18] De slavist Wap zette een nieuwe afdeling op voor uitgaven in de Slavische talen. Tot dusverre werd om de zoveel jaar een catalogus uitgebracht voor het gehele niet-oosterse fonds, maar men stapte nu over op vaker verschijnende catalogi per vakgebied. De door Folkers opgerichte exploitatie-afdeling verzond op grote schaal prospectussen en folders om de wereld te attenderen op het aanbod van Brill; in 1952 gingen welgeteld 67.850 ‘booklists’ en 77.000 ‘weekly lists’ de deur uit. Gezien de hoeveelheid uitgaande post resulteerde de verhoging van de portoprijzen in 1957 in een forse kostenverzwaring van f 7500. Posthumus hield van cijfers en placht dergelijke zaken nauwkeurig uit te rekenen.

Hij berekende ook de snelheid waarmee de investeringen in eigen uitgaven werden terugverdiend. Door de trage omloopsnelheid van wetenschappelijke boeken duurde het volgens hem gewoonlijk vier jaar voordat een kostendekking van 75% was bereikt. Jaarlijks werd 20% op het fonds afgeschreven en ook de opslag bracht kosten met zich mee, zodat het twijfelachtig was of het laatste kwart ooit werd gedekt. De onvoordelige ratio was Posthumus een doorn in het oog en hij streefde naar maximale kostendekking tijdens het eerste jaar; met andere woorden, van elke uitgave moesten vlak na de verschijning zoveel mogelijk exemplaren worden verkocht. Hij ontwikkelde een rekenmodel voor het dekkingspercentage van de uitgavekosten en gebruikte dat als maatstaf voor de rentabiliteit.

![](04-12.jpg)

<=.ill_04-12.jpg ‘Wir drucken und verlegen in allen Sprachen der Welt’: de stand van Brill op de Buchmesse in Frankfurt in 1960. Coll. Brill =>

<=.pb 125 =>
### Werk in uitvoering
De uitgeverij bracht in de jaren vijftig gemiddeld zestig titels per jaar uit, ongeveer twee keer zoveel als in vooroorlogse jaren. Het aantal tijdschriften bedroeg ruim dertig, waaronder oude getrouwen als _T’oung Pao_, _Mnemosyne_ en het _Tijdschrift voor Nederlandsche Taal- en Letterkunde_. De samenstelling van het fonds was niet wezenlijk veranderd. Brill ging voort op de ingeslagen weg, gebruikmakend van het eigen verleden en van de mogelijkheden in het heden. De continuïteit kwam tot uitdrukking in de oriëntalistiek in ruime zin, die het handelsmerk bleef van de uitgeverij. Dankzij een nieuwe reeks tekstedities waren de klassieke letteren meer geprononceerd aanwezig dan voorheen. Religiestudies hadden zich ontwikkeld tot een vast bestanddeel van het fonds en als vanouds was de historiografie een belangrijke component. Posthumus’ oude kennis Jan Romein publiceerde enige werken bij Brill: _Aera van Europa: de Europese geschiedenis als afwijking van het algemeen menselijk patroon_ (1954) en _De eeuw van Azië_ (1956). Gezien de titels was Romein zich bewust dat de tijden aan hetvveranderen waren.

Nieuw voor de uitgeverij en ook voor de drukkerij waren enige uitgaven in brailleschrift - vreemde tekens die pasten binnen het stramien van Brill. De _Encyclopedie van de Islam_ was een erfenis uit het verleden die zich inmiddels leende voor vernieuwing. In 1948 werd voor het eerst gesproken over een nieuwe editie en een jaar later vond in Leiden de eerste vergadering plaats van de arabisten die de internationale redactie samenstelden. Men ging uit van hetzelfde formaat als de vooroorlogse uitgave, namelijk vier delen met een supplement. De tweede druk zou verschijnen in het Engels en het Frans, met voorbijgaan van een Duitse uitgave. De prognose was dat de klus binnen

![](04-13.jpg)

<=.ill_04-13.jpg Een gezelschap van buitenlandse wetenschappers, vermoedelijk indologen, op bezoek in de directiekamer van Brill aan de Oude Rijn. Zittend in het midden Posthumus; achter hem, met donkere bril, de latere adjunct-directeur B.A. van Proosdij; rechts van deze de adjunctdirecteur, later directeur F.C. Wieder jr. Foto rond 1955. Coll. Brill =>

<=.pb 126 =>
tien jaar geklaard kon worden, maar dat bleek rijkelijk optimistisch. De eerste losse aflevering verscheen in 1953 en pas in 1960 zag het eerste gebonden deel het licht. Het zou uiteindelijk duren tot 2006 voordat de tweede editie van de encyclopedie in veertien delen werd voltooid. Wederom begon Brill aan een monumentale productie met een onvoorziene looptijd van meer dan een halve eeuw. Wel verscheen in 1953 als smaakmaker _The shorter Encyclopaedia of Islam_ van de Oxfordse hoogleraar H.A.R. Gibbs, op grond van een contract dat Folkers al voor de oorlog had gesloten. Het boek
beleefde vier drukken, de laatste in 2001 onder de titel _Concise Encyclopaedia of Islam_.

### Einde van een tijdperk
Het weeshuis aan de Oude Rijn was niet langer in staat de toename van personeel en materieel te verstouwen. Vanaf 1954 werd al gesproken over de bouw van een nieuw bedrijfspand voor de drukkerij, dat op den duur ook onderdak moest bieden voor de uitgeverij. Het benodigde kapitaal kon maar ten dele uit eigen middelen worden gehaald. Zowel de Amsterdamsche Bank als de Utrechtse Hypotheekbank waren bereid op grond van de bedrijfsresultaten van Brill krediet te verstrekken; de medewerking van de laatste bank werd bevorderd door het feit dat haar directeur J.J. van den Broek onlangs bij Brill was aangetreden als commissaris. Onderhandelingen met de gemeente Leiden resulteerden in het najaar van 1958 in de aankoop van een terrein van bijna een hectare aan de Trekvlietweg, aan de zuidelijke rand van de stad. Posthumus verrichtte het voorwerk voor de nieuwbouw, maar liet de afronding over aan anderen. Met ingang van 1 april 1958, kort na zijn 78ste verjaardag, trad hij af als directeur. In tegenstelling tot andere ‘troonswisselingen’ bij Brill was deze keer de opvolging goed geregeld: F.C. Wieder jr., sinds jaar en dag adjunct-directeur, werd in zijn plaats aangesteld en B.A. van Proosdij werd benoemd tot adjunct-directeur.

Posthumus, die aan het bedrijf verbonden bleef als adviseur, was van plan zich verder wijden aan de geschiedschrijving. Hij had een thema gevonden dat zijn oude liefde voor economische geschiedenis paarde aan de belangstelling voor de oosterse wereld die hij bij Brill opgedaan. Hij had de afgelopen jaren een reeks _Economic and Social History of the Orient_ opgezet, die naar schatting een stuk of dertig monografieën zou omvatten - een ambitieus internationaal project. Ter voorbereiding daarvan richtte hij in 1957 het _Journal of the Economic and Social History of the Orient_ op, dat tot 1996 door Brill werd uitgegeven. Dezelfde historische invalshoek lag ten grondslag aan het Instituut voor het Midden en Nabije Oosten van de Universiteit van Amsterdam, waartoe Posthumus in 1956 de aanzet gaf. Het IMNO zou zich richten op de geschiedenis van het Oosten, terwijl de Leidse universiteit meer de taalkundige traditie van de arabistiek cultiveerde.

Voorlopig kwam de historicus niet toe aan de sociaal-economische geschiedenis van de Arabische wereld, want hij wilde eerst het tweede deel van zijn Nederlandse Prijsgeschiedenis afmaken. Op 26 februari 1960, ter gelegenheid van zijn tachtigste verjaardag, werden hem de eerste drukproeven aangeboden. Helaas zou hij de publicatie niet meer meemaken. Posthumus overleed enige maanden later, op 18 april 1960, en zijn laatste werk verscheen postuum bij Brill.[^19]

![](04-14.jpg)

<=.ill_04-14.jpg Wisseling van de wacht in 1958. AB/UBA =>

<=.pb 127 =>
## Uitbundige groei: Frederik Casper Wieder, 1958-1979

### Geen Luchtmansstraat
Evenals de Nederlandse economie in haar geheel beleefde Brill in de jaren zestig en zeventig van de vorige eeuw een periode van sterke expansie.[^20] De omzet steeg van twee miljoen gulden in 1958 naar een gemiddelde van ruim twaalf miljoen in de tweede helft van de jaren zeventig. Afzonderlijke bedrijfsonderdelen haalden nu een veel hogere omzet dan het maximum van het hele bedrijf in het recente verleden: zo bedroeg de omzet van de uitgeverij alleen in 1972 bijna 5,5 miljoen gulden en die van de drukkerij 4,5 miljoen. De winsten van de vennootschap in haar geheel liepen in sommige jaren op tot zes à zeven ton - ongekend hoge bedragen voor Brill, maar lager dan men op grond van de sterk gestegen omzet zou verwachten. De loon- en prijsspiraal had gedurende deze decennia een merkbaar effect op de bedrijfshuishouding. Niettemin was de groei van het bedrijf groot genoeg om de inflatie voor te blijven. Het aandelenkapitaal nam in deze periode toe van f 260.000 tot f 860.000; in 1965 werd voor een ton aan nieuwe aandelen uitgegeven en in 1972 vond een grote emissie plaats ter waarde van een half miljoen.

In het voorjaar van 1960 ging de eerste paal in de grond voor de nieuwe drukkerij. Bij de aanbesteding bleek dat het aanvankelijke ontwerp te duur uitviel, zodat op de uitvoering moest worden beknibbeld. Met de bouw en inrichting van de drukkerij was

![](04-15.jpg)

<=.ill_04-15.jpg In het voorjaar van 1961 verhuisde de drukkerij van Brill naar het nieuwe gebouw aan de Plantijnstraat. De uitgeverij zou pas in 1985 volgen. Coll. Brill =>

<=.pb 128 =>
in totaal een bedrag van een miljoen gulden gemoeid, waarvan dankzij reserveringen in voorgaande jaren de helft uit eigen middelen kon worden betaald. Het nieuwe gebouw stond in een onbestemd niemandsland, aan een naamloze zijstraat van de Trekvlietweg. Brill deed de gemeente een aantal mogelijke straatnamen aan de hand, die geënt waren op de rijke boekhistorie van Leiden. De Luchtmansstraat werd jammer genoeg afgewezen, de Raphelengiusstraat was een hele mondvol en zo werd het uiteindelijk de Plantijnstraat.

De verhuizing vond plaats in de maanden mei en juni van het jaar 1961. De plannen om de nieuwbouw uit te breiden met opslagruimtes en een kantoor voor de uitgeverij werden een jaar later op de lange baan geschoven. De uitgeverij bleef voorlopig aan de Oude Rijn, waar dankzij de verhuizing van de drukkerij plaats genoeg was. De Chinese zetterij bleef aanvankelijk in het oude bedrijfspand, maar verhuisde in 1964 ook naar de Plantijnstraat. De beschikbare investeringsruimte werd gebruikt om de productiecapaciteit van de drukkerij zoveel mogelijk te vergroten. De uitgeverij kon alleen maar groeien voorzover de drukkerij de aanzwellende stroom opdrachten kon verwerken.

### Drukken in Bombay
Ondanks de investeringen in nieuw materieel bleef de drukkerij kampen met achterstanden. Het grootste probleem was het aantrekken van gekwalificeerd personeel, niet alleen door krapte op de arbeidsmarkt maar ook tengevolge van de woningnood. De gemeente maakte in de jaren zestig weinig haast met woningbouw, terwijl typografen van elders alleen naar Leiden wilden komen als ze woonruimte kregen aangeboden. Door het invoeren van een winstdeling voor het gehele personeel probeerde Brill in 1961 het bedrijf aantrekkelijker te maken voor nieuwe werknemers. Niettemin hadden de drukkerij en zetterij een chronisch personeelstekort, met paradoxale gevolgen: bij gebrek aan handzetters moesten in 1965 geregeld zetmachines worden stilgezet, omdat de machinezetters de achterstanden bij handmatig zetwerk moesten wegwerken. De omkering van de technologische vooruitgang was alleen mogelijk dankzij het feit dat de machinezetters het oude handwerk nog hadden geleerd.

Vanwege het tekortschieten van de eigen capaciteit werd een deel van het drukwerk uitbesteed aan een drukkerij in Nijmegen. Verder nam men zijn toevlucht tot Belgische bedrijven, die voor de helft van de prijs bleken te werken en dus een aanzienlijke besparing opleverden. Een Arabische uitgave werd in 1963 zelfs uitbesteed aan een drukkerij in Bombay, die kennelijk beschikte over de expertise voor zulk zetwerk. Het capaciteitsprobleem was voor Wieder rond 1970 aanleiding om te gaan experimenteren met het zetten op een digiset-machine bij een bedrijf in Amsterdam. Het zetten werd in dat procédé vereenvoudigd tot het intikken van ponskaarten of magneetbanden, met behulp waarvan een mainframe-computer het drukproces aanstuurde. De digitale techniek stond in de kinderschoenen en vormde voorlopig geen alternatief voor zetwerk in lood of voor offset. Niettemin voorzag Wieder een toekomst waarin alle zetwerk op de computer zou worden verricht en waarin dat apparaat ook anderszins een grote rol zou spelen in het bedrijf. Sneller dan hij verwachtte zou die prognose worden bewaarheid. Reeds in 1979-80 schafte Brill vijf ‘Compugraphics’ aan voor het digitaal fotozetten.

![](04-16.jpg)

<=.ill_04-16.jpg Besluit van de gemeente Leiden van 7 juli 1961, inhoudende dat de naamloze straat tussen Trekvlietweg en Kanaalweg de Plantijnstraat zal worden genoemd. Coll. Brill =>

<=.pb 129 =>
### Gestroomlijnde traditie
Gaf Brill in het begin van de jaren zestig ongeveer 70 titels per jaar uit, in de tweede helft van de jaren zeventig was dat aantal gestegen tot een gemiddelde van 175. Evenals Posthumus en Folkers placht Wieder op de jaarlijkse vergadering van aandeelhouders de uitgaven van het voorgaande jaar op te sommen, maar in 1970 hield hij daarmee op - het aantal was simpelweg te groot geworden. De toename valt niet alleen te verklaren uit de intrinsieke groei van het bedrijf en de verbeterde afzetmogelijkheden, vooral in de Verenigde Staten. Brill kreeg ook steeds meer manuscripten aangeboden, omdat andere uitgeverijen steeds minder belangstelling hadden voor gespecialiseerde werken in kleine oplagen. De winstbelustheid van uitgevers in tijden van hoogconjunctuur versterkte de ‘niche’ waarin Brill zich vanouds had genesteld.

Sinds de oertijd had Brill een sterke wetenschappelijke component in zijn fonds, maar gedurende deze twee decennia kreeg de uitgeverij een exclusief wetenschappelijk karakter. De uitgaven werden ondergebracht in series monografieën die ook het huidige fonds kenmerken, zij het dat sommige intussen van naam zijn veranderd. Om te volstaan met een willekeurige greep: ‘Philosophia Antiqua’, ‘Old’ en ‘New Testament Studies’, ‘Documenta et Monumenta Orientis Antiqui’, ‘Studies in the History of Religions’, ‘Studies in Judaism’, ‘Orientalia Rheno-Trajectina’, ‘Sinica Leidensia’, ‘International Studies in Sociology and Social Anthropology’ en wat dies meer zij - zelfs het aantal series is te groot om ze allemaal op te noemen. De meeste van deze reeksen waren gekoppeld aan wetenschappelijke tijdschriften, in de onderhavige periode ongeveer dertig in getal.


Gezien de groeiende omvang van de productie werd ook de wetenschappelijk staf van de uitgeverij uitgebreid. Na zijn pensionering als adjunct-directeur in 1965 bleef B.A. van Proosdij nog enige jaren werkzaam als algemeen redacteur. T. Edridge, die in zijn plaats werd aangesteld als adjunct-directeur, was verantwoordelijk voor het klassieke fonds, terwijl F. Th. Dijkema zich belastte met de arabistiek. In de toenemende professionalisering werd de traditie van Brill toegespitst, zonder dat het eigen karakter verloren ging. Integendeel, de uitgeverij zette het meest uitgesproken aspect van haar traditie om in haar ‘core-business’ en sloeg munt uit haar verleden.

Wieder gaf herhaaldelijk te kennen dat de uitgeverij zich moest richten op ‘goedlopende series, vervolg- en standaardwerken’. In de laatste categorie viel uiteraard het eeuwige ‘werk in uitvoering’ aan de nieuwe editie van de _Encyclopedie van de Islam_, waarvan tussen 1960 en 1978 vier delen verschenen. Iets minder ambitieus van opzet was het _Handbuch der Orientalistik_ van B. Spuler e.a., dat in acht delen werd uitgegeven tussen 1952 en 1988. Een andere monumentale reeks was de negendelige _Geschichte des arabischen Schrifttums_ (1967-84) van de Turkse auteur Mehmet Fuat Sezgin. In 1969 verscheen het zevende en laatste deel van _Concordance et indices de la tradition musulmane_, in de jaren dertig begonnen door A.J. Wensinck en voltooid door de arabist J. Brugman, tevens commissaris bij Brill.[^21] Bij zulke ondernemingen kan men alleen maar constateren dat Brill een eigenaardige voorliefde had voor projecten waaraan andere uitgevers met een bocht voorbijliepen.

![](04-17.jpg)

<=.ill_04-17.jpg In 1959 bracht Mohammed Reza Pahlavi, sjah van Perzië, een staatsbezoek aan Nederland. Tijdens een receptie in het paleis op de Dam in Amsterdam werd hem een in rood leder gebonden exemplaar aangeboden van het zojuist verschenen _Archéologie de l’Iran Ancien_ (1959; 2de ed. 1966). Van links naar rechts: sjah Mohammed Reza, auteur L. van den Berghe, directeur F.C. Wieder jr. en adjunct-directeur B.A. van Proosdij. AB/UBA =>

<=.pb 130 =>
### Roofdrukken
De _Concordance_ is een compilatie van de orale traditie van de islam, die voor moslims bijna even belangrijk is als de Koran zelf. In de Arabische wereld bestond dan ook veel belangstelling voor het boek, dat in zijn oorspronkelijke uitvoering echter zeer duur was. Reeds in 1970, amper een jaar na de voltooiïng van het origineel, werd in Beiroet een roofdruk van de _Concordance_ uitgebracht. Zodra dit nieuws was doorgedrongen in Leiden, stapte Joop van der Walle, hoofd van Brills boekhandel, op het vliegtuig naar Libanon. Een Leidse arabist vergezelde hem om als tolk op te treden. Van der Walle slaagde erin de editie door de rechter te laten verbieden en het restant op te kopen, wat met inbegrip van een dure advocaat een strop opleverde van een halve ton. De zeven delen kostten bij Brill f 2300, terwijl de illegale kopie voor f 600 van de hand ging. De Libanese roofdruk werd verscheept naar Nederland, maar werd niet vernietigd - integendeel, Brill probeerde de schade te herstellen door de kwalitatief slechte uitvoering te slijten als een goedkope ‘studenteneditie’.

Dankzij de offsettechniek was het gemakkelijk een fotomechanische herdruk van de _Concordance_ te maken. In dit geval waren gerechtelijke stappen mogelijk, maar bij werken waarvan het copyright na vijftig jaar was verstreken had men in juridisch opzicht geen been om op te staan. Tegen de illegale editie van de _Bibliotheca Geographorum Arabicorum_

![](04-18.jpg)

<=.ill_04-18.jpg Drukker bij Brill, midden jaren zestig. Coll. Brill =>

<=.pb 131 =>
(1870-1894) van M.J. de Goeje bijvoorbeeld kon Brill zich moeilijk verweren. Om te voorkomen dat anderen aan de haal gingen met oudere werken uit het fonds ging de uitgeverij uit eigen beweging over tot enige herdrukken, eveneens in de vorm van fotomechanische reprints. Zo verscheen preventief een nieuwe uitgave van de _Annalen van al-Tabarî_ van M.J. de Goeje, oorspronkelijk uitgebracht in veertien delen tussen 1851 en 1876. Dezelfde gedachte lag ten grondslag aan de iets latere herdruk van de eerste editie van de _Encyclopedie van de Islam_, waarvan het copyright in 1986 afliep en waarvan in het Midden-Oosten al een roofdruk was opgedoken. Overigens haalde de
actie in Libanon weinig uit, want naar verluidt circuleren in de Arabische wereld tientallen roofdrukken van de _Concordance_. Reprints waren ook nadelig voor het antiquariaat, omdat zeldzame oude werken hun waarde verloren.

### British Brill
Het antiquariaat was het minst stabiele bedrijfsonderdeel van Brill. De omzet kon het ene jaar een miljoen bedragen en het volgende teruglopen tot de helft, afhankelijk van de mogelijkheden van in- en verkoop. In samenhang daarmee fluctueerden ook de opbrengsten sterk: in 1976 een verlies van meer dan f 50.000, maar een jaar later een winst van bijna f 350.000. Sinds 1970 werd het antiquariaat geleid door Rijk Smitskamp, die het vanaf 1990 als zijn eigen onderneming zou voortzetten onder de naam ‘Het Oosters Antiquarium’.

Het filiaal in Keulen, de opvolger van dat in Heidelberg, was niet in staat zich te ontwikkelen tot het beoogde distributiecentrum voor de Duitse markt. Het sukkelde van jaar tot jaar verder met kleine verliezen dan wel kleine winsten, waarbij in 1971 bleek dat de Duitse bedrijfsleider vier jaar lang de cijfers had geflatteerd uit angst dat de dochteronderneming zou worden gesloten. De Slavische afdeling bleek op den duur niet levensvatbaar en werd in 1973 opgedoekt. In datzelfde jaar nam Brill de Londense boekhandel Bryce over, gevestigd in Museum Street (W.C.1) vlakbij het British Museum. De ‘nieuwe boekhandel’, die samen met het antiquariaat was gehuisvest aan de Nieuwe Rijn, werd in zijn geheel overgeheveld naar Brill-Londen. J. van der Walle, die in Leiden aan het hoofd stond van deze afdeling, verhuisde eveneens naar Londen.

Brill-Leiden bestond voortaan uit de uitgeverij (Oude Rijn), de drukkerij (Plantijnstraat) en het antiquariaat (Nieuwe Rijn). Tegen het einde van de jaren zeventig waren bij het moederbedrijf ongeveer honderdtwintig mensen werkzaam, bij Brill-Keulen zeven en bij Brill-Londen vijftien.

### Brill’s Weekly
Op 9 september 1967 verscheen het duizendste nummer van ‘Brill’s Weekly’. Enig
voorbehoud is hier op zijn plaats, want het is niet geheel duidelijk hoe de mijlpaal werd vastgesteld. Folkers was in 1938 begonnen met een ‘Weekly list of new books’, een enkel blad met titels dat in een oplage van paar honderd stuks werd verspreid. Tijdens de Tweede Wereldoorlog werd de Engelse boekenlijst stopgezet, maar vanaf 1946 werden in steeds grotere getale ‘Booklists’ en ‘Weekly lists’ de wereld in gezonden om nieuwe uitgaven aan te kondigen. Vanaf juni 1952 ging dat onder de noemer ‘Brill’s Weekly’, maar strikt genomen kon deze in 1967 niet aan zijn duizendste aflevering toe zijn. Bovendien werd de ‘Weekly’ in weerwil van zijn naam lang niet elke week

![](04-19.jpg)

<=.ill_04-19.jpg Folder voor de _Concordance_ (1992). Deze keer geen roofdruk, maar een herdruk door Brill. AB/UBA =>

<=.pb 132 =>

![](04-20.jpg)

<=.ill_04-20.jpg Het filiaal van Brill in Museum Street, London W.C.1. Coll. Brill =>

<=.pb 133 =>
uitgebracht. Kennelijk werden alle mogelijke voorgangers meegerekend om deze mijlpaal te bereiken.

Het eerste nummer was een simpele prospectus van Brills uitgaven, maar nr. 1000 had aanzienlijk meer allure. In de loop der tijd was de gewoonte ontstaan een aflevering te wijden aan een bepaald vakgebied en de relevante nieuwe literatuur te vermelden. De ‘Weekly’ noemde niet alleen de uitgaven van Brill over het desbetreffende thema, maar ook die van anderen. Het blad ontwikkelde zich in de loop van de tijd tot een internationaal gewaardeerd overzicht van onlangs verschenen wetenschappelijke werken. In plaats van een prospectus ter bevordering van de verkoop was de ‘Weekly’ veeleer een vehikel voor Brills reputatie als wetenschappelijke uitgeverij. De afleveringen werden zorgvuldig voorbereid en maanden voor de verschijning van een nummer was men al bezig met het verzamelen van titels. De thematisch geordende afleveringen boden de gebruikers een handzaam instrument met recente bibliografische informatie.

Het blad oversteeg het directe zakelijke belang van Brill en was een vorm van dienstverlening aan de wetenschap. Het was een kostbare aangelegenheid om de ‘Weekly’ te maken en te verspreiden, maar daar stond tegenover dat Brill veel eer inlegde met het blad. In de digitale revolutie is er geen plaats voor de ‘Weekly’ - het dure medium op papier is vervangen door digitale nieuwsbrieven met informatie over nieuwe boeken.

### Fusie of overname?
Brill gedijde in de jaren zestig en werd daardoor een aantrekkelijke partij voor fusie of overname. In mei 1965 deed M.D. Frank, directeur en eigenaar van de Noord-Hollandse Uitgevers Maatschappij, het voorstel om zijn bedrijf te fuseren met Brill. Aanvankelijk had Wieder daar wel oren naar, in tegenstelling tot de commissarissen. Naderhand draaiden de rollen om: de commissarissen neigden naar een fusie, terwijl Wieder zich steeds meer tegen het idee verzette. Op een vergadering in het begin van 1967 leidde de zaak tot een verhitte discussie. Een van de commissarissen verbond zijn aanblijven aan het doorgaan van de fusie, terwijl een andere te kennen gaf dat Brill zich op den duur nooit als zelfstandig bedrijf kon handhaven. Op zijn beurt betoogde
Wieder dat een fusie door de onverenigbaarheid van beide uitgeverijen op niets

![](04-21.jpg)

<=.ill_04-21.jpg Brill’s Weekly nr.1000. AB/UBA =>

<=.pb 134 =>
zou uitlopen en dat ‘het karakter van Brill’ in de versmelting verloren zou gaan. Hij hield voet bij stuk en de commissarissen bonden in.[^22]

Het was de tijd waarin de grote conglomeraten ontstonden die de huidige Nederlandse uitgeverswereld kenmerken. De firma Elsevier nam destijds de ene uitgeverij na de andere over en sloeg ook een begerig oog op Brill. Vermoedelijk niet toevallig kwam dezelfde gedachte op bij concurrent Kluwer, die zojuist Tjeenk Willink had ingelijfd. Zo kon het gebeuren dat Wieder op dezelfde dag in januari 1969 vertegenwoordigers van beide uitgeverijen op bezoek kreeg, zij het niet gelijktijdig. Beiden lieten in bedekte termen weten dat hun respectievelijke broodheren interesse hadden voor het overnemen van Brill. Wieder hield de boot af en meldde de zaak voor de goede orde aan de commissarissen. Van Kluwer werd niets meer vernomen, maar Elsevier liet de moed niet varen en kwam eind augustus 1969 met een schriftelijk voorstel tot overname van Brill. Rond dezelfde tijd nam Elsevier de Noord-Hollandse Uitgevers Maatschappij over, wat kennelijk aanvullende informatie opleverde over de gevoeligheden bij Brill: in een tweede schrijven van vijf weken later was niet langer

![](04-22.jpg)

<=.ill_04-22.jpg Zetter achter de zetmachine, midden jaren zestig. Coll. Brill =>

<=.pb 135 =>
![](04-23NL.jpg)
![](04-24NL)

<=.ill_04-23NL.jpg Een van de meer excentrieke auteurs van Brill was de sinoloog en diplomaat Robert van Gulik (1910-1967), bij een breed publiek bekend als schepper van de detective-verhalen over Rechter Tie. Deze magistraat was een speurder die met veel vernuft allerlei ingewikkelde misdaden oploste in het oude China. Van Gulik haalde zijn inspiratie voor Tie uit een oud Chinees manuscript, waarvan hij een Engelse vertaling verzorgde. Deze verscheen in 1956 bij Brill als _T’ang-yin-pi-shih. Parallel cases from under the pear-tree. A 13th-century manual of jurisprudence and detection_. Spijtig dat de minder wetenschappelijke Rechter Tie niet door Brill werd uitgegeven, want Van Guliks detectives waren en zijn internationale bestsellers. Rechter Tie onstond kort na de oorlog, toen Van Gulik zijn kennis van China op een andere manier wilde gebruiken. De uitgever van het eerste verhaal over Tie wilde erotisch getinte prenten opnemen om de verkoop te bevorderen. Van Gulik maakte daarop een aantal houtsneden met pornografische afbeeldingen en kreeg de smaak te pakken: hij maakte een hele serie erotische plaatjes, die evenals Tie in een vaag Chinees verleden waren gesitueerd. Vervolgens gebruikte hij zijn pornografie voor een fraaie practical joke: in eigen beheer publiceerde hij _Erotic Colour Prints of the Ming Period_, waarin hij zijn eigen gravures liet doorgaan voor oude Chinese prenten. Het curiosum verscheen in 1951 in een oplage van vijftig exemplaren in Tokio.

Het begeleidende essay over de erotiek in het oude China vormde de opmaat van zijn studie _Sexual Life in Ancient China_, die in 1961 wél door Brill werd uitgegeven. In het voorwoord vertelde Van Gulik uitvoerig hoe hij de uiterst zeldzame gravures in een obscuur winkeltje met rariteiten had gevonden. Een unieke vondst! In _Sexual Life in Ancient China_ werd een aantal prenten uit
de _Erotic Colour Prints_ afgedrukt, maar de meer gewaagde konden in 1961 bij Brill niet door de beugel. De twee onderstaande afbeeldingen zijn afkomstig uit de authentieke en vervalste _Erotic Colour Prints_. De overeenkomst in stijl met de prenten van Rechter Tie zal de kenner niet ontgaan. UBA =>

<=.pb 136 =>
sprake van overname, maar van opname van Brill ‘in de federatieve organisatie van Elseviers wetenschappelijke uitgeverijen’.

Het voorstel leidde bij Brill tot soortgelijke discussies als enige jaren tevoren. De commissarissen waren van oordeel dat de zelfstandigheid in deze constructie was gewaarborgd en wezen op de voordelen van schaalvergroting. Wieder verzette zich resoluut tegen het opgaan in een grotere organisatie en hamerde op de identiteit van Brill. Hij was niet overtuigd dat Elsevier de zelfstandigheid van Brill zou respecteren, vermoedelijk niet ten onrechte. Hij hield ook deze keer het been stijf en andermaal bonden de commissarissen in. Elsevier verwierf bij de emissie van 1972 een aantal aandelen Brill, kennelijk met de bedoeling een vinger in de pap te krijgen. Het belang was echter te klein om de aandeelhoudersvergadering te kunnen manipuleren, des te minder omdat Brill als barrière tegen zulke bedreigingen een aantal prioriteitsaandelen in vertrouwde handen had geplaatst.[^23]

Intussen was ook een fusie met Samsom in Alphen aan de Rijn van de hand gewezen (deze uitgeverij fuseerde naderhand met Sijthoff).[^24] Het voornaamste effect van de interne discussie over fusie of overname was een versterking van het besef van Brills eigenheid. Bovendien was duidelijk dat het bedrijf sterk genoeg was om voort te gaan op zijn eigen weg.

## Krimp en groei: 1979-2008

### Jubileum
Wieder, die het uitdijende bedrijf meer dan twee decennia had geleid, ging in 1979 op de leeftijd van 68 jaar met pensioen. Evenals indertijd bij Posthumus’ afscheid was de wisseling van de wacht goed geregeld: Tom Edridge, redacteur van het klassieke fonds en sinds 1974 adjunct-directeur, werd aangesteld als opvolger van Wieder. Zijn Nieuw-Zeelandse afkomst vereiste een kleine aanpassing van de statuten, aangezien deze voorschreven dat alleen een Nederlander directeur van de vennootschap kon worden. Helaas overleed Edridge plotseling, nog geen jaar nadat hij de leiding van het bedrijf had overgenomen.

Wieder keerde tijdelijk terug, in afwachting van een nieuwe directeur. Kennelijk kon men binnenshuis geen geschikte kandidaat vinden, want de keuze viel enigszins verrassend op de Rotterdamse biologieleraar Wim Backhuys. Deze was gepromoveerd op een slakkenkundig onderwerp, wat hem in het spraakgebruik der biologen bestempelde tot een malacoloog. Hij had tevens een kleine uitgeverij met een fonds op het gebied van biologie en palaeontologie, dat als imprint werd overgenomen door Brill. In vroegere tijden had Brill zich ook op dat terrein bewogen, maar rond 1980 was het vrijwel uit het zicht verdwenen. Het was de bedoeling dat Backhuys de biologie nieuw leven zou inblazen. Hij deed dat met veel voortvarendheid, met als gevolg dat de biologie na verloop van tijd ongeveer een tiende deel van Brills uitgaven besloeg.

Backhuys kreeg het niet alleen druk met het heden, ook het verleden vroeg zijn aandacht. In 1983 vierde Brill zijn derde eeuwfeest, gerekend vanaf het jaar 1683 waarin Jordaan Luchtmans met zijn boekhandel was begonnen. De herdenking ging gepaard

![](04-25.jpg)

<=.ill_04-25.jpg Malacologie oftewel slakkenkunde. AB/UBA =>

<=.pb 137 =>
met de nodige festiviteiten. In het Leidse Gemeente-archief werd een tentoonstelling georganiseerd over de rijke geschiedenis van Luchtmans en Brill.[^25] Zes wetenschappers wijdden een beschouwing aan de historische verwevenheid van hun vakgebied met de uitgeverij. Hun bijdragen vormden een staalkaart van de belangrijkste componenten van het fonds van Brill: religiestudies, judaïca, assyriologie, egyptologie, arabistiek, islamitische studies, indologie, sinologie, japanistiek en klassieke oudheid. Backhuys
schreef een artikel over de vroegere uitgaven van Brill op biologisch gebied, de gelegenheid aangrijpend voor een _oratio pro domo_.

De bijdragen werden gebundeld in de herdenkingsuitgave _Tuta sub aegide Pallas_, die werd gepresenteerd op een bijeenkomst in de Hooglandse Kerk. Het eerste exemplaar van het boek werd aangeboden aan Wim Deetman, de toenmalige minister van Onderwijs en Wetenschappen. Ter gelegenheid van het jubileum publiceerde Brill ook een _Short-title Catalogue 1683-1983_, die met zijn opsomming van vijfduizend titels een beeld geeft van de indrukwekkende productie van driehonderd jaar.

Leiden gaf invulling aan drie eeuwen verbondenheid door Brill de gouden erepenning van de gemeente toe te kennen. Juister gezegd de ‘eerepenning’ - Leiden was niet scheutig met dit eerbewijs, getuige het feit dat de fraaie medaille van vooroorlogse kwaliteit was. Burgemeester C. Goekoop memoreerde in zijn toespraak dat Brill een ‘grote bijdrage heeft geleverd aan de werkgelegenheid in Leiden en aan de bekendheid van de stad in binnen- en buitenland’.[^26] Tenslotte dient vermeld te worden dat de aartsbisschop van Antiochië in het kader van de herdenking een bezoek aflegde bij Brill. De prelaat kreeg een exemplaar aangeboden van de vierdelige Bible in Aramaic
van A. Sperber.[^27] Het was een geschenk dat op passende wijze de geschiedenis van de uitgeverij tot uitdrukking bracht: een van de eerste uitgaven van Jordaan Luchtmans was het _Opus Aramaeum_ (1686) van Carolus Schaaf.

### Verhuizing
In het jaar 1983 vond nog een jubileum plaats, want het was precies een eeuw geleden dat Brill was verhuisd naar het voormalige weeshuis aan de Oude Rijn. Dit feit werd herdacht op andere wijze: men besloot dat het tijd was ook de uitgeverij te verhuizen naar de Plantijnstraat. Bij de drukkerij vond een verbouwing plaats om voldoende kantoorruimte te scheppen. Tegelijkertijd werd besloten een centraal magazijn voor de boekenvoorraad in te richten in Zoeterwoude, ter vervanging van de verschillende opslagruimtes in de stad.

In verband met de verhuizing moest het oude gebouw worden leeggemaakt, wat
door de accumulatie van goederen in de loop van een eeuw geen geringe operatie was. De verhuizing kreeg zijn beslag in 1985 en liet het oude bedrijfspand ontmanteld achter, met op de gevelstenen de naam ‘E.J. Brill’. Het gebouw vereeuwigt niet alleen de naamgever van het bedrijf, maar is tevens een tastbare herinnering aan Van Oordt en De Stoppelaar die hier de grondslagen legden voor de huidige uitgeverij. Meer dan dat, het is een monument voor allen die zich gedurende meer dan een eeuw voor Brill hebben ingezet. Bij benadering moet hun aantal enige duizenden bedragen - een volksstam van Leidenaars die met het bedrijf verbonden waren, vaak hun hele leven lang. Het lege gebouw deed de bedrijvigheid van de afgelopen honderd jaar in een verstilde vorm weerklinken. Leidse krakers, om minder poëtische redenen in die

![](04-26.jpg)

<=.ill_04-26.jpg De _Short-title Catalogue 1683-1983_, uitgegeven ter ere van het derde eeuwfeest. De eigenaar was zeer zuinig op zijn exemplaar. AB/UBA =>

![](04-27.jpg)

<=.ill_04-27.jpg ‘Eerepenning’ van de gemeente Leiden, aangeboden aan Brill ter gelegenheid van het derde eeuwfeest in 1983. Coll. Brill =>

<=.pb 138 =>
leegte geïnteresseerd, betrokken weldra de voormalige behuizing van Brill. Zij vestigden daar ondermeer een paddestoelenkwekerij en een winkel in tweedehands kleren.

### Vijandelijk spook
In datzelfde jaar 1985 maakte zich een grote paniek meester van Brill. Een mysterieuze investeerder, naar verluidde van Belgische herkomst, was bezig op grote schaal aandelen op te kopen met de bedoeling het bedrijf over te nemen. Deze spookachtige partij zou langs slinkse wegen reeds 70% van het aandelenpakket in handen hebben gekregen. Om een vijandelijke overname te voorkomen werd in allerijl een beschermingsconstructie opgetuigd onder de naam ‘Stichting Luchtmans’. De vennootschap kreeg het recht in geval van nood een hoeveelheid cumulatief preferente aandelen uit te geven, en wel tot een maximum van 50% van het totale kapitaal aan geplaatste aandelen. Deze preferente aandelen konden bij een dreigende overname worden overgeheveld naar de Stichting Luchtmans, opdat deze beschikte over voldoende stemrecht om indringers buiten de deur te houden.

Het was onnodig de Stichting Luchtmans in het geweer te brengen tegen de heimelijke opkoper die bijna driekwart van de aandelen Brill zou hebben verworven. Bij nader inzien

![](04-28.jpg)

<=.ill_04-28.jpg In 1985 verhuisde de uitgeverij naar de Plantijnstraat, waar de drukkerij al sinds 1961 was gevestigd. Het bedrijfspand aan de Oude Rijn 33a werd leeggehaald. Coll. Brill =>

<=.pb 139 =>
bleek sprake te zijn van een rekenfoutje. De buitenstaander had geen volwaardige aandelen van f 1000 gekocht, maar gesplitste aandelen van f 100. De dreiging nam in evenredigheid daarmee af, want zijn vermeende belang van 70% was in werkelijkheid niet groter dan 7%. Niettemin was de Stichting Luchtmans een beschermingsconstructie die enige jaren later haar nut zou bewijzen.


De uitbundige groei van de jaren zeventig zette zich niet voort in de jaren tachtig. De omzet stabiliseerde zich en lag gemiddeld iets lager dan in het voorgaande decennium. In 1981 boekte het bedrijf een onverwacht verlies van twee ton, maar dat werd ruim gecompenseerd door winsten van vier à vijf ton in volgende jaren; in 1984 en 1986 lag de winst zelfs in de orde van een miljoen. De productie van de uitgeverij bleef steken op hetzelfde peil als de jaren zeventig: per jaar verschenen ongeveer tweehonderd titels, terwijl daarnaast ruim dertig tijdschriften werden uitgegeven.

De drukkerij ontwikkelde zich in toenemende mate tot het zorgenkind van het
bedrijf. De stijgende kosten maakten een rendabele exploitatie van dit onderdeel in de jaren tachtig steeds moeilijker. Het handzetten in lood werd alleen nog toegepast voor uitgaven in het Chinees of Japans. De Mono- en Intertypes verdwenen uit de zetterij, want ook het machinaal loodzetten was een verouderde techniek geworden. Men schakelde over op digitaal fotozetten, terwijl in samenhang daarmee de drukkerij voornamelijk werkte met offset. Toen in 1987 de laatste Chinese handzetter M.W. Zandvliet met pensioen ging, kwam voorgoed een einde aan het loden tijdperk. Zetwerk in het Chinees of Japans
werd voortaan uitbesteed in het Verre Oosten. Ondanks de technische innovaties en een geleidelijk inkrimpend personeelsbestand bleef de drukkerij verliesgevend.

### Brill & Brown
Over het boekjaar 1987 leed Brill een onverwacht verlies van zeven ton. Negatieve resultaten kwamen in de annalen van de vennootschap amper voor en een verlies van deze omvang was ronduit schokkend. Ten dele waren de rode cijfers te wijten aan macroeconomische overmacht: de lage koers van de dollar was nadelig voor een internationaal opererend bedrijf als Brill, te meer omdat de Verenigde Staten de grootste afnemer van boeken waren. Bovendien was de uitgeverij in deze tijd bezig met de invoering van automatisering, wat zoals elders gepaard ging met grote onkosten en onvermijdelijke mislukkingen. De drukkerij drukte de resultaten nog meer.

Daarnaast was het beleid van Backhuys van invloed op het verlies. De directeur had ingezet op nieuwe buitenlandse vestigingen ter bevordering van de afzet, hoewel weinigen binnen het bedrijf de noodzaak daarvan inzagen. De oudere vestigingen in Duitsland en Engeland waren nauwelijks rendabel en de nieuwe in Denemarken[^28] en de Verenigde Staten brachten voorlopig alleen kosten met zich mee. Hang naar globalisering verleidde Backhuys in 1987 tot het verwerven van een meerderheidsbelang in de Australische uitgeverij Robert Brown, à raison van een half miljoen gulden. Volgens de directeur zou de dochteronderneming ‘down under’ Brill een uitstraling bezorgen in Nieuw-Zeeland en Papoea Nieuw-Guinea, een regio die door hem werd omschreven als ‘een kweekvijver van prachtige antropologische en biologische literatuur’.[^29] Uitgeverij Robert Brown bleek een regelrechte miskoop: het bedrijfje gaf alleen reisgidsen uit en telde afgezien van de heer Brown één werknemer.

![](04-29.jpg)

<=.ill_04-29.jpg Souvenir van de Oude Rijn: het bordje dat de bezoeker de weg wees naar het kantoor. AB/UBA =>

<=.pb 140 =>

![](04-30.jpg)

<=.ill_04-30.jpg Het magnum opus van Brill: _The Encyclopaedia of Islam_. Over het ontstaan van deze klassieker valt een boek te schrijven. De plannen ontstonden rond 1895, maar pas in 1908 verscheen de eerste losse aflevering met de letter ‘A’. In 1913 werd het eerste gebonden deel uitgebracht. De eerste editie, in vier delen en in zowel een Franse, Engelse als Duitse uitgave, werd uiteindelijk voltooid in 1936. Kort na de Tweede Wereldoorlog vatte Brill het plan op voor een tweede editie, deze keer alleen in het Engels en Frans. Men ging uit van vier delen met een supplement en dacht de klus binnen tien jaar te klaren. Dat viel tegen: pas in 2006 werd de tweede editie voltooid, die met inbegrip van de index veertien delen omvatte. Deze prospectus voor de Amerikaanse markt toont het werk in uitvoering in het jaar 1996, toen acht delen van de tweede editie klaar waren. Inmiddels is Brill met de derde editie begonnen, waarvan in juni 2008 het vierde deel verschijnt. AB/UBA =>

<=.pb 141 =>
Velen bij Brill waren van oordeel dat Backhuys zijn biologische hobby’s uitleefde ten koste van het bedrijf. De kritiek had niet alleen te maken met zijn beleid, maar ook met zijn optreden. De directeur verkeerde in de veronderstelling dat hij in een ‘pittoresk, maar archaïsch museum’ terecht was gekomen waar hij de bezem doorheen moest halen.[^30] Dat lijkt geen adequate beschrijving van het hoogwaardige bedrijf dat Wieder had achtergelaten. Backhuys kon slecht uit de voeten met de staf, die hij beschouwde als een ‘oude garde’ die niet met haar tijd meeging en tegen hem samenspande. Op hun beurt waren deze hooggekwalificeerde werknemers weinig onder de indruk van de directeur of van de vernieuwing die hij voorstond. Ook bij de rest van het personeel zette Backhuys’ optreden kwaad bloed.

### Crisis
Het grote verlies van Brill over 1987 bracht de onderhuidse spanning tot uitbarsting. In mei 1988 verzocht de ondernemingsraad de directeur schriftelijk om opheldering over de slechte jaarcijfers en de ingehouden gratificatie voor het personeel. Backhuys beende naar de secretaris van de ondernemingsraad, stak de brief in brand en liet deze brandend neerdalen op diens bureau. Een tweede schrijven werd door de voorzitter van de ondernemingsraad persoonlijk afgeleverd bij de directeur, die het voor de ogen van de verbouwereerde bezorger verscheurde. Dergelijke reacties waren weinig bevorderlijk voor de sfeer in het bedrijf.

Toen de ondernemingsraad daarop haar beklag deed bij de raad van commissarissen, raakten de poppen aan het dansen. Prof. dr. E.H. van der Beugel, president-commissaris sinds vijfentwintig jaar, eiste een onafhankelijk onderzoek naar het beleid van Backhuys. Hij kon zijn medecommissarissen echter niet van de noodzaak daarvan overtuigen. Van der Beugel trad verontwaardigd af en werd opgevolgd door mr. R.P.M. de Bok, voorzitter van de Rotterdamse Kamer van Koophandel. Enige maanden later kwam de raad van commissarissen alsnog tot de conclusie dat het management aan een onderzoek moest worden onderworpen. De bevindingen van de externe deskundige waren dermate vernietigend dat Backhuys eind november werd geschorst. De directeur ging ‘voor onbepaalde tijd met vakantie’, zoals de ironische mededeling aan het personeel luidde.

![](04-31.jpg)

<=.ill_04-31.jpg Terwijl Brill bezig was met de tweede editie van de _Encyclopaedia of Islam_, werd in 1987 een herdruk uitgebracht van de eerste editie (1913-36). De heruitgave was tevens bedoeld om roofdrukken te voorkomen, want het copyright verliep in 1986. AB/UBA =>

<=.pb 142 =>
Frans Pruijt, eerder werkzaam bij uitgeverij Samsom-Sijthoff, werd met ingang van januari 1989 aangesteld als interim-directeur. In de maanden daarna werden de cijfers over 1988 bekend, die nog veel dramatischer waren dan die over 1987: Brill leed een ongekend groot verlies van f 1.900.000. Naast werknemers en commissarissen raakten nu ook de aandeelhouders bij de zaak betrokken. Begin maart werd een bijzondere vergadering belegd waarop de aandeelhouders zich moesten uitspreken over het ontslag en de afvloeiingsregeling van Backhuys. Een aantal van hen eiste opheldering over de gang van zaken en openbaarmaking van het rapport van de externe deskundige. De commissarissen weigerden dat laatste, omdat zij met Backhuys waren overeengekomen de vuile was niet buiten te hangen. Een grootaandeelhouder betichtte hen openlijk van wanbeleid en dreigde een motie van wantrouwen in te dienen. Volgens hem probeerden de commissarissen te verdoezelen dat in het rapport ook op hen
kritiek werd uitgeoefend. Deze keer bleef het bij dreigen.

De zaak werd zienderogen ingewikkelder, want de ‘kritische’ aandeelhouders waren oude bekenden van Backhuys dan wel leden van zijn schoonfamilie Scholte. Gezamenlijk beschikten zij over een fors pakket aandelen. Het ontslag van de directeur ontaardde in een conflict tussen deze aandeelhouders en de commissarissen, die werden

![](04-32.jpg)

<=.ill_04-32.jpg Het einde van het loden tijdperk in de jaren tachtig: een zetter gooit de letterbakken leeg. Alleen werken in het Chinees en Japans werden tot 1987 nog met de hand gezet. Coll. Brill =>

<=.pb 143 =>
gesteund door de andere aandeelhouders en het personeel. In de pers verscheen het ene artikel na het andere over de moeilijkheden bij Brill, tot dusverre een degelijke onderneming met een onberispelijke reputatie. Het bedrijf kreeg in de eerste helft van 1989 een stroom van negatieve publiciteit over zich heen. Voor de buitenwacht was de interne stammenstrijd amper te volgen, want iedereen leek ruzie te hebben met iedereen. Zoveel was duidelijk dat Brill diep in de rode cijfers was gedoken en geteisterd werd door problemen - men begon zich af te vragen of het bedrijf nog wel overlevingskansen had.

Begin april trad de commissaris J.H. Scholte plotseling af, een dag voordat een tweede bijzondere vergadering van aandeelhouders zou plaatsvinden. Hij verklaarde het oneens te zijn met zijn medecommissarissen over het ontslag van Backhuys en kritiek te hebben op de jaarcijfers van 1988. Men kan zich niet aan de indruk onttrekken dat Scholte wist wat op de vergadering van de volgende dag stond te gebeuren. Bij die gelegenheid probeerden de aandeelhouders van de factie-Backhuys namelijk de macht te grijpen door de raad van commissarissen naar huis te sturen. De coup mislukte doordat de commissarissen de preferente aandelen van de Stichting Luchtmans inzetten,
die hun een riante meerderheid van stemmen bezorgden. Met 551 stemmen vóór en
1300 tegen werd de motie van wantrouwen verworpen. Het gebruik van de beschermingsconstructie werd aangevallen door de misnoegde aandeelhouders, op grond van het argument dat in dit geval geen sprake was van een vijandelijke overname. Zij spanden een kort geding aan bij de Haagse rechtbank, maar werden door de rechter in het ongelijk gesteld. Een hoger beroep haalde evenmin iets uit. Waarmee in de loop van 1989 geleidelijk een einde kwam aan dit drama dat Brill tot op het bot had geraakt.

![](04-33.jpg)

<=.ill_04-33.jpg Industriële monumentjes: matrijzen van een zetmachine. AB/UBA =>

<=.pb 144 =>
### Reorganisatie
Onder de oppervlakte van het gekrakeel speelde een meer wezenlijk probleem: de crisis en de grote verliezen hadden duidelijk gemaakt dat Brill in zijn huidige gedaante niet kon voortbestaan. Het was eveneens duidelijk welke aanpassingen nodig waren om dat voortbestaan te verzekeren. Kort na zijn aantreden in januari 1989 schreef Pruijt een nota waarin hij de hoofdlijnen van de onvermijdelijke reorganisatie uiteenzette. In de eerste plaats moesten alle buitenlandse vestigingen worden opgedoekt, niet alleen de recente die door Backhuys in het leven waren geroepen maar ook de oudere in Londen en Keulen. De filialen waren onrendabel en hadden in termen van verkoop, distributie of verwerving van manuscripten nauwelijks een functie. Met de moderne communicatiemiddelen konden klanten overal ter wereld worden bediend vanuit Leiden. De strategische ligging van Brill ten opzichte van Schiphol was in dat opzicht een groot voordeel, zoals Pruijt benadrukte: ‘Distribueren doe je via vliegtuigbuiken, niet via allemaal aparte kantoren’.[^31]

In de tweede plaats moest de drukkerij worden afgestoten, een beslissing die veel pijnlijker was dan het opheffen van de buitenlandse vestigingen. Brill bestond sinds 1848 als ‘boekhandel en drukkerij’ en de internationale reputatie van de uitgeverij was gevestigd op de unieke typografische expertise die zij in huis had. Het beëindigen van die roemrijke traditie was snijden in eigen vlees, maar het was al jaren duidelijk dat dit bedrijfsonderdeel niet meer rendabel was. Backhuys was in 1983 al tot de overtuiging gekomen dat de drukkerij een molensteen was geworden om de nek van Brill.[^32] De crisis van 1989-90 stelde een probleem op scherp dat men jarenlang voor zich uit had geschoven, juist omdat daarvoor maar één pijnlijke remedie denkbaar was.


Pruijt pakte de kwestie voortvarend aan, in overleg met de ondernemingsraad en de vakbonden. De grafische bonden van FNV en CNV erkenden dat Brill niet langer in staat was de drukkerij rendabel te maken en aanvaardden de onvermijdelijkheid van de reorganisatie. Wel eisten zij dat bij de afstoting geen arbeidsplaatsen verloren zouden gaan. Ontslag van de ruim twintig mensen die nog bij de drukkerij werkten lag evenmin in de bedoeling van Brill. Men zocht naar een oplossing waarbij de gehele drukkerij werd overgenomen door een andere, met behoud van de arbeidsplaatsen. Een koper diende zich aan in de vorm van drukkerij Sigma uit Zoetermeer, die per 1 oktober 1989 de grafische afdeling van Brill overnam.

De transactie kwam tot stand in goed overleg tussen alle betrokken partijen en de werknemers die overgingen naar Zoetermeer ontvingen een baangarantie. Daarnaast garandeerde Brill dat gedurende een periode van vier jaar een deel van de uitgaven zou worden gedrukt bij Sigma. Naar het oordeel van alle betrokkenen waren de condities voor de scheiding van uitgeverij en drukkerij optimaal geregeld. Het was inderdaad een scheiding, die een einde maakte aan een huwelijk dat anderhalve eeuw had geduurd. Buiten toedoen van Brill was de afloop niettemin tragisch, want vijf jaar later ging drukkerij Sigma failliet.

In het kader van de afslanking werd in 1990 ook het antiquariaat afgestoten, een onderdeel dat sinds de oertijd tot de inventaris had behoord. Het besluit kwam niet zozeer voort uit gebrek aan rendement van deze afdeling, maar uit het voornemen de activiteiten van Brill voortaan te beperken tot de uitgeverij. Deze scheiding kon bovendien

<=.pb 145 =>
probleemloos worden geregeld, omdat Rijk Smitskamp, sedert 1970 hoofd van het antiquariaat, het als zijn eigen bedrijf wilde voortzetten. Hij veranderde de naam van de winkel aan de Nieuwe Rijn in ‘Het Oosters Antiquarium’. De zaak van Smitskamp was een begrip in Leiden en zou tot 2006 het trefpunt blijven van boekenliefhebbers.

### Wederopstanding
Na de exodus van de typografen bleef de uitgeverij verweesd achter in het veel te grote gebouw aan de Plantijnstraat. Ook de drukpersen en zetmachines verdwenen, zodat men over gebrek aan ruimte niet had te klagen. Sinds de reorganisatie werkten bij Brill nog zestig mensen, terwijl vijf jaar tevoren het totale personeelsbestand, met inbegrip van de buitenlandse vestigingen, ongeveer het dubbele had bedragen. Brill trok zich terug op zijn kernactiviteit, namelijk het uitgeven. In zekere zin was de afslanking een terugkeer naar het verleden, want ook de Luchtmansen waren uitgevers zonder drukkerij geweest.

Pruijt vatte zijn visie op de toekomst samen in nuchtere bewoordingen: Brill moest ‘alleen die dingen doen waar we goed in zijn’.[^33] In het verlengde daarvan noemde hij de traditionele sterke punten van het fonds - encyclopedieën, meerdelige standaardwerken, handboeken en reeksen monografieën op het gebied van religiestudies, oriëntalia, arabistiek, klassieke oudheid en geschiedenis. De uitgeverij was niet van plan zich in nieuwe avonturen te storten, zij streefde naar consolidatie en uitbouw van haar eigen traditie. Wieder had het in de jaren zeventig niet anders geformuleerd, maar Pruijt had het voordeel van een veel overzichtelijker bedrijf om het programma ten uitvoer te brengen.

![](04-34.jpg)

<=.ill_04-34.jpg Directeur Frans Pruijt in actie. Coll. Brill =>

<=.pb 146 =>
Uit de crisis werd een ‘meaner and leaner’ Brill geboren, ontdaan van overbodige ballast. Dankzij de pijnlijke smaldeling kon Brill niet alleen zijn continuïteit handhaven, maar ook zijn historisch gegroeide identiteit vervolmaken. De uitgeverij kon zich ontwikkelen volgens het stramien dat haar vanouds op het lijf geschreven stond, niet langer gehinderd door andere bedrijfsonderdelen. In het kader van de afslanking werd ook de omslachtige naam ‘N.V. Boekhandel en Drukkerij voorheen E.J. Brill’ ingekort: omdat de boekhandel en drukkerij waren verdwenen en ‘voorheen’ een misplaatst anachronisme was geworden, heette het bedrijf voortaan ‘E.J. Brill N.V.’.

Met ingang van 1990 werd Pruijt aangesteld als directeur in vaste dienst. De verwachting was dat ook 1989 verliesgevend zou zijn, maar ondanks de kosten van de reorganisatie kon zowaar een bescheiden winstje worden opgevoerd. De omzet in zijn geheel daalde door het afstoten van de drukkerij tot 6,5 miljoen gulden; daar stond echter tegenover dat de omzet van de uitgeverij enigszins was toegenomen dankzij het feit dat de verkoop van de buitenlandse vestigingen nu via Leiden verliep.


In het begin van de jaren negentig herstelde Brill zich geleidelijk. Een mijlpaal in de wederopstanding waren de facsimile- en tekstuitgaven van de Dode Zee-rollen, op verzoek van de Israëlische regering en in samenwerking met het Leidse bedrijf IDC. Vanwege het unieke karakter van het materiaal was dit een hoogst vererende opdracht, los van het feit dat het in commercieel opzicht een interessant project was. Brill had de opdracht mede te danken aan eerdere facsimile- en tekstedities van de Koptische Nag Hammadi-rollen die in 1945 in de Egyptische woestijn waren gevonden.[^34] De Dode Zee-rollen genereerden een vloedgolf van aanvullende publicaties bij de uitgeverij,
waaronder een speciaal tijdschrift met de titel _Dead Sea Discoveries_. De facsimile-uitgave op microfiche werd verzorgd door IDC, gespecialiseerd in de fotografische reproductie van bijzondere documenten. De samenwerking met IDC droeg ertoe bij dat dit bedrijf naderhand werd opgenomen in Brill. De facsimile van de Dode Zee-rollen verscheen in gedigitaliseerde vorm op cd-rom en werd in 1999 op het internet gepubliceerd.

Een teken van herstel was ook de Exportprijs die in 1993 door het Ministerie van Economische Zaken werd toegekend aan Brill. Terecht, want er was geen tweede bedrijf in Nederland te vinden dat zozeer op het buitenland was gericht: Brill haalde 95% van zijn omzet uit de export en verzond boeken naar welgeteld 104 landen. Een andere mijlpaal in datzelfde jaar was van trieste aard: Frans Pruijt, door wiens toedoen Brill als een feniks uit zijn as was herrezen, overleed plotseling op de leeftijd van 48 jaar. Zijn luchthartige manier van doen paste wonderwel bij de zwaarte van zijn taak. In zijn jonge jaren had hij als chauffeur met de liedjeszanger drs. P. door het land getoerd
en in een soortgelijke lichtvoetige stijl had hij Brill trefzeker uit het slop gehaald.

### Expansie
In 1994 werd Reinout Kasteleijn benoemd tot directeur van Brill, na een tijdelijk bewind door commissaris J. Kist. De feniks begon nu in alle ernst zijn vleugels uit te slaan. Met een productie van tweehonderd titels haalde de uitgeverij in 1996 een omzet van 15,8 miljoen gulden en een winst van 1,15 miljoen. Zulke cijfers had de vennootschap nimmer gekend in de dagen dat zij nog bestond als conglomeraat van bedrijfsonderdelen. Sinds 1990 waren vijftien nieuwe werknemers in dienst getreden,

![](04-35.jpg)

<=.ill_04-35.jpg Prospectus van de facsimile-editie van de Dode Zee-rollen. AB/UBA =>

<=.pb 147 =>
wat het totale personeelsbestand bracht op 75.

Anno 1996 werd wederom een jubileum gevierd: het was honderd jaar geleden dat Adriaan van Oordt en Frans de Stoppelaar de naamloze vennootschap hadden opgericht. Op grond van dit eeuwfeest verkreeg Brill het recht om het predikaat ‘koninklijk’ te voeren. De officiële naam van de uitgeverij veranderde in ‘Koninklijke Brill N.V.’ - de oude Evert Jan moest om redenen van uitspreekbaarheid zijn initialen opofferen. Eigenlijk was men bij Brill niet bijster onder de indruk van de nieuwe status, aangezien de geschiedenis veel verder terugging dan 1896. Bovendien had de koninklijke rang weinig te betekenen in de meeste landen waarmee de uitgeverij zaken deed. Brill vond het onnodig zijn uitgeversmerk te voorzien van een kroontje, Pallas zat veilig en wel achter haar schild en kon het stellen zonder monarchale parafernalia. Kasteleijn vatte de koninklijke onderscheiding vooral op als een erkenning dat Brill ‘een fatsoenlijk bedrijf’ was.[^35]

Een jaar later waagde de herdoopte N.V. de sprong naar de beurs. Sinds haar oprichting stond de vennootschap genoteerd aan de incourante markt, maar dit ouderwetse bijverschijnsel van de effectenhandel werd afgeschaft. Brill besloot daarop in juli 1997 de overstap te maken naar de officiële beurs. Tot dusverre waren de meeste aandelen in handen van een vaste clan van Brill-adepten - familieleden van vroegere directeuren, medewerkers, auteurs en wetenschappers. Om te voorkomen dat Brill zou worden opgeslokt door giganten als Reed-Elsevier of Wolters-Kluwer werden voorzorgsmaatregelen getroffen. Naast de Stichting Luchtmans werden aanvullende beschermingsconstructies ingevoerd. De aandelen zijn gecertificeerd. Het voornaamste doel van de beursgang was het aantrekken van kapitaal voor toekomstige uitbreidingen.[^36]

![](04-36.jpg)

<=.ill_04-36.jpg Een belangrijk project voor Brill in het begin van de jaren negentig was de publicatie van de Dode Zee-rollen. De facsimile-editie op microfiche was een coproductie van Brill en het Leidse IDC, gespecialiseerd in de reproductie van zeldzaam bronnenmateriaal. Sinds 2006 maakt IDC deel uit van Brill. Coll. Brill =>

![](04-37.jpg)

<=.ill_04-37.jpg Vanwege het honderdjarig bestaan van de N.V. kreeg Brill in 1996 het recht zich ‘koninklijk’ te noemen. Coll. Brill =>

<=.pb 148 =>
Dat kapitaal was inderdaad nodig, want sindsdien heeft Brill een enorme sprong voorwaarts gemaakt. Was de jaarproductie in 1996 nog tweehonderd titels, in 2008 is deze meer dan verdubbeld tot bijna zeshonderd. Het aantal tijdschriften bij de uitgeverij ligt inmiddels boven de honderd. In samenhang daarmee is het aantal werknemers gestegen van 75 in 1996 naar 130 in 2008. De afgelopen jaren werden meerdere kleine fondsen overgenomen, passend in de ‘niche’ die door Brill wordt bediend: Humanities Press (1998), VSP (wetenschappelijke tijdschriften, 1999), Styx (geschiedenis en archeologie
van het Nabije Oosten, 2001), de _Index Islamicus_ (2001), Gieben (Grieks, 2006) en Hotei (Japanse kunst, 2006). Grotere overnames waren het reeds genoemde IDC (2006) en Martinus Nijhoff Publishers (2003). De laatste is een vooraanstaand uitgever op het gebied van volkenrecht, internationale betrekkingen en mensenrechten, waardoor Brill een sterke positie verwierf op de academische markt voor rechtswetenschappen. VSP, Nijhoff, IDC en Hotei verschijnen als imprints van Brill. In 2007 verwierf Brill fondsonderdelen van Koninklijke Van Gorcum in Assen op het gebied van religiestudies en geschiedenis.

In toenemende mate maakt Brill gebruik van de mogelijkheden van het internet,
onder andere in samenwerking met Google. Informatie over nieuwe uitgaven wordt langs elektronische weg verspreid en het huidige fonds bevat meerdere digitale publicaties. De contacten met Rusland en vooral met China zijn geïntensiveerd, landen die ook als afzetmarkten in opkomst zijn. De productie van boeken vindt steeds meer plaats in het buitenland, zowel wat betreft het drukwerk als de opmaak. Brill is niet alleen een bedrijf dat sterk groeit, het is ook een bedrijf dat sterk verandert.

Desondanks bestaat Brill bij de gratie van het traditionele boek en dat zal in de voorzienbare toekomst niet anders worden. De tweede editie van de _Encyclopedie van de Islam_, voltooid in 2006, is ook in digitale vorm te raadplegen en hetzelfde geldt voor de derde die de uitgeverij thans onderhanden heeft. Aan het fysieke substraat

![](04-38.jpg)

<=.ill_04-38.jpg Eerste notering van het aandeel Brill op de Amsterdamse Beurs, juli 1997. Coll. Brill =>

<=.pb 149 =>
van de veertien delen van de tweede en het nog onbekende aantal van de derde editie ontlenen de digitale publicaties gezag. Ook een mammoetproductie als de twintigdelige _Brill’s New Pauly: Encyclopaedia of the Ancient World_ (sinds 2002 gevorderd tot deel XII) veronderstelt naast zijn digitale verschijningsvorm het boek in zijn papieren gedaante.

Brill heeft het advies van Pruijt ter harte genomen: het bedrijf is blijven doen waar het goed in is en heeft ingespeeld op de mogelijkheden die zich aandienen. De lijnen uit het verleden zijn doorgetrokken naar het heden. Niet alleen is Brill blijven doen waar het goed in is, getuige de sterke groei doet Brill het goed. Het aandeel behoort sinds zijn introductie tot de snelle stijgers op de beurs en is op grond van zijn groeicijfers al enkele keren genomineerd voor de ‘Gazelle-award’.

### Continuïteit
De geschiedenis van een bedrijf is een beschrijving van de veranderingen die het in de loop der tijd heeft ondergaan. Ter afsluiting worden hier twee recente veranderingen vermeld: in 2005 verhuisde Brill nogmaals, deze keer naar een nieuw gebouw aan de Plantijnstraat dat geheel is ingericht op de eisen van de uitgeverij. Het oudere gebouw uit het begin van de jaren zestig van de vorige eeuw, de trots van het toenmalige bedrijf, is inmiddels gesloopt. Zo gaan die dingen, althans met gebouwen die geen monumentale
status hebben zoals het voormalige weeshuis aan de Oude Rijn. En de laatste verandering, die overigens een jaar eerder plaatsvond: Reinout Kasteleijn trad in maart 2004 af als directeur en werd opgevolgd door Herman Pabbruwe, sinds 1683 de achttiende directeur in successie. Met hem verlaten wij het verleden en betreden het heden.


In het laatste hoofdstuk van dit boek komt zijn visie op de toekomst van Brill aan de orde. Luchtmans in 1683 en Brill in 2008: de verschillen zijn groot, maar zo ook de overeenkomsten. Het bedrijf is als wetenschappelijke uitgeverij ter wereld gekomen en heeft in al zijn metamorfosen dat karakter behouden. Tussen de zes boeken per jaar van Jordaan Luchtmans en de zeshonderd van Brill bestaat een doorgaande lijn, met opvallende constanten zoals de syriaca. Toch is continuïteit allerminst vanzelfsprekend, zoals aan het begin van dit boek reeds werd opgemerkt. Een bedrijf heeft geen ingebouwd mechanisme dat het in staat stelt de wisselvalligheden van tijd en conjunctuur te overleven. Het had meer voor de hand gelegen dat Luchtmans dan wel Brill ergens in de loop van die 325 jaar ten onder was gegaan. Het tegendeel mag een wonder heten. Om het minder metafysisch uit te drukken, het toeval speelt een grote rol in het voortbestaan op zo’n lange termijn.

Valt het toeval niet uit te sluiten, deels wordt het gecorrigeerd door andere factoren. De kwaliteit van een boek is geen toevalstreffer, maar het resultaat van doelgericht menselijk handelen. Een bedrijf dat de wereld vervuilt met minderwaardige producten heeft op den duur geen bestaansrecht. Daarentegen hebben kwaliteit en de daarop gevestigde reputatie een bestendigend effect op het voortbestaan. Ook de mensen die in de loop der tijd aan het bedrijf gestalte hebben gegeven bieden weerstand tegen de grillen van de fortuin - zij anticiperen op de toekomst en proberen het toeval een pootje te haken.

![](04-39.jpg)

<=.ill_04-39.jpg Omslag van het eerste deel van de derde editie van de _Encyclopaedia of Islam_. De uitgave is inmiddels zo vermaard dat in de titel wordt volstaan met de afkorting ‘E.I’. =>

<=.pb 150 =>
Onvermijdelijk viel in deze beknopte geschiedenis de nadruk op leidende figuren, met voorbijzien van de vele naamlozen die Brill groot hebben gemaakt. Juist de laatsten, in een estafette van opeenvolgende geslachten, zijn de voornaamste motoriek van de continuïteit. Niet alleen de vakkennis wordt van generatie op generatie overgeleverd, maar ook de verbondenheid met het bedrijf. De irrationele inhoud van die traditie valt moeilijk te benoemen - beroepstrots, loyaliteit, een besef van deelgenootschap in een bijzondere onderneming. Noem het een Brillgevoel, gecultiveerd door werknemers en andere betrokkenen bij het bedrijf. Tegelijk overstijgt dat Brillgevoel de lokale context dankzij het internationale karakter van de uitgeverij. Wie met Brill te maken heeft weet zich verbonden met een wereld die veel groter is dan Leiden.

Zonder de verknochtheid van de achterban had Brill niet kunnen uitgroeien tot een uitgeverij van mondiale allure. De stroom van uitgegeven boeken, inmiddels zo’n twintigduizend titels, komt niet alleen voort uit zakelijke of wetenschappelijke motieven, maar evenzeer uit die speciale bedrijfscultuur.

<=.pb 151 =>

![](04-40.jpg)

<=.ill_04-40.jpg Het nieuwe gebouw van Brill aan de Plantijnstraat. =>

<=.pb 152 =>

![](05-01.jpg)

<=.ill_05-01.jpg Het recent overgenomen fonds Hotei geeft letterlijk en figuurlijk kleur aan de Asian Studies van Brill en is een gevestigde naam op het gebied van de bestudering van de Japanse prentkunst. =>

<=.pb 153 =>
# Brill: heden en toekomst

### Onder de fax
‘In 2015 leven we in een andere wereld. Amerika en Japan liggen next door. We kunnen als het ware zelf onder de fax gaan liggen’. Deze uitlating van directeur Frans Pruijt uit 1989 laat zien hoe snel de wereld, in het bijzonder de wereld van de uitgeverij verandert.[^1] Nog geen twintig jaar later is Japan als economische macht ingehaald door China en is de beeldspraak van Pruijt geheel en al achterhaald: niemand gaat meer onder de fax liggen, want wij verzenden onszelf via het internet en de e-mail. Temidden van de ingrijpende veranderingen van de samenleving, de economie en de technologie
probeert Brill een koers te varen die realistisch en haalbaar is.

Daarbij is de eigen geschiedenis een bron van inspiratie voor het huidige Brill. De reorganisatie van 1989-90 en het afstoten van onrendabele bedrijfsonderdelen stelde de uitgeverij in staat zichzelf opnieuw uit te vinden. Brill beperkte zich voortaan tot de kernactiviteit van het uitgeven en benadrukte hierdoor de eigen traditie. In cijfers uitgedrukt maakte het bedrijf in de afgelopen twee decennia een kwantumsprong: rond zeshonderd titels per jaar, meer dan honderd tijdschriften en een omzet van € 26 miljoen waren rond 1990 onvoorstelbare parameters.

Kan de onderneming met reden trots zijn op zulke resultaten, in vergelijking met de grote concerns op de wereldmarkt is zij een relatief kleine speler. Niettemin kan zonder overdrijving worden gezegd dat Brill behoort tot de mondiale top van wetenschappelijke uitgeverijen. Terwijl grote internationale uitgeverijen zich profileren met een breed aanbod voor wetenschap, onderwijs en vakopleiding, bedient Brill voornamelijk nissen van de wetenschappelijke onderzoeksmarkt voor humaniora en rechtswetenschappen. Gezien zijn oriëntatie is het bedrijf eerder verwant met een universitaire uitgeverij als Oxford University Press dan met een concern als Reed-Elsevier.

### Aanpassingsvermogen en meegaan met de tijd
Globalisering, digitalisering en gebruikerseisen van nieuwe generaties zijn ontwikkelingen die een uitgever serieus dient te nemen. Geen enkele internationaal opererende onderneming kan zich onttrekken aan dergelijke veranderingen, wil zij niet het risico lopen het eigen voortbestaan in gevaar te brengen. Het inspelen daarop vergt echter meer dan de overlevingskunst die wordt opgedrongen door de internationale concurrentieslag. Veeleer wordt in dit verband een aanspraak gedaan op de kunst de mogelijkheden van het heden te combineren met de in het verleden verworven reputatie en positie.

Uit een geschiedenis van meer dan drie eeuwen is te leren dat Brill steeds kans heeft gezien aanpassingen te doen en keuzes te maken, en anno 2008 is dat nog steeds een belangrijk talent. De logische vereniging van het inwendige karakter van de onderneming

<=.pb 154 =>
met de uitwendige socio-economische context zal ook de komende tijd de beste kansen voor het voortbestaan bieden. Dat betekent dat niet op elke nieuwe ontwikkeling wordt ingespeeld. Er zijn uiteraard ontwikkelingen die onverenigbaar zijn met het eigen karakter. De keuze van wat wel en wat niet past en toekomst biedt vereist een actieve benadering en een scherpe bewaking van de eigen identiteit. Een louter passieve assimilatie aan de uitwendige realiteit is een vorm van mimicry die alleen op korte termijn werkt, niet langer dan dat een volgende verandering in de buitenwereld noopt tot het
aannemen van weer een andere kleur. Een bedrijf dat slechts meedeint op de golven van de tijd wordt een speelbal van de conjunctuur; opportunisme alleen leidt tot te veel Hermes en te weinig Pallas.

### Duizenden titels
Dezelfde gedachtegang is van toepassing op de technologische uitdagingen waarvoor de onderneming zich nu en in de nabije toekomst geplaatst ziet. Wat allemaal mogelijk wordt, is op zichzelf niet doorslaggevend of zaligmakend voor de toekomst van Brill. Minstens zo belangrijk is de wijze waarop de organisatie technologie weet te integreren en toe te passen. Ook in dit geval gaat het niet alleen om de aanpassing van het bedrijf aan de externe omstandigheden, maar evenzeer om het afstemmen van de digitale mogelijkheden op de eigen traditie. Digitalisering om wille van de digitalisering biedt geen garantie voor het overleven in de 21ste eeuw. Met het oog op de continuïteit dient zich een andere afweging aan, namelijk in hoeverre Brill vanuit zijn historisch gegroeide eigenheid dat proces naar zijn hand kan zetten.

Dat laatste blijkt wonderwel te kunnen. Getuige de ontwikkeling vanaf het midden van de jaren negentig is de traditie van Brill in hoge mate compatibel met de computer en het internet. De sterke groei van de uitgeverij tijdens het afgelopen decennium is geholpen door een adequaat gebruik van de nieuwe digitale mogelijkheden. Zelfs in letterlijke zin digitaliseert de onderneming haar verleden: oudere titels uit het fonds krijgen nieuw leven ingeblazen door indexering via het internet, zoals verderop in dit hoofdstuk aan de orde zal komen.

Technologie en geschiedenis gaan eveneens hand in hand bij het opstellen van een digitale fondslijst van titels die sinds 1683 onder Luchtmans en vanaf 1848 op naam van Brill zijn verschenen. Gezien de omvang van die taak gaat het voorlopig nog om een werk in uitvoering, waarbij aan internetgebruikers wordt gevraagd mee te zoeken naar onbekende uitgaven van Brill. Het historische fonds op naam van Luchtmans is tamelijk volledig in kaart gebracht in het kader van de ‘Short Title Catalogue of the Netherlands’ (STCN). De fondslijst van Luchtmans over de jaren 1683-1800 is tegenwoordig ook te raadplegen op de website van Brill.[^2] Met inbegrip van de periode 1800-1848 omvat het fonds van Luchtmans in zijn geheel ongeveer 2500 titels. Opgeteld bij de voortgaande productie van Brill komt men uit bij hoeveelheden boeken die geen mens meer kan overzien; de teller staat nu al boven de twintigduizend. Wel kan men constateren dat de uitgeverij tijdens haar lange bestaan een belangrijke bijdrage heeft geleverd aan ons intellectuele erfgoed.

![](05-02.jpg)
![](05-03.jpg)
![](05-04.jpg)

<=.pb 155 =>
![](05-05.jpg)
![](05-06.jpg)
![](05-07.jpg)
![](05-08.jpg)
![](05-09.jpg)
![](05-10.jpg)
![](05-11.jpg)
![](05-12.jpg)

<=.ill_05-05.jpg Variërend van twee keer per jaar tot een keer per twee jaar worden in een groot aantal disciplines catalogi uitgebracht. Naast vanouds zeer omvangrijke domeinen zoals Asian Studies, Biblical & Religious Studies en Middle East & Islamic Studies zijn er nieuwere ontwikkelingen zoals African Studies. Al het promotiemateriaal is door een zelfde huisstijl zeer herkenbaar geworden. Catalogi worden in toenemende mate gedownload vanaf <www.brill.nl>. =>

<=.pb 156 =>
![](05-13.jpg)
![](05-14.jpg)
![](05-15.jpg)
![](05-16.jpg)
![](05-17.jpg)
![](05-18.jpg)

<=.ill_05-13.jpg In het verleden had Brill geen speciaal uitgeefprogramma voor talen of linguïstiek. Eerdere uitgaven op dit gebied verschenen onder de noemer van oriëntalistiek, godsdienstwetenschappen of geschiedenis. Toen dwars door alle fondsen heen de talenuitgaven werden geïnventariseerd voor een catalogus, bleek er een uitstekende basis te zijn voor een afzonderlijk programma gericht op kleine, bedreigde en oude talen. Language & Linguistics kreeg dankzij het verleden een vliegende start. Ook voor Slavic & Eurasian Studies, Philosophy en Art, Architecture & Archeology zijn op basis van het bestaande fonds catalogi samengesteld. Zij brengen Brills latente kracht in deze domeinen aan het licht en bevorderen de verkoop van oudere titels. De acquisities van IDC heeft versnelling gebracht in de plannen voor slavistiek en kunstgeschiedenis. De overname van Hotei gaf een impuls aan het plan om een kunsthistorisch fonds binnen de Asian Studies te ontwikkelen. =>

<=.pb 157 =>
![](05-19.jpg)
![](05-20.jpg)
![](05-21.jpg)
![](05-22.jpg)

<=.pb 158 =>
![](05-23.jpg)
![](05-24.jpg)
![](05-25.jpg)
![](05-26.jpg)

<=.pb 159 =>
![](05-27.jpg)
![](05-28.jpg)
![](05-29.jpg)
![](05-30.jpg)
![](05-31.jpg)
![](05-32.jpg)
![](05-33.jpg)

<=.ill_05-27.jpg De zoektocht binnen Brills fondsen bracht een groot aantal titels naar voren die relevant zijn voor een catalogus kunstgeschiedenis en archeologie. De overname van Hotei en de samenwerking met Sciences Press in China geven vaart aan de ontwikkeling van een Visual Arts programma binnen de Asian Studies. IDC leverde de basis voor een sterke kunsthistorische component binnen de Slavic Studies. Bovendien bood de acquisitie van IDC aan Brill de mogelijkheid om documentaire bronnencollecties voor de Westerse kunstgeschiedenis online te brengen, zoals de _Art Sales Catalogues, 1600-1900_ en het _Répertoire des catalogues de ventes publiques_ van Frits Lugt. De contacten met het Rijksbureau voor Kunsthistorische Documentatie in Den Haag vormden de basis voor het uitgeefcontract van _Oud Holland_, dat met ingang van 2008 bij Brill verschijnt. _Oud Holland_ is een internationaal gerenommeerd tijdschrift voor de bestudering van Nederlandse kunst en het langst bestaande kunsthistorische tijdschrift ter wereld. =>

<=.pb 160 =>

![](05-34.jpg)
![](05-34bis.jpg)

<=.ill_05-34.jpg Dankzij de overname van Martinus Nijhoff Publishers in 2003 verwierf Brill een leidende positie op het gebied van internationaal recht. Het jaar daarop gunde The Hague Academy of International Law het uitgeefcontract voor zijn _Recueil des Cours_ ofwel _Collected Courses_ opnieuw aan Nijhoff. Sinds begin 2008 wordt deze belangrijke en omvangrijke collectie ook online aangeboden. Alle uitgaven van Brill in International Law en Human Rights dragen de gevestigde imprint ‘Martinus Nijhoff Publishers’.

<=.pb 161 =>
### Pallas en Hermes
Naast de technologie spelen vanouds andere zaken een rol in de boekenmarkt, zoals de reputatie van een uitgever en de kwaliteit van zijn producten. Een sterk aandeel in een bepaald segment van de markt draagt eveneens bij tot de bestendigheid van de onderneming. In beide opzichten mag Brill zich gelukkig prijzen. Verder is de menselijke factor in een bedrijf als Brill van eminent belang. De huidige uitgeverij kan niet bestaan zonder de inzet van hoogopgeleide en gemotiveerde medewerkers die wereldwijde netwerken onderhouden en naar nieuw wetenschappelijk onderzoek speuren. Handhaving en aanscherping van kwaliteitsnormen zijn daarbij noodzakelijke uitgangspunten voor een gezonde groei.

In aansluiting bij haar eigen traditie streeft de uitgeverij naar een evenwicht tussen handel en wetenschap. Pallas Athene, de godin van de wetenschap, en Hermes, de god van de handel, figureren naast elkaar in het logo van Brill. Hun gezamenlijke inbreng is de beste garantie voor het welvaren van de onderneming. De samenwerkende goden geven niet alleen invulling aan een bedrijfsvisie, maar ook aan een wereldbeschouwing die de nadruk legt op betrouwbaarheid in het maatschappelijk verkeer en duurzaamheid van economische groei. Een maatschappelijk verantwoorde wijze van ondernemen is niet alleen een houding ten opzichte van de buitenwereld, maar draagt tevens bij tot een gezond werkklimaat binnen de onderneming.


De portfolio van Brill is in vrijwel alle opzichten goed gespreid. Het fonds is evenwichtig verdeeld over disciplines en productformaten, de afzet verloopt via verschillende verkoopkanalen en ook de samenstelling van de omzet uit recente en oudere titels is in balans. Brill waakt ervoor in teveel verschillende en niet met elkaar samenhangende disciplines terecht te komen. Groei wordt gezocht in productontwikkeling en acquisities die passen binnen het historische gegroeide stramien van de onderneming, dat wil zeggen met een concentratie op de humaniora. Door de acquisitie van Nijhoff is ook in het internationaal recht een sterke uitgangspositie verkregen. De strategie van de uitgeverij volgt de ontwikkeling in het onderzoek binnen een specifiek vakgebied. De primaire doelgroep bestaat steeds uit de deelnemers aan een bepaald wetenschappelijk discours, een kring van vakgenoten waartoe zowel de auteurs als de afnemers van Brill behoren.

Voor de uitgeverij zijn de auteurs en de door hen aangeleverde teksten uiteraard van vitaal belang. Een netwerk van wetenschappers beoordeelt de teksten voordat deze worden uitgegeven. Deze zogenaamde ‘peer-review’ garandeert de wetenschappelijke kwaliteit en waarborgt tevens de onafhankelijkheid van de uitgeverij.

### Dynamisch erfgoed
Vanouds onderscheidde Brill zich van andere uitgeverijen door het specialisme van de ‘eigenaardige’ talen. Reeds het fonds van Luchtmans bevatte verschillende uitgaven in het oud-Syrisch, Arabisch en Hebreeuws. Rond 1825 namen de arabica een hoge vlucht dankzij de systematische publicatie van manuscripten uit het Legatum Warnerianum van de Leidse universiteit. Deze reeks uitgaven verscheen tot 1848 op naam van Luchtmans, maar was in feite een coproductie met Johannes en Evert Jan Brill. Nadat de laatste de firma had overgenomen, zette hij de traditie van Arabische boeken voort.

<=.pb 162 =>
![](05-35.jpg) 

<=.ill_05-35.jpg IDC is al vijftig jaar specialist in het fotograferen van kwetsbare en zeldzame boeken en uniek bronnenmateriaal. Dat gebeurt niet willekeurig, maar volgens een zorgvuldig plan waarbij geografisch verspreide bronnen in samenhangende onderzoekscollecties worden bijeengebracht. De onderwerpen van IDC passen vrijwel naadloos in de gebieden waarop Brill al een sterke positie heeft. De eerste samenwerking betrof de uitgave van teksten van de Dode Zee-rollen in de jaren negentig van de vorige eeuw. De Waldenses-collectie laat zien dat IDC samenwerkt met de fine fleur van bibliotheken in binnen- en buitenland. =>

<=.pb 163 =>
De naamgever van de huidige uitgeverij had een uitgesproken voorliefde voor vreemde tekens en waagde zich zelfs aan uitgaven in het Sanskriet, het Japans en het hiëratisch schrift van het oude Egypte. In het laatste kwart van de negentiende eeuw werden exotische talen het kenmerk bij uitstek van de uitgeverij, vooral op het gebied van de oriëntalia. Brill gaf wetenschappelijke boeken uit in meer dan dertig talen en verwierf daarmee een internationale reputatie. Om de trotse leuze te citeren waarmee de uitgeverij zich in 1960 presenteerde op de Buchmesse van Frankfurt: ‘Wir drucken und verlegen in allen Sprachen der Welt’.


De afgelopen decennia heeft Brill betrekkelijk weinig titels uitgebracht op het specifieke gebied van bijzondere, bedreigde en dode talen. Onlangs heeft de uitgeverij besloten dit oude specialisme in ere te herstellen, met welk doel het fondsonderdeel ‘Language and Linguistics’ is opgericht. De beoogde publicaties brengen met zich mee dat opnieuw de expertise moet worden ontwikkeld die vanouds de roem van Brill uitmaakte, namelijk de typografie van exotische tekens.

Wanneer de uitgeverij in het loodtijdperk een boek publiceerde in een onbekend schrift als het Lydisch of het Estrangelo, werden daarvoor speciale typefonts vervaardigd. Boeken in zulke talen werden met de hand gezet door de meest ervaren zetters van het bedrijf. Zij waren reeds op jonge leeftijd vertrouwd geraakt met het zetten in vreemde tekens en gaven op hun beurt hun vakkennis door aan de volgende generatie van zetters. Zij waren de meesterzetters en vormden de typografische elite van Brill. Alleen bij de gratie van deze vaklieden kon de uitgeverij zich erop beroemen boeken te kunnen publiceren ‘in alle talen van de wereld’. In 1987 ging de laatste meesterzetter met pensioen, waarmee een einde kwam aan deze ambachtelijke traditie. Tot die tijd werden werken in het Chinees en Japans nog steeds met de hand gezet.

Met het oog op het nieuwe fondsonderdeel ‘Language and Linguistics’ worden de benodigde exotische tekens gecodeerd met behulp van de computer, wat de ontwikkeling vereist van een standaard of ‘unicode’ voor de desbetreffende software. Een handmatige activiteit die voorheen een essentiële rol speelde in het bedrijf keert terug in een eigentijdse en digitale gedaante. De cirkelgang in de tijd is een treffend voorbeeld van de wijze waarop Brill de mogelijkheden van het heden in overeenstemming brengt met de eigen traditie. Omdat in Brill’s uitgeefpraktijk zo’n zesduizend tekens voorkomen is onlangs besloten een eigen letter met de naam ‘Brill Text’ te laten ontwerpen, die digitale coderingen in unicode omzet in heldere letters en tekens, zowel in druk als op het scherm. De gangbare typefonts dekken Brills behoefte aan bijzondere tekens niet.

### Digitale tijden
De digitalisering is voor Brill ongetwijfeld de belangrijkste ontwikkeling van het afgelopen decennium geweest. De uitgeverij speelt in op alle mogelijke ontwikkelingen die zich de laatste jaren aandienden, van online-monografieën en tijdschriftarchieven tot concordanties en encyclopedieën en alles wat daar tussen in ligt. Het toevoegen van zogenaamde elektronische schillen aan traditionele uitgaven op papier en het creëren van hybride producten gaan hand in hand met de ontwikkeling van puur elektronische publicaties.

<=.pb 164 =>
![](05-36.jpg) 
![](05-37.jpg)
![](05-38.jpg)

<=.ill_05-36.jpg Acquisities worden alleen gedaan wanneer zij op afzienbare termijn bijdragen aan het resultaat en vooral strategisch voordeel bieden. De meeste overgenomen activiteiten worden onder de imprint Brill voortgezet. De overname van Gieben versterkte de Classic Studies van Brill door de omvangrijke reeks _Supplementum Epigraphicum Graecum_. Dankzij Van Gorcum verwierf Brill nieuwe uitgaven op het gebied van godsdienstwetenschappen, judaïca, geschiedenis en filosofie. Het tijdschrift _Hobbes Studies_ vormt een welkome aanvulling op het eigen fonds. Transnational versterkte Nijhoffs programma met postdoctorale tekstboeken en standaardwerken zoals het Ocean Yearbook, waarvan inmiddels de 22ste editie is verschenen. =>

<=.pb 165 =>
De technologie van het internet heeft de effectiviteit van Brill op het gebied van marketing in hoge mate verbeterd, vooral dankzij het gebruik van e-bulletins, gerichte emailing en samenwerking met Google. Sinds 2005 worden de meeste boeken gedigitaliseerd en opgenomen in het programma Google Book Search. Ook de backlist van het fonds wordt op deze wijze ontsloten, waardoor oudere titels weer zichtbaar worden en een tweede leven krijgen. Een titel die nog leverbaar is kan de klant bestellen bij de uitgeverij, de boekhandel of Amazon.com. De nieuwe technologie van het internet ondersteunt de uitgeverij in een van haar hoofdtaken jegens auteurs: het zo breed mogelijk verspreiden en bekend maken van een publicatie.

Wanneer een bepaald boek niet langer leverbaar is, kan de uitgever bij voldoende vraag besluiten tot heruitgave. Daarnaast zijn vrijwel alle meer recente titels door het zogenaamde ‘Printing on Demand’ per stuk bij te drukken zodat in de nabije toekomst een enorm aanbod mogelijk is zonder dat daarvoor nog grote voorraden nodig zijn. Brill streeft naar wereldwijde verspreiding van zijn producten, zowel van boeken als van elektronische publicaties. Met het oog op conservering voor de toekomst worden de gedrukte teksten opgeslagen in verschillende archieven. De elektronische publicaties worden bewaard in het digitale depot van de Koninklijke Bibliotheek te Den Haag. Daar is de expertise beschikbaar om de digitale publicaties ook in de toekomst met de dan geldende technologische middelen raadpleegbaar te houden.

Brill ontwikkelt zijn uitgaven in het algemeen voor eigen rekening en risico. Bovendien worden bij uitgebreide referentiewerken de auteurs en redacteuren dikwijls door de uitgever uitgenodigd en gecontracteerd. Om die reden speelt de discussie over de zogenaamde ‘Open Access’ waarbij de auteur of zijn wetenschappelijk instituut de uitgever voor diens diensten betaalt, nog geen grote rol. Bij Brill wordt vooral om de gunst van de klant en de lezer gestreden. Op voorwaarde dat wetenschappelijke onafhankelijkheid gewaarborgd is en de uitgeverij een duidelijke toevoegende waarde kan hebben, is Brill echter bereid ook andere betaalmodellen te hanteren zoals die waarbij wetenschappelijke publicaties gratis aan lezers ter beschikking worden gesteld.

### Brill en de wereld
Sinds de dagen van Luchtmans heeft de uitgeverij een internationaal karakter gehad. Leiden was in de zeventiende en achttiende eeuw de ‘stad van boeken’ van Europa, een trefpunt voor schrijvers, wetenschappers en uitgevers. Zoals in het voorgaande werd beschreven reisden de Luchtmansen geregeld naar het buitenland en onderhielden zij een netwerk van internationale contacten. Hetzelfde gold voor de negentiende-eeuwse directeuren van Brill, die heel Europa afreisden om internationale congressen te bezoeken en zich opstelden als makelaars van de wetenschap. Zij bouwden de uitgeverij uit tot het knooppunt van een internationaal wetenschappelijk netwerk, vooral op het gebied van de arabistiek en oriëntalistiek.

Het huidige Brill heeft die traditie voortgezet en onderhoudt contacten met alle belangrijke centra van academisch onderzoek in de wereld. Vanouds is Brill sterk verbonden met de Leidse universiteit, zij het niet langer in de rol van ‘academiedrukker’ die het bedrijf gedurende anderhalve eeuw vervulde. Met name wat betreft islamitische studies, arabistiek, sinologie en archeologie zijn Leidse wetenschappers goed vertegenwoordigd in de kring van auteurs.

<=.pb 166 =>
Noord-Amerika is en blijft een belangrijke markt voor Brill. Omdat het Engels steeds meer de taal van de internationale wetenschap is geworden, vestigde de uitgeverij in 1998 een kantoor in Boston. Het Amerikaanse marktaandeel lag rond de 25% en Brill was van plan dat op te voeren. Boston was een strategische keuze vanwege de nabijheid van belangrijke universiteiten als Harvard, Yale en Boston University. Zij vormen een natuurlijke afzetmarkt, terwijl de aanwezigheid van schrijvers en onderzoekers bijdraagt aan de toelevering van manuscripten en tijdschriftartikelen. Aanvankelijk meende Brill de Amerikaanse markt het beste met een aparte lijn van ‘softcovers’ te kunnen bedienen, maar van dat idee is men teruggekomen. Boston is nu als het ware een verlengstuk van Leiden, met een team van vijftien medewerkers onder wie uitgevers, redacteuren en marketeers. Het Amerikaanse marktaandeel is inmiddels gegroeid naar ruim 40%.

Op termijn zal Azië als markt opkomen en misschien even belangrijk worden als de Verenigde Staten. Wanneer in China meer geld beschikbaar komt voor universitair onderzoek en onderwijs, zal de vraag naar vakliteratuur sterk toenemen. De afgelopen jaren is Brill in China samenwerkingsverbanden aangegaan met verschillende uitgeverijen, waaronder Peking University Press. Deze uitgeverij richt zich met name op de humaniora, een terrein waarop Brill van oudsher goed thuis is. De samenwerking beperkt zich voorlopig tot de uitwisseling van stafleden en kennis en de lancering van gezamenlijke projecten.

Ook met de Social Science Academic Press van de Chinese Academy of Social Sciences is een strategische alliantie gesloten. Er wordt in deze en andere samenwerkingsverbanden gewerkt aan de Engelse vertaling van Chinese uitgaven dan wel het omgekeerde daarvan. Brill heeft de ambitie een van de belangrijkste uitgevers in en over China te worden. De haalbaarheid van een Chinese vestiging zal op termijn een punt van serieuze overweging zijn. Intussen heeft Brill een eigen Chinese identiteit en uitgeversnaam aangenomen en wordt een eigen Chinese website onderhouden.

Brill heeft zijn intrede gedaan op de Russische markt dankzij de aankoop van IDC, dat verschillende producten op het gebied van Russische geschiedenis aanbiedt. Bepaalde fondsonderdelen van Brill passen uitstekend bij een fonds Russian Studies. Met het oog op groei voor de langere termijn is recent een overzicht van uitgaven samengesteld dat de basis zal vormen voor een multimediaal ‘Slavic & Eurasian Studies’ programma.

### De archieven van Brill en Luchtmans
In 2006 gaf Brill zijn bedrijfsarchief in bruikleen aan de Bibliotheek van het Boekenvak, die is ondergebracht bij de Bijzondere Collecties van de Universiteitsbibliotheek Amsterdam. In opdracht van Brill wordt het archief van 45 strekkende meter geïnventariseerd, geordend en ontsloten ten behoeve van wetenschappelijk onderzoek. Voor de onderhavige uitgave is voor het eerst gebruik gemaakt van dit archiefmateriaal, dat niet alleen de geschiedenis van Brill bestrijkt maar informatie geeft over die van de uitgeverij in het algemeen.

<=.pb 167 =>

![](05-39.jpg) 
![](05-40.jpg)
![](05-41.jpg)

<=.ill_05-39.jpg Op de Beijing International Bookfair van 2007 sloot Brill belangrijke samenwerkingscontracten met enige Chinese uitgeverijen. De ene dag kwam een contract tot stand met de Social Sciences Academic Press van de Chinese Academy of Social Sciences, daags daarop met Peking University Press. Brill heeft een eigen website voor de Chinese markt en zelfs een eigen Chinese naam, fonetisch overeenkomend met ‘Brill’. De eerste twee karakters vormen de woorden ‘Bo Rui’ en betekenen respectievelijk ‘rijke kennis of bron’ en ‘verre blik en diepe wijsheid’. De daarop volgende karakters geven aan dat Brill een wetenschappelijke uitgever is. De tekens zijn gekalligrafeerd door SHU Zhongru
(Loudi, provincie Hunan). =>

<=.pb 168 =>
Het archief van Luchtmans - elf strekkende meter over de periode 1683-1848 - was al eerder terechtgekomen in de Bijzondere Collecties van de Universiteitsbibliotheek Amsterdam, zoals in het eerste hoofdstuk van dit boek werd beschreven. In combinatie met het Brill-archief over de periode 1848-1991 is een nagenoeg compleet beeld te vormen van de ontwikkeling van het bedrijf.[^3] Met een aaneengesloten bereik van meer dan drie eeuwen vormen de twee archieven een unieke bron voor de Nederlandse boekwetenschap en een cultuurhistorisch monument van de eerste orde. Een aanvullend archief bevindt zich in Leiden: in 2007 schonk Rijk Smitskamp, eigenaar van ‘Het Oosters Antiquarium’ (het voormalige antiquariaat van Brill), zijn archief aan de Universiteitsbibliotheek Leiden. Het omvat correspondentie tussen 1970 en 2004, een grote collectie geannoteerde verkoopcatalogi van circa 1930 tot 2005, speciale dossiers, foto’s en bijna honderd laden met fiches van verkochte boeken.

Sinds 2006 bekostigt Brill een of twee zogenaamde ‘Brill-fellows’ bij het Scaliger Instituut van de Universiteitsbibliotheek Leiden. Het Scaliger Instituut is opgericht in 2000 met als doel het gebruik van de Bijzondere Collecties van de Universiteitsbibliotheek Leiden te bevorderen en te ondersteunen. Het instituut zet onderzoeksprojecten op, organiseert lezingen en symposia, publiceert onderzoeksresultaten en verleent beurzen aan (gast)onderzoekers. Een Brill-fellow doet in de Bijzondere Collecties gedurende korte tijd onderzoek naar bronnen die passen bij de uitgeefgebieden van Brill. Brill onderstreept met deze ‘fellowships’ de verbondenheid die van oudsher bestaat tussen de uitgeverij en de universiteit van Leiden.

### De wereld van Brill
_Zoals al eerder is aangegeven ontwikkelt Brill zich in een dynamische balans tussen auteurs, lezers, klanten, leveranciers, aandeelhouders, medewerkers en zijn directe omgeving. Uiteraard spelen de belangen van economische groei en rendement een grote rol bij een beursgenoteerd bedrijf. De zakelijke doelen worden echter zo gekozen dat een vol te houden strategie voor de lange termijn mogelijk is. Daarvoor moet letterlijk en figuurlijk gelet worden op een gezonde balans en verdienen de verschillende belangen van alle belanghebbenden aandacht. In de zich ontwikkelende visie op de rol van Brill als maatschappelijk verantwoord ondernemende uitgeverij past een stakeholders-oriëntatie en biedt het streven naar harmonie tussen Pallas en Hermes een blijvend houvast._


<=.pb 169 =>
![](05-44.jpg)
![](05-45.jpg)
![](05-46.jpg)
![](05-47.jpg)

<=.ill_05-39.jpg Sinds vier jaar worden notitieboekjes gemaakt met kleurige kaftjes, die de illustraties herhalen op de omslagen van Brills jaarverslagen en bedrijfsbrochures. De notitieboekjes worden op beurzen en wetenschappelijke congressen als relatiegeschenk uitgedeeld in aantallen van meer dan tienduizend per jaar. De ‘hebbedingetjes’ zijn zeer gewild bij auteurs en bibliothecarissen. De eerste drie boekjes zijn bedrukt met letters, cijfers en leestekens die in unicode zijn geprogrammeerd. Zij herinneren aan het specialisme in exotische talen waaraan Brill zijn reputatie dankt en onderstrepen dat deze traditie zich ook in het digitale heden voortzet. =>

<=.pb 170 =>
<=.pb 171 =>
## Directeuren van Luchtmans en Brill, 1683-2008

Directeur | jaren
----- | -----
Jordaan Luchtmans | 1683 - 1708
Samuel I Luchtmans | 1708 - 1755
Samuel II Luchtmans | 1755 - 1780
Johannes Luchtmans | 1755 - 1809
Samuel III Luchtmans| 1809 - 1812
(Johannes Brill, bedrijfsleider | 1812 - 1821)
Johannes Tiberius Bodel Nijenhuis | 1821 - 1848
Evert Jan Brill | 1848 - 1871
Adriaan Pieter Marie van Oordt | 1872 - 1903
Frans de Stoppelaar | 1872 - 1906
Cornelis Peltenburg | 1906 - 1934
Theunis Folkers | 1934 - 1947
Nicolaas Willem Posthumus | 1946 - 1958
Frederik Casper Wieder jr. | 1958 - 1979
Tom A. Edridge | 1979 - 1980
(Frederik Casper Wieder jr., a.i. | 1980 - 1981)
Willem Backhuys | 1981 - 1989
Frans H. Pruijt | 1989 - 1993
(Joost Kist, a.i. | 1993 - 1994)
Reinout J. Kasteleijn | 1994 - 2004
Herman A. Pabbruwe | 2004 -

## Commissarissen van Brill sinds de oprichting van de N.V. in 1896

Commissaris | jaren
----- | -----
Prof. dr. M.J. de Goeje | 1896 - 1909
W.H. van Oordt | 1896 - 1904
Dr. W. Pleyte | 1896 - 1903
J. de Stoppelaar | 1896 - 1911
Prof. dr. A.C. Vreede | 1896 - 1908
D. van Oordt | 1905 - 1934
Mr. Th.B. Pleyte | 1909 - 1913
W.P. van Stockum jr. | 1911 - 1927
Mr. D.W.K. de Roo de la Faille | 1914 - 1948
Dr. F.C. Wieder sr. | 1928 - 1943
W.H. van Oordt | 1934 - 1954
Prof. mr. N.W. Posthumus | 1943 - 1949
Ir. L.W.G. de Roo de la Faille | 1949 - 1969
Ir. J.J. van den Broek | 1957 - 1967
Mr. J.L. Heldring | 1961 - 1989
Prof. dr. E.H. van der Beugel | 1964 - 1988
Prof. dr. A. Kraal | 1969 - 1987
Prof. dr. mr. J. Brugman | 1969 - 1995
F.C. Wieder jr. | 1979 - 1987
Drs. J.H. Scholte | 1984 - 1989
Mr. R.P.M. de Bok | 1988 - 1999
Mr. N.J. Westdijk | 1989 - 1993
Mr. J. Kist | 1990 - 2001
Prof. dr. P.J. Idenburg | 1993 - 2007
Jhr. Mr. H.A. van Karnebeek | 1998 - 2008
Drs. ing. H.P. Spruijt | 2000 -
Mr. R.E. Rogaar | 2007 -
Mr. A.R. baron van Heemstra| 2008 -

<=.pb 172 =>
<=.pb 173 =>
## I Van oude tijden. Het huis Luchtmans en het huis Brill: 1683-1848

[^1] UBA/BC, Archief Luchtmans BVA 71-44 a/b. Ter verduidelijking: het archief Luchtmans bevindt zich in de Universiteitsbibliotheek van Amsterdam en maakt deel uit van de Bijzondere Collecties. Zie toelichting bij I.5.

[^2] De Clercq, At the sign of the oriental lamp: the Musschenbroek workshop in Leiden.

[^3] UBA/BC, Archief Luchtmans F.21, ‘Balance’ van Samuel Luchtmans dd.1 augustus 1714.

[^4] Hoftijzer, ‘Veilig achter Minerva’s schild’, in: Bouwman e.a., Stad van Boeken, p.200-201; Van Vliet, Elie Luzac, p.52-53.

[^5] Blaak, Geletterde levens, p.270; Goinga, Alom te bekomen, p.64; Bibliopolis, p.71-72.

[^6] Ophuijsen, Three centuries of scholarly publishing, p.12-14.

[^7] Samuel Luchtmans waardeerde zijn aandeel in dit compagnonschap in 1714 op bijna f 7000,- (zie n.3). De bijzonderheid over deelname van Joh. Enschedé bij Muller, ‘Bodel Nijenhuis. Brill’, p.II, III.

[^8] Hoftijzer, Pieter van der Aa, p.21-25.

[^9] Van Eeghen, ‘Archief Luchtmans’, p.132.

[^10] UBA/BC, Archief Luchtmans F.21 en 26, balansen van Samuel Luchtmans van
augustus 1714 en oktober 1747.

[^11] Van Vliet, Elie Luzac, p.51.

[^12] UBA/BC, Archief Luchtmans F.21, Catalogus librorum quos Samuel Luchtmans vel ipse typis mandavit, vel quorum major ipsi copia suppetit. Lugduni Batavorum, 1714.

[^13] J. Israel, Radical Enlightenment (Oxford, 2002), p.278-279; W.P.C. Knuttel, Verboden boeken in de Republiek der Vereenigde
Nederlanden (Den Haag, 1914), p.110-111, 115-116. In de spinozistische sleutelroman Vervolg van ‘t Leven van Philopater (1697) van J. Duijkerius wordt op p.194-197 gerefereerd aan de Rechtsinnige Theologant; cf. G. Maréchal (ed.), Het leven van Philopater en Vervolg van ‘t Leven van Philopater (Amsterdam, 1991; ook op www.dbnl.org).

[^14] Hoftijzer, Van der Aa, p.26-32.

[^15] Folkers, ‘Oostersche boekdrukkerij te Leiden’, p.63-64.

[^16] Ophuijsen, Three centuries of scholarly publishing, p.19.

[^17] G. Schwetschke, Codex nundinarius Germaniae literatae bisecularis. Meß-Jahrbücher des Deutschen Buchhandels (Halle, 1850). Jordaan Luchtmans wordt vermeld onder de jaren 1686, 1691 en 1699, Samuel I komt nergens ter sprake. Mogelijk ging hij schuil achter de anonieme Leidenaar die onder 1722 wordt vermeld.

[^18] UBA/BC, Archief Luchtmans H1-5; Kruseman, Aanteekeningen betreffende den
Boekhandel, p.606-622.

[^19] Cf. Hoftijzer en Van Waterschoot (eds.), Johannes Luchtmans. Reis naar Engeland in 1772; Van Waterschoot, ‘Samuel Luchtmans, een reislustig boekhandelaar’.

[^20] Van Vliet, Elie Luzac, p.177-197.

[^21] UBA/BC, Archief Luchtmans H.1.

[^22] Ibid., F.1, F.29-32.

[^23] J.T. Bodel Nijenhuis, Dissertatio historicojuridica, de juribus typographorum et bibliopolarum in regno Belgico. Lugduni Batavorum,
apud S. et J. Luchtmans, Academiae typographos. MDCCCXIX. Het werk verscheen
tevens in een Nederlandse vertaling.

[^24] UBA/BC, Archief Luchtmans, ongecatalogiseerd.

[^25] Muller, ‘Bodel Nijenhuis. Brill’, p.III.

[^26] Zie ook Van Eeghen, ‘Het archief van de Leidse boekverkopers Luchtmans’.
27 Cf. Blaak, Geletterde Levens, p.270-274; Smilde, ‘Lezers bij Luchtmans’.
28 UBA/BC, Archief Luchtmans F.21, ‘Balance’ van Samuel Luchtmans dd.1 augustus 1714.

[^29] Du Rieu, ‘Levensschets Bodel’, p.260.

[^30] Regionaal Archief Leiden, Notarieel (H. Obreen), testament J.T. Bodel Nijenhuis, 22-6-1866; M. Storms, ‘Dit waarlijk vorstelijk legaat’, in: idem e.a., De verzamelingen van Bodel Nijenhuis, p.9-24.

[^31] Muller, ‘Bodel Nijenhuis. Brill’, p.I.

[^32] UBA/BC, Archief KVB 1886/14, J.L. Bienfait aan het Bestuur van de Vereeniging, 11-12-1885 (met dank aan Martine van den Burg); Hoftijzer en Lankhorst, Drukkers, boekverkopers en lezers tijdens de Republiek, p.12-28:
‘De periode van de bouwstoffen’.

[^33] UBA/BC, Archief KVB 1886/14, A.C. Kruseman, L.D. Petit en R.W.P. de Vries als leden van de historische commissie aan het Bestuur van de Vereeniging, 3-3-1886; ibid., Verslagen van het Bestuur (1852-1915), ‘Verslag der werkzaamheden… over het Vereenigingsjaar 1885-1886’, p.9-10, 14.

## II De firma E.J. Brill: 1848-1896
[^1] Voor dit en volgende hoofdstukken is uitvoerig gebruik gemaakt van het Archief Brill. Evenals het Archief Luchtmans bevindt het zich in de Bibliotheek van het Boekenvak, ondergebracht bij de Bijzondere Collecties
van de Bibliotheek van de Universiteit van Amsterdam. De definitieve inventaris van het Archief Brill is nog niet vastgesteld, zodat de verwijzingen met enig voorbehoud worden gegeven.

[^2] UBA/BC, Archief Brill fv.139, I-IV, Catalogus van eene uitgebreide en zeer belangrijke verzameling van Hebreeuwsche, Grieksche, Latijnsche, Nederduitsche en Fransche ongebonden boeken, zijnde de kopijen en een zeer
belangrijk assortiment, toebehoorende aan de firma S. en J. Luchtmans, 20 augustus 1849 en volgende dagen; in het vierde exemplaar een afrekening en een brief van Brill aan Bodel Nijenhuis, 5-8-1850;

<=.pb 174 =>
Nieuwsblad voor den Boekhandel, nr.33 (17-8-1849) en nr.40 (4-10-1849); Kruseman, Bouwstoffen dl.I, p.635-637.

[^3] Bijzonderheden over de uitgaven van Brill op diverse terreinen in Lebram e.a., Tuta sub aegide Pallas. E.J. Brill and the world of learning.

[^4] UBA/BC, Archief Brill fv.233.I, Catalogus eener belangrijke verzameling ongebonden boeken, aangekocht en uitgegeven door E. J. Brill te Leiden (1871).

[^5] N.N., ‘Adriaan P.M. van Oordt’.

[^6] UBA/BC, Archief Brill, Bedrijfsgeschiedenis 01, verkoopakte van het bedrijf dd. 21-1-1872.

[^7] F. de Stoppelaar en G.J. Dozy, Proza en Poëzie. Leesboek ten gebruike van de laagste klassen der gymnasiën en hoogere burgerscholen. Het boek werd uitgegeven door Van Looy in Tiel en beleefde in 1906 zijn elfde druk. Zie voorts De Goeje, ‘Frans de Stoppelaar’; Vreede, ‘Frans de Stoppelaar’; <www.destoppelaar.com/genealogy.htm>.

[^8] UBA/BC, Archief Brill, Bedrijfsgeschiedenis 01, vennootschapsakte dd. 17-10-1872

[^9] Catalogue des collections étendues historiques et artistiques formées et délaissées par feu Mr. Bodel Nijenhuis... (3 dln., Leiden-Amsterdam, 1873-74); A. van der Lem, ‘Verzamelaar en schenker van boeken: de bibliotheek van Bodel Nijenhuis’, in: Storms e.a., De verzamelingen van Bodel Nijenhuis, p.62-86.

[^10] Voluit: Annales quos scripsit Abu Djafar Mohammed ibn Djarir at-Tabari cum aliis. Edidit M.J. de Goeje (Leiden, 1879-1901).

[^11] UBA/BC, Archief Brill, Bedrijfsgeschiedenis 03, contract dd. 21-5-1875. Het Ministerie van Koloniën was eigenaar van de druktypen en had ze in bruikleen gegeven aan de universiteit.

[^12] Brusse, ‘De uitgeverij’, Nieuwe Rotterdamsche Courant 5-1-1927; E. Zürcher, ‘East Asian Studies’, in: Lebram e.a., Tuta sub aegide Pallas, p.62.

[^13] Wolters, ‘De firma Brill in hare nieuwe woning’; Regionaal Archief Leiden
519/4762, Verslag van de verbouwing van het Heilige Geest of Arme Wees- en Kinderhuis, met litho’s van de architect J. Bijtel (Leiden, 1882).

[^14] Verslag omtrent het onderzoek, ingesteld door de Derde Afdeeling der Staat-commissie van Arbeids-enquête (Den Haag, 1894), p.201-202 (met dank aan B. Dongelmans).

[^15] UBA/BC, Archief Brill N.26, afschrift van de notariële akte dd. 23-1-1881.

[^16] Catalogue de Manuscrits arabes provenant d’une bibliothèque privée à El-Medina et appartenant à la maison E.J. Brill. Rédigé par C. Landberg (Leiden, 1883); Witkam, ‘Verzamelingen van Arabische handschriften’, p.26.

[^17] C. Snouck Hurgronje, Het Leidsche Oriëntalistencongres. Indrukken van een Arabisch Congreslid (Leiden, 1883).

[^18] Ibid., p.54.

[^19] Verslag omtrent het onderzoek, ingesteld door de Derde Afdeeling der Staat-commissie van Arbeids-enquête, benoemd krachtens de Wet van 19 Januari 1890 (Den Haag, 1894), p.201-204; 223-226 (met dank aan B. Dongelmans).

[^20] B. Dongelmans, ‘Industrialisatie, emancipatie en technologische vernieuwing’, in: Bouwman e.a., Stad van Boeken, p.346.

[^21] Volgens het Verslag over den toestand van handel en nijverheid in de Gemeente Leiden van de Kamer van Koophandel (Leiden, 1891) werkten in 1890 bij Brill in de drukkerij en zetterij 44 personen boven de zestien jaar en vijf beneden die leeftijd. Met het personeel van de winkel, het antiquariaat en het kantoor is zestig werknemers een aannemelijke schatting.

## III De N.V. Boekhandel en Drukkerij voorheen E.J. Brill: 1896-1945

[^1] UBA/BC, Archief Brill, Bedrijfsgeschiedenis 01, akte van oprichting der N.V. Nadat de akte op 21 maart 1896 was gepasseerd door de Amsterdamse notaris C.J. Pouw, werd het bedrijf op 24 maart geregistreerd aan de Beurs. Het Koninklijk Besluit ter goedkeuring van de oprichting dateerde van 2 maart. De akte van oprichting en het K.B. werden gepubliceerd in het Bijvoegsel van de Nederlandsche Staatscourant van 22 april 1896.

[^2] UBA/BC, Archief Brill 1.2-1, Notulen van de Vergaderingen van de Raad van Commissarissen (RvC) en 1.2-2, Notulen van de Algemene Vergaderingen van Aandeelhouders (AVA), 1896-1945.

[^3] Ibid. 1.2-1, RvC 28-1-1896, 3-6-1897, 19-6-1902, 19-6-1903, 21-6-1904, 22-11-1907, 2-2-1909, 31-3-1911.

[^4] Ibid. 1.2-1, RvC 30-12-1896 en 3-6-1897; 1.2-2, AVA 19-6-1902; B. Dongelmans, ‘Aanbieders en aanbod’, in: Bouwman e.a,, Stad van Boeken, p.362-364.

[^5] Ibid. 1.2-1, RvC 3-6-1897, 9-6-1898, 15-6-1899, 22-11-1907.

[^6] Ibid. 1.2-1, RvC 19-6-1903.

[^7] B. Dongelmans, ‘Aanbieders en aanbod’, in: Bouwman e.a,, Stad van Boeken, p.402.

[^8] Krom, ‘Levensbericht van C.M. Pleyte Wzn.’, p.9.

[^9] UBA/BC, Archief Brill 1.2-1, RvC 1-4-1898, 15-6-1899, 20-3-1900. Naderhand was zijn broer Th.B. Pleyte enige jaren commissaris bij Brill, totdat hij in 1913 werd benoemd tot minister van Koloniën.

[^10] Ibid. 1.2-1, RvC 9-6-1898; 1.2-2, AVA 9-6-1898.

[^11] Ibid. 1.2-1, RvC 21-6-1900.

[^12] Wieder, ‘Peltenburg’, p.174.

[^13] UBA/BC, Archief Brill 1.2-1, RvC 11-9-1908, 19-10-1911, 29-12-1911.

[^14] Verslag van den toestand van handel en nijverheid in de gemeente Leiden over 1911. Uitgebracht door de Kamer van Koophandel en Fabrieken aan den Gemeenteraad van Leiden (Leiden, 1912), p.30. (Met dank aan B. Dongelmans).

[^15] UBA/BC, Archief Brill 1.2-1, RvC 30-9-1913; B. Dongelmans, ‘Aanbieders en aanbod’, in: Bouwman e.a, Stad van Boeken, p.373.

[^16] Ibid. 1.2-1, RvC 31-12-1912, 9-4-1914; ibid., Bedrijfsgeschiedenis 02, ‘Enige herinneringen aan de heer C. Peltenburg’, manuscript z.d. van de meesterzetter P.W. Martijn sr.

[^17] Ibid. 1.2-1, RvC 30-12-1915.

[^18] Ibid. 1.2-1, RvC en 1.2-2, AVA 1914-1918.

[^19] Ibid. 1.2-1, RvC 25-5-1918.

[^20] Ibid. 1.2-1, RvC 1919-1920.

[^21] Bondsorgaan van den Nederlandschen Grafischen Bond (jrg.7, nr.2), 15-4-1920.

[^22] UBA/BC, Archief Brill 1.2-1, RvC 23-4-1920.

[^23] Ibid., Bedrijfsgeschiedenis 02, ‘Enige herinneringen aan de heer C. Peltenburg’, manuscript z.d. van de meesterzetter P.W. Martijn sr.

[^24] Ibid. 1.2-1, RvC 1923-1932.

[^25] Brusse, ‘Onder de Menschen: De uitgeverij’ (X-XII), Nieuwe Rotterdamsche Courant, 5/12/19-1-1927. 

<=.pb 175 =>
[^26] UBA/BC, Archief Brill, Bedrijfsgeschiedenis 02, ‘Enige herinneringen aan de heer C. Peltenburg’, manuscript z.d. van de meesterzetter P.W. Martijn sr.

[^27] Nieuwsblad voor den Boekhandel, 30-12-1933.

[^28]] UBA/BC, Archief Brill 1.2-1, RvC 13-4-1938; 1.2-2, AVA 4-6-1938. De toezegging van een legaat van f 20.000 door de weduwe Peltenburg vormde de aanzet tot het Peltenburg Pensioenfonds. Het was uitsluitend bedoeld voor het kantoorpersoneel van Brill; de pensioengelden van de typografen werden inmiddels beheerd door de vakbonden.

[^29] Ibid. 1.2-1, RvC 12-5-1934.

[^30] Ibid. 1.2-1, RvC 1934-39.

[^31] Ibid. 1.2-2, AVA 1934-1939.

[^32] Ibid. 1.2-1, RvC 10-10-1935.

[^33] Ibid. 1.2-1, RvC 3-5-1940. De nieuwe exportafdeling kwam eerder aan de orde op 20-10 en 24-11-1939.

[^34] Ibid. 1.2-2, AVA 6-7-1940.

[^35] Ibid. 1.2-2, AVA 26-6-1943.

[^36] Ibid. 1.2-2, AVA 22-7-1944.

[^37] G. Groeneveld, Zwaard van de geest. Het bruine boek in Nederland 1921-1945 (Nijmegen, 2001), p.222. Met dank aan B. Dongelmans voor de verwijzing.

[^38] UBA/BC, Archief Brill 1.2-1, RvC 28-4-1943.

[^39]  Ibid. 1.2-2, AVA 26-6-1943.

[^40] Ibid. 1.2-1, RvC 1-10-1943; ook als typoscript ‘Overzicht voor nà den oorlog’ in Bedrijfsgeschiedenis 10.03.

## IV Het uitdijende universum van Brill: 1945-2008

[^1] Nederlandse Staatscourant, 3-10-1947; Nieuwsblad voor de Boekhandel, 9-10-1947; UBA/BC, Archief Brill 1.2-3, RvC 16-1-1948.

[^2] J. Meihuizen, Noodzakelijk kwaad. De bestraffing van economische collaboratie na de Tweede Wereldoorlog (Amsterdam, 2003), p.695.

[^3] Ibid., p.226-227.

[^4] Ibid., p.739-750. Meihuizen maakt in zijn werk geen melding van Folkers. A. Venema besteedt evenmin aandacht aan de affaire (Schrijvers, uitgevers en hun collaboratie; 4 dln., Amsterdam 1988-92).

[^5] UBA/BC, Archief Brill 1.2-3, RvC 11-11-1949.

[^6] Ibid. 1.2-11, AVA 1946, jaarverslag Posthumus over 1945.

[^7] Ibid. 1.2-3, RvC 1-3-1949, 5-8-1949, 11-11-1949; ibid. 10.03, Posthumus aan het Beheersinstituut, 30-9-1949.

[^8] Ibid. 1.2-3, RvC en 1.2.-11, AVA 1945-1949.

[^9] Ibid. 1.2-3, RvC 4-4-1948 en 19-8-48; J.W. Christopher, Conflict in the Far East: American Diplomacy in China from 1928-1933 (Leiden, 1950).

[^10] Ibid. 1.2-3, RvC 1-3-1949.

[^11] Ibid. 1.2-3, RvC 16-5-1950; Bedrijfsgeschiedenis 03, Christopher aan Brill, 8 en 9-6-1950.

[^12] Ibid. 1.2-3, RvC jan-aug.1950; 1.2-11, AVA 1951 (jaarverslag Posthumus over 1950).

[^13] Ibid. 1.2-3, RvC 12-2-1953, 18-1-1954, 14-2-1955, 9-5-1955, 7-3-1960.

[^14] Lehning publiceerde ook bij Brill: vijf boeken over zijn held Michaël Bakoenin (1965-1977) en zeven delen Archives Bakounine (1961-81).

[^15] UBA/BC, Archief Brill N.23, memo van Posthumus over zijn Amerikaanse relaties ten behoeve van zijn opvolger F.C. Wieder.

[^16] Deze en volgende paragrafen zijn gebaseerd op de notulen van de Raad van Commissarissen en de Algemene Vergadering van Aandeelhouders over de jaren 1950-1958 (UBA/BC, Archief Brill, resp. 1.2-3 en 1.2-11).

[^17] C. Gerretson, History of the Royal Dutch (4 dln., 1956-57); R.J. Forbes, Technical Development of the Royal Dutch/Shell (1890-1940) (1957). Van dezelfde auteur bij Brill: Studies in Ancient Technology (9 dln., 1955-64).

[^18] Zijn proefschrift verscheen in 1952 bij Brill: B.A. van Proosdij, Babylonian magic and sorcery: being ‘The prayers of the lifting of the hand’.

[^19] Het tweede deel van Posthumus’ Nederlandse Prijsgeschiedenis werd met
enige vertraging in 1964 gepubliceerd, in een Nederlandse en Engelse editie.

[^20] Deze en volgende paragrafen zijn gebaseerd op de notulen van de Raad
van Commissarissen (1958-75), die van de Algemene Vergadering van Aandeelhouders (1958-79) en de daarbij behorende jaarverslagen van F.C. Wieder
(UBA/BC, Archief Brill, resp. 1.2-3, 1.2-11, 1.2-19).

[^21] In 1988 verscheen een aanvullend achtste deel van W. Raven en J.J. Witkam.

[^22] UBA/BC, Archief Brill 1.2-3, RvC 24-5-1965, 2-12-1966, 13-1-1967.

[^23] Ibid. 1.2-3, RvC 15-1-1969, 15-10-1969.

[^24] Ibid. 1.2-3, RvC 7-7-1970.

[^25] Luchtmans & Brill: driehonderd jaar uitgevers en drukkers in Leiden, 1683-1983. Samenstellers van de catalogus en de tentoonstelling waren M. Castenmiller, J. M. van Ophuijsen en R. Smitskamp.

[^26] ‘Erepenning voor jubilerende drukkerij Brill’, Leidse Courant 3-10-1983.

[^27] A. Sperber, The Bible in Aramaic (4 delen in 5 banden). De eerste druk verscheen tussen 1959-1973, de tweede in 1992 en de derde in 2004.

[^28] In 1984 kocht Brill de Scandinavian Science Press in het Deense Klampenborg. Het fonds was gespecialiseerd in biologie.

[^29] H. Kops, ‘Opwaaiend stof. De tragi-komische bestuurscrisis bij E.J. Brill’, Elsevier 22-4-89.

[^30] Ibid.

[^31] P. de Waard, ‘Platvoerse ruzie onder professoren bij uitgever Brill’, Volkskrant 6-5-1989.

[^32] B. Büch, ‘Brill, werelduitgever. Driehonderd jaar uitgeven in oplagen van 500 exemplaren of minder’, Vrij Nederland 22-10-1983.

[^33] J. Nijsen, ‘E.J. Brill wil zich uit de rode cijfers tillen’, Boekblad 39, 29-9-1989.

[^34] The facsimile edition of the Nag Hammadi codices, ed. F. Shafik (13 delen, 1972-1984). Tussen 1984 en 1996 verscheen bij Brill een teksteditie met vertaling (Nag Hammadi Codices, 11 delen); tweede uitgave onder de titel The Coptic Gnostic Library (5 delen, 2000).

[^35] G. Stroucken, ‘Beursdebutant Brill koestert professorale werkwijze’, Parool 30-7-1997.

[^36] J. Alberts, ‘Uitgever Dode Zeerollen gaat niet voor snel geld’, NRC/Handelsblad 20-7-1997.

## V Brill: heden en toekomst
[^1] J. Nijsen, ‘E.J. Brill wil zich uit de rode cijfers tillen’, Boekblad 39, 29-9-1989.

[^2] Zie www.brill.nl.

[^3] Op bc.uba.uva.nl/bbc.collectiebeschrijvingen kan binnenkort de inhoud van beide archieven worden geraadpleegd.

<=.pb 176 =>
# Literatuurlijst

Blaak, J., Geletterde levens. Dagelijks leven en schrijven in de vroegmoderne tijd in Nederland 1624-1770 (Hilversum, 2004)

Bodel Nijenhuis, J.T , ‘Luchtmans, Jordaan; Samuel (I); Samuel (II) en Johannes; Samuel (III)’, in: A. J. van der Aa, Biographisch Woordenboek der Nederlanden IV (Haarlem, 1852), p.214-215

Bodel Nijenhuis, J.T , ‘Die Buchhändler-Familie Luchtmans in Leyden’, in: H. Lempertz, Bilderhefte zur Geschichte des Bücherhandels 4 (Keulen, 1856)

Bouwman, A., B. Dongelmans, P. Hoftijzer, E. van der Vlist en C. Vogelaar, Stad van Boeken. Handschrift en druk in Leiden 1260-2000 (Leiden, 2008)

Brugman, J., ‘De Arabische Studiën in Nederland’, in: N. van Dam e.a., Nederland en de Arabische Wereld van Middeleeuwen tot Twintigste Eeuw (Lochem-Gent, 1987), p.9-18

Brugmans, H.J., ‘Bodel Nijenhuis (Mr. Johannes Tiberius)’, in: Nieuw Nederlandsch Biografisch Woordenboek IV ( Leiden, 1918)

Brugmans, I.J., ‘N.W. Posthumus’, Economisch Historisch Jaarboek 28 (1961), p.281-287

Brusse, M.J., ‘Onder de menschen. De uitgeverij’ (afl. X-XII: N.V. Boekhandel en Drukkerij v.h. E.J. Brill), Nieuwe Rotterdamsche Courant, 5, 12 en 19 januari 1927

Castenmiller, M., J. M. van Ophuijsen en R. Smitskamp, Luchtmans & Brill: driehonderd jaar uitgevers en drukkers in Leiden, 1683-1983. Catalogus van de tentoonstelling gehouden van 1 september tot 1 oktober in het Gemeente-archief te Leiden (Leiden, 1983)

Clercq, P.R. de, At the sign of the oriental lamp. The Musschenbroek workshop in Leiden, 1660-1750 (Rotterdam, 1997)

Deahl, J.G., ‘1683 - E.J. Brill - 1983’, in: Short-title Catalogue E.J. Brill, 1683-1983 (Leiden, 1983)

Deahl, J.G., ‘E.J. Brill’s role in Arabic and Islamic studies’, New Books Quarterly on Islam & the Muslim world, vol. I/4-5 (Londen, 1981), p.11-13

Deahl, J.G., Brill Leiden (Leiden, 1991)

Delft, M. van, en C. de Wolf (eindred.), Bibliopolis. Geschiedenis van het gedrukte boek in Nederland (Zwolle/Den Haag, 2003)

Dillen, J.G. van, ‘Ter gedachtenis aan Nicolaas Wilhelmus Posthumus, een ondernemend historicus’, Tijdschrift voor Geschiedenis 73 (1960), p.337-339
Dongelmans, B.P.M., P.G. Hoftijzer en O.S. Lankhorst, Boekverkopers van Europa. Het zeventiende-eeuwse Nederlandse uitgevershuis Elzevier (Zutphen, 2000)

Eeghen, I.H. van, ‘Het archief van de Leidse boekverkopers Luchtmans’, De Amsterdamse Boekhandel 1680-1725 V.I (Amsterdam, 1978), p.131-177

Eeghen, I.H. van, ‘De uitgeverij Luchtmans en enkele andere uitgeverijen uit de 18de eeuw’, Documentatieblad Werkgroep 18de Eeuw (1977), p.5-8

Folkers, Th., ‘De geschiedenis van de Oostersche boekdrukkerij te Leiden’, Cultureel Indië (3, 1941), p.53-68; ook in Cultureel Indië. Bloemlezing uit de eerste zes jaargangen 1939-1945 (Leiden, 1948), p.280-295

Goeje, M.J. de., ‘Levensbericht van F. de Stoppelaar.’, Levensberichten der afgestorvene medeleden van de Maatschappij der Nederlandsche Letterkunde, 1905-1906 (Leiden, 1906), p.187-194

Goinga, H. van, ‘Alom te bekomen’. Veranderingen in de boekdistributie in de Republiek 1720-1800 (Amsterdam, 1999)

Hoftijzer, P.G., Pieter van der Aa (1659-1733). Leids drukker en boekverkoper (Hilversum, 1999)

Hoftijzer, P.G., en J. van Waterschoot (eds.), Johannes Luchtmans. Reis naar Engeland in 1772 (Leiden, 1995)

Hoftijzer, P.G., en O.S. Lankhorst, Drukkers, Boekverkopers en lezers tijdens de Republiek. Een historiografische en bibliografische handleiding (tweede ed.; Den Haag, 2000)

Jansma, T.S., ‘Nicolaas Wilhelmus Posthumus’, Jaarboek van de Maatschappij van Nederlandse Letterkunde (1961), p.126-133

Kerling, J.B.J., ‘C. Peltenburg Pzn., 1852-1934’, Leidsch Jaarboekje 1935 (Leiden, 1935)

Krom, N.J., ‘Levensbericht van C.M. Pleyte Wzn.’, Jaarboek van de Maatschappij der Nederlandsche Letterkunde (1919), p.5-25

Kruseman, A.C., Bouwstoffen voor een geschiedenis van den Nederlandsche boekhandel, gedurende de halve eeuw 1830-1880

<=.pb 177 =>
(2 dln.; Amsterdam, 1886-1887)

Kruseman, A.C., Aantekeningen betreffende den boekhandel van Noord-Nederland, in de 17e en 18e eeuw (Amsterdam, 1893)

Lebram, J.C.H. e.a., Tuta sub aegide Pallas. E.J. Brill and the world of learning (Leiden, 1983)

Masurel, G.J. ‘‘Door die zucht geleid, waarvan u ’t harte brandt’. Johannes Tiberius Bodel Nijenhuis (1797-1872)’, De Boekenwereld 8 (1991-1992), p.70-74

Molhuysen, P.C. ‘De Academie-drukkers’, Pallas Leidensis MCMXXV (Leiden, 1925), p.305-322

Muller, F., ‘Mr. J.T. Bodel Nijenhuis. E.J. Brill’, Algemeen Adresboek voor den Nederlandsche boekhandel 19 (Amsterdam, 1873), p.I-VI; ook in Bijdragen tot de geschiedenis van den Nederlandschen Boekhandel I (Amsterdam, 1884), p.174-182

Muller, F., ‘De feestgave der firma E.J. Brill, bij het derde jubileum der Leidsche Hoogeschool - Leiden, vóór 300 jaren en thans’, Nieuwsblad voor den Boekhandel (1875), p.79-80

N.N., ‘N.V. Boekhandel en Drukkerij voorheen E.J. Brill, 1848-1923, en S. en J. Luchtmans, 1683-1848’, Nieuwsblad voor den boekhandel (1923), p.631-634

N.N., ‘Adriaan P. M. van Oordt’, Leidsch Jaarboekje 1905 (Leiden, 1905), p.13-17

Ophuijsen, J.M. van, E.J. Brill. Three centuries of scholarly publishing (Leiden, 1994)

Otterspeer, W., Groepsportret met dame: dl.II, De vesting van de macht. De Leidse universiteit 1673-1775 (Amsterdam, 2001); dl.III, De werken van de wetenschap. De Leidse universiteit 1776-1876 (Amsterdam, 2005)

Proosdij, B.A. van, 250 jaar Tuta sub aegide Pallas, adagium van Luchtmans en Brill (Leiden, 1971); ook als artikel in De Antiquaar II/4 (1971), p.81-92

Rieu, W.N. du, ‘Levensschets van Mr. Johannes Tiberius Bodel Nijenhuis’, Levensberichten der afgestorvene medeleden van de Maatschappij der Nederlandsche Letterkunde 1873 (Leiden, 1873), p.247-288

Smilde, A., ‘Lezers bij Luchtmans’, De Negentiende Eeuw 14 (1990), p.147-158

Storms, M. e.a., De verzamelingen van Bodel Nijenhuis. Kaarten, portretten en boeken van een pionier in de historische cartografie (Leiden, 2008)

Tersteeg, J., ‘Het 75-jarig bestaan van de N.V. Boekhandel en Drukkerij v.h. E.J. Brill te Leiden, 1 juli 1848 - 1 juli 1923’, De Uitgever 6, nr.7 (Leiden, 1923), p.79-81

Vliet, R. van, ‘Nederlandse boekverkopers op de Buchmesse te Leipzig in de achttiende eeuw’, Jaarboek voor Nederlandse Boekgeschiedenis 9 (2002), p.89-109

Vliet, R. van, Elie Luzac (1721-1796). Boekverkoper van de Verlichting (Nijmegen, 2005)

Vreede, A.C., ‘Frans de Stoppelaar’, Leidsch Jaarboekje 1907 (Leiden, 1907), p.1-8

Warendorf, S. jr., ‘Frans de Stoppelaar 1841-1906’, Adresboek van den Nederlandschen Boekhandel 53 (Leiden, 1907), p.1-8

Waterschoot, J. van, ‘Samuel Luchtmans, een reislustig boekhandelaar’, De Boekenwereld 15 (1998-99), p.298-306

Wieder, F.C. sr., ‘C. Peltenburg Pzn.’, Handelingen en levensberichten van de Maatschappij der Nederlandsche Letterkunde, Jaarboek 1934-1935 (Leiden, 1935), p.174-178

Wieder, F.C. sr., ‘C. Peltenburg Pzn.’, Tijdschrift van het Koninklijk Nederlandsch Aardrijkskundig Genootschap 51 (Leiden, 1934), p.647-648

Wieder, F.C. jr., ‘Tuta sub aegide Pallas in former times’, Catalogue No 505. E.J. Brill Antiquarian Booksellers (Leiden, 1979), p.1-2

Witkam, J.J., ‘Verzamelingen van Arabische handschriften en boeken in Nederland’, in: N. van Dam e.a., Nederland en de Arabische Wereld van Middeleeuwen tot Twintigste Eeuw (Lochem-Gent, 1987), p.19-29

Witkam, J.J., ‘De sluiting van Het Oosters Antiquarium van Rijk Smitskamp’, De Boekenwereld 23 (2006-2007), p.207-214

Wolters, W.P., ‘De firma E.J. Brill in hare nieuwe woning’, Eigen Haard (1883), p.356-360

Wijnmalen, T.L.C., ‘Ter nagedachtenis van Mr. J.T. Bodel Nijenhuis’, De Nederlandsche Spectator 18-1-1872

<=.pb 178 =>
<=.pb 179 =>
# Gebruikte afkortingen in onderschriften van illustraties

Afkorting | voluit
----- | -----
AB/UBA | Archief Brill, Universiteitsbibliotheek Amsterdam
AL/UBA | Archief Luchtmans, Universiteitsbibliotheek Amsterdam
Coll. Brill | Collectie Brill
KVB/UBA | Koninklijke Vereniging van het Boekenvak, Universiteitsbibliotheek Amsterdam
RAL | Regionaal Archief Leiden
SML | Stedelijk Museum De Lakenhal, Leiden
St. Mus. Leipzig | Stadtgeschichtliches Museum Leipzig
UBA | Universiteitsbibliotheek Amsterdam
UBL | Universiteitsbibliotheek Leiden

<=.pb 179 =>
# Personalia

Dr. Paul Dijstelberge (1956) promoveerde in 2007 op De beer is los!, een onderzoek naar zeventiende-eeuws siermateriaal als hulpmiddel voor de identificatie van anoniem geproduceerd drukwerk. Hij is adjunct-conservator bij de Bijzondere Collecties van de Universiteit van Amsterdam en universitair docent boekgeschiedenis bij de leerstoelgroep boekwetenschap en handschriftkunde. Zijn bibliografisch onderzoek naar het fonds van Brill komt in dit boek tot uitdrukking in de onderschriften van diverse illustraties.

Mirte D. Groskamp (1982) is afgestudeerd aan de Reinwardt Academie voor Museologie. Zij werkt sinds 2006 bij de Bijzondere Collecties van de Universiteit van Amsterdam aan de inventarisatie, ordening en ontsluiting van het Archief Brill. De informatie die zij uit het archief haalde en de conceptteksten die zij aanleverde vormen een wezenlijke bijdrage aan dit boek.

Drs. Kasper van Ommen (1964) is kunsthistoricus en coördinator van het Scaliger
Instituut bij de Universiteitsbibliotheek Leiden. Hij publiceert regelmatig over boeken bibliotheekhistorische onderwerpen. Hij schreef het laatste hoofdstuk op basis van gesprekken met Herman Pabbruwe en leverde een aantal teksten voor onderschriften van illustraties.

Dr. Sytze van der Veen (1952) is historicus en publicist. Hij schreef de tekst van deze uitgave en was verantwoordelijk voor de eind- en beeldredactie. Van zijn hand verscheen in 2007 Een Spaanse Groninger in Marokko. De levens van Johan Willem Ripperda (1682-1737). Hij werkt thans aan een boek over de betrekkingen tussen Nederland en Latijns-Amerika in het begin van de negentiende eeuw.

<=.pb 180 =>1
# Colofon
© 2008 Koninklijke Brill N.V., Leiden
Niets uit deze uitgave mag worden verveelvoudigd, opgeslagen in een geautomatiseerd gegevensbestand, of openbaar gemaakt, in enige vorm of op enige wijze, hetzij elektronisch, mechanisch, door fotokopieën, opnamen, of enige andere manier, zonder voorafgaande schriftelijke toestemming van de uitgever. Indien instellingen en/of personen van mening zijn dat zij rechten hebben op het gebruik van beeldmateriaal worden zij verzocht contact op te nemen met Koninklijke Brill N.V.

Tekst
Dr. Sytze van der Veen, met bijdragen van Dr. Paul Dijstelberge, Mirte D. Groskamp en Drs. Kasper van Ommen
Vormgeving
André van de Waal en Remco Mulckhuyse, Coördesign, Leiden
Ontwerp omslag
André van de Waal, Coördesign, Leiden
Fotografie
Joost Kolkman, Voorschoten
Bijzondere Collecties, Universiteitsbibliotheek Amsterdam
Bijzondere Collecties, Universiteitsbibliotheek Leiden
Stedelijk Museum De Lakenhal, Leiden
Lettertype
Scala
Papier
Arctic Volume White 130 gr/m2, binnenwerk
Arctic Volume White 150 gr/m2, schutbladen
Arctic Volume White, houtvrij opdikkend machinegestreken papier
is FSC mixed sources gecertificeerd. Een produktgroep uit goed
beheerde bossen en andere gecontroleerde bronnen.
Sulfaatkarton 240 gr/m2, omslag
Grafisch Papier, Andelst
Druk
Drukkerij Groen, Leiden
Binder
Jansenbinders, Leiden
ISBN 978 90 04 17031 5
